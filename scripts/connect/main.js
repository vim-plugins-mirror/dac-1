var helper = require('./lib/helper');
var JavaScriptAPI = require('./lib/javascript-api');
var Navigation = require('./lib/navigation');
var Content = require('./lib/content');
var RSVP = require('rsvp');

// URLS
const JAVASCRIPT_DATA_URL = 'https://bitbucket.org/khanhfucius/dac_files/downloads/jsapi.json';
const COMMON_MODULE_DATA_URL = 'https://bitbucket.org/khanhfucius/dac_files/downloads/deref-common-schema.json';
const JIRA_MODULE_DATA_URL = 'https://bitbucket.org/khanhfucius/dac_files/downloads/deref-jira-schema.json';

const PRODUCT = {
  'jiracloud': {
    path: ['cloud', 'jira', 'platform']
  },
  'jswcloud': {
    path: ['cloud', 'jira', 'software']
  },
  'jsdcloud': {
    path: ['cloud', 'jira', 'service-desk']
  } 
}

function generateConnectModules(name, moduleData) {
  var thisModule = new Content(name, moduleData);
  Navigation.getProducts().forEach(product => {
    try {
      Navigation.addItemToSubcategory({
        title: moduleData.items.title,
        url: `/${PRODUCT[product].path.join('/')}/connect/modules/${name}`
      }, {
        product: product,
        category: 'reference',
        subcategory: 'modules'
      });
      
      thisModule.write(`${PRODUCT[product].path.join('/')}/connect/modules`, {
        platform: 'cloud',
        product: product,
        category: 'reference',
        subcategory: 'modules',
        type: 'connectModule'
      });

    } catch (e) {
      console.log(`${product} does not have reference.modules.`);
      return;
    }
  })
}

function parseModuleData(moduleData) {
  var rawModules = JSON.parse(moduleData).properties;

  Object.keys(rawModules).forEach(modName => {
    if (rawModules[modName].items) {
      generateConnectModules(modName, rawModules[modName]);
    }
  });
}

const downloadPromises =  {
  jsApi: helper.downloadData(JAVASCRIPT_DATA_URL),
  commonModules: helper.downloadData(COMMON_MODULE_DATA_URL),
  jiraModules: helper.downloadData(JIRA_MODULE_DATA_URL) 
};


RSVP.hash(downloadPromises).then(data => {
  var jsApi = new JavaScriptAPI(JSON.parse(data.jsApi));
  parseModuleData(data.commonModules);
  parseModuleData(data.jiraModules);

  // Generate JavaScript modules docs
  jsApi.modules.forEach(mod => {
    if (mod.name) {
      var connectContent = new Content(mod.name, mod);
      connectContent.write(['cloud', 'connect', 'reference', 'jsapi'], {
        platform: 'cloud', 
        product: 'connect',
        subcategory: 'jsapi'
      });
    
      Navigation.getProducts().forEach(product => {
        var isValidProduct = false; 
        try {
          Navigation.addItemToSubcategory({
            title: mod.humanizeName,
            url: `/${PRODUCT[product].path.join('/')}/connect/jsapi/${mod.name}`
          }, {
            product: product,
            category: 'reference',
            subcategory: 'jsapi'
          });
          isValidProduct = true;
        } catch (e) {
          console.log(`${product} does not have reference.jsapi.`);
          return;
        }

        if (isValidProduct) {
          connectContent.write(`${PRODUCT[product].path.join('/')}/connect/jsapi`, {
            platform: 'cloud', 
            product: product,
            subcategory: 'jsapi'
          });
        }
      });
    }
  });

  // Update navigation.yml
  Navigation.write();
}).catch(err => {
  console.log(err);
});