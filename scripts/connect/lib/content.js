var jsonfile = require('jsonfile');
var fs = require('fs-extra')
var _ = require('lodash');
var helper = require('./helper');

const CONTENT_ROOT_DIR = '../../content';

class Content {
  constructor(title, frontMatter) {
    this.title = title;
    this.frontMatter = _.clone(frontMatter);    
  }

  write(section, meta) {
    if (!section) {
      throw new Error('Section and sub sections(s) must be provided.');
    }
    
    // section deeper than one level must be joined to a string format
    if (section instanceof Array) {
      section = section.join('/');
    }
    
    // final destination of the content file
    var dir = `${CONTENT_ROOT_DIR}/${section}`;

    var thisContent = this;
    
    // ensure that the write directory exists, make one if it's not there
    fs.ensureDir(dir, function (dirError) {
      if (dirError) {
        console.error(`Could not create ${dir}`);
      } else {
        jsonfile.writeFile(`${dir}/${thisContent.title}.md`, _.extend(thisContent.frontMatter, meta), {spaces: 2},function (writeError) {
          if (writeError) {
            console.error(`Could not write ${thisContent.title} to ${dir}`);
          } else {
            console.log(`Writing content ${thisContent.title}.md to ${dir}`);
          }

        });
      }
    });
  }
}

module.exports = Content;