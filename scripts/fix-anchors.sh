#!/bin/bash 

# rewrites intra page anchor links according to https://ecosystem.atlassian.net/browse/DAC-394
# thus shown to be a member of the set of problems that can be solved with one line of perl

find "`dirname $0`/../content" -name '*.md' -exec perl -pi -e 's@^\s*(\[[^]]+\]:)\s*#.*@"  $1 #".lc($1=~s%([\[\].:?,\x{27}"]|\\[#*])%%rg=~s%[-/\s]+%-%rg)@ge' {} \;
