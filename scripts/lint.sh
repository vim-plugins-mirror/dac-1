#!/usr/bin/env bash

set -ex

. scripts/enter-virtualenv.sh

mkdir -p test-reports
echo running content lint
export/lint.py $@
