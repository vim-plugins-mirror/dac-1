---
title: "Announcing HipChat Connect beta - Chat will never be the same again!"
date: "2015-10-13T16:00:00+07:00"
author: "tcrusson"
categories: ["hipchat", "atlassian-connect", "apis"]
---
<style>
.image-shadow {
        -webkit-box-shadow: 3px 4px 5px 0px rgba(136,136,136,1);
        -moz-box-shadow: 3px 4px 5px 0px rgba(136,136,136,1);
        box-shadow: 3px 4px 5px 0px rgba(136,136,136,1);
  }
  .center-small {
      display: block;
      width: 80%
  }

</style>

At AtlasCamp 2015 we announced we were working on Atlassian Connect for HipChat. During a recent hackathon week in
San Francisco we gave early access to 25 developers from awesome companies - including
<a href="http://www.newrelic.com" target="_blank">New Relic</a>,
<a href="http://www.pagerduty.com" target="_blank">PagerDuty</a>,
<a href="http://statuspage.io" target="_blank">StatusPage.io</a>,
<a href="http://www.tempoplugin.com" target="_blank">Tempo</a>,
<a href="http://www.wittified.com" target="_blank">Wittified</a>,
<a href="http://www.meekan.com" target="_blank">Meekan</a>,
<a href="http://notify.ly" target="_blank">Notify</a>,
and <a href="http://www.zendesk.com" target="_blank">Zendesk</a> - and the result was quite impressive!

The API is currently in private alpha and will enter public beta in a few short weeks. So get ready - and on that front -
we're running developer training at Atlassian Summit 2015.
<p align="center" style="margin: 35px 0 35px 0;">
  <a href="https://summit15marketplacetraining.eventbrite.com"
class="aui-button aui-button-primary" target="_blank" style="font-size: 16px; line-height: 24px;">Register for training</a>
  <a href="http://goo.gl/forms/MrtV7ZUQXR" target="_blank" class="aui-button" style="font-size: 16px; line-height: 24px;"> Join the beta</a>

</p>

See what you could quickly build with this demo from <a href="http://supportkit.io" target="_blank">SupportKit</a>:<br>
<br>
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/8XxZxqCcxJE" frameborder="0" allowfullscreen></iframe>
</center>

## What is HipChat Connect?

HipChat Connect is HipChat's next generation API that lets you build add-ons extending the HipChat apps. It is based
on the same Atlassian Connect framework you already use to build add-ons for JIRA, Confluence, and Bitbucket.
You can build HipChat add-ons in any language, and run them on any platform: all you need is for your add-on to talk
to HipChat over open Web protocols (HTTPS, REST, OAuth, JWT).

<center><img class="center-small" src="architecture.png" /></center>

Here are the components of the API:

### HipChat Cards
Your app can send a Card to HipChat by POSTing structured JSON data to a REST endpoint.
HipChat will display it as a Card in a HipChat room, and will optimise the rendering of the Card
for the device the user is viewing it on:

<img class="image-shadow" src="cards.png" />
<center><p style="font-size:12px">Cards: notifications that should interrupt/educate the conversation</p></center>

### HipChat Actions

You can add Actions to various places in the HipChat apps. Currently, supported locations are input and message actions.
Actions can open a sidebar/modal dialog View, or redirect the user to another application.

<table>
<tr><td>
<img class="image-shadow" width="420" src="messageaction.png" />
<center><p style="font-size:12px">Message action: deploy, add to your todo list, etc.</p></center>
</td><td>
<img class="image-shadow"  width="420" src="inputaction.png" />
<center><p style="font-size:12px">Input action: start a hangout, select a meme, etc.</p></center>
</td></tr>
</table>

### HipChat Glances

You can add a Glance to the HipChat main sidebar. Notifications come and go in Chat rooms, but Glances are sticky, and
always show in the sidebar in the context of a room. You can use them to display time critical information that needs
someone's attention: there are 3 open incidents, your product was mentioned 30 times on Twitter in the past half an hour,
etc. Clicking on a Glance opens a sidebar View.

<img class="image-shadow" src="glances.png" />
<center><p style="font-size:12px">Glances: what's the status of what my team's working on?
Does anything need my attention?</p></center>

### HipChat Views

Your app can extend the HipChat user interface by adding Views (HTML/JS/CSS) to specific locations,
currently in the HipChat right sidebar or in a modal dialog. HipChat will create a container and load the content
from your app.

From a View, your UI can use the HipChat JavaScript API to interact with the HipChat App,
for example to create/open a room or a 1-1 chat, or populate the input field.

<table>
<tr><td>
<img class="image-shadow"  width="420" src="views.png" />
<center><p style="font-size:12px">Sidebar View, e.g. list all blockers impacting the current sprint</p></center>
</td><td>
<img class="image-shadow"  width="420" src="dialogview.png" />
<center><p style="font-size:12px">Dialog View, e.g. select a Giphy</p></center>
</td></tr>
</table>

These are the basic building blocks of HipChat Connect. Now we'll show you how you can use them to build powerful
integrations.

## Using HipChat Connect to build immersive user experiences

Do you have an app you'd like to integrate with HipChat using HipChat Connect?
Here are a few examples of what you can do:

### Improve the signal-to-noise ratio

**The problem**: if your application already integrates with HipChat, teams can stay up to date on important events thanks to
notifications your app sends to HipChat rooms. But these notifications can sometimes become somewhat...spammy.

**The solution**: You can now improve the signal-to-noise ratio using HipChat Connect.your app can add a HipChat Glance
to let users know if something needs their intention, and a HipChat sidebar view to show more details.
If something needs urgent attention, your application can still send a notification as a Card in the main chat view.

<img class="image-shadow" src="statuspage.png" />
<center><p style="font-size:12px">Monitor the status of Cloud services with
<a href="http://www.statuspage.io" target="_blank">StatusPage.io</a></p></center>

### Provide better context to conversations

**The problem**: users share a lot of links in HipChat rooms. In some cases, for example a public YouTube link,
HipChat can automatically resolve the link and show more information, e.g. a thumbnail of the video.
But if users share links from apps requiring authentication or deployed behind a firewall, then we can't do that.

**The solution**: your app can implement a webhook that listens for messages in HipChat rooms matching
regex patterns (URL, issue key). As a response, your app can post a HipChat Card to the room, providing more information
about the resource the link points to.

<img class="image-shadow" src="zendesk.png" />
<center><p style="font-size:12px">Rich context in conversations when talking about
<a href="http://www.zendesk.com" target="_blank">Zendesk tickets</a></p></center>

Going further, Cards can expose links/actions which open an up-to-date view of the linked resource in the HipChat
sidebar. This is very handy for users when discussing time sensitive, critical events.

<img class="image-shadow" src="sentry.png" />
<center><p style="font-size:12px">Discuss new exceptions in your apps with
<a href="http://getsentry.com" target="_blank">Sentry</a></p></center>

### Close the loop

**The problem**: HipChat is where users make decisions. But where do they go to take action? Today they have to keep
switching back and forth between applications, and navigating to the right context to take action.

**The solution**: your application can add HipChat Actions to Cards, which can open a View in the HipChat sidebar or modal
dialog, or redirect the user to your application in the right context.

### Create rich content in Chat: death to slash commands!

**The problem**: if you were to create polls in HipChat today, you would do it via a bot or a slash command.
They are both difficult to discover and not very intuitive to use.

**The solution**: user interfaces... Doh!

<table>
<tr>
<td>
<img class="image-shadow"  width="320" src="poll11.png" />
<center><p style="font-size:12px">Conversational style with
<a href="https://github.com/hipchat/hubot-hipchat" target="_blank">Hubot</a></p></center>
</td>
<td>
<img class="image-shadow"  width="520" src="poll2.png" />
<center><p style="font-size:12px">Extending the HipChat UI with  <a href="https://www.convergely.com" target="_blank">Convergely</a></p></center>
</td>
</tr>
</table>


### Contribute actions to other apps

**The problem**: let's say your app lets users escalate incidents, and most of your customers are also using New Relic,
which sends notifications about incidents to HipChat.
Today, to escalate an incident, users would have to go to your app and copy/paste the New Relic data.

**The solution**: your application can contribute HipChat Actions to other application Cards, for example you could add a
"escalate incident" Action to New Relic incident Cards. That is very powerful.

<img class="image-shadow" src="cardlinking.png" />
<center><p style="font-size:12px">Linking it all together from a  <a href="http://newrelic.com" target="_blank">New Relic alert</a></p></center>

## Excited? Time to get started!

You can read the API documentation here:
<a href="https://developer.atlassian.com/hipchat/getting-started" target="_blank">HipChat developer docs</a>,
and start to familiarize yourself with the API components and how they fit together. The API is in private alpha for
few more short weeks. But you can register your interest by sending an email to integrations@hipchat.com -
make sure to tell us what you'd like to build! We will send you instructions as soon as the public beta comes out.

We will also run developer training at Atlassian Summit 2015 so you can get started and build a HipChat add-on
using most of the components of the API. It's your chance to learn directly from the developers of the API.
<p align="center" style="margin: 35px 0 35px 0;"><a href="https://summit15marketplacetraining.eventbrite.com"
class="aui-button aui-button-primary" target="_blank" style="font-size: 16px; line-height: 24px;">Register for training</a>
</p>
