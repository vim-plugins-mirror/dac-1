---
title: "1:40 of code art: 2014 in Bitbucket seen through Gource"
date: "2015-02-03"
author: "jgarcia"
categories: ["git", "bitbucket"]
---

<style>
  .video-player {
    display: block;
    margin: 25px auto;
  }

  .video-player {
    width: 560px;
  }
</style>

It's been an incredibly busy year here at Bitbucket! We've served more active 
users, made more improvements, rolled out more features, and fixed more bugs 
than ever before. To commemorate the year that was, I've visualized our Git 
log using the [Gource] log visualization system to give you something to look 
at while [your code is compiling]. You can read about how I made the magic 
happen below the video.
<div class="video-player">
  <iframe width="560" height="315" src="//www.youtube.com/embed/Rkl7Cl90CRI?vq=hd720" 
          frameborder="0" allowfullscreen></iframe>  
</div>

-----

Making code art
--

As a weekend project, I kicked around the Gource source-code visualization 
system, which walks through your log and generates a video display showing 
the results of your commit activity. After mapping each commit to a video 
frame and each file to a node, user avatars float around and zap each commit 
into its destination file, color-coded for creates, changes, and removes. With 
the help of my household's Artist-in-Residence (aka my dear wife), we found a 
pattern that makes a very visually appealing show of a year of commits in a 
large software project. I then set this to an appropriate piece of music 
from YouTube's royalty-free music library, and voila, code-art is made.

Checkout repo
--

For starters, I [brew]ed gource and cloned out a copy of the repo. Since 
I want to build this from the main development branch (Staging), simply 
running over to Bitbucket and cloning 
the repository sufficed; otherwise, I'd want to checkout a specific branch:

```bash
$ brew install gource
$ cd ~/source
$ git clone <link>
```

Grab and set avatars
--

Next, we get the user avatars into the repository for use in composing the 
video. This is done through a Perl script found at the [Gource Gravatar 
Example] which is run from the root folder of the repo:

```sh
$ cd <repo>
$ nano ./getavatars.pl #Paste script into this file and save
$ sudo chmod +x ./getavatars.pl
$ ./getavatars.pl
```

If you want, you can then enter `<repo>/.git/avatar` and clean up the images.
 Alternately, you can add in Identicons for users with no avatar by using 
modifications to the script found in the comments. I elected to use Charlie, 
our corporate mascot.
The avatars are set using the following, including a `user-scale` argument 
that will ensure they're visible once we have built a huge tree:

```sh
$ gource --default-user-image charlie_cmyk_cyan.png --user-image-dir .git/avatar/ --user-scale 4
```

Frame size, titles, logos, and visual effects
--

We're going to set framesize, hide some distracting elements, and add titles
and logos. This makes for a more appealing, less cluttered presentation, 
with graphics that break up the flat black background as well as helping the 
viewer gain context:

```sh
$ gource -1280x720 --hide progress,filenames,usernames,mouse --date-format "%Y-%m-%d %H:%M:%S" \
--title "Bitbucket Commit Activity 2014"
$ gource --background-image bitbucket_rgb_blue_small.png --logo atlassian_cmyk_white_stacked.png \
--font-colour 3b73af
```

`Bloom` describes the etherial glow that the folder-nodes have, with 
`intensity` describing its brightness and `multiplier` describing its radius. 
I decided to go with a gentle, diffuse glow; `multi-sampling` describes a 
computationally expensive but very effective smoothing technique. Since we're 
rendering this offline, we'll throw that in, too:

```sh
$ gource --bloom-multiplier 1.1 --bloom-intensity 0.4 --multi-sampling
```

Speed and timing effects
--

Bitbucket, like most large projects, has an astronomical number of objects. 
First and foremost, we need to speed this thing up. Let's set the seconds 
per day and the number of seconds we'll dwell at the end of a day's commits 
to really small values:

```sh
$ gource -s 0.0001 --auto-skip-seconds 0.25
```

Next, we want to manage the animation properties to fit these timings. 
We'll make sure the files appear appropriately quickly and slow down the 
'jumpy' user avatars by decreasing their max speed and the time they're 
allowed to coast (friction):

```sh
$ gource --max-file-lag 0.1 --max-user-speed 150 --user-friction 1
```

Since Bitbucket has been around for a very long time, we're going to limit 
this exercise to the last calendar year. This will keep our movie under two 
minutes, and also provide us with a finished product that commemorates a 
milestone. Also, as this is just one year of commit activity, we'll allow the 
nodes to be permanent by turning off their idle-time expiration:

```sh
$ gource --start-date '2014-01-01' --stop-date '2015-01-01' --file-idle-time 0
```

Making it into a movie
--

Any Linux or Mac die-hard will tell you that a primary reason for being a Linux
or Mac die-hard is this little thing called a pipe, which allows you to **pipe** 
the **st**an**d**ard **out**put (or **stdout**) of one program into another. 
We'll send this into `ffmpeg`, which will encode it directly into an mp4 file 
(your codecs may vary). By using the `gource -o -` option to direct the output 
to stdout, we can supply the pipe with a stream of bitmaps.
I won't get into the magical incantations required to make `ffmpeg` go - there 
be dragons. That said, this little bit of copy-pasta I got from the [Gource 
Videos] page worked great for me. You can adjust the `-threads` option for 
more processing speed:

```bash
$ gource -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 \
-preset ultrafast -pix_fmt yuv420p -crf 1 -threads 0 -bf 0 gource.mp4
```

Putting it all together
--

Naturally, all of this put together is a huge command line, but it makes for 
a nice, minute and thirty-five seconds of modern art:

```bash
$ gource -1280x720 -s 0.000001 --user-image-dir .git/avatar/ --title \
"Bitbucket Commit Activity 2014" --background-image bitbucket_rgb_blue_small.png \
--max-file-lag 0.1 --auto-skip-seconds 0.25 \
--hide progress,filenames,dirnames,usernames,mouse --date-format \
"%Y-%m-%d %H:%M:%S" --multi-sampling --bloom-multiplier 1.1 --bloom-intensity 0.4 \
--user-scale 4 --logo atlassian_cmyk_white_stacked.png  --file-idle-time 0 \
--start-date '2014-01-01' --stop-date '2015-01-01' \
--default-user-image charlie_cmyk_cyan.png --font-colour 3b73af --git-branch staging \
--max-user-speed 150 --user-friction 1 -o - | ffmpeg -y -r 60 -f image2pipe \
-vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 8 \
-bf 0 gource.mp4
```

Digital art to delight your customers
--

This was a bunch of fun to make, and I highly recommend that you try it out 
on your own repo. Because the process abstracts so much of the information in 
the log prior to two layers of visual transcoding, there's no risk of any 
trade secrets being released through the process. As many commercial software 
packages are closed source, this offers an attractive way to expose your fans 
and evangeists to your development process and generate exitement in the 
developer community. Go try it yourself! Get started at the Gource Homepage 
at Google Code.

Follow me on Twitter for updates about Atlassian software, robotics, Maker art, 
and software philosophy at [@bitbucketeer][bitbucketeer].

[Gource Videos]: https://code.google.com/p/gource/wiki/Videos
[Gource]: https://code.google.com/p/gource/
[Gource Gravatar Example]: https://code.google.com/p/gource/w/list
[your code is compiling]: http://xkcd.com/303/
[brew]: http://brew.sh/
[bitbucketeer]: https://twitter.com/bitbucketeer

