---
title: "Join Us for Our Next Dev Den Open Office Hours"
date: "2015-01-09"
author: "gfrancisco"
categories: ["git", "dev den", "office hours"]
---
<style>
  .video-player, .screen-caps, .big-image {
    display: block;
    margin: 30px auto;
  }

  .video-player {
    width: 100%;
  }

  .screen-caps img {
    width: 40%;
  }

</style>


The gang is reassembling again for another Dev Den Open Office Hours at the end of this month where we'll be talking all things Git again including discussion on last month's Git security incident. 

<div class="screen-caps">
  <a href="thegang.jpg"><img style="float: left;margin-right: 25px;" alt="The Gang" src="thegang.jpg"></a>
</div>

We ran last month's broadcast all in one room in Sydney as we were there for a team offsite. It was great to have the team in one place for the first time and brainstorm future content and projects. If you missed our last session we were able to sort out all the kinks of running a Google Hangout and posted both a recording and transcript. This month we'll be "dialing in" from different locations so some of us may be slower at answering those burning questions as we perk up with some coffee. :) Look forward to seeing you there!



## [Dev Den Open Office Hours](https://plus.google.com/events/c1hndjoump6lj98nd1c4fl3ulu4) ##
* Tuesday, January 27  
* 8 AM PST
* Sign up on the [event Google+ page](https://plus.google.com/events/c1hndjoump6lj98nd1c4fl3ulu4)

## Catch Up with our December Session ##

<div class="video-player">
<iframe width="100%" height="510px" src="//www.youtube.com/embed/KkXM0HGDZtg" frameborder="0" allowfullscreen></iframe>
[Transcript](transcript.html)
</div>

