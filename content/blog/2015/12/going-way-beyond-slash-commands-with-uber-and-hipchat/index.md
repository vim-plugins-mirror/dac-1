---
title: "Going (way) beyond slash commands with Uber and HipChat"
date: "2015-12-14T17:35:00+07:00"
author: "rmanalang"
categories: ["atlassian-connect", "hipchat", "opensource", "add-ons"]
lede: "Here at Atlassian, we are big fans of Uber. So, a few
weeks ago, we took a crack at building an Uber integration inside HipChat.
The best part...it only took us a few days to build!"
---
Here at Atlassian, we're big fans of [Uber](https://www.uber.com). So, a few
weeks ago, my colleague [Julien](https://twitter.com/madgn0me) and I took a
crack at building an Uber integration inside HipChat. Our teams use
Uber for sharing rides to outings, airports, and off-sites all the time; It
makes sense for Uber to be available inside of HipChat. The best part...
<strong>it only took us a few days to build!</strong> <span style="display:
inline-block;position: absolute;padding-left: 10px;">![Boom!](boom.gif)</span>

<hr style="background-color:#ddd;">

Ever wanted to go to lunch with some colleagues and end up in an endless string
of messages trying to line everyone up to meet in the lobby, pick a spot and
show up on time? Next, everyone claims that their car is dirty and they don't
want to drive. "It is not always this dirty I swear!"

<div style="text-align:center;margin:10px">![YUNO wash?](yuno.jpg)</div>

* What if you could call for an Uber right from HipChat?
* What if your colleagues could join your ride?
* What if you could get a fare estimate then split the cost between the riders?
* What if you could see when the car pulled up in front of your office?

Sure, we could easily build an integration with Uber that used slash commands,
but do you really want to trouble your users with typing in commands?

## Introducing Uber for HipChat

<div style="text-align:center;margin:10px;box-shadow:rgba(0,0,0,.4) 0 0 8px;margin:20px auto;width:640px;max-width:640px;height:480px;max-height:480px;">![Uber for HipChat](uber-for-hipchat2.gif)</div>

If you've used the Uber app before, you'll know that the experience of
requesting an Uber ride is pretty special. With essentially a tap, you can have
a car head your way within just a few minutes. It's magic made possible by the
little devices we all carry around.

We wanted to build an experience within HipChat that represents Uber and that
magic. Slash commands or talking to a chat bot would not have provided that
experience. With HipChat Connect, requesting a ride with Uber is intuitive and
easy. Once a ride is requested, you're instantly notified in the room of your
ride's status (as well as on your Uber mobile app).

In addition to requesting a ride, your team can can join a ride that's been
requested. This gives you the ability to self organize without having to do the
curbside shuffle.

<div style="text-align:center;margin:20px">![Join ride](pending-rides.jpg)</div>

When your team's ride is arriving, each rider will will be notified in the room:

<div style="text-align:center;margin:10px">![Notifications](notification.jpg)</div>

All of these features amount to a rich experience that's intuitive and useful.
Here's a more complete demo:

<div style="text-align:center;margin:10px"><iframe width="830" height="467" src="https://www.youtube.com/embed/YRHUJgWHCuM" frameborder="0" allowfullscreen></iframe></div>

## The Uber API + HipChat Connect

[Uber's API](https://developer.uber.com/) is very straightforward and easy to
use. Everything you can do in the Uber app, you can pretty much do in their API -
right down to tracking where your ride is in real-time. So, we set out to design
the experience we wanted. We could have easily built a slash command based
integration in a matter of hours:

<div style="text-align:center;margin:10px">![Slash Uber](slash-commands.jpg)</div>

But, now that we have [HipChat
Connect](https://developer.atlassian.com/blog/2015/10/announcing-hipchat-connect/),
a slash command solution seems like an anti-pattern. Slash commands are
difficult to discover and use; Imagine typing in your destination address
without auto-complete. While many of our technical users adore them, the other
80% of us are often confused with how to interact with them. HipChat Connect
allowed us to build a more immersive user experience.

## How did this come together?

This new integration with Uber was made possible by [HipChat
Connect](https://developer.atlassian.com/blog/2015/10/announcing-hipchat-connect/)
and [Uber's API](https://developer.uber.com/). If we had built this
integration without HipChat Connect, we'd be stuck with a bunch of slash
commands that are difficult to use and impossible to remember - all the magic of
Uber would be gone. The command line experience is still useful for those who
prefer that. We actually have an <code>/uber</code> command stubbed out in our
add-on, but haven't implemented it - and we may never implement it. The fact of
the matter is, most users won't use a slash command if there's a better option
available.

Underneath this new add-on is a bunch of really cool tech:

* [Firebase](http://firebase.com/) - Firebase makes it easy for us to build a
  real-time service and UI.
* [React](https://facebook.github.io/react/) / [Redux](http://redux.js.org/) -
  React is this decade's jQuery and what all the cool-kids are using to build
  highly maintainable UI views. Redux is the state manager that brings sanity
  and maintainability to web apps.
* [Node](https://nodejs.org/en/) / [Express](http://expressjs.com/en/index.html) -
  We're using our own [HipChat Connect
  SDK](https://bitbucket.org/hipchat/atlassian-connect-express-hipchat) that's
  built on top of Node and Express

If you're interested in seeing what it looks like underneath the hood, feel free
to clone it and run it locally. We've open sourced this add-on and would love
for you to contribute to it:

<div style="text-align:center;font-size:24px;margin:20px;"><strong><https://bitbucket.org/atlassianlabs/hipchat-uber-addon></strong></div>

## How do I install this thing?

HipChat Connect is still in beta, but [you can use
it](https://hipchat.com/connect) and [build on
it](https://developer.atlassian.com/hipchat/getting-started)
today. The next time you need a ride for your team, try using Uber from HipChat.
Add it to your room today:

<div style="text-align:center;margin:20px;"><a href="https://hipchat.com/addons/install?key=hc-uber-production" class="aui-button aui-button-primary" style="font-size:24px;"><span class="aui-icon aui-icon-large aui-iconfont-hipchat">HipChat</span> Add to HipChat</a> </div>

You can also find [Uber for HipChat](https://marketplace.atlassian.com/plugins/hc-uber-production/cloud/overview)
along with 100+ other awesome HipChat integrations and 2000+ add-ons for all Atlassian
products in the [Atlassian Marketplace](https://marketplace.atlassian.com/home/hipchat).

If you'd like to see more features, reach out to us on Twitter at
[@hipchat](https://twitter.com/hipchat) or [send us a pull
request](https://bitbucket.org/atlassianlabs/hipchat-uber-addon).
