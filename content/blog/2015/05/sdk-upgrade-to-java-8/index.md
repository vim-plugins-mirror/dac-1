---
title: "Atlassian SDK Upgrade to Java 8"
date: "2015-05-13T16:00:00+07:00"
author: "dtester"
categories: ["sdk", "tools", "jira", "confluence"]
---
We've upgraded to Java 8! Our SDK now requires that you run with JDK 1.8. Given 
the recent [end-of-life of Java 7][java7eol], our products are on the road to
deprecating JDK 1.7 as a supported environment. For you add-on devs out there, 
you'll need to upgrade your environment to Java 8 and [JDK 1.8][jdk8]. If you've 
run your environment lately and ran into issues such as a blank screen, read on 
to update and get back into action!

### Updating a previously installed Vagrant box
To make the transition easy, we've updated the Vagrant boxes to automatically
include Java 8. If you're new to add-on development, simply follow the 
[instructions for environment setup][vagrant-instructions], and you'll be good 
to go. If you have previously installed the Vagrant box for local development, 
you'll need to dismantle and rebuild it. Thankfully there are just a few steps
to get you back up and running with the latest SDK!

Please note that the `vagrant destroy` step is, well, destructive! You will lose 
any changes you've made to the virtual machine, so back up anything you hold dear!

1. Back-up any work from the vagrant box.
2. `vagrant halt`
3. `vagrant box update`
4. `vagrant destroy`
5. `vagrant up`

### Updating a local installation of the SDK
For an environment not using the Vagrant box, the steps are easy! Just go to the
Oracle website and download the [Java 8 SDK][jdk8] for your platform. You can
install it from there and re-run your local environment. Simple as that!

### How to find supported platforms in the future.
If you're interested in keeping up-to-date on supported environments for 
Atlassian products, check out each product's supported platforms page. 

* [JIRA supported platforms][jira-supported]
* [Confluence supported platforms][confluence-supported]
* [Bamboo supported platforms][bamboo-supported]
* [Stash supported platforms][stash-supported]

[java7eol]: https://www.java.com/en/download/faq/java_7.xml
[jdk8]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
[vagrant-instructions]: https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally.html
[stash-supported]: https://confluence.atlassian.com/display/STASH/Supported+platforms
[jira-supported]: https://confluence.atlassian.com/display/JIRA/Supported+Platforms
[confluence-supported]: https://confluence.atlassian.com/display/DOC/Supported+Platforms
[bamboo-supported]: https://confluence.atlassian.com/display/BAMBOO/Supported+platforms