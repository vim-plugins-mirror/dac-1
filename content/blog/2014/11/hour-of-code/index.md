---
title: "Teaching New Developers - an Hour of Code at a Time"
date: "2014-11-26"
author: "nwade"
categories: ["education"]
---

90 percent of American schools don't teach computer science. Fewer students are learning how computers work than a decade ago. Girls and minorities are severely underrepresented in technology. And yet, technology is increasingly shaping almost every aspect of how we live our lives all over the world. Here at Atlassian, we're putting our money where our mouth is and are supporting [Code.org](http://code.org) in order to help teach girls and boys the basics of computer science.

For starters, that's why at Atlassian we're joining in on the largest computer science education event in history: [The Hour of Code](http://csedweek.org). On December 11th, many Atlassians are bringing their kids, and their kids friends to work, to spend one hour learning computer science, doing online tutorials featuring Bill Gates, Mark Zuckerberg, and Angry Birds - provided by Code.org.

More importantly we also believe in and support Code.org's efforts with direct support. For a number of months we've been providing a workplace for a small team from Code.org in our offices in San Francisco. They've been working on new elementary school curricula. I'm with the guys in my sweet [@codeorg](http://twitter.com/codeorg) cap [here](https://twitter.com/wadenick/status/513032557483413504):

<blockquote class="twitter-tweet tw-align-center" lang="en"><p>We&#39;re honored and thrilled to provide a home for some of the <a href="https://twitter.com/codeorg">@codeorg</a> team here at <a href="https://twitter.com/hashtag/Atlassian?src=hash">#Atlassian</a>! <a href="http://t.co/Tr0IZhjyqX">http://t.co/Tr0IZhjyqX</a></p>&mdash; Nick Wade (@wadenick) <a href="https://twitter.com/wadenick/status/513032557483413504">September 19, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</br>

My favorite new thing the Code.org team are working on is a brand new tutorial for all young fans of Disney's hit movie Frozen, especially young girls. It's a new Hour of Code featuring Princess Anna and Elsa of Arendelle. Your child can start by following easy instructions and drag'n'drop code objects that make Elsa and Anna skate in increasingly complex winter patterns, that they can later share with their friends.

If you're not familiar with Code.org's tutorials, the easiest way I can describe them is that they blend block-based coding - the same method you might have first learned at [Berkeley](http://www.cs.berkeley.edu) or [Harvard](http://www.seas.harvard.edu/computer-science) - with things that kids are naturally interested in. 

<img src="Code.org-Code_with_Anna_and_Elsa.jpg" alt="Code.org Frozen tutorial" style="width:80%;border:1px solid black;display: block;
margin-right:auto;margin-left:auto;margin-top:30px;margin-bottom:30px;">  

Kids really want to learn, it's a built in drive. And they thrive in an environment that is fun, captivates their interests, and stimulates their imaginations. Code.org has been able to acheive a great deal of engagement in a short time by recognizing and using the simple combination of these things to everyone's benefit. 

We've started on similar efforts here in Atlassian Developer Relations with our new online, in-browser Hello World tutorial, [Try Atlassian Connect](https://try.atlassianconnect.com). Whilst we haven't gotten as far as a Code.org tutorial, we believe this is a great start to getting new developers interested in our tools. You can write your first add-on for [Atlassian JIRA](https://www.atlassian.com) in two minutes. And you have a live JIRA instance in the cloud where you can customize the add-on and try other more interesting things for another twenty minutes, all right in your browser. We'd love your feedback. 

## Get Involved

If you haven't tried out Code.org yet, I highly recommend you start by spending an hour learning to [Code with Anna and Elsa](http://code.org/frozen). Even if you're a seasoned software programmer who knows java or javascript inside and out, you'll likely still enjoy seeing innovative new ways we can encourage all our children to learn computer science early.

It's not too late to get involved with The Hour of Code. Join us, thousands of schools, and many other organizations across the world and find a way to get your child to join, or [host your own Hour of Code](http://csedweek.org/) - it's quite easy! We're proud of the tiny part we've been able to help contribute to the Code.org efforts so far and hope to be part of many more - working with the team, and hosting our own Hour of Code is just the start.
