---
title: Using the REST APIs Prototype Only 39985857
aliases:
    - /confcloud/using-the-rest-apis-prototype-only-39985857.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985857
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985857
confluence_id: 39985857
platform:
product:
category:
subcategory:
---
# Confluence Connect : Using the REST APIs - Prototype Only

This prototype REST API is **deprecated since Confluence 5.5**.
Confluence has a new [REST API] that is progressively replacing our existing APIs, including this prototype REST API. We recommend plugin developers use the new REST APIs where possible.

The content on this page refers to the old prototype REST API only.

This page contains information on the factors common across all or most of the Confluence prototype REST APIs. For details of the specific REST resources, please refer to the <a href="http://docs.atlassian.com/atlassian-confluence/REST/latest/" class="external-link">REST resources reference guide</a>.

**On this page:**

-   [REST Authentication]
-   [REST Resources and URI Structure]
-   [Media Types]
-   [API Versions]
-   [HTTP Response Codes]
-   [Methods]

## REST Authentication

You can authenticate yourself for the REST APIs in two ways:

-   **Log in to Confluence manually.** You will then be authenticated for the REST APIs for that same browser session.
-   **Use HTTP basic authentication** (Authorization HTTP header) containing '`Basic username:password`'. Please note however, `username:password` must be base64 encoded. The URL must also contain the '`os_authType=basic`' query parameter.

## REST Resources and URI Structure

URIs for a Confluence REST API resource have the following structure:
With context:

``` javascript
http://example.com:1234/context/rest/api-name/api-version/resource-name
```

Or without context:

``` javascript
http://example.com:1234/rest/api-name/api-version/resource-name
```

![(info)] In Confluence 3.1 and Confluence 3.2, the only available `api-name` is `prototype`.
**Examples:**
With context:

``` javascript
http://example.com:8080/confluence/rest/prototype/1/space/ds
http://localhost:8080/confluence/rest/prototype/latest/space/ds
```

Or without context:

``` javascript
http://confluence.example.com:8095/rest/prototype/1/space/ds
http://confluence.example.com:8095/rest/prototype/latest/space/ds
```

Here is an explanation for each part of the URI:

-   `host` and `port` define the host and port where the Confluence application lives.
-   `context` is the servlet context of the Confluence installation. For example, the context might be `confluence`. Omit this section if your URI does not include a context.
-   `rest` denotes the REST API.
-   `api-name` identifies a specific Confluence API. For example, `admin` is the API that allows interaction with the Confluence Administration Console. (This is the path declared in the REST module type in the REST plugin descriptor.)
-   `api-version` is the API version number, e.g. `1` or `2`. See the section on [API version control].
-   `resource-name` identifies the required resource. In some cases, this may be a generic resource name such as `/foo`. In other cases, this may include a generic resource name and key. For example, `/foo` returns a list of the `foo` items and `/foo/{key`} returns the full content of the `foo` identified by the given `key`.

Refer to the details of the specific REST resources in the <a href="http://docs.atlassian.com/atlassian-confluence/REST/latest/" class="external-link">REST resources reference guide</a>.

## Media Types

The Confluence REST APIs return HTTP responses in one of the following formats:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Response Format</p></th>
<th><p>Requested via...</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>JSON</p></td>
<td><p>Requested via one of the following:</p>
<ul>
<li><code>application/json</code> in the HTTP Accept header</li>
<li><code>.json</code> extension</li>
</ul></td>
</tr>
<tr class="even">
<td><p>XML</p></td>
<td><p>Requested via one of the following:</p>
<ul>
<li><code>application/xml</code> in the HTTP Accept header</li>
<li><code>.xml</code> extension</li>
</ul></td>
</tr>
</tbody>
</table>

## API Versions

The Confluence REST APIs are subject to version control. The version number of an API appears in its URI. For example, use this URI structure to request version 1 of the 'admin' API:

``` javascript
http://example.com:1234/context/rest/prototype/1/...
```

To get the latest version of the API, you can also use the `latest` key-word. For example, if versions 1 and 2 of the 'admin' API are available, the following two URIs will point to the same resources:

-   ``` javascript
    http://example.com:1234/context/rest/prototype/latest/...
    ```

-   ``` javascript
    http://example.com:1234/context/rest/prototype/2/...
    ```

Notes:

-   The API version number is an integer, such as `1` or `2`.
-   The API version is independent of the Confluence release number.
-   The API version may, or may not, change with a new Confluence release. The API version number will change only when the updates to the API break the API contract, requiring changes in the code which uses the API. An addition to the API does not necessarily require a change to the API version number.
-   In the future, when there are multiple API versions available, it is the intention that each version of Confluence will support at least two API versions i.e. the latest API version and the previous API version.

## HTTP Response Codes

An error condition will return an HTTP error code as described in the [Atlassian REST API Design Guidelines version 1].

## Methods

You will use the standard HTTP methods to access Confluence via the REST APIs. Please refer to the <a href="http://docs.atlassian.com/atlassian-confluence/REST/latest/" class="external-link">REST resources reference guide</a> to see the HTTP methods available for each resource.

##### RELATED TOPICS

<a href="http://docs.atlassian.com/atlassian-confluence/REST/latest/" class="external-link">REST resources reference guide</a>
[Confluence REST APIs - Prototype Only]
[Confluence Server developer documentation]

  [REST API]: https://developer.atlassian.com/display/CONFDEV/Confluence+Server+REST+API
  [REST Authentication]: #rest-authentication
  [REST Resources and URI Structure]: #rest-resources-and-uri-structure
  [Media Types]: #media-types
  [API Versions]: #api-versions
  [HTTP Response Codes]: #http-response-codes
  [Methods]: #methods
  [(info)]: /confcloud/images/icons/emoticons/information.png
  [API version control]: #api-version-control
  [Atlassian REST API Design Guidelines version 1]: https://developer.atlassian.com/display/DOCS/Atlassian+REST+API+Design+Guidelines+version+1
  [Confluence REST APIs - Prototype Only]: https://developer.atlassian.com/display/CONFDEV/Confluence+REST+APIs+-+Prototype+Only
  [Confluence Server developer documentation]: https://developer.atlassian.com/display/CONFDEV

