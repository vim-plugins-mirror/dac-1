---
title: Cqlsupporteddatefunctionparams 39985870
aliases:
    - /confcloud/-cqlsupporteddatefunctionparams-39985870.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985870
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985870
confluence_id: 39985870
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_CQLSupportedDateFunctionParams

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.



