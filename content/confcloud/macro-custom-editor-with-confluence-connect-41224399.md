---
title: Macro Custom Editor with Confluence Connect 41224399
aliases:
    - /confcloud/macro-custom-editor-with-confluence-connect-41224399.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41224399
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41224399
confluence_id: 41224399
platform:
product:
category:
subcategory:
---
# Confluence Connect : Macro Custom Editor with Confluence Connect

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Add a custom editor to your macro experience.</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>15 minutes</td>
</tr>
</tbody>
</table>

# Prerequisites

Ensure you have worked through the [Quick Start Guide for Confluence Connect].

# Editor Magic

As we discovered in [Macro Custom Editor with Confluence Connect], Confluence Connect ships with features which allows macros defined by your add-on to provide highly customised and focused user experiences.

Normally when a macro is inserted into the Confluence Editor, we rely on the macro browser and editor to configure our macro and then insert it into a page. However, with Confluence Connect, this editor experience can be provided by a custom dialog completely under your control. Let's take another look at what we are looking to build.

<img src="/confcloud/attachments/41224402/41224417.gif" class="image-center" width="1200" />

 

We have already taken a look at [autoconvert][Macro Custom Editor with Confluence Connect] and how it allows for URLs to be mapped to an instance of your macro. In this tutorial, we will be looking at the custom editor experience above.
 

-   **Custom Editor:**
    When attempting to enter a new GIPHY macro, we were presented with a custom macro editing experience.

Once again, as a developer, this allows you to provide users with a dedicated user experience. Let's go through the process of building this.

This tutorial extends the GIPHY macro we built in a [Quick Start Guide for Confluence Connect]. Check it out before moving on!

# Searching GIPHY in style

The purpose of building a custom editor in our case is to provide users with a search screen through which they can easily browse for and choose the GIF they need. Under the hood, the custom editor capability uses a Connect dialog to render our view. This simplifies the process of building your own Confluence insert dialog - by simply specifying a width, height and url, we have essentially replaced the default macro insert dialog! 

Let's go back to our plugin's descriptor. We need only to define the mentioned parameters under the "editor" key of our GIPHY macro.

``` js
{
  "editor": {
    "url": "/gif-picker",
    "editTitle": {
      "value": "Giphy Search"
    },
    "insertTitle": {
      "value": "Giphy Search"
    },
    "width": "640px",
    "height": "370px"
  }
}
```

Check out the full <a href="https://bitbucket.org/atlassian/confluence-giphy-addon/src/5d34957b0c2769d0f4202ee848e94a77e88d03e8/atlassian-connect.json?fileviewer=file-view-default" class="external-link">atlassian-connect.json descriptor here</a>.

 

Now, we add a router handler for the above url: **"/gif-picker"**. In this handler, we will render a view which we have defined. Here's an example of how to go about this with a basic **gif-picker.hbs**: 

``` xml
{{!< layout}}

<div class="search-container">
    <form onsubmit="return false;" autocomplete="off">
        <input id="giphy-search-field" class="search-field" type="search" placeholder="Search">
    </form>
</div>

<div id="giphy-container"></div>

<script>
    $(function () {
        
        console.log("Running awesome script.");
 
        //  Hardcoded URL for demonstrative purposes.
        var data = {
            url: "http://giphy.com/gifs/applause-clapping-clap-aLdiZJmmx4OVW"
        };
 
        //  Use the Connect JS API to pass current macro parameters. 
        AP.require(["confluence", "dialog"], function (confluence, dialog) {
            //  Latch onto the macro editor's 'submit' event.
            dialog.getButton("submit").bind(function () {
                confluence.saveMacro({
                    'url': data.url
                });
                confluence.closeMacroEditor();
                return true;
            });
        });
    });
</script>
```

Unsure of how to add a route handler?

``` js
app.get('/gif-picker', addon.authenticate(), function (req, res) {
    // Handle request here.
    return res.render('<your-template>', { 
        // More params. 
    });
});
```

If you'd like a full example, take a quick look at how we did this [earlier][Quick Start Guide for Confluence Connect]!

 

As mentioned in the code snippet above, we use the Connect JS API to interface between our custom editor implementation and the insertion of a macro instance with certain parameters. To understand the Confluence and Dialog JS API's better, head over to the [related Connect Documentation]. 

<img src="/confcloud/attachments/41224399/41224398.png" class="image-center" width="634" height="487" />

*Our very own macro editor!*

Through the extensibility provided by the plugin descriptor and Connect APIs, we are able to provide a custom experience with relative ease. This concludes our deep dive into the advanced capabilities provided by Confluence Connect macros. 

All the best! 

  [Quick Start Guide for Confluence Connect]: https://developer.atlassian.com/confcloud/quick-start-to-confluence-connect-39987884.html
  [Macro Custom Editor with Confluence Connect]: /confcloud/macro-custom-editor-with-confluence-connect-41224399.html
  [related Connect Documentation]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/macro-editor.html

