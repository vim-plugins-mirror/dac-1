---
title: Atlassian Marketing Plugin.Xml Branding 24805599
aliases:
    - /market/-atlassian-marketing-plugin.xml-branding-24805599.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24805599
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24805599
confluence_id: 24805599
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_atlassian-marketing-plugin.xml branding

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th>Asset reference</th>
<th>Tag</th>
<th>Size of asset</th>
<th>Description of asset</th>
<th>Example reference to asset</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Add-on logo</td>
<td><code>logo</code></td>
<td>144 x 144px PNG/JPG</td>
<td>Reference your add-on logo. Use transparent or bounded, chiclet-style backgrounds. Marketplace automatically creates an add-on icon from your logo.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;logo image=&quot;images/01_logo.jpg&quot;/&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Add-on banner</td>
<td><code>banner</code></td>
<td>1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</td>
<td>Include your add-on name, your vendor name, and brief text about your add-on's functionality. Users see this banner displayed in the UPM when they browse the Marketplace from their Atlassian host product.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;banner image=&quot;images/02_banner.jpg&quot;/&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Highlight images</td>
<td><code>highlight</code></td>
<td><p>Full-size highlight:</p>
<p>1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</p>
<p>Cropped highlight:</p>
<p>580 x 330px PNG/JPG</p></td>
<td><p><strong>Full-size:</strong> Illustrate your highlight with a screenshot. High-definition display screenshots encouraged.</p>
<p><strong>Cropped:</strong> Provide a cropped version of your screenshot. This image is displayed prominently on the add-on details page.</p></td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;highlights&gt;
        &lt;highlight image=&quot;images/03_highlight1_ss.jpg&quot; cropped=&quot;images/04_highlight1_cropped.jpg&quot;/&gt;
        &lt;highlight image=&quot;images/05_highlight2_ss.jpg&quot; cropped=&quot;images/06_highlight2_cropped.jpg&quot;/&gt;
        &lt;highlight image=&quot;images/07_highlight3_ss.jpg&quot; cropped=&quot;images/08_highlight3_cropped.jpg&quot;/&gt;
      &lt;/highlights&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Additional screenshots</td>
<td><code>screenshot</code></td>
<td>1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</td>
<td>Provide additional screenshots to help illustrate your add-on's capabilities.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;screenshots&gt;
        &lt;screenshot image=&quot;images/09_addl_ss1.jpg&quot;/&gt;
        &lt;screenshot image=&quot;images/10_addl_ss2.jpg&quot;/&gt;
        &lt;screenshot image=&quot;images/11_addl_ss3.jpg&quot;/&gt;
      &lt;/screenshots&gt;</code></pre>
</div>
</div></td>
</tr>
</tbody>
</table>



