---
title: Content Voice and Tone 39996306
aliases:
    - /market/content-voice-and-tone-39996306.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39996306
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39996306
confluence_id: 39996306
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Content voice and tone

## Introduction

### Why do we need a style guide?

The style guide is here to help you write with precision, while maintaining personality and authenticity, so our audiences will engage with us.

The Atlassian ecosystem is growing quickly, with hundreds of developers building add-ons. The people add-on developers are speaking to - via in-product copy, help docs, and the Atlassian Marketplace - have thousands of bits of media vying for their attention every day. **In order to get through, your copywriting must be relevant, consistent, authentic, and engaging.**

### Who is this guide for?

Add-on developers who want their in-product and Marketplace copy to have that "Atlassian feel".

### How to use this guide

-   Bookmark for future reference.
-   Read the whole thing, or use the table of content to the left tor find your issue topic.
-   Understand that this is a living document.

## Best practices

-   **Content**
    -   Short, direct, and pithy is better than (and outperforms) long and granular. *Write for busy people with short attention spans*.
    -   Never use a long word where a short one will do. (Example: Don't say *breviloquent* when you can just say *short*.)

  Examples

-   **Bad**: "We're excited to announce the heavily anticipated release of Atlassian's Bamboo 5!"
-   **Good**: "Bamboo 5 is here!"

-   **Active voice… mostly**
    -   To write in active voice, make sure that the subject of your sentence is doing the action. ("She ran to the park" instead of "The park was where she ran to.") In passive voice, the target of the action gets promoted to subject status.
    -   Passive voice has its uses, but save it for content written in a narrative style - and even then, use it sparingly.

  Examples

-   In the sentence "Giancarlo wrote the blog post," Giancarlo is the subject, and he is doing the action. The object of the sentence is the blog post, and it's having an action done to it: it's being written.
-   This same sentence in passive voice would be "The blog post was written by Giancarlo." Even though Giancarlo is the one doing the writing, the focus of the sentence has switched to the blog post.

-   **First, second, or third person?**
    -   In most cases, using the second person is preferable. It fits Atlassian's casual, conversational tone to refer to the reader directly.
        -   Exceptions can be made for specific types of writing, such as whitepapers and press releases.
        -   The concept of teams is central to our branding. Whenever possible, use "your team" instead of simply "teams" - it's far more personal.

  Examples

-   Good: Bitbucket will change your life.
-   Good: Your team will love using pull requests in Bitbucket.
-   Bad: Teams love using pull requests in Bitbucket.

## Brand and voice

### Why it's important

Atlassian has a unique public voice. It's refreshing. It's a big part of what separates us from the herd of humdrum B2B vendors. And it's a big part of why customers (both present and future) love us.

In other words, it's a strategic asset.

By infusing this voice into modal dialogs, error messages, etc., your add-on will feel more like an organic part of the product and provide a better experience for your users.

### Voice vs. tone

Voice is your personality, tone is your mood.

This means that the characteristics of our voice will remain constant, while our tone will change depending on the situation. Imagine you're a word DJ. (Go on, imagine it.) You've got your mixer in front of you with knobs and sliders you can tweak to create whatever effect your audience needs at the moment. There are knobs for bold, optimistic, and practical with a wink.

Here's how to adjust your levels.

### Writing copy in (and about) your add-on

#### Blank slate

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> A blank slate message is used when an area doesn't have any content.</p>
<p>It's waiting on the user to take an action, and is often a first experience.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Communicate <em>possibility</em>.</p>
<p>What they will see? What they can do? How this will help them get stuff done?</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Curious</li>
<li>Motivated</li>
<li>Empowered</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Be welcoming</li>
<li>Have a call to action</li>
<li>Communicate a benefit</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | -------------------![][4]---------------------- | bold       |
| neutral  | ---------------------------------![][5]-------- | optimistic |
| factual  | ------------------![][6]----------------------- | playful    |

  Examples

#### Spot on

<img src="/market/imgs/content-voice/image1.png" width="728" /> (Bitbucket)
<img src="/market/imgs/content-voice/image2.png" width="728" /> (Confluence)

#### Empty state

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> When an area that usually displays content is temporarily empty.</p>
<p>Typically (though not always), it means the user has completed whatever work normally appears in that space.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Explain why the area is empty.</p>
<p>What's happened, what will appear here, and how it'll get there.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Pleased</li>
<li>Informed</li>
<li>Reassured</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Be reassuring</li>
<li>Explain what, why, when</li>
<li>If appropriate, celebrate!</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | -----------------------------------![][4]------ | bold       |
| neutral  | ---------------------------------------![][5]-- | optimistic |
| factual  | ----------------------------------![][6]------- | playful    |

  Examples

#### Spot on

<img src="/market/imgs/content-voice/image3.png" width="728" /> (Confluence)
![][7] (JIRA Service Desk)

#### Alert

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> Communicating new information.</p>
<p>Especially information that may affect the user's next action.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Update the user about what happened.</p>
<p>Let them know how it affects them, and then let them get back to work. You can be fun, but not super chatty.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Informed</li>
<li>Helped</li>
<li>Empowered</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Be clear</li>
<li>Be concise</li>
<li>Say why its important</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | -----![][4]------------------------------------ | bold       |
| neutral  | ----------![][5]------------------------------- | optimistic |
| factual  | ---![][6]-------------------------------------- | playful    |

  Examples

#### Spot on

<img src="/market/imgs/content-voice/image5.png" width="728" /> (Confluence)
<img src="/market/imgs/content-voice/image6.png" width="728" /> (HipChat)

#### Error message

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> When something has gone wrong.</p>
<p>It often means a dead-end for the user.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Explain the problem and provide next steps.</p>
<p>Don't baffle users with technical detail, keep it simple and direct. If it's our fault, own up. If it's user error, don't belittle them.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Supported</li>
<li>Reassured</li>
<li>In control</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Be clear</li>
<li>Explain the cause</li>
<li>Provide an alternative</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | ---------![][4]-------------------------------- | bold       |
| neutral  | ----------------![][5]------------------------- | optimistic |
| factual  | --![][6]--------------------------------------- | playful    |

  Examples

#### Spot on

![][8] (Confluence)
![][9] (Confluence)

#### Feature discovery dialog

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> Highlighting a new or recently updated feature.</p>
<p>It's all about user engagement in the product. Which is all about MAU.</p>
<p>And we're all about MAU.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Be persuasive.</p>
<p>Let the user know what's changed and invite and entice them to try something new.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Curious</li>
<li>Excited</li>
<li>Empowered</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Introduce the change</li>
<li>Communicate a benefit</li>
<li>Provide a call to action</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | -----------------------------------![][4]------ | bold       |
| neutral  | ---------------------------------------![][5]-- | optimistic |
| factual  | ----------------------------------![][6]------- | playful    |

  Examples

#### Spot on

![][10] (Confluence)
![][11] (Team Calendars)

#### Form

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> Getting the user to enter data or configure options.</p>
<p>Forms can be long or short.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Help the user succeed.</p>
<p>Make their path clear and simple. Skip the long explanations -- use good examples and placeholders instead.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Supported</li>
<li>Encouraged</li>
<li>Eager</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Be helpful</li>
<li>Guide the user</li>
<li>Give examples</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | ---------------------------------![][4]-------- | bold       |
| neutral  | -----------------------------![][5]------------ | optimistic |
| factual  | ----------------------------------![][6]------- | playful    |

  Examples

#### Spot on

![][12] (Confluence)
<img src="/market/imgs/content-voice/image12.png" width="728" /> (Confluence Questions)

#### Warning

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> Preventing error situations.</p>
<p>Help the user course-correct before things go wrong.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Inform, without alarming the user.</p>
<p>Clearly communicate the consequences of proceeding, and provide an alternative action (if there is one).</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Informed</li>
<li>Supported</li>
<li>Reassured</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Be clear</li>
<li>Explain the situation</li>
<li>Provide an alternative</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | -------------------------![][4]---------------- | bold       |
| neutral  | -------------------------![][5]---------------- | optimistic |
| factual  | -----------------![][6]------------------------ | playful    |

  Examples

#### Spot on

![][13] (Confluence)

#### Marketplace page

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> Telling prospective customer's why your add-on is so awesome.</p>
<p>Add-on pages with rich descriptions get more sales! But don't go overboard -- customers don't have time to read a novel.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Tell a compelling story.</p>
<p>How will your add-on help their team work smarter? Focus on benefits to the user, rather than just describing features.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Informed</li>
<li>Eager</li>
<li>Delighted</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Focus on how users will benefit, rat</li>
<li>Go easy on the exclamation points</li>
<li>Don't over-promise</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | ---------------------------------------![][4]-- | bold       |
| neutral  | ---------------------------------------![][5]-- | optimistic |
| factual  | ---------------------------------------![][6]-- | playful    |

  Examples

#### Spot on

<img src="/market/imgs/content-voice/image14.png" width="728" /> (From Balsamiq -- Great job selling it, without over-promising. Copy is dynamic, and the middle section is especially focused on benefits to the user.)
#### Room for improvement

<img src="/market/imgs/content-voice/image15.png" width="728" /> (From Zephyr -- Inconsistent capitalization in the headlines doesn't make an awesome first impression. We prefer sentence case, like the "Test right inside JIRA" headline.)

### Other communications

#### Help docs

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> Showing users how to get $#!t done using your add-on.</p>
<p>Technical documentation has to be clear and specific. It does not, however, have to be boring.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Get the user back into the product.</p>
<p>Give &quot;just enough&quot; info to help them understand the use case and perform the operation.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Empowered</li>
<li>Relieved</li>
<li>Delighted</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Keep it casual</li>
<li>Represent diversity in examples/screenshots</li>
<li>Just the facts, ma'am... just the facts</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | ---------------![][4]-------------------------- | bold       |
| neutral  | -------------------------------![][5]---------- | optimistic |
| factual  | ----------![][6]------------------------------- | playful    |

  Examples

#### Spot on

<img src="/market/imgs/content-voice/image16.png" width="728" /> (From the [Confluence docs] -- Very informative, yet casual. Using "you're" instead of "you are" and phrases like "those really long tables" contribute to the friendly, informal vibe.)
<img src="/market/imgs/content-voice/image17.png" width="728" /> (From the [Confluence docs][14] -- Provides some context around this operation, but doesn't go into an exhaustive list of reasons why you'd want to move a page.)
#### Room for improvement

<img src="/market/imgs/content-voice/image18.png" width="728" /> (From the [Confluence docs] -- Good representation of gender diversity, but the names are noticeably Euro-centric.)
<img src="/market/imgs/content-voice/image19.png" width="728" /> (From the [JIRA docs] -- Informative, but really stiff. "We recommend" instead of "it is recommended" and "make sure X because Y" instead of "ensure X as Y" would be friendlier.)

#### Release notes

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><img src="/market/imgs/lightbulb_on.png" /> <strong>What it's for:</strong> Communicating what's changed in your add-on's latest release.</p>
<p>Tout the shiny new features, plus provide the essential info that'll impact an admin's decision to upgrade.</p></td>
<td><p><img src="/market/imgs/thumbs_up.png" /> <strong>Your job:</strong> Drive adoption.</p>
<p>Get users excited about shiny new features, and help them feel confident about upgrading.</p></td>
<td><p><img src="/market/imgs/heart.png" /> <strong>Users should feel...</strong></p>
<ul>
<li>Eager</li>
<li>Informed</li>
<li>Connected</li>
</ul></td>
<td><p><img src="/market/imgs/check.png" /> <strong>Quick tips</strong></p>
<ul>
<li>Think about how each feature benefits a team</li>
<li>Have a little fun</li>
<li>Give admins the 'full story&quot; so they can upgrade with confidence</li>
</ul></td>
</tr>
</tbody>
</table>

|          |                                                 |            |
|----------|-------------------------------------------------|------------|
| reserved | -----------------------------------![][4]------ | bold       |
| neutral  | -------------------------------![][5]---------- | optimistic |
| factual  | -----------------------------------![][6]------ | playful    |

  Examples

#### Spot on

<img src="/market/imgs/content-voice/image20.png" width="728" /> (From CAC -- It is ![indeed] with "great pleasure" that we ship new releases. Go ahead and let that show!)
<img src="/market/imgs/content-voice/image21.png" width="728" /> (From the Confluence 5.8 release notes -- Phrases like "a real drag," "automagically" and "mammoth tables" make this section delightful to read.)
#### Room for improvement

<img src="/market/imgs/content-voice/image22.png" width="728" /> (From the [JIRA 4.4 release notes] -- There's no real flow between sentences and no narrative, which makes it hard for the reader to get excited. And the screenshot of an issue titled "Crappy initial experience" is rather un-optimistic.)

  []: /market/imgs/lightbulb_on.png
  [1]: /market/imgs/thumbs_up.png
  [2]: /market/imgs/heart.png
  [3]: /market/imgs/check.png
  [4]: /market/imgs/star_green.png
  [5]: /market/imgs/star_blue.png
  [6]: /market/imgs/star_red.png
  [7]: /market/imgs/content-voice/image4.png
  [8]: /market/imgs/content-voice/image7.png
  [9]: /market/imgs/content-voice/image8.png
  [10]: /market/imgs/content-voice/image9.png
  [11]: /market/imgs/content-voice/image10.png
  [12]: /market/imgs/content-voice/image11.png
  [13]: /market/imgs/content-voice/image13.png
  [Confluence docs]: https://confluence.atlassian.com/display/DOC/Working+with+Tables
  [14]: https://confluence.atlassian.com/display/DOC/Move+and+Reorder+Pages
  [JIRA docs]: https://confluence.atlassian.com/display/JIRA064/Customizing+the+Look+and+Feel
  [indeed]: /market/imgs/indeed.png
  [JIRA 4.4 release notes]: https://confluence.atlassian.com/display/JIRA064/JIRA+4.4+Release+Notes

