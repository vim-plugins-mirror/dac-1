---
title: Index
aliases:
    - /market/index.html
dac_edit_link: 
dac_view_link: 
confluence_id: 
platform:
product:
category:
subcategory:
---
# Space Details:

|             |                        |
|-------------|------------------------|
| Key         | MARKET                 |
| Name        | Atlassian Marketplace  |
| Description |                        |
| Created by  | smaddox (Sep 25, 2011) |



