---
title: Atlassian Verified Program 23299224
aliases:
    - /market/atlassian-verified-program-23299224.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23299224
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23299224
confluence_id: 23299224
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Atlassian Verified program

Atlassian Verified vendors uphold Atlassian standards for add-on traction, timely support, and vendor reliability. If you sell at least one paid-via-Atlassian add-on in the Marketplace and meet Verified benchmarks, you can apply to become a Verified vendor. Once your application is approved, all of your add-ons display the Atlassian Verified badge. This badge helps customers feel confident in their decision to try and buy your products.

<img src="/market/attachments/23299224/29524430.png" class="confluence-thumbnail" width="300" />

This page explains the following:

-   [Objectives]
-   [Requirements]
-   [How to apply]
-   [Resolving a flagged status]
-   [Reapplying to the Verified program]

## Objectives

We introduced the Atlassian Verified program to reward vendors who provide exemplary customer experiences. The Verified program aims to encourage great customer experiences in three main areas:

 

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
<p>Area</p>
</div></th>
<th><div class="tablesorter-header-inner">
<p>Details</p>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Add-on traction</p></td>
<td><p>Vendors demonstrate Marketplace traction by having their paid-via-Atlassian add-ons installed in a minimum number of customer instances.</p></td>
</tr>
<tr class="even">
<td><p>Timely support</p></td>
<td><p>Vendors adhere to service level agreements (SLAs) and provide support websites. Support is offered at least 8 hours a day, 5 days a week.</p></td>
</tr>
<tr class="odd">
<td><p>Vendor reliability</p></td>
<td><p>Vendors are routinely vetted by Atlassian to ensure they offer great customer experiences.</p></td>
</tr>
</tbody>
</table>

## Requirements

This section details the Atlassian Verified program requirements and how we measure them. In addition to Verified requirements, your add-on listings must follow our [branding guidelines].

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Requirement</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Sell at least one paid-via-Atlassian add-on</td>
<td>The Verified program requirements apply to paid-via-Atlassian add-ons. You must sell at least one paid-via-Atlassian add-on to become a Verified vendor.</td>
</tr>
<tr class="even">
<td>Your paid-via-Atlassian add-on versions are installed in at least 500 active instances</td>
<td><p>At least 500 actively used product instances must report installation of your paid-via-Atlassian add-on versions. This is measured by the<a href="https://confluence.atlassian.com/x/8AJTE" class="external-link"> Universal Plugin Manager (UPM)</a>, which reports actively used host products (like JIRA or Confluence) that have your add-ons installed.</p></td>
</tr>
<tr class="odd">
<td>Provide documentation for all paid-via-Atlassian add-ons</td>
<td><p>Provide a documentation URL for all paid-via-Atlassian add-ons.  </p></td>
</tr>
<tr class="even">
<td>Provide a support URL for all paid-via-Atlassian add-ons</td>
<td><p>Customers should be able to access your support URL and see a clear way to get support for your add-on - file a ticket, email your support team, or other ways to contact you. Your support URL should be hosted under your vendor name domain.</p></td>
</tr>
<tr class="odd">
<td>Offer support at least 8 hours a day, 5 days a week in your local time zone for all paid-via-Atlassian add-ons</td>
<td>Support hours can be any time, relative to your local timezone.</td>
</tr>
<tr class="even">
<td>Publish and adhere to an SLA statement</td>
<td><p>Publish and adhere to a service level agreement (SLA) outlining your support and service level terms, available via URL. Your SLA should include: </p>
<ul>
<li>Support hours that match what you have listed on the Marketplace</li>
<li>Holidays</li>
<li>Response time expectations</li>
</ul></td>
</tr>
<tr class="odd">
<td>Use an issue tracker like JIRA to resolve and track customer-reported bugs and feature requests, for all paid-via-Atlassian add-ons</td>
<td>You don't need to use an Atlassian product to track your issues, but some kind of tracker to keep on top of customer-reported bugs and improvement requests.</td>
</tr>
<tr class="even">
<td>Provide Atlassian with 24/7 emergency contact information</td>
<td>Provide an email address and phone number to Atlassian just in case we need to contact you for emergency support issues, such as those involving customer data loss or downtime. This information will only be used by Atlassian and will not be shared with customers. If something goes wrong, we should be able to reach you via this contact information 24/7. This information will be verified when reviewing the application.</td>
</tr>
<tr class="odd">
<td>Your site, Marketplace listing, and support documentation is available in English</td>
<td>The documentation should be spell checked and easy to understand.</td>
</tr>
</tbody>
</table>

## How to apply

You can apply to become an Atlassian Verified vendor directly from your vendor profile in the Marketplace. First, ensure you meet all requirements. Then, here's how to apply:

1.  Log into the Atlassian Marketplace with your vendor account.
2.  Click **Manage listings** to visit your vendor profile page.
3.  Click** Verified program** from the horizontal navigation bar. 
    <img src="/market/attachments/23299224/40512438.png" width="600" />
4.  Ensure all requirements show **READY** under the **Status** column.
    <img src="/market/attachments/23299224/40512439.png" width="600" />
5.  Click **Get Verified** in the upper right-hand corner of the screen. 
    You'll see a confirmation modal advising that your request has been sent. Our friendly Marketplace team will respond in 3-5 business days.
6.  Your current Verified status is always visible on this page in your vendor profile.

## Resolving a flagged status

If you see a message that your profile is flagged, this can mean one of two things: 

-   A customer has filed a complaint, and we need to follow up to verify that you meet our requirement(s).
-   We automatically check metrics periodically to make sure our Verified vendors are current. One or more of the above requirements came back as unmet, so we need you to make requisite changes and reapply.

If you're flagged, our Marketplace admin sends you an email outlining what fields need modification. During this period, **customers still see your add-ons as Verified**. When you've modified the fields, our admin is automatically alerted to review your Verified status. After you make changes to the requisite fields, you'll see the lozenge in the Verified table turn from FLAGGED to PENDING REVIEW. Once we've reviewed them, the lozenge will change back to APPROVED.

## Reapplying to the Verified program

If you applied to the Verified program and were rejected, or if you lost Verified status, you can reapply at any time**.**  

  [Objectives]: #objectives
  [Requirements]: #requirements
  [How to apply]: #how-to-apply
  [Resolving a flagged status]: #resolving-a-flagged-status
  [Reapplying to the Verified program]: #reapplying-to-the-verified-program
  [branding guidelines]: /market/branding-your-add-on-9699545.html

