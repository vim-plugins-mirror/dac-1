---
title: Accessing Sales Reports with the REST API 13633048
aliases:
    - /market/accessing-sales-reports-with-the-rest-api-13633048.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13633048
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13633048
confluence_id: 13633048
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Accessing Sales Reports with the REST API

This documentation describes an older version of the REST API. For the latest documentation, see the `reporting` resource in the [Atlassian Marketplace API].

 

The vendor dashboard provides vendors with sales information add-ons in a visual format. To access the information in its raw format, you can use the Sales Report API.

The Sales Report API is a REST API that returns sale records in JSON format. Each sale record lists the invoice ID, the buyer, the purchase price, and so on. The data represents add-on sales only. It does not include information on evaluations.

To use the API, you send a GET HTTP request to the Atlassian Marketplace REST service, passing the username and password associated with the vendor as HTTP Basic Auth credentials.

The records in the response may be *paged*, that is, they may represent a subset of the results. The subset is determined by the offset and limit parameters to the query. Included in the response are "next" and "previous" elements with URL links for retrieving the next or previous set of records. You can use these elements to create an HTML page that lets users page through the results. 

An issue affects the Sales Report API that causes the service to lose the ability to link sales of an add-on to its vendor when the add-on name has been changed. 

## Request Format

The request URL takes the following form:

    https://marketplace.atlassian.com/rest/1.0/vendors/VENDOR_ID/sales?start-date=STARTDATE&end-date=ENDDATE&offset=OFFSET&limit=LIMIT&sort-by=SORTBY&order=ORDER&license-type=LICENSETYPE&add-on=PLUGINKEY&q=SUBSTRING

Replace the `VENDOR_ID` in the URL with your numeric vendor identifier for the Marketplace website. You can find this ID in the URL for your vendor page in the Marketplace.

The service takes the following parameters:

-   `STARTDATE` is the beginning of the date range for the sales report request in the form `YYYY-MM-DD`. This parameter is optional.
-   `ENDDATE` is the end of the date range for the sales report request. The format for this parameter is `YYYY-MM-DD`. This parameter is optional.
-   `OFFSET` specifies the index of the beginning of this page of sales records. For example, when requesting a page of data after the first 50 sales transactions, the offset is 50. This parameter is optional and defaults to 0.
-   `LIMIT` specifies the maximum number of sales records to return in the page of sales data. This parameter is optional and defaults to 10. The maximum value is 50.
-   `SORTBY` specifies the field on which to sort the sale records in the response. The default order is ascending, but you can use the `order` parameter to specify either descending or ascending order. Results for requests that do not specify a `sort-by` parameter are sorted by reverse date order (equivalent to `&sort-by=date&order=desc`).  The following sort keys, corresponding to the similarly-named fields in the `sale `object, are valid:
    -   `add-on`
    -   `customer`
    -   `date`
    -   `invoice`
    -   `license-id`
    -   `license-size`
    -   `license-type`
    -   `price`
    -   `sale-type`
-   `ORDER` specifies the order by which the records should be sorted in the response. Allowable values are `asc` for ascending order or `desc` for descending order. This parameter is optional. The default order if the request specified a `sort-by` field is ascending.
-   `LICENSETYPE` returns records associated with a specific license type. Options are academic, commercial or starter. You can specify multiple types by repeating the parameter (for example, `&license-type=commercial&license-type=starter`). Optional. If not specified, the response includes sales of all license types.
-   `PLUGINKEY` returns records for a particular add-on identified by plugin key. You can specify multiple add-ons by repeating the parameter for each add-on. This parameter is optional.
-   `SUBSTRING` filters sales records by a case-insensitive comparison of the query parameter value. This parameter lets you retrieve records by organization name or technical contact, for example. Fields subject to substring matching are `technicalContact`, `organisationName`, `invoice`, and `licenseId`. All applicable fields are evaluated, so a query of `har` would match all records with technical contacts of `Charlie` along with records for organisations named `HarrisonStreet`. Similarly, a substring value of `123` matches records with an invoice or license ID number that contains '123'.

An example of the request URL with parameters is:

    https://marketplace.atlassian.com/rest/1.0/vendors/85/sales?offset=25&limit=25&sort-by=date&licenseTypes=commercial&q=har

Using <a href="http://curl.haxx.se/" class="external-link">curl</a> for example, you can invoke the Sales API as follows:

    curl -u myusername "https://marketplace.atlassian.com/rest/1.0/vendors/85/sales?start-date=2012-09-24"

## Successful Response

The following listing shows an example of a successful response:

    {
        "links": [{
            "href": "/rest/1.0/vendors/99999/sales",
            "rel": "self"
        }, {
            "href": "/rest/1.0/vendors/99999/sales?offset=10",
            "rel": "next",
            "type": "application/json"
        }],
        "sales": [{
            "invoice": "AT-999999",
            "date": "2012-09-18",
            "licenseId": "SEN-1234567",
            "pluginKey": "org.atlassian.tutorial.example-plugin", 
            "pluginName": "Example Plugin",
            "organisationName": "Customer",
            "technicalContact": {
                "email": "sysadmin@example.com",
                "name": "Sys Admin"
            },
            "billingContact": {
                "email": "billing@example.com",
                "name": "Billing Contact"
            },
            "country": "USA",
            "licenseSize": "25 Users",
            "licenseType": "Commercial",
            "saleType": "New",
            "purchasePrice": 25.0,
            "vendorAmount": 21.25,
            "maintenanceStartDate": "2012-09-18",
            "maintenanceEndDate": "2013-09-18"
        },
        // ...
      ]
    }

An HTTP 200 status response indicates a successful request.

The first object in the response is the `links` object. It enables you to construct links in a results page that enables users to page through sales data. The `sales` object contains the following data for each sale:

**Data**

**Description**

**Example**

`invoice`
A unique transaction ID. Note that credit memos do not have the "AT-" prefix.
AT-123456
`date`
The date the sales transaction completed. The format for this field is `YYYY-MM-DD`.
2012-02-04
`licenseId`

A unique value in Atlassian SEN format. Note that in Atlassian Cloud, the same license ID will be used for all products licensed for a given Atlassian Cloud instance.

SEN-205346

`pluginKey`
The plugin key that identifies the plugin. If the sale is for a plugin in Atlassian Cloud, the key will end with `.ondemand`.
com.pyxis.greenhopper.jira
`pluginName`

The name of the purchased plugin.

GreenHopper for JIRA

`organisationName`

A string representing the add-on customer's organisation name. This field is optional.

Example Company, Inc

`technicalContact`
The technical contact information configured for this plugin. 
`name`
A string representing the name of the contact. This field is optional.
Alice Adams
`email`
A valid email address. This field is required and functions as a unique customer ID.
<a href="mailto:alice.adams@example.com" class="external-link">alice.adams@example.com</a>
`billingContact`
The billing contact information configured for this plugin.
`name`

A string representing the name of the contact. This field is optional.
Joe Blatt

`email`

A valid email address. This field is optional.
<a href="mailto:joe.blatt@example.com" class="external-link">joe.blatt@example.com</a>

`country`

A string. This field is optional.

USA

`licenseSize`

A string describing the number of users.

25 Users, Enterprise 500 users

`licenseType`

An enumeration that describes the customer's license for the Atlassian product. This value is one of:

-   Commercial
-   Academic

Commercial

`saleType`

A string representing the type of sale. This is one of:

-   New
-   Renewal
-   Upgrade

New

`maintenanceStartDate`

The start of the maintenance period associated with this sale. The customer may purchase the add-on before this. The format for this field is `YYYY-MM-DD`.

2012-02-04

`maintenanceEndDate`

The end of the maintenance period associated with this sale. The customer may renew before this date. The format for this field is `YYYY-MM-DD`.

2013-02-04

`purchasePrice`

This is before tax (GST) and includes discounts. This value is negative for refunds.

100.00

`vendorAmount`
The amount from the sale due to the vendor. This value is negative for refunds.
75.00
`discount`

Occurs if sale was by an Atlassian expert. This value is negative for refunds.

25.00

`expertName`
The name of the expert involved in the sale, if applicable.
AtlassianExperts, Inc.

## Error Response

If the REST call fails, the service returns one of the following HTTP statuses: 

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>HTTP Status Code</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>401 Unauthorized</code></p></td>
<td>Returned for unauthenticated requests, including requests with incorrect credentials or missing credentials.<br />
</td>
</tr>
<tr class="even">
<td><code>403 Forbidden</code></td>
<td>The credentials aren't associated with a user who has permission to access the specified vendor.</td>
</tr>
</tbody>
</table>

# License report

Your license report details user information for those that have started an evaluation or purchased a license for your paid-via-Atlassian add-on. This report is available through your Marketplace account. 

1.  **Log in** from the upper right corner of any page in the Marketplace.
2.  Click **Manage listings** from the upper right corner.
3.  Click **Sales and evals** to access your vendor dashboard.
4.  Click **Download license report**. 
    <img src="/market/attachments/10420478/27230291.png" title="Click sales and evals, then Download license" alt="Click sales and evals, then Download license" width="500" />
    The downloaded file is titled licenseReport.csv.

### **Data included in the license report**

The license report includes the following fields and columns: 

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Data</p></th>
<th><p>Description</p></th>
<th><p>Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Add-on license ID</p>
<p><code>licenseId</code></p></td>
<td><p>A unique value in Atlassian SEN format. Note that in Atlassian Cloud, the same license ID will be used for all products licensed for a given Atlassian Cloud instance. Evaluations have a license ID beginning with <code>SEN-L</code>, and that license ID will change once the customer purchases a license.</p></td>
<td><p>SEN-205346</p></td>
</tr>
<tr class="even">
<td><p>Organization name</p>
<p><code>organisationName</code></p></td>
<td><p>A string. This field is optional.</p></td>
<td><p>BMW AG</p></td>
</tr>
<tr class="odd">
<td><p>Add-on name</p>
<p><code>addOnName</code></p></td>
<td><p>A string representing the name of your add-on. This field is populated by the Marketplace.</p></td>
<td><p>GreenHopper for JIRA</p></td>
</tr>
<tr class="even">
<td><p>Add-on key</p>
<p><code>addOnKey</code></p></td>
<td>Your add-on key. If the sale is for a plugin in Atlassian Cloud, the key will end with <code>.ondemand</code>.</td>
<td>com.atlassian.plugins.jira.greenhopper</td>
</tr>
<tr class="odd">
<td><p>Technical contact name</p>
<p><code>technicalContactName</code></p></td>
<td><p>A string representing the contact name. This field is optional.</p></td>
<td><p>Alice Adams</p></td>
</tr>
<tr class="even">
<td><p>Technical contact email</p>
<p><code>technicalContactEmail</code></p></td>
<td><p>A valid email address. This field functions as a unique customer ID.</p></td>
<td><p>alice.adams@supercompany.com</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact phone number</p>
<p><code>technicalContactPhone</code></p></td>
<td><p>A phone number. This field is optional.</p></td>
<td><p>+61294811111</p></td>
</tr>
<tr class="even">
<td><p>Technical contact address 1</p>
<p><code>technicalContactAddress1</code></p></td>
<td><p>A string representing the address. This field is optional.</p></td>
<td><p>1 Spam Street</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact address 2</p>
<p><code>technicalContactAddress2</code></p></td>
<td><p>A string representing the address. This field is optional.</p></td>
<td><p>Suite 210</p></td>
</tr>
<tr class="even">
<td><p>Technical contact city</p>
<p><code>technicalContactCity</code></p></td>
<td><p>A string representing an city. This field is optional.</p></td>
<td><p>San Francisco</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact state</p>
<p><code>technicalContactState</code></p></td>
<td><p>A string representing a state. This field is optional.</p></td>
<td><p>CA</p></td>
</tr>
<tr class="even">
<td><p>Technical contact postal code/zip code</p>
<p><code>technicalContactPostcode</code></p></td>
<td><p>A string representing a postal code. This field is optional.</p></td>
<td><p>94103</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact country</p>
<p><code>technicalContactCountry</code></p></td>
<td><p>A string representing a country. This field is optional.</p></td>
<td><p>US</p></td>
</tr>
<tr class="even">
<td><p>Billing contact name</p>
<p><code>billingContactName</code></p></td>
<td><p>A string representing the contact name. This field is optional.</p></td>
<td><p>Steve Peters</p></td>
</tr>
<tr class="odd">
<td><p>Billing contact email</p>
<p><code>billingContactEmail</code></p></td>
<td><p>A valid email address. This field is optional.</p></td>
<td><p>steven.peters@company.com</p></td>
</tr>
<tr class="even">
<td><p>Billing contact phone</p>
<p><code>billingContactPhone</code></p></td>
<td><p>A string representing the phone number. This field is optional.</p></td>
<td><p>+61294811111</p></td>
</tr>
<tr class="odd">
<td><p>Edition</p>
<p><code>edition</code></p></td>
<td>A string representing the user tier of the license.</td>
<td>10 Users</td>
</tr>
<tr class="even">
<td><p>Application license type</p>
<p><code>licenseType</code></p></td>
<td><p>The value of the Atlassian product license. This value is one of:</p>
<ul>
<li>Evaluation</li>
<li>Commercial</li>
<li>Academic</li>
<li>Open Source</li>
<li>Community</li>
</ul></td>
<td><p>Commercial</p></td>
</tr>
<tr class="odd">
<td><p>Add-one license start date</p>
<p><code>startDate</code></p></td>
<td><p>When the product was originally purchased. The format for this field is <code>YYYY-MM-DD</code>.</p></td>
<td><p>2013-02-04</p></td>
</tr>
<tr class="even">
<td><p>Add-on license end date</p>
<p><code>endDate</code></p></td>
<td><p>When the license is schedule to expire. The format for this field is <code>YYYY-MM-DD</code>.</p></td>
<td><p>2013-02-04</p></td>
</tr>
<tr class="odd">
<td><p>Renewal action</p>
<p><code>renewalAction</code></p></td>
<td><p>When the license expires, the sales system does one of the following:</p>
<ul>
<li>AUTO_QUOTE : automatically quoted 90 days prior to expiration</li>
<li>AUTO_RENEW : automatically renewed using the saved CC on the license expiration date.</li>
<li>NONE : a reminder is not sent to the customer</li>
</ul></td>
<td><p>AUTO_QUOTE</p></td>
</tr>
</tbody>
</table>

  [Atlassian Marketplace API]: https://developer.atlassian.com/market/api/2/reference/resource/vendors/%7BvendorId%7D/reporting

