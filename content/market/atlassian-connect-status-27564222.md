---
title: Atlassian Connect Status 27564222
aliases:
    - /market/atlassian-connect-status-27564222.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=27564222
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=27564222
confluence_id: 27564222
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Atlassian Connect Status

Open critical issues

The following issues currently affect Atlassian Connect add-ons in production:

Key
Summary
T
Created
P
Status
Resolution
[ACJIRA-1038]
[Single select fields cannot be used in Order clause in JQL queries][ACJIRA-1038]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15303&amp;avatarType=issuetype" alt="Bug" class="icon" />][ACJIRA-1038]
Sep 11, 2016
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
In Progress
Unresolved
[CE-590]
[Confluence Connect REST API getContent is inconsistent][CE-590]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15303&amp;avatarType=issuetype" alt="Bug" class="icon" />][CE-590]
Aug 14, 2016
<img src="https://ecosystem.atlassian.net/images/icons/priorities/blocker.svg" alt="Blocker" class="icon" />
Open
Unresolved
[ACJIRA-486]
[Concurrency problems with project and issue related REST APIs][ACJIRA-486]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15303&amp;avatarType=issuetype" alt="Bug" class="icon" />][ACJIRA-486]
Jul 17, 2015
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
In Progress
Unresolved
<a href="https://developer.atlassian.com/plugins/servlet/applinks/oauth/login-dance/authorize?applicationLinkID=61b6191d-d412-3043-a96c-75b7bceaed1f" class="static-oauth-init">Authenticate</a> to retrieve your issues

[3 issues]

You can receive notifications about new critical issues by subscribing to <a href="https://ecosystem.atlassian.net/sr/jira.issueviews:searchrequest-rss/temp/SearchRequest.xml?jqlQuery=project+%3D+AC+AND+issuetype+%3D+Bug+AND+status+%21%3D+Closed+AND+priority+IN+%28Blocker%2C+Critical%29+ORDER+BY+created+DESC+&amp;tempMax=1000" class="external-link">this RSS feed</a>. If you want to be notified of the progress of a particular issue, click on the issue in the table, which will open the issue in JIRA. Then click on **Start watching this issue**.

## Recently closed critical issues

The following critical issues were closed in the past month:

Key
Summary
T
Created
P
Status
Resolution
[CE-623]
[Dynamic content macro iframes in tables render with a height of 0][CE-623]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15303&amp;avatarType=issuetype" alt="Bug" class="icon" />][CE-623]
Aug 26, 2016
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
Closed
Fixed
[CE-680]
[Confluence 6.0: Cannot retrieve page attachments from Macro code][CE-680]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15300&amp;avatarType=issuetype" alt="Support Request" class="icon" />][CE-680]
Sep 19, 2016
<img src="https://ecosystem.atlassian.net/images/icons/priorities/blocker.svg" alt="Blocker" class="icon" />
Closed
Tracked Elsewhere
[AC-2104]
[Shared secret is invalid on plugin re-install][AC-2104]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15300&amp;avatarType=issuetype" alt="Support Request" class="icon" />][AC-2104]
Aug 22, 2016
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
Closed
Cannot Reproduce
[CE-662]
[\[Confluence 6.0\] Synchrony: Connections could not be acquired from the underlying database][CE-662]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15300&amp;avatarType=issuetype" alt="Support Request" class="icon" />][CE-662]
Sep 08, 2016
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
Closed
Tracked Elsewhere
[ACJIRA-47]
[REST API to clone issues in bulk][ACJIRA-47]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15315&amp;avatarType=issuetype" alt="Story" class="icon" />][ACJIRA-47]
Jul 28, 2014
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
Closed
Won't Fix
[ACJIRA-46]
[Ability to customize toolbar on view issue page][ACJIRA-46]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15315&amp;avatarType=issuetype" alt="Story" class="icon" />][ACJIRA-46]
Jul 28, 2014
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
Closed
Won't Fix
[ACJIRA-1025]
[Atlassian connect post-functions are not guaranteed to resolve in order][ACJIRA-1025]
[<img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15303&amp;avatarType=issuetype" alt="Bug" class="icon" />][ACJIRA-1025]
Aug 29, 2016
<img src="https://ecosystem.atlassian.net/images/icons/priorities/critical.svg" alt="Critical" class="icon" />
Closed
Won't Fix
<a href="https://developer.atlassian.com/plugins/servlet/applinks/oauth/login-dance/authorize?applicationLinkID=61b6191d-d412-3043-a96c-75b7bceaed1f" class="static-oauth-init">Authenticate</a> to retrieve your issues

[7 issues]

 

  [ACJIRA-1038]: https://ecosystem.atlassian.net/browse/ACJIRA-1038?src=confmacro
  [CE-590]: https://ecosystem.atlassian.net/browse/CE-590?src=confmacro
  [ACJIRA-486]: https://ecosystem.atlassian.net/browse/ACJIRA-486?src=confmacro
  [3 issues]: https://ecosystem.atlassian.net/secure/IssueNavigator.jspa?reset=true&jqlQuery=project+in+%28AC%2C+CE%2C+ACJIRA%2C+JSDECO%29+AND+issuetype+%3D+Bug+AND+status+NOT+IN+%28Triage%2C+Closed%29+AND+priority+IN+%28Blocker%2C+Critical%29+ORDER+BY+created+DESC+++++&src=confmacro "View all matching issues in JIRA."
  [CE-623]: https://ecosystem.atlassian.net/browse/CE-623?src=confmacro
  [CE-680]: https://ecosystem.atlassian.net/browse/CE-680?src=confmacro
  [AC-2104]: https://ecosystem.atlassian.net/browse/AC-2104?src=confmacro
  [CE-662]: https://ecosystem.atlassian.net/browse/CE-662?src=confmacro
  [ACJIRA-47]: https://ecosystem.atlassian.net/browse/ACJIRA-47?src=confmacro
  [ACJIRA-46]: https://ecosystem.atlassian.net/browse/ACJIRA-46?src=confmacro
  [ACJIRA-1025]: https://ecosystem.atlassian.net/browse/ACJIRA-1025?src=confmacro
  [7 issues]: https://ecosystem.atlassian.net/secure/IssueNavigator.jspa?reset=true&jqlQuery=project+in+%28AC%2C+CE%2C+ACJIRA%2C+JSDECO%29+AND+status+%3D+Closed+AND+priority+IN+%28Blocker%2C+Critical%29+AND+resolved+%3E+-30d+ORDER+BY+resolved+DESC+++++&src=confmacro "View all matching issues in JIRA."

