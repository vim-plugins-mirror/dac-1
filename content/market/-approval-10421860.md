---
title: Approval 10421860
aliases:
    - /market/-approval-10421860.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10421860
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10421860
confluence_id: 10421860
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Approval

Following the guidelines listed below will ensure your add-on submission process is a fast and smooth process.

-   [Criteria all add-ons must meet]
-   [Additional criteria for Paid-via-Atlassian Plugins 2 add-ons]
-   [Criteria for Atlassian Connect add-ons]

## Criteria all add-ons must meet

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>No.</p></th>
<th><p>Approval Criteria</p></th>
<th><p>Notes</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>Provides all the marketing assets (icon, logo, banner, screenshots) in the correct format and dimensions on the <a href="https://developer.atlassian.com/display/MARKET/Branding+your+add-on">Marketplace</a> and in the <a href="https://developer.atlassian.com/display/MARKET/Adding+Marketing+Assets+to+Your+Plugin">plugin artifact</a>. Atlassian Connect add-ons can reference assets in the add-on descriptor.</p></td>
<td><p>Recommended but not required for free or commercial add-ons.</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>If the add-on is a paid add-on, it is reasonably priced (that is, it's not unreasonably expensive).</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>The add-on does what is advertised (further quality evaluation and rating is up to customers in trials).</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>Doesn't break the UI of the host application.</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>Doesn't negatively affect performance of the host application.</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>Doesn't use Atlassian assets in a <a href="http://www.atlassian.com/company/trademark" class="external-link">trademark infringing way</a>.</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>If your add-on is open source, please ensure that your source is publicly available.</p></td>
<td><p>We recommend a code hosting site like <a href="http://bitbucket.org/" class="external-link">Bitbucket</a>. You must also include a license file in your source code that matches what you've reported in the Atlassian Marketplace.</p></td>
</tr>
</tbody>
</table>

## Additional criteria for Paid-via-Atlassian Plugins 2 add-ons

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>No.</p></th>
<th><p>Approval Criteria</p></th>
<th><p>Notes</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>Uses the UPM API/UI for license management by including the <a href="https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Enablinglicensing"><code>atlassian-licensing-enabled</code> param in <code>atlassian-plugin.xml</code></a>.</p></td>
<td><p>Without this parameter, customers will not be able to manage their add-on license within UPM 2.</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>Specifies <a href="https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Instructionsforthemaven-jira-plugin">OSGI bundle instructions</a> required by the Licensing API.</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>Uses a non-milestone release of the Licensing API/<code>plugin-license-storage-library</code> dependency. Version <code>2.2.2</code> is the oldest version which will be approved, although we prefer the <a href="https://maven.atlassian.com/content/repositories/atlassian-public/com/atlassian/upm/plugin-license-storage-lib/" class="external-link">latest available</a>.</p></td>
<td><p>Milestone or older versions may be unsafe for production use.</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>Add-on can start up without the Licensing API (Plugin License Storage add-on) being previously installed.</p></td>
<td><p>Even though the Licensing API may already be present in your development environment, it may not be previously installed on a customer's environment. Use the <a href="https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support">code generation tool</a> to work around this. If it still fails, you may need to consider an <a href="https://developer.atlassian.com/display/UPM/Alternate+Deployment+Model+for+Licensed+Plugins">alternate deployment model</a>.</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>Has the <code>Atlassian-Build-Date</code> entry specified in your plugin manifest.</p></td>
<td><p><a href="https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Instructionsforthe%7B%7Bmavenjiraplugin%7D%7D">Using AMPS 3.9+</a> adds this manifest entry for you. If you edit this date manually, ensure the value is on or close to the actual add-on release date. See <a href="/market/plugin-metadata-files-used-by-upm-and-marketplace-852095.html">Plugin metadata files used by UPM and Marketplace</a>.</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>If the <code>licenseManager</code> component reports a license error, the add-on shouldn't function.</p></td>
<td><p>You decide how to enforce your plugin licenses when an <a href="https://developer.atlassian.com/display/UPM/License+Validation+Rules">invalid license</a> is detected.</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>Vendor accepted the <a href="https://developer.atlassian.com/display/MARKET/The+Atlassian+Marketplace+Publisher+Agreement">Atlassian Marketplace Agreement</a>.</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>If a plugin is sold through the Marketplace <strong>and</strong> elsewhere then the prices must be the same in both channels.</p></td>
<td><p>This is discussed in the <a href="https://developer.atlassian.com/display/MARKET/The+Atlassian+Marketplace+Publisher+Agreement">Atlassian Marketplace Agreement</a>.</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>The add-on is marked on the Marketplace as <strong>Deployable</strong> and has been uploaded to the Marketplace.</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>Paid-via-Atlassian add-on version is listed as <strong>Stable</strong>.</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>Has a license administration screen with a URL path scoped by plugin key. This license administration screen should provide Buy/Try/Renew/Upgrade buttons (when appropriate) and should allow customers to manually enter license keys.</p></td>
<td><p>Scoping it by plugin key prevents multiple add-ons from providing resources on the same URLs.</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>Has a <a href="https://developer.atlassian.com/display/DOCS/Adding+a+Configuration+UI+for+your+Plugin">configure link</a> in UPM that links to either a license administration screen or a more general configuration page that includes a link to the license administration screen.</p></td>
<td><p>Users can easily manage their plugin with this link.</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>Uses <a href="https://developer.atlassian.com/display/DOCS/Converting+from+Version+1+to+Version+2+%28OSGi%29+Plugins">Plugins 2</a> framework.</p></td>
<td><p>New plugins that use the prior framework version, Plugins 1, are not accepted.</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>Vendor offers support by email and/or phone and/or web-based (JIRA/GetSatisfaction/UserVoice/Zendesk...) as described in <a href="https://developer.atlassian.com/display/MARKET/The+Atlassian+Marketplace+Publisher+Agreement">Atlassian Marketplace Agreement</a></p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>At least one contact from the Vendor is registered with the <a href="http://groups.google.com/group/atlassian-marketplace" class="external-link">Atlassian Marketplace Google Group</a> for future correspondence.</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td>16</td>
<td>The listing should reference documentation that describes how to set up and use the add-on.</td>
<td> </td>
</tr>
<tr class="odd">
<td>17</td>
<td>The listing should not contain advertising for other add-ons, products or services. The add-on should not place similar advertising in-product (within the user interface of JIRA, Confluence or other host application).</td>
<td> </td>
</tr>
</tbody>
</table>

## Criteria for Atlassian Connect add-ons

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>No.</p></th>
<th><p>Approval Criteria</p></th>
<th><p>Notes</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>Vendor accepted the <a href="https://developer.atlassian.com/display/MARKET/The+Atlassian+Marketplace+Publisher+Agreement">Atlassian Marketplace Agreement</a> .</p></td>
<td> </td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>Uses the <a href="https://developer.atlassian.com/display/AC/Atlassian+Connect">Atlassian Connect</a> plugin framework.</p></td>
<td> </td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>Add-on is available for cloud customers only.</p></td>
<td><p>Add-ons developed with the Atlassian Connect framework are only installable in cloud instances.</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>The end version for <strong>Compatible to</strong> in the add-on approval form is set to <strong>Any</strong>.</p></td>
<td><p>Atlassian Connect add-ons are forward-compatible so the Compatible to version is not applicable.</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>At least one contact from the Vendor is registered with the <a href="http://groups.google.com/group/atlassian-marketplace" class="external-link">Atlassian Marketplace Google Group</a> for future correspondence.</p></td>
<td> </td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>The listing should reference documentation that describes how to set up and use the add-on.</p></td>
<td> </td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>The listing should not contain advertising for other add-ons, products or services. The add-on should not place similar advertising in-product (within the user interface of JIRA, Confluence or other host application).</p></td>
<td> </td>
</tr>
<tr class="even">
<td>8</td>
<td>Vendor must publish a security statement.</td>
<td>Here is <a href="https://www.atlassian.com/hosted/security" class="external-link">Atlassian's Security Statement</a> as an example.</td>
</tr>
<tr class="odd">
<td>9</td>
<td>Add-ons should not have separate listings for cloud and server versions.</td>
<td>If the Connect add-on for cloud is <em>essentially</em> the same as an existing Plugins 2 add-on for server instances, use a single Marketplace listing.</td>
</tr>
</tbody>
</table>

  [Criteria all add-ons must meet]: #criteria-all-add-ons-must-meet
  [Additional criteria for Paid-via-Atlassian Plugins 2 add-ons]: #additional-criteria-for-paid-via-atlassian-plugins-2-add-ons
  [Criteria for Atlassian Connect add-ons]: #criteria-for-atlassian-connect-add-ons
  [Marketplace]: https://developer.atlassian.com/display/MARKET/Branding+your+add-on
  [plugin artifact]: https://developer.atlassian.com/display/MARKET/Adding+Marketing+Assets+to+Your+Plugin
  [`atlassian-licensing-enabled` param in `atlassian-plugin.xml`]: https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Enablinglicensing
  [OSGI bundle instructions]: https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Instructionsforthemaven-jira-plugin
  [code generation tool]: https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support
  [alternate deployment model]: https://developer.atlassian.com/display/UPM/Alternate+Deployment+Model+for+Licensed+Plugins
  [Using AMPS 3.9+]: https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Instructionsforthe%7B%7Bmavenjiraplugin%7D%7D
  [Plugin metadata files used by UPM and Marketplace]: /market/plugin-metadata-files-used-by-upm-and-marketplace-852095.html
  [invalid license]: https://developer.atlassian.com/display/UPM/License+Validation+Rules
  [Atlassian Marketplace Agreement]: https://developer.atlassian.com/display/MARKET/The+Atlassian+Marketplace+Publisher+Agreement
  [configure link]: https://developer.atlassian.com/display/DOCS/Adding+a+Configuration+UI+for+your+Plugin
  [Plugins 2]: https://developer.atlassian.com/display/DOCS/Converting+from+Version+1+to+Version+2+%28OSGi%29+Plugins
  [Atlassian Connect]: https://developer.atlassian.com/display/AC/Atlassian+Connect

