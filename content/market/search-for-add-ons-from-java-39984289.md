---
title: Search for Add Ons From Java 39984289
aliases:
    - /market/search-for-add-ons-from-java-39984289.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984289
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984289
confluence_id: 39984289
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Search for add-ons from Java

The [Marketplace API Java client] lets you query add-on listings through the `Addons` API.

For a stand-alone demo, see `ListAddons.java` in the <a href="https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials" class="external-link">Marketplace API Tutorials</a> repository.

-   [Prerequisites]
-   [Step 1: Build an add-on query]
-   [Step 2: Perform the query]
-   [Step 3: Process results]
-   [Step 4: Handle multiple pages of results]

## Prerequisites

You will need:

-   A Java project that uses the `marketplace-client-java` library
-   A `MarketplaceClient` object (it does not have to be configured with any credentials)

    You may wish to configure the client with your Atlassian account credentials if you want your queries to include private add-ons or private add-on versions.

## Step 1: Build an add-on query

The `AddonQuery` class contains all the optional properties for specifying which add-ons you are interested in. If you want all add-ons, use the special value `AddonQuery.any()`. Otherwise, use `AddonQuery.builder()` to construct a query. Below are some example queries:

``` java
    // A query for all add-ons that are compatible with JIRA
    AddonQuery myQuery = AddonQuery.builder()
        .application(Option.some(ApplicationKey.JIRA))
        .build());

    // A query for all add-ons that are compatible with JIRA - will only return the first 5 results
    AddonQuery myQuery = AddonQuery.builder()
        .application(Option.some(ApplicationKey.JIRA))
        .limit(Option.some(5))
        .build();

    // A query for all paid-via-Atlassian add-ons that are compatible with JIRA Server 7.1.0
    AddonQuery myQuery = AddonQuery.builder()
        .application(Option.some(ApplicationKey.JIRA))
        .appBuildNumber(Option.some(71003))  // this is the build number for JIRA 7.1.0
        .cost(Option.some(Cost.PAID_VIA_ATLASSIAN))
        .hosting(Option.some(HostingType.SERVER))
        .build();
```

For more details on all of the supported properties in `AddonQuery`, see the full [API documentation].

## Step 2: Perform the query

The `find` method in the `Addons` API sends the query to Marketplace, and returns a `Page` object that represents the result set.

``` java
    Page<AddonSummary> results = client.addons().find(myQuery);
```

## Step 3: Process results

`Page` implements `Iterable`, so you can easily loop through the items in it as you would with any Java collection. For instance, this example prints the name of each add-on to the console:

``` java
    for (AddonSummary a: results) {
        System.out.println(a.getName());
    }
```

Note that the individual items are instances of `AddonSummary`, which is like `Addon` but does not include every add-on property; if you want the full add-on details, you will need to do an [add-on detail query].

## Step 4: Handle multiple pages of results

As the name implies, a `Page` is not necessarily the full result set, but rather a single page of results. The default size of a page is `10`, but you can specify a different number using `limit` as shown in Step 1 (up to a maximum of `50`). If there are more results than will fit on one page, the `size()` method will tell you how many results are in the current page, and `totalSize()` will tell you how many results matched the query in general:

``` java
    System.out.println("Total number of add-ons found: " + results.totalSize());
    System.out.println("Number of add-ons on this page: " + results.size());
```

If you want to read through every add-on in the full result set one page at a time, then use the `getNext()` method. This returns an `Option` containing a `PageReference` value which you can pass to the `getMore()` method to get the next page (or `none()` if you are on the last page):

``` java
    for (Option<PageReference<AddonSummary>> nextPage = results.getNext();
         nextPage.isDefined(); nextPage = results.getNext()) {
        // Since Option implements Iterable, we can use "for" to execute this code only if it has a value
        for (PageReference<AddonSummary> nextRef: nextPage) {
            results = client.getMore(nextRef);
            for (AddonSummary a: results) {
                System.out.println(a.getName());
            }
        }
    }
```

  [Marketplace API Java client]: /market/using-the-marketplace-api-java-client-39984232.html
  [Prerequisites]: #prerequisites
  [Step 1: Build an add-on query]: #step-1-build-an-add-on-query
  [Step 2: Perform the query]: #step-2-perform-the-query
  [Step 3: Process results]: #step-3-process-results
  [Step 4: Handle multiple pages of results]: #step-4-handle-multiple-pages-of-results
  [API documentation]: https://developer.atlassian.com/display/MARKET/Using+the+Marketplace+API+Java+client#UsingtheMarketplaceAPIJavaclient-Moreinformation
  [add-on detail query]: /market/get-add-on-details-from-java-39984291.html

