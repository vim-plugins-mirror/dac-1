---
title: Using the Marketplace API Java Client 39984232
aliases:
    - /market/using-the-marketplace-api-java-client-39984232.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984232
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984232
confluence_id: 39984232
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Using the Marketplace API Java client

The Marketplace API Java client is an open-source library that provides a convenient Java wrapper for commonly used operations in the [Marketplace API].

If you do not use Java, there is currently no equivalent wrapper for accessing the Marketplace API from other programming languages. However, since the API uses common standards (HTTP, JSON, and HAL), it should be fairly straightforward to do so using standard web service tools. To get an understanding of the underlying JSON requests and responses, we suggest that you read [Examples of API usage through JSON requests].

 

-   [Get the library]
-   [The MarketplaceClient class]
-   [Commonly used data types]
    -   [Option]
    -   [DateTime/LocalDate]
    -   [HtmlString]
    -   [ModelBuilders]
-   [Next steps]

## Get the library

The source code is available at <a href="https://bitbucket.org/atlassian-marketplace/marketplace-client-java" class="uri" class="external-link">https://bitbucket.org/atlassian-marketplace/marketplace-client-java</a>. It can be built with Java 7 or later and Maven 3.

All released versions are available as jars through Maven Central, so the easiest way to add the Marketplace API Java client to your project is to add a Maven dependency:

``` xml
<dependency>
    <groupId>com.atlassian.marketplace</groupId>
    <artifactId>marketplace-client-java</artifactId>
    <version>2.0.0</version> <!-- replace with latest release version -->
</dependency>
```

The example above includes only the client library itself; however, the client also makes use of other libraries including <a href="https://commons.apache.org/" class="external-link">Apache Commons</a>, <a href="https://github.com/google/gson" class="external-link">GSON</a>, <a href="https://github.com/google/guava" class="external-link">Guava</a>, <a href="http://www.joda.org/joda-time/" class="external-link">Joda-Time</a>, <a href="http://www.slf4j.org/" class="external-link">SLF4j</a>, and Atlassian's <a href="https://bitbucket.org/atlassian/fugue" class="external-link">Fugue</a>. If your project does not already use these, you can include them all at once by adding `<classifier>dependencies</classifier>` to the Maven dependency:

``` xml
<dependency>
    <groupId>com.atlassian.marketplace</groupId>
    <artifactId>marketplace-client-java</artifactId>
    <version>2.0.0</version> <!-- replace with latest release version -->
    <classifier>dependencies</classifier>
</dependency>
```

## The MarketplaceClient class

The entry point for all API methods is the `MarketplaceClient` class, which can be instantiated and configured using `MarketplaceClientFactory`. Typically, your Java code will only create a single instance of `MarketplaceClient` and use it for all subsequent API operations.

``` java
    MarketplaceClient client = MarketplaceClientFactory.createMarketplaceClient();
```

The above example creates a client instance with default settings, which does not use any authentication. This is adequate if you only want to access the same data that can be viewed publicly on the Marketplace site. If you want to query data that is only visible to a vendor or administrator-- or post new data to the server (such as publishing a new add-on version listing)-- you can configure the client with your Atlassian Account credentials:

``` java
    HttpConfiguration config = HttpConfiguration.builder()
        .credentials(Option.some(new HttpConfiguration.Credentials("my.username@example.com", "my.password")))
        .build();
    MarketplaceClient client = MarketplaceClientFactory.createMarketplaceClient(config);
```

## Commonly used data types

### Option

The client library makes heavy use of Fugue's `Option` class, which is similar to Java 8's `Optional` but can be used in Java 7. A property or parameter is represented as `Option<T>` if it can have no value or a value of type T. An `Option<T>` value can either be `Option.some(value)` or `Option.none(),` but it will never be `null`.

### DateTime/LocalDate

Since the client supports Java 7, it does not use the Java 8 `java.time` classes for dates and times, but rather the <a href="http://www.joda.org/joda-time/" class="external-link">Joda-Time</a> classes `DateTime` (for a combination date and time) and `LocalDate` (for a date that does not specify a time nor a time zone).

### HtmlString

Some properties of Marketplace objects, such as the release notes for an add-on version, allow a limited amount of HTML markup (for instance, links and bullet lists). All properties of this kind use the `HtmlString` class. This is simply a wrapper for a `String`, but makes it harder to accidentally mix HTML with plain-text content in your code. Marketplace sanitizes all HTML content so that it cannot contain unsafe elements like `<script>`. You should always assume that string properties that do not use `HtmlString` are plain text. However, these properties can still contain `<` characters that must be escaped if you are going to display their contents in a web browser.

### ModelBuilders

The `com.atlassian.marketplace.model` package contains many classes that can be queried from the server, such as `Addon` and `AddonVersion`. These have only property getter methods and should be considered immutable. When you want to construct your own instance of such a class and send it to the server (for instance, when creating a new add-on version), you will use a corresponding builder class provided by `ModelBuilders`. The general pattern for these is to call a factory method to create a new builder; call setter methods to set properties on the builder; and then call `build()` to produce the finished object:

``` java
    ModelBuilders.SomeObjectBuilder builder = ModelBuilders.someObject();
    builder.name("name of my object")
           .someOtherProperty("some other property");
    SomeObject myObject = builder.build();
```

## Next steps

The following tutorials contain examples of using Java:

-   [Create an add-on version from Java]
-   [Get add-on details from Java]
-   [Search for add-ons from Java]

Check out the following resources for more information:

-   <a href="https://docs.atlassian.com/marketplace-client-java/2.0.0-m9/apidocs/index.html" class="external-link">Full API documentation (Javadoc)</a>
-   <a href="https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials" class="external-link">marketplace-v2-api-tutorials</a> project on Bitbucket contains standalone Java examples

  [Marketplace API]: /market/marketplace-api-39368006.html
  [Examples of API usage through JSON requests]: /market/examples-of-api-usage-through-json-requests-39984261.html
  [Get the library]: #get-the-library
  [The MarketplaceClient class]: #the-marketplaceclient-class
  [Commonly used data types]: #commonly-used-data-types
  [Option]: #option
  [DateTime/LocalDate]: #datetime-localdate
  [HtmlString]: #htmlstring
  [ModelBuilders]: #modelbuilders
  [Next steps]: #next-steps
  [Create an add-on version from Java]: /market/create-an-add-on-version-from-java-39984255.html
  [Get add-on details from Java]: /market/get-add-on-details-from-java-39984291.html
  [Search for add-ons from Java]: /market/search-for-add-ons-from-java-39984289.html

