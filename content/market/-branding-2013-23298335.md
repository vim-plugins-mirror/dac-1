---
title: Branding 2013 23298335
aliases:
    - /market/-branding-2013-23298335.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23298335
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23298335
confluence_id: 23298335
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Branding 2013

Listing add-ons publicly in the Atlassian Marketplace requires two branding components. We require vendor material that represents you or your organization, and add-on specific assets. As you'd expect, your branding can't violate Atlassian <a href="http://www.atlassian.com/company/trademark" class="external-link">trademark and copyright</a> guidelines or those of other parties.

## Add-on branding guidelines

We've designed the add-on details page so Marketplace users can more easily learn basic benefits of your add-on. For public listings, provide a tagline, concise highlights, and screenshots. 

<img src="/market/attachments/23298719/23134568.png" title="Add-on highlights" alt="Add-on highlights" width="600" height="383" />

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Item</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Add-on name</p>
<p> </p></td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p>Use title casing (for example, <em>Project Planner</em>, not <em>Project planner</em>) to name your add-on in 40 characters or less. Using the Atlassian product name is acceptable (<em>Add-on Name <strong>for</strong> Product Name),</em> but <strong> not</strong> starting with the product name. For example, <em>JIRA Plugin X</em> would be rejected, but <em>Plugin X for JIRA</em> would be approved.</p>
<p>Ensure your add-on name <strong>omits</strong> the following words and names: <em>Atlassian, plugin, beta, BETA, Beta</em> and <em>add-on.</em></p>
<p><strong>Length:</strong> 40 characters or less</p>
<p><strong>Example:</strong> <em>Project Planner for JIRA</em></p></td>
</tr>
<tr class="even">
<td><p>Add-on tagline</p>
<p> </p>
<p> </p></td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p>
<div>
<em><br />
</em>
</div></td>
<td><p>Provide a short phrase that summarizes what your add-on can do.</p>
<p><strong>Length</strong>: 130 characters or less</p>
<p><strong>Example:</strong> <em>Project planner makes project management a cinch. Plan and schedule tasks using our intuitive interface in JIRA.</em></p></td>
</tr>
<tr class="odd">
<td><p>Add-on summary (Legacy)</p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></td>
<td><p>Provide a plain text add-on summary in 250 characters or less. Marketplace displays your add-on summary in search results.</p>
<p><strong>Length</strong>: 250 characters or less</p>
<p><strong>Example:</strong> <em>Project planner makes project management a cinch. Plan and schedule tasks using our intuitive interface, and see real-time updates when a task runs long or short.</em></p></td>
</tr>
<tr class="even">
<td><p>Description (Legacy)</p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></td>
<td><p>Provide a description of your add-on in plain text except for bullets, line breaks, and hyperlinks.</p>
<p><strong>Length:</strong> No limit, but we recommend 1000 characters or less.</p>
<p><strong>Example:</strong> <em>Project Planner helps you plan and track projects more efficiently. Get real-time calendar updates when an issue runs longer or shorter than expected, and capture snapshots of your project health to share with stakeholders and contributors.</em></p>
<p><em>Project Planner is used in over 90 countries to make project management easier and more efficient. You can feel secure in your timeline projections using Project Planner.</em></p></td>
</tr>
<tr class="odd">
<td><p>More details</p>
<p> </p></td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p>List awards, testimonials, accolades, language support, or other details about your add-on. Marketplace displays this field in plain text except for hyperlinks, bullets, and line breaks.</p>
<p><strong>Length</strong>: 1000 characters or less</p>
<p><strong>Example:</strong> <em>Project Planner helps you plan and track projects more efficiently. Get real-time calendar updates when an issue runs longer or shorter than expected, and capture snapshots of your project health to share with stakeholders and contributors.</em></p>
<p><em>Project Planner is used in over 90 countries to make project management easier and more efficient. You can feel secure in your timeline projections using Project Planner.</em></p></td>
</tr>
<tr class="even">
<td><p>Video</p></td>
<td><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></td>
<td><p>Provide a YouTube link to a short video about your add-on. Users like short videos (30 seconds or less), and videos that show how your add-on is used. Marketplace displays your video as 800 x 600px.</p>
<p><strong>Length/Size:</strong> No limit.</p></td>
</tr>
<tr class="odd">
<td><p>Add-on logo<br />
144 x 144px </p>
<p> </p></td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p>
<p> </p></td>
<td><p>Upload a crisp logo at 144 x 144px. Use transparent or bounded, chiclet-style backgrounds. Marketplace automatically creates an add-on icon from your logo.<br />
<br />
Note: You cannot use any Atlassian product logo as part of your add-on logo or contents. For more details <a href="https://www.atlassian.com/legal/trademark" class="external-link">see the Trademark Guidelines</a>.  </p>
<p>Ensure your logo is unique. If you have add-ons that provide the same function across different Atlassian products, it's okay to use the same logo.</p>
<p><strong>Size:</strong> 144 x 144px PNG/JPG</p>
<p><strong>Photoshop template:</strong> <a href="attachments/23298719/23134627.psd">Download template</a></p></td>
</tr>
<tr class="even">
<td><p>Add-on banner<br />
1120 x 548px  </p>
<p> </p>
<p> </p></td>
<td><p> </p>
<div>
<br />

</div></td>
<td><p>Provide a banner at 1120 x 548px in PNG/JPG format. Include your add-on name, your vendor name, and brief text about your add-on's functionality. Users see this banner displayed in the UPM when they browse the Marketplace from their Atlassian host product.</p>
<p><strong>Size</strong>: 1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</p>
<p><strong>Photoshop templates:</strong> <a href="attachments/23298719/23134628.psd">Download high-resolution template</a> • <a href="attachments/23298719/23134629.psd">Download standard-resolution template</a></p></td>
</tr>
<tr class="odd">
<td><p>Highlight title (3)</p>
<p> </p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></td>
<td><p>Add a short action-oriented title for your highlight.</p>
<p><strong>Length:</strong> 50 characters or less.</p>
<p><strong>Example:</strong> <em>Track time</em></p></td>
</tr>
<tr class="even">
<td><p>Highlight summary (3)</p>
<p> </p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></td>
<td><p>Summarize your add-on's key features.</p>
<p><strong>Length:</strong> 220 characters or less.</p>
<p><strong>Example:</strong>  <em>View issue and task progress relative to your deadlines and sprints</em></p></td>
</tr>
<tr class="odd">
<td><p>Highlight screenshot (3)</p>
<p> </p></td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p>
<p> </p></td>
<td><p>Illustrate your highlight with a screenshot. High-definition display screenshots encouraged.</p>
<p><strong>Size: 1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</strong>.<strong> </strong></p>
<p><strong>Photoshop template:</strong> <a href="attachments/23298719/23134723.psd">Download high-resolution template</a> • <a href="attachments/23298719/23134725.psd">Download standard template</a></p></td>
</tr>
<tr class="even">
<td><p>Highlight screenshot - cropped (3)</p>
<p> </p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></td>
<td><p>Provide a cropped version of your screenshot. This image is displayed prominently on the add-on details page.</p>
<p><strong>Size: 580 x 330px PNG/JPG</strong></p>
<p><strong>Photoshop template:</strong> <a href="attachments/23298719/23134630.psd">Download template</a></p></td>
</tr>
<tr class="odd">
<td><p>Highlight screenshot explanation (3)</p>
<p> </p></td>
<td><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></td>
<td><p>Provide an explanation for your screenshot.</p>
<p><strong>Length:</strong> 220 characters or less.</p>
<p><strong>Example:</strong>  <em>In this screenshot, you can see how to set deadlines for multiple issues at a time.</em></p></td>
</tr>
<tr class="even">
<td><p>Additional highlight resources</p>
<p> </p></td>
<td><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></td>
<td><ul>
<li>Up to 5 additional screenshots per highlight, 1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</li>
<li>Up to 5 additional screenshot explanations, up to 220 characters each.</li>
</ul></td>
</tr>
</tbody>
</table>

-   Images from high-resolution or Retina displays are encouraged
-   Animated GIFs are not permitted.
-   You're unable to submit your add-on for approval without images exactly matching size requirements.

## Support & documentation guidelines

Publicly listed add-ons require documentation resources, even if your add-on is free. Paid-via-Atlassian add-ons require vendor support. 

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Item</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Documentation URL</p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></td>
<td><p>Provide a URL where add-on users can find version-specific or general documentation for your add-on.</p>
<p><strong>Format:</strong> URL</p></td>
</tr>
<tr class="even">
<td><p>Version officially supported</p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> for paid-via-Atlassian, <img src="/market/images/icons/emoticons/information.png" alt="(info)" /> for others</td>
<td><p>Paid-via-Atlassian add-ons require vendor support. All other public listings can choose &quot;No.&quot;</p>
<p><strong>Format:</strong> Radio buttons with options for yes and no.</p></td>
</tr>
<tr class="odd">
<td><p>Atlassian Answers enabled</p></td>
<td><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></td>
<td><p>Indicate if you want Marketplace to create a special tag for your add-on in Atlassian Answers. When enabled, add-on users can ask and receive answers from members of the Atlassian Answers community.</p>
<p><strong>Format:</strong> Radio buttons with options for yes and now.</p></td>
</tr>
<tr class="even">
<td><p>Issue collector</p></td>
<td><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></td>
<td><p>Provide a URL where add-on users can file issues and bugs.</p>
<p><strong>Format:</strong> URL</p></td>
</tr>
<tr class="odd">
<td><p>Release summary</p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></td>
<td><p>Summarize your add-on release in 40 characters or less.</p>
<p><strong>Length:</strong> 40 characters or less</p>
<p><strong>Example:</strong> <em>Better compatibility for Google Chrome</em></p></td>
</tr>
<tr class="even">
<td><p>New in this release</p></td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Unless information provided in &quot;Fixed&quot; field</p></td>
<td><p>Describe new add-on enhancements introduced in your release.</p>
<p><strong>Length:</strong> 500 characters or less</p>
<p><strong>Example:</strong></p>
<ul>
<li><em>Customizable project health snapshots</em></li>
<li><em>Project health snapshot exports in Excel and Google Spreadsheets</em></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>Fixed in this release</p></td>
<td><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Unless information provided in &quot;New&quot; field</td>
<td><p>List bug fixes and specific tickets resolved in your add-on release.</p>
<p><strong>Length:</strong> 500 characters or less</p>
<p><strong>Example:</strong></p>
<ul>
<li><em>Display issues in Google Chrome resolved</em></li>
</ul></td>
</tr>
</tbody>
</table>

## Vendor branding guidelines

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Item</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Vendor name</p></td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p>Displayed with your add-ons, homepage sidebars, and on your vendor page. Use your company name or, if an individual, your own name. Be mindful of trademark and copyright guidelines. Short names are preferred.</p>
<p><strong>Length/Size</strong>: No longer than 40 characters.</p></td>
</tr>
<tr class="even">
<td><p>Vendor logo</p></td>
<td><p><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></p></td>
<td><p>Upload a large, clear, front-facing image. Your logo should be relevant to your vendor name. This will appear on your vendor page.</p>
<p><strong>Length/Size</strong>: 72x72px PNG/JPG/GIF</p></td>
</tr>
<tr class="odd">
<td><p>Vendor icon</p></td>
<td><p><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></p></td>
<td><p>Upload a smaller version of your vendor logo. This appears in the sidebar and top-ranked lists. If your logo does not scale down well, create an additional thumbnail image. In the future, your logo may be scaled to be your vendor icon.</p>
<p><strong>Length/Size</strong>: 16x16px PNG/JPG/GIF</p></td>
</tr>
</tbody>
</table>

  [(info)]: /market/images/icons/emoticons/information.png
  [Download template]: attachments/23298719/23134627.psd
  [Download high-resolution template]: attachments/23298719/23134628.psd
  [Download standard-resolution template]: attachments/23298719/23134629.psd
  [1]: attachments/23298719/23134723.psd
  [Download standard template]: attachments/23298719/23134725.psd
  [2]: attachments/23298719/23134630.psd

