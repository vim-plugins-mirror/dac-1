---
title: Sales Reports for Paid via Atlassian Listings 13632227
aliases:
    - /market/sales-reports-for-paid-via-atlassian-listings-13632227.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632227
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632227
confluence_id: 13632227
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Sales reports for paid-via-Atlassian listings

### **What kind of data do I get about my sales?**

You can access data about your sales and customers in several forms - we provide a vendor dashboard on the Atlassian Marketplace, or you can access the same data directly with our [sales report REST API].

We also provide a license report to you as a vendor. This report shows information about users who obtain a license for your add-on.

[Read more about sales and license reports].

### **Are Atlassian-provided sales reports up-to-date?**

These reports might be behind real time by as much as 24 hours, and in some cases licensing information may appear before sales data. Contact <a href="https://ecosystem.atlassian.net/secure/CreateIssue!default.jspa?selectedProjectId=11672" class="external-link">Marketplace Support</a> if the information doesn't appear after 24 hours.

### **Do I see customer data?**

Yep! Our end-user license agreement (EULA) specifies that we can provide you customer data as a third-party vendor. You'll see each customer differentiated by their unique email address.

### **What if I want access to more data about add-on purchases?**

If you'd like to collect more data about add-on purchases, you can contact customers directly via their email addresses. Otherwise, all information collected during transactions is already reported to you via the sales report.

  [sales report REST API]: /market/accessing-sales-reports-with-the-rest-api-13633048.html
  [Read more about sales and license reports]: https://developer.atlassian.com/x/ygCU

