---
title: Getting Add On Details Using Json 39984327
aliases:
    - /market/getting-add-on-details-using-json-39984327.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984327
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984327
confluence_id: 39984327
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Getting add-on details using JSON

The add-on resource within the `/rest/2/addons` API lets you query add-on details, including every field that is visible in a Marketplace add-on listing or that can be set through Manage Listings.

For an example of a script that uses this (using the Bash shell and `curl`), see `get-addons.sh` in the <a href="https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials" class="external-link">Marketplace API Tutorials</a> repository.

-   [Prerequisites]
-   [Step 1: Getting the resource URL]
-   [Step 2: Optional query parameters]
-   [Step 3: Performing the query]
    -   [Request]
    -   [Response]
    -   [Response properties]

## Prerequisites

You will need:

-   Any [tool for making HTTP requests]
-   The add-on key of an existing add-on

## Step 1: Getting the resource URL

The URL for an add-on resource is currently `https://marketplace.atlassian.com/rest/2/addons/ADDON_KEY` - substituting your add-on key for `ADDON_KEY`.

Rather than hard-coding the URL path for the resource, you can also obtain it by starting from the entry point of the API. This takes additional steps, but is a better practice in case resource paths ever change in the future.

1.  Do a `GET` request to `https://marketplace.atlassian.com/rest/2` -- you will receive a JSON response that looks something like this:

    ``` js
    {
      "_links": {
        "self": {
          "href": "/rest/2"
        },
        [some more links...]
        "addons": {
          "href": "/rest/2/addons"
        }
      }
    }
    ```

2.  The `href` property under `addons` is the URL path of the resource for finding add-ons. Do a `GET` request to this resource, with `?limit=0` added to indicate that you don't actually want a full list of add-ons (`https://marketplace.atlassian.com/rest/2/addons?limit=0`) -- you will receive a JSON response like this:

    ``` js
    {
      "_links": {
        "self": {
          "href": "/rest/2/addons"
        }, [...some more links that you can ignore...]
        "byKey": {
          "href": "/rest/2/addons/{addonKey}",
          "templated": true
        }
    }
    ```

3.  Take the `href` property under `byKey`, and substitute the add-on key of your existing add-on for `{addonKey}` (for instance, `/rest/2/addons/my.excellent.addon`). This gives you the URL path of the add-on resource.

## Step 2: Optional query parameters

You may optionally add any of the same query parameters that are supported by a [query for multiple add-ons], except for `offset` and `limit` which are not applicable to a single add-on. The usual reason for doing this would be to get the current version of the add-on (`?withVersion=true`)-- or to get the latest version that meets some criteria (`?withVersion=true&cost=free&hosting=server`).

## Step 3: Performing the query

### Request

-   HTTP method: `GET`
-   URL: the add-on resource, as described above, along with any optional query parameters
-   Authentication: if you want to see private data, you must provide Basic Authentication with the username and password of your Atlassian Account

``` text
# get the details for my.excellent.addon, along with the current version
curl "https://marketplace.atlassian.com/rest/2/addons/my.excellent.addon?withVersion=true"
```

### Response

``` js
{
  "_links": {
    "self": {
      "href": "/rest/2/addons/my.excellent.addon"
    },
    "alternate": {
      "href": "/plugins/my.excellent.addon"
    }
  },
  "_embedded": {
    "logo": {
      "_links": {
        "image": "https://marketplace-cdn.atlassian.example.com/files/my.excellent.addon/icons/default/e1692eef-eda8-4508-909a.png"
      }
    },
    "version": {
      "_links": {
        "href": "/rest/2/addons/my.excellent.addon/build/100"
      },
      "buildNumber": "100",
      "name": "1.0.0",
      [...other version properties...]
    }
  },
  "key": "my.excellent.addon",
  "name": "My Excellent Addon",
  [...other addon properties...]
}
```

### Response properties

-   `/_links/alternate`: A web link for viewing the add-on details on the Marketplace site.

-   `/_embedded/logo/_links/image`: An image URL for the add-on's logo. Note that there is also a `/_links/logo` link, which you will not normally use.

-   `/_embedded/version`: Information about the current (or latest matching) version of the add-on, if you specified `withVersion=true`.

There are many other add-on and add-on version properties and resource links; for details, see the REST API documentation.

  [Prerequisites]: #prerequisites
  [Step 1: Getting the resource URL]: #step-1-getting-the-resource-url
  [Step 2: Optional query parameters]: #step-2-optional-query-parameters
  [Step 3: Performing the query]: #step-3-performing-the-query
  [Request]: #request
  [Response]: #response
  [Response properties]: #response-properties
  [tool for making HTTP requests]: /market/examples-of-api-usage-through-json-requests-39984261.html
  [query for multiple add-ons]: https://developer.atlassian.com/display/MARKET/Searching+for+add-ons+using+JSON#Searchingforadd-onsusingJSON-Step2:Optionalqueryparameters

