---
title: Examples Of API Usage Through Json Requests 39984261
aliases:
    - /market/examples-of-api-usage-through-json-requests-39984261.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984261
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984261
confluence_id: 39984261
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Examples of API usage through JSON requests

The pages in this section illustrate how to perform some common [Marketplace API] operations by accessing the API directly and using <a href="http://www.json.org/" class="external-link">JSON</a> documents. For general characteristics and conventions of the API, please see [Marketplace API].

If you are writing a Java tool that integrates with Marketplace, it's more convenient to use the [Marketplace API Java client] instead; but if you are using a scripting language or some other tool, you will want to read these examples to become familiar with the low-level API details.

-   [Tools for making HTTP requests]
-   [Referring to resource properties]
-   [Resource links]

## Tools for making HTTP requests

For these examples, you can use any tool that is designed for making HTTP requests to a web service. There are many such tools: browser plugins, such as <a href="https://www.getpostman.com/" class="external-link">Postman</a>; standalone applications, such as the Windows <a href="https://github.com/wiztools/rest-client" class="external-link">rest-client</a>; and command-line tools, such as the standard Linux command <a href="https://curl.haxx.se/" class="external-link"><code>curl</code></a>.

These examples do not require any specific tool, but we will provide Linux command-line examples using `curl`.

## Referring to resource properties

Suppose that in one of the API examples, you have received a JSON document that looks like this:

``` js
{
  "_links": {
    "self": {
      "href": "/rest/2/resource"
    }
  },
  "name": "gold",
  "details": {
    "atomicNumber": 79
  }
}
```

We will use <a href="https://tools.ietf.org/html/rfc6901" class="external-link">JSON Pointer</a> syntax to describe the location of any property in this document:

-   `/name` - the top-level property called "name" (value is "gold")
-   `/details/atomicNumber` - the property "atomicNumber" within the property "details" (value is 79)
-   `/_links/self/href` - the property "href" within the property "self" within the property "links" (value is "/rest/2/resource")

If you are using a language such as JavaScript or Python where JSON documents are represented as native object/dictionary values, and your document is currently in a variable called `doc`, you can easily refer to a property like `/_links/self/href` using dot notation: `doc._links.self.href`.

## Resource links

Since the Marketplace API uses <a href="https://en.wikipedia.org/wiki/Hypertext_Application_Language" class="external-link">HAL</a>, resource links are always contained within a property called `_links`; so "the `self` link" is another way of referring to `/_links/self`.

Note that resource links are always relative to the server root, which is `https://marketplace.atlassian.com`. So, in this example, if we asked you to take the `self` link and do a GET request to it, you would actually be doing a request to `https://marketplace.atlassian.com/rest/2/resource`.

  [Marketplace API]: /market/marketplace-api-39368006.html
  [Marketplace API Java client]: /market/using-the-marketplace-api-java-client-39984232.html
  [Tools for making HTTP requests]: #tools-for-making-http-requests
  [Referring to resource properties]: #referring-to-resource-properties
  [Resource links]: #resource-links

