---
title: Banner 11305309
aliases:
    - /market/-banner-11305309.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11305309
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11305309
confluence_id: 11305309
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Banner

1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images



