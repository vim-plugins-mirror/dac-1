---
title: Deployable 10420847
aliases:
    - /market/-deployable-10420847.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10420847
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10420847
confluence_id: 10420847
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_deployable

If your add-on is in a single jar, the UPM can install it in a single step. This is called a *deployable* add-on. When you fill out your Marketplace submission, check the **Deployable** option. If your add-on is made up of multiple jars, configuration files, or in some other format such as an EXE file, then UPM cannot install it in a single step. It is a non-deployable add-on. Do not check **Deployable** in your submission.

The Atlassian Marketplace and the Universal Plugin Manager does not fully support non-deployable add-ons. You can list a non-deployable add-on but Marketplace customers do not see a **Try** or **Buy** button with it. Neither can customers install, upgrade, or renew etc, your add-on using UPM. Instead, UPM displays a **Download** button for your add-on which users can use to install.



