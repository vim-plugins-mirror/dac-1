---
title: Press Release Guidelines 39984911
aliases:
    - /market/press-release-guidelines-39984911.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984911
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984911
confluence_id: 39984911
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Press release guidelines

Thanks for your interest in making a public announcement with Atlassian! We're proud to support companies that leverage, extend, or integrate our products in amazing ways or provide us with important technology and services. In order to manage the proper and consistent usage of our brand assets, we've developed a process for approval and support for brand and press­related activities. These guidelines are intended to support your launch strategy without adding additional overhead. If at any time you need additional guidance, please feel free to reach out to  <a href="mailto:press@atlassian.com" class="external-link">press@atlassian.com</a> or connect directly with your Atlassian contact.

### The approval process for announcements and launches

-   When making a new public announcement, mentions of Atlassian or any of our brands (including JIRA Software, HipChat, Confluence, and Bitbucket) should be approved by Atlassian. 
-   This includes, but is not limited to, references in press releases, case studies, logo usage, announcement blog posts, results or earnings scripts, and Atlassian employee quotes. 
-   Approval is also requested if you'd like to repurpose previous public quotes for an additional purpose.
-   Non-announcement related, day to day marketing activities should adhere to the brand and language guidance provided on this page. Questions can be submitted via Charlie for any fringe cases.
-   Please allow 7--­10 business days for Atlassian to review and provide an approved version of the materials.
-   If a quote from an Atlassian employee is requested, please provide a draft for review within your materials.
-   To expedite the process, please stick closely to the PR and brand guidelines below.
-   Expert Partners can submit their materials online via Charlie, and any third party can request a review directly by emailing <a href="mailto:press@atlassian.com" class="external-link">press@atlassian.com</a>.

 

------------------------------------------------------------------------

### Press annoucement guidelines

A partner press release should:

-   Make it clear that you're an Atlassian Expert or Marketplace Vendor, and explain why this is unique and valuable.
-   Be used to highlight a clear call to action and the customer benefit of the announcement.
-   Highlight approved and the most up­-to­-date product information and marketing messages provided to you by Atlassian.
-   If possible, include support from a real live customer (via a quote). 
     

A partner press release should not:

-   Appear as a joint press release. Your release should be written from your point of view.
-   Use the terms "partners," "partnership," or "alliance" to describe the Atlassian relationship.  We prefer the terms "collaboration," "teamed," "in cooperation with," or "relationship." The best approach is to describe your company as a participant of an Atlassian program, for example: XYZ Company is a member of the Atlassian Expert Program or a vendor on Atlassian Marketplace.
-   Include any declarative statements around being "the best," "the first," "the only," "exclusive," "preferred," etc.
-   Include a quote from an Atlassian employee *unless previously agreed upon and approved by* Atlassian.
-   Include any sales projections, Atlassian business statistics, roadmap, or forward looking statements about what we may do in the future.
-   Disclose proprietary information about Atlassian.
-   Refer to our stock ticker symbol, or use the "About Atlassian" boilerplate.

 

------------------------------------------------------------------------

### Brand guidelines

-   Please follow our [published copywriting guidelines] and <a href="https://www.atlassian.com/legal/trademark" class="external-link"> trademark guidelines</a> for using Atlassian brand assets. No modification of any assets is allowed. 
-   Any third party communications should include the most up­-to-­date product information and names (for example, JIRA Software, not JIRA).
-   Do not copy Atlassian photography, illustrations, graphics, or icons from our website or other channels for use in marketing or press materials.
-   Third party communications should clearly represent your relationship with Atlassian.

#### Examples of approved language:

Approved:

-   "XYZ is joining Atlassian's Expert Program..."
-   "XYZ is working together with Atlassian to        "
-   "XYZ now integrates with Atlassian JIRA Software"
     

Not approved:

-   "XYZ announces a strategic partnership with Atlassian"
-   "The Atlassian alliance with XYZ significantly extends what is already a clear and dramatic leadership position for both companies. No other company currently can match the flexibility and power of this offering"
-   "XYZ is the first to provide these capabilities for enterprise customers"

 

  [published copywriting guidelines]: 

