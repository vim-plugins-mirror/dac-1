---
title: Add On Approval Guidelines 10421849
aliases:
    - /market/add-on-approval-guidelines-10421849.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10421849
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10421849
confluence_id: 10421849
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Add-on approval guidelines

After creating and testing your add-on, your next step is to submit the add-on for approval. To uphold standards of quality and reliability that our customers expect, all publicly listed add-ons are subject to an approval process.

Which add-ons are subject to the approval process?

-   **Publicly listed, new add-ons**
-   **Payment model changes:** For example, if your add-on changes from free or paid-via-vendor to a paid-via-Atlassian add-on.
-   **Hosting changes:** If you add a version that uses a different hosting model. For example, if you have a download/behind-the-firewall add-on, and add a version for Atlassian-hosted cloud applications.

If an earlier add-on version is already approved, routine version updates are exempt from the approval process except in the above cases.

 

On this page: 

-   [The approval process]
-   [Criteria for all add-ons]
-   [Additional criteria for paid-via-Atlassian Plugins 2 add-ons]
-   [Additional criteria for Atlassian Connect add-ons]

## The approval process

Approval times usually take about 5 business days, unless volume is high or you need to resubmit your add-on. Our team checks a range of requirements for your add-on, like if it installs properly and has documentation. It's not uncommon to experience a few rounds of the approval process. To avoid unnecessary updates and resubmissions, **make sure your add-on adheres to the guidelines** on this page. If you believe your approval is taking too long, feel free to check in <a href="http://go.atlassian.com/marketplace-support" class="external-link">via a question</a> to our team.

1.  **Log in** to <a href="https://marketplace.atlassian.com/" class="uri" class="external-link">https://marketplace.atlassian.com/</a>
2.  Click **Manage listings** in the header
3.  Click **Create add-on**
4.  Fill out the add-on **submission form** and upload related assets. 
5.  Accept the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Marketplace vendor agreement</a>, and **submit your add-on**.
6.  Our Marketplace team automatically creates a JIRA issue to track the approval process. <a href="https://ecosystem.atlassian.net/secure/Signup!default.jspa" class="external-link">Create an account for Ecosystem JIRA</a> so you can track the status. Use the same email as your Marketplace vendor profile.
7.  You'll be notified via email that your submission was successful, and **receive a URL to track** the approval process.
    You can click **View workflow** in the details section of your JIRA issue to see the submission status.
8.  Once approved, your JIRA issue is closed and your **add-on is published** to the Marketplace. Brilliant!

## Criteria for **all** add-ons

All add-ons, free or paid, for cloud or server instances, need to meet the following criteria: 

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Area</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Provide marketing assets</p></td>
<td><p><strong><img src="/market/images/icons/emoticons/information.png" alt="(info)" /></strong> Recommended but not required for free add-ons.</p>
<p>Provide branding and marketing assets like a logo, banner, and screenshots as defined by our <a href="https://developer.atlassian.com/display/MARKET/Branding+your+add-on">branding guidelines</a>.</p>
<p><a href="https://developer.atlassian.com/x/TACU">Reference these assets in the plugin descriptor</a>.</p></td>
</tr>
<tr class="even">
<td>Reasonable pricing</td>
<td>Your paid add-on should be reasonably and competitively priced.</td>
</tr>
<tr class="odd">
<td>Performs as described</td>
<td>Your add-on does what it advertises.</td>
</tr>
<tr class="even">
<td>Provide documentation</td>
<td>Your listing should reference documentation that describes how to set up and use your add-on.</td>
</tr>
<tr class="odd">
<td>Doesn't break the host product UI</td>
<td><p>The UI of the host product should be functional and intact when your add-on is installed. If the UI breaks, your add-on will be rejected.</p></td>
</tr>
<tr class="even">
<td>Doesn't degrade host product performance</td>
<td>Your add-on shouldn't significantly impact the host product performance.</td>
</tr>
<tr class="odd">
<td>Doesn't infringe Atlassian trademarks</td>
<td><a href="http://www.atlassian.com/company/trademark" class="external-link">Trademark infringement</a> is an automatic rejection, so ensure your assets are original. This includes visual assets as well as naming conventions. For example, <em>JIRA Plugin X </em>would be rejected, but<em> </em> <em>Plugin X for JIRA</em> would be approved.</td>
</tr>
<tr class="even">
<td>Available source code for open-source add-ons</td>
<td><p>If your add-on is open source, ensure your source is publicly available. We recommend a hosting site like <a href="http://bitbucket.org/" class="external-link">Bitbucket</a>.</p>
<p>Include a license file in your source code that matches what you report in the Marketplace.</p></td>
</tr>
<tr class="odd">
<td>No advertisements</td>
<td><p>Your listing shouldn't contain advertising for other add-ons, products, or services. You <strong>can</strong> mention paid versions of the same add-on or products that complement your add-on.</p>
<p>In the host application, your add-on should abide by the same rules. Reminders for upgrades, complementary products, or additional features are acceptable. We recommend placing these reminders in configuration screens.</p></td>
</tr>
<tr class="even">
<td>Free and paid external service elements are clear</td>
<td><p>If your add-on listing is free, ensure your add-on provides some useful function in the Atlassian parent product 'as is'. If your add-on requires a separate third-party account this must be clear.</p>
<p>If a separate third-party account is required and is subject to payments and licensing, ensure this is clear. We recommend in most cases these add-ons be listed as Paid-via-Vendor, unless your add-on provides some useful functionality <em>without</em> the third-party account.</p></td>
</tr>
</tbody>
</table>

## Additional criteria for **paid-via-Atlassian Plugins 2 **add-ons

Your add-on adheres to the criteria listed for all add-ons, **and:**

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Area</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Accept the Marketplace vendor agreement</td>
<td>Upon submission, accept the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Marketplace vendor agreement</a>.</td>
</tr>
<tr class="even">
<td>Specify the <code>Atlassian-Build-Date</code> in your manifest</td>
<td><a href="https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Instructionsforthe%7B%7Bmavenjiraplugin%7D%7D">Using AMPS 3.9+</a> adds this manifest entry for you. If you edit this date manually, ensure the value is on or close to the actual add-on release date. See <a href="/market/plugin-metadata-files-used-by-upm-and-marketplace-852095.html">Plugin metadata files used by UPM and Marketplace</a>.</td>
</tr>
<tr class="odd">
<td>Specifies OSGI bundle instructions</td>
<td>Specify the <a href="https://developer.atlassian.com/x/VwOf"><code>bundledArtifact</code> entries</a> required by the Licensing API.</td>
</tr>
<tr class="even">
<td><p>Uses the UPM API/UI for license management</p></td>
<td><p>Your <code>atlassian-plugin.xml</code> descriptor file includes the <a href="/market/understanding-the-generated-license-api-support-10421079.html"><code>atlassian-licensing-enabled</code> param</a> .</p>
<p>This parameter enables customers to manage their add-on licenses in UPM versions 2+.</p></td>
</tr>
<tr class="odd">
<td>Provides a license administration screen scoped by plugin key (&lt; UPM 2.0)</td>
<td><p>This screen should provide <em>Buy, Try, Renew</em> and <em>Upgrade</em> buttons when appropriate, and allow customers to manually enter license keys.</p>
<p>Your license administration screen should have a URL path scoped by plugin key. Scoping by plugin key prevents multiple add-ons from providing resources on the same URLs.</p></td>
</tr>
<tr class="even">
<td><p>Use a non-milestone release of the <code>plugin-license-storage-library </code>(&lt; UPM 2.0)</p></td>
<td><p>The oldest approvable version of the Licensing API/<code>plugin-storage-library</code> dependency is 2.2.2, but we prefer the <a href="https://maven.atlassian.com/content/repositories/atlassian-public/com/atlassian/upm/plugin-license-storage-lib/" class="external-link">latest available</a>.</p>
<p>Milestone or versions older than 2.2.2 may be unsafe for production use.</p></td>
</tr>
<tr class="odd">
<td>Add-on is runnable without the Licensing API being previously installed</td>
<td>Even though the Licensing API might already be present in your development environment, it may not be previously installed on a customer's environment. Use the <a href="https://developer.atlassian.com/x/VwOf">code generation tool</a> to work around this. If it still fails, you may need to consider an <a href="https://developer.atlassian.com/x/Lwaf">alternate deployment model</a> .</td>
</tr>
<tr class="even">
<td>Your add-on stops working if the <code>licenseManager</code> component reports an license error</td>
<td>Your add-on should stop functioning if or when an <a href="https://developer.atlassian.com/x/JoOI">invalid license</a> is detected by the <code>licenseManager</code> component.</td>
</tr>
<tr class="odd">
<td>Marked as <em>Deployable</em></td>
<td>You qualify your add-on as <strong>Deployable</strong> in the create add-on form.</td>
</tr>
<tr class="even">
<td>Match prices listed elsewhere</td>
<td>If you sell your add-on through the Marketplace <strong>and</strong> another channel, the prices must be the <strong>same</strong>. This is part of the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Marketplace vendor agreement</a>.</td>
</tr>
<tr class="odd">
<td>Stable plugin version</td>
<td>Your paid-via-Atlassian add-on version is listed as <strong>Stable</strong>.</td>
</tr>
<tr class="even">
<td>Uses a <em>Configure</em> link in the UPM (&lt; UPM 2.0)</td>
<td><p>Has a <a href="https://developer.atlassian.com/display/DOCS/Adding+a+Configuration+UI+for+your+Plugin">configure link</a> in UPM that links to either a license administration screen or a more general configuration page that includes a link to the license administration screen. If the minimum version you support bundles UPM 2.0 or higher, this requirement doesn't apply.</p></td>
</tr>
<tr class="odd">
<td>Use the <a href="https://developer.atlassian.com/display/DOCS/Converting+from+Version+1+to+Version+2+%28OSGi%29+Plugins">Plugins 2</a> framework</td>
<td>New plugins that use the prior framework, Plugins 1, are no longer accepted.</td>
</tr>
<tr class="even">
<td>Provide support</td>
<td><p>Offer support by email, phone, or web-based applications like JIRA or Zendesk as described in the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Marketplace vendor agreement</a>.</p></td>
</tr>
<tr class="odd">
<td>Register with our Google Group</td>
<td>At least one contact from your vendor profile is registered with the <a href="http://groups.google.com/group/atlassian-marketplace" class="external-link">Atlassian Marketplace Google Group</a> for future correspondence.</td>
</tr>
</tbody>
</table>

## Additional criteria for **Atlassian Connect **add-ons

Your add-on adheres to the criteria listed for all add-ons, **and:**

| Area                                                                        | Details                                                                                                                                                                                                                                                                                                         |
|-----------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| No duplicate, separate listing(s)                                           | If your Connect add-on has the same essential functionality as an existing Plugins 2 add-on you've published previously, it should **share** the same listing. You can do this by clicking **Add version** from the *Manage add-on* screen.                                                                     |
| Accept the Marketplace vendor agreement                                     | Upon submission, accept the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Marketplace vendor agreement</a>.                                                                                                                                           |
| Available for cloud users only                                              | Customers using server versions of Atlassian host products should not be able to use your add-on. Add-ons developed with the Connect framework are only compatible with cloud instances at the moment.                                                                                                          |
| Uses the Atlassian Connect framework                                        | Your add-on is built with our [Atlassian Connect] plugin framework, **not** Plugins 1 or Plugins 2.                                                                                                                                                                                                             |
| End version compatibility is *Any*                                          | The end version for **Compatible to** in the add-on approval form is set to **Any**.                                                                                                                                                                                                                            |
| Publish a security statement                                                | Atlassian Connect add-ons require a published security statement to be listed in the Marketplace. Here's <a href="https://www.atlassian.com/hosted/security" class="external-link">Atlassian's Security Statement</a> as an example.                                                                            |
| Register with our Google Group ............................................ | At least one contact from your vendor profile is registered with the <a href="http://groups.google.com/group/atlassian-marketplace" class="external-link">Atlassian Marketplace Google Group</a> for future correspondence.                                                                                     |
| No advertisements                                                           | Your listing shouldn't contain advertising for other add-ons, products or services. The add-on shall not place similar advertising in-product (within the user interface of JIRA, Confluence or other host application).                                                                                        |
| Secure authentication                                                       | Whilst your add-on may make use of **Basic** authentication with Atlassian's product REST APIs for ease of development / speed during development, this shall not be the case for any public, approved add-on. Additionally, shared secrets from customer-installed add-ons shall be stored in a secure manner. |

  [The approval process]: #the-approval-process
  [Criteria for all add-ons]: #criteria-for-all-add-ons
  [Additional criteria for paid-via-Atlassian Plugins 2 add-ons]: #additional-criteria-for-paid-via-atlassian-plugins-2-add-ons
  [Additional criteria for Atlassian Connect add-ons]: #additional-criteria-for-atlassian-connect-add-ons
  [(info)]: /market/images/icons/emoticons/information.png
  [branding guidelines]: https://developer.atlassian.com/display/MARKET/Branding+your+add-on
  [Reference these assets in the plugin descriptor]: https://developer.atlassian.com/x/TACU
  [Using AMPS 3.9+]: https://developer.atlassian.com/display/UPM/Understanding+the+Generated+License+API+Support#UnderstandingtheGeneratedLicenseAPISupport-Instructionsforthe%7B%7Bmavenjiraplugin%7D%7D
  [Plugin metadata files used by UPM and Marketplace]: /market/plugin-metadata-files-used-by-upm-and-marketplace-852095.html
  [`bundledArtifact` entries]: https://developer.atlassian.com/x/VwOf
  [`atlassian-licensing-enabled` param]: /market/understanding-the-generated-license-api-support-10421079.html
  [alternate deployment model]: https://developer.atlassian.com/x/Lwaf
  [invalid license]: https://developer.atlassian.com/x/JoOI
  [configure link]: https://developer.atlassian.com/display/DOCS/Adding+a+Configuration+UI+for+your+Plugin
  [Plugins 2]: https://developer.atlassian.com/display/DOCS/Converting+from+Version+1+to+Version+2+%28OSGi%29+Plugins
  [Atlassian Connect]: https://developer.atlassian.com/display/AC/Atlassian+Connect

