---
title: Marketplace Basics 13632211
aliases:
    - /market/marketplace-basics-13632211.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632211
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632211
confluence_id: 13632211
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Marketplace basics

### **What's the Atlassian Marketplace?**

The Atlassian Marketplace is a platform where developers sell add-ons for Atlassian products. There's no cost to list add-ons in the Marketplace. The Marketplace lets you access Atlassian's large customer base so you can market add-ons for our products like JIRA and Confluence. Add-on developers, or vendors, on the Marketplace:

-   Use Atlassian licensing support inside their add-on
-   Use Atlassian's payment and billing systems to collect license fees
-   Have access to Marketplace-generated sales and licensing reports

### **How do I get an account with the Marketplace?**

To get an account with the Marketplace, <a href="https://my.atlassian.com/signup/index" class="external-link">create an account</a> with <a href="http://my.atlassian.com/" class="external-link">AtlassianID</a>. Use these credentials to <a href="https://marketplace.atlassian.com/login?redirect=%2F" class="external-link">log into the Marketplace</a>.

### **What's an add-on vendor? **

Add-ons belong to vendors. A vendor profile describes the company that "owns" the add-on. For example, Atlassian and Gliffy are vendors. If you're an individual author without a parent company, (e.g., Ross Rowe) then create a vendor profile using your name. Your user account must be associated with a vendor profile before you can modify that vendor's add-on listing.

### **How do I associate my account with a vendor profile?**

If your account should belong to a particular vendor but doesn't, ask somebody on the account to add you. Here's how they can do this: 

1.  Log into the vendor account, and navigate to the *Manage vendor* page.
    You can access this page via the **Manage listings** link in the header.
2.  Click **Contacts**.
    <img src="/market/attachments/13632211/28411022.png" width="500" />
3.  Click **Add contact**.
4.  Enter the email address and click **Add**.

If there is no other user for your vendor profile, then email <a href="mailto:marketplace@atlassian.com" class="external-link">marketplace@atlassian.com</a> and we'll add your information for you.

### **How do I get support for Marketplace issues?**

 The Marketplace has a vibrant add-on vendor Google Group. We recommend <a href="https://groups.google.com/forum/#!forum/atlassian-marketplace" class="external-link">joining the Marketplace vendor mailing list</a> on Google Groups.

Alternately, for non-vendor support, you can ask a question via Atlassian Answers.

1.  Log into <a href="http://answers.atlassian.com/" class="external-link">http://answers.atlassian.com</a>.

    Answers uses the same username/password combination you use on MyAtlassian.

2.  Choose the **Ask A Question** link.

    The system displays the **Ask A Question** page.

    <img src="/market/attachments/13632211/17072556.png?effects=drop-shadow" width="650" />

3.  Fill out the form.

4.  Add the keyword "marketplace" to the tags field.

To contact Atlassian directly about an add-on listing, send an email to <a href="mailto:marketplace@atlassian.com" class="external-link">marketplace@atlassian.com</a>.

### **How do add-on customers get support? **

 If you offer support for your add-on, the **Get support** button is on the Support tab on your add-on details page. This tab also lists other resources including documentation links, issue trackers, forums, and more. Add-on customers are discouraged from writing reviews to get support, and should instead contact vendors directly.

### **What's the difference between deployable and non-deployable add-ons?**

The Universal Plugin Manager (UPM) can install add-ons automatically into Atlassian applications. However, some items in the Atlassian Marketplace may not be suitable for automatic installation.

The **Create new add-on** form has a **Deployable** option that indicates whether an add-on installs with UPM. You should not check the **Deployable** option if your add-on consists of multiple JARs. The Atlassian Marketplace and UPM do not support **Try**, **Buy**, **Install**, **Upgrade**, and **Renew** buttons for non-deployable add-ons. Non-deployable add-ons cannot be sold under the Atlassian Marketplace Vendor Agreement as they do not support the UPM licensing API.

Additionally, if your add-on requires multiple downloads, configuration or license files on the file system, or if your add-on is not meant to be installed into the host product (like JIRA Client, for example), then do not check the **Deployable** option. Your add-on is still available for download, but the UPM does not attempt to install it in the host product.

### **Can I sell add-ons for users in the cloud?**

Yes! Get started [building an add-on using Atlassian Connect].

 

  [building an add-on using Atlassian Connect]: https://developer.atlassian.com/static/connect/docs/index.html

