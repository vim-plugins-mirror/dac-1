---
title: Vendor Icon 11305293
aliases:
    - /market/-vendor-icon-11305293.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11305293
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11305293
confluence_id: 11305293
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Vendor Icon

16x16px PNG/JPG/GIF



