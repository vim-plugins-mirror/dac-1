---
title: Create an Add On Version From Java 39984255
aliases:
    - /market/create-an-add-on-version-from-java-39984255.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984255
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984255
confluence_id: 39984255
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Create an add-on version from Java

The [Marketplace API Java client] lets you create add-on versions through the `Addons` API. You might want to do this as part of an automated release process. The manual equivalent would be to go to <a href="https://marketplace.atlassian.com/manage/plugins" class="external-link">Manage listings</a> on Marketplace, select one of your existing add-ons, select "Create version" and fill in the version creation form.

For a stand-alone demo, see `CreateAddonVersion.java` in the <a href="https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials" class="external-link">Marketplace API Tutorials</a> repository.

-   [Prerequisites]
-   [Step 1: Specify an installable artifact]
-   [Step 2: Build a version object]
-   [Step 3: Post the new version to Marketplace]
-   [Approval for new versions]

## Prerequisites

You will need:

-   A Java project that uses the `marketplace-client-java` library
-   An existing add-on listing on Marketplace (can be public or private)
-   A `MarketplaceClient` object that was configured with the credentials of your Atlassian account
-   If the add-on is directly installable, the installable artifact (`.jar`, `.obr`, or workflow file) for the new add-on version

## Step 1: Specify an installable artifact

If the add-on is directly installable, and the installable artifact is in a file on your local filesystem, you can use the `uploadArtifact` method in the `Assets` API to upload the file. It will return an `ArtifactId`, which is a reference to the file data on the Marketplace server.

``` java
    File file = new File("/path/to/my-addon-1.0.jar");
    ArtifactId artifactId = client.assets().uploadArtifact(file);
```

If Marketplace does not recognize the file as a valid installable artifact or if anything else prevents the upload from succeeding, `uploadArtifact` will not return a value but will instead throw an `MpacException `exception.

Alternatively, if the artifact is not on your local filesystem but can be downloaded from a publicly accessible server (such as a Maven repository), you can simply construct an `ArtifactId` that tells Marketplace where to find it:

``` java
    ArtifactId artifactId = ArtifactId.fromUri(URI.create("http://my-server.example.com/url/to/download/my-addon-1.0.jar"));
```

## Step 2: Build a version object

Calling `ModelBuilders.addonVersion()` gives you a builder object which you can use to specify the properties for your new version.

``` java
    ModelBuilders.AddonVersionBuilder versionBuilder = ModelBuilders.addonVersion();

    // These properties must always be specified for every new version:
    versionBuilder.buildNumber(100)                // build numbers are unique values you choose to determine version ordering
        .paymentModel(PaymentModel.FREE)           // is this version of the add-on free, paid via vendor, or paid via Atlassian?
        .status(AddonVersionStatus.PUBLIC)         // you can make a version public or private
        .releaseDate(new LocalDate("2016-03-17"))  // this date will appear in the add-on's version history
        .releaseSummary(Option.some("this version fixes some bugs"));  // a brief description of the release

    // If you have an ArtifactId (see step 1), attach it to the version like this:
    versionBuilder.artifact(Option.some(artifactId))

    // Or, if there is no ArtifactId because the add-on is not directly installable, you will normally
    // provide an external download URI, like this:
    versionBuilder.externalLinkUri(AddonVersionExternalLinkType.BINARY, URI.create("http://my-vendor.example.com/page-for-getting-the-addon"));
    versionBuilder.name("1.0");  // Marketplace can detect the version string automatically for installable add-ons; but
                                 // if there is no installable artifact, you'll need to specify the version string yourself.

    // You do not need to specify compatibility details if they are the same as the previous version;
    // but if they have changed, you can set them for the new version like this (the numeric values
    // are application build numbers):
    versionBuilder.compatibilities(ImmutableList.of(
        ModelBuilders.versionCompatibilityForServer(ApplicationKey.JIRA, 70107, 71003)
    ));

    // Finally, call build() to produce an AddonVersion object that has all of the specified properties.
    AddonVersion version = versionBuilder.build();
```

## Step 3: Post the new version to Marketplace

Call `createVersion` in the `Addons` API (specifying the add-on key of your existing add-on) to send the new version to the server. If successful, it returns another `AddonVersion` object containing the state of the version as it now exists on the server.

``` java
    AddonVersion createdVersion = client.addons().createVersion("my.addon.key", version);
```

If the version is not created successfully, `createVersion` will throw a `MpacException` exception instead of returning a value.

## Approval for new versions

Note that Atlassian may need to approve the new version of your add-on if you are creating a public version. For example, approval is required if the add-on was previously using a different payment model or hosting option. See [Add-on approval guidelines] for more information.

 

  [Marketplace API Java client]: /market/using-the-marketplace-api-java-client-39984232.html
  [Prerequisites]: #prerequisites
  [Step 1: Specify an installable artifact]: #step-1-specify-an-installable-artifact
  [Step 2: Build a version object]: #step-2-build-a-version-object
  [Step 3: Post the new version to Marketplace]: #step-3-post-the-new-version-to-marketplace
  [Approval for new versions]: #approval-for-new-versions
  [Add-on approval guidelines]: /market/add-on-approval-guidelines-10421849.html

