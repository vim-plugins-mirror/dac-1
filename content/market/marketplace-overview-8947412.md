---
title: Marketplace Overview 8947412
aliases:
    - /market/marketplace-overview-8947412.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8947412
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8947412
confluence_id: 8947412
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Marketplace overview

This page explains what the Atlassian Marketplace is, and how to use this documentation space. This documentation space isn't intended to provide plugin development fundamentals. Instead, this space gives you everything you need to successfully market, list, and sell your add-on in the Atlassian Marketplace. 

------------------------------------------------------------------------

## What this documentation contains

This documentation explains how the Atlassian Marketplace works. You can find topics and tutorials on branding, selling, and licensing your add-on. Here's a list of topics: 

-   [Add-on approval guidelines]
-   [Listing step-by-step]

## What's the Marketplace? Why use it?

The Marketplace is an easy way for you to distribute and sell your add-ons for Atlassian products. List your add-on as paid-via-Atlassian and get all kinds of perks:

-   **Access to our huge customer base**. 
    Customers are already using Atlassian host products. All customers using our products are routed to the Marketplace when they look for an add-on.
-   **Customers can try or buy, right from their product instance**. 
    Your add-on is accessible for installation, trial, and purchase inside our product instances via the <a href="https://marketplace.atlassian.com/plugins/com.atlassian.upm.atlassian-universal-plugin-manager-plugin" class="external-link">Universal Plugin Manager (UPM)</a>.
-   **Use our licensing support.**
    Let customers pay for your add-on via Atlassian, and you can use our Marketplace licensing support. Simpler for you, profit for all of us. We simplify licensing and store information at <a href="http://my.atlassian.com" class="external-link">my.atlassian.com</a>.
-   **We'll handle checkout. **
    Your paid-via-Atlassian add-on is purchasable through our shopping cart system. We'll help you set pricing, and you can forget about the rest.
-   **Make money. **
    Our model is similar to other app stores. You receive 75% of the revenue your add-on sales and Atlassian receives 25%. Atlassian uses the shared add-on sale revenue to run the Atlassian Marketplace and to reinvest in our development ecosystem.
-   **We tell your customers when you release new versions. **
    We tell customers when there are upgrade or update opportunities for your add-on, right inside their Atlassian host product.
-   **It's in our best interest to help you profit, and vice versa.** Access to solid add-ons helps market our products, and our flexible products help market your add-ons.

## Other payment models

In the Marketplace, we welcome free add-ons, open-source add-ons, or add-ons sold directly by vendors. This means you can offer free add-ons, or add-ons that use your own checkout and licensing system. However, using different payment models means you have access to varying perks of the Marketplace:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Free</p></td>
<td><p>Customers can install your add-on for free. You're responsible for all supporting systems.</p></td>
</tr>
<tr class="even">
<td><strong>Paid via Atlassian</strong></td>
<td><p>Customers buy your add-on in the Marketplace, and you get the perks mentioned above.</p>
<p>Your customers use the same EULA language that applies to our own customers. You license your add-on using the Atlassian Licensing APIs and you must agree to our Atlassian Marketplace Vendor Agreement.</p></td>
</tr>
<tr class="odd">
<td><p>Paid via vendor</p></td>
<td><p>Customers purchase your add-on through your private, external website. You determine what kind of licensing agreements that apply. You're responsible for all the systems for licensing, collecting, and reporting sales data.</p></td>
</tr>
</tbody>
</table>

 

## Marketplace vendor agreements and terms of use

Listing an add-on in the Marketplace means you agree to the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Marketplace Vendor Agreement</a>. This agreement is between Atlassian and you as a vendor.

Customers using paid-via-Atlassian add-ons are subject the<a href="http://www.atlassian.com/licensing/marketplace/termsofuse" class="external-link">Marketplace Terms of Use</a>. Similarly, this agreement is between Atlassian and your add-on customers. Your customers agree to the terms when they purchase or use your add-on.

## Fundamentals

[Atlassian Connect development]

[Atlassian SDK development]

[Atlassian Design Guidelines]

  [Add-on approval guidelines]: /market/add-on-approval-guidelines-10421849.html
  [Listing step-by-step]: /market/listing-step-by-step-10420373.html
  [Atlassian Connect development]: https://developer.atlassian.com/x/KoArAQ
  [Atlassian SDK development]: https://developer.atlassian.com/x/EYBW
  [Atlassian Design Guidelines]: https://developer.atlassian.com/design/latest/index.html

