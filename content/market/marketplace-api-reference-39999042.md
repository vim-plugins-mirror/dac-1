---
title: Marketplace API Reference 39999042
aliases:
    - /market/marketplace-api-reference-39999042.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39999042
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39999042
confluence_id: 39999042
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Marketplace API reference



