---
title: Marketing Requests 11927920
aliases:
    - /market/-marketing-requests-11927920.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11927920
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11927920
confluence_id: 11927920
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Marketing Requests

In general, Atlassian prioritizes marketing support in favor of Paid-via-Atlassian add-ons and focuses on general promotion of the Atlassian Marketplace, rather than individual comarketing with vendors. However, we're always on the lookout for new opportunities. Just <a href="mailto:marketplace@atlassian.com" class="external-link">drop us a line</a> and we will triage your request and get back to you.

 



