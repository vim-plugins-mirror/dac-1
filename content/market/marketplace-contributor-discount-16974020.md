---
title: Marketplace Contributor Discount 16974020
aliases:
    - /market/marketplace-contributor-discount-16974020.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16974020
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16974020
confluence_id: 16974020
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Marketplace Contributor Discount

<a href="http://www.atlassian.com/software/ondemand/overview" class="external-link">Atlassian Cloud</a> makes it easier then ever to use products like JIRA and Confluence. Cloud customers use any combination of Atlassian products, paying only for what they need. All while never having to install or maintain their application instances. 

Just like server customers, cloud customers are eager to extend their instances with add-ons. With that in mind we want to make it easier then ever to develop and list an add-on into the Atlassian Marketplace.  

## Marketplace Contributor Discount

As an add-on developer, you can use our cloud applications to track and collaborate on the development of your Atlassian add-on. This is particularly useful if any of these describe you:

-   You have a team of at least several people working on your add-on.
-   You need an instance to develop your add-on against.
-   You need the best, most flexible issue and project tracking software.
-   You already have issue data in JIRA or content in Confluence you'd like to keep.

### The deal

Atlassian is delighted to offer our developer community a special deal on our cloud products for developing add-ons!

Normally, our cloud products are priced per application and number of users. At the starting level, each application is $10 per month for 10 users.

We've learned that many popular add-on projects serve large user communities, and that while the number of active contributors to an add-on may be small, there may be dozens or hundreds of add-on users who contribute through JIRA issues or Confluence pages. We want to make sure that developers using cloud applications are able to communicate with all of their users.

Therefore, we are pleased to make the following offer:

**A cloud instance used to develop Atlassian add-ons is $10 per month, per application, for up to 500 users.**

Here are some examples of your cost:

-   JIRA Software Cloud only - **$10 per month, for up to 500 users**
-   JIRA Software and Confluence Cloud - **$20 per month, for up to 500 users**
-   JIRA Service Desk (with 25 Service Desk Agents) and Confluence Cloud - **$20 per month, for up to 500 users**

We also offer free cloud development environments with limited users for JIRA and Confluence developers. See <a href="http://go.atlassian.com/cloud-dev" class="external-link">go.atlassian.com/cloud-dev</a> for more details and to sign up.

### Applying

1.  To apply for the discount, <a href="https://my.atlassian.com/ondemand/signup" class="external-link">sign up for an instance</a> which has a 7-day trial so you won't need to add a credit card right away
2.  Once your account is active, <a href="https://ecosystem.atlassian.net/secure/CreateIssue!default.jspa?selectedProjectId=11672" class="external-link">contact us</a> with the following information:
    1.  Your cloud application user details
    2.  Your cloud instance and URL
    3.  The add-on projects you plan to track there, along with:
        1.  A brief description of the add-on
        2.  The license you intend to use (open source or commercial)
        3.  Whether you plan to list the add-on in the marketplace

3.  We'll convert your instance to a 500 user community license in a few business days.

## FAQ for Marketplace Contributor discount

#### For how long is this offer valid?

We have committed to this special offer in perpetuity. You can have $10 per month per application for up to 500 users for as long as you keep and maintain an Atlassian Add-on in the Marketplace.

#### **How many instances can I apply to convert to this offer?**

We can only offer this discount for one instance per company. 

#### Can I track other projects in my cloud instance unrelated to Atlassian add-ons?

This offer is intended to help developers who want to contribute new or improved add-ons to our ecosystem via the Marketplace. This instance is provided to support your development work related to ecosystem add-on(s) only, any other projects need to managed in a separate (list price) paid instance. As part of our standard cloud application support offering, we monitor all instances and your community license can be rescinded under *Section 11: No-Charge Products* of the <a href="https://www.atlassian.com/end-user-agreement/" class="external-link">Atlassian Customer Agreement</a>.

#### **My add-on has more than 500 users how can we provide support to all those users?**

We suggest that you use <a href="https://www.atlassian.com/software/jira/service-desk" class="external-link">JIRA Service desk</a> to have your users report bugs so that your users will not need to be licensed.  With JIRA Service Desk only JIRA users (collaborators) and Service Desk agents need to be licensed and thus you can keep your licensing costs low. 

#### I already have a cloud instance for my add-on projects. Can I convert my billing to this offer?

If your instance meets the other requirements, yes. [Drop us a line for help].

#### Who will support my cloud instance?

Your instance is supported by the same support teams responsible for regular cloud instances, and they will attend to any issues you may have as part of their regular duties. For support details, <a href="http://confluence.atlassian.com/display/AOD/Support+Policies" class="external-link">see this page</a>.

#### Can I install third-party add-ons in my cloud instance?

Sure, depending on what's available. Check out our <a href="https://confluence.atlassian.com/x/IYHLEg" class="external-link">cloud application documentation here</a>.

#### Are my add-ons required to be open source to take advantage of this offer?

While we prefer and encourage our developers to use open source licenses whenever possible, we understand that a commercial license is best in some cases. As long as you are contributing to our ecosystem with your add-on, you are welcome to this offer regardless of your intended licensing scheme.

#### Are there any options for hosting my add-on source code?

If you want source code hosting along with a simple issue tracker and wiki, we're happy to point you to <a href="http://bitbucket.org/" class="external-link">Bitbucket</a>. Sign up and have as many private repositories as you like for free!

#### Can I pay only for JIRA or Confluence Cloud and host my source code somewhere else?

Of course. We think you'll have a better experience if you host with us (<a href="http://bitbucket.org/" class="external-link">Bitbucket</a>), but you can host your code wherever you like and still take advantage of this offer.

#### I've done a lot for the community already. Can I get a better deal?

Some members of our community have made outstanding contributions for a sustained period of time. If you feel you belong in this category, [ask us about here][Drop us a line for help].

#### I have a question not answered here. What do I do?

See the <a href="http://confluence.atlassian.com/display/AOD/Atlassian+OnDemand+Documentation+Home" class="external-link">documentation</a> or [visit our help portal here][Drop us a line for help]. We'll help!

  [Drop us a line for help]: https://developer.atlassian.com/help#contact-us

