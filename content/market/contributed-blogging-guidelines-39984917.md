---
title: Contributed Blogging Guidelines 39984917
aliases:
    - /market/contributed-blogging-guidelines-39984917.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984917
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984917
confluence_id: 39984917
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Contributed blogging guidelines

In Atlassian Marketplace we're committed to providing vendors a thought leadership platform to promote your brand and amplify your solution's benefit to our joint customers. Below are our guidelines and best practices for submitting a guest blog to our corporate Atlassian Blog. Please follow the guidelines below.

## Blog guidelines

### Topics for consideration

While we cannot publish blog posts that only promote a specific add­-on or feature, encourage and welcome vendors to contribute thought leadership pieces that inform and educate our customer audience on industry challenges. We've started to move away from product announcements and are more interested in thought leadership and best practices posts (as they tend to perform better).

-   "10 tips for improving collaboration with wikis"
-   "5 ways to impress your CEO with charts and diagrams "
-   "Key insights on Agile Project Management"
-   "The biggest trends in Test management"
-   "How to address key challenges in deployment"

### For example, a diagramming vendor might submit the following:

-   "How enterprises can foster more efficient collaboration through visualization"
-   "Tips/Best practices to building the right diagram for your business challenge"
-   "How to impress your boss and improve your business with diagrams"

### Do's:

-   Blogs should be 400­ 800 words in length.
-   Discuss your solution in a broad/industry terms.
-   Include high­resolution screenshots and visuals (and captions and links).U
-   Use third party data/stats to demonstrate your point.
-   Leverage SEO (see table below).
-   Use blog content for email campaigns.

### Don'ts:

-   Blogs should NOT be an advertisement for your product (do not recreate your listing). Instead, your call to action can speak to how your solution can help solve the challenges in your post.

### Process:

1.  Submit blog idea to (allow for 1 week approval).
2.  Once approved, submit outline (allow for 2 week approval).
3.  Submit draft  (allow for 2 week approval).
4.  Receive publish date.Post across channels: social, email, newsletter, etc.
5.  Receive results 60 days after publication (traffic, evals, etc.)

 

------------------------------------------------------------------------

## SEO considerations

Below we present some SEO considerations to make before writing your copy. Please fill this out when submitting your outline.

### SEO table

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>Focus keyword</strong></p></td>
<td><p><strong><em>Choose a word or phrase to optimize for. e.g. knowledge base, git branching, etc.</em></strong></p></td>
</tr>
<tr class="even">
<td><p><strong>URL</strong></p></td>
<td><p><strong><em>The part that comes after &quot;<a href="http://blogs.atlassian.com" class="external-link">blogs.atlassian.com</a>&quot;. Be sure to include your focus keyword! e.g.</em></strong></p>
<p><strong><em>.../git­branching­webinar­recording­available</em></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>Meta description</strong></p></td>
<td><p><strong><em>This is the little summary that shows up on search result pages. Max of 156 chars. And include that focus keyword!</em></strong></p></td>
</tr>
<tr class="even">
<td><p><strong>Title</strong></p></td>
<td><p><strong><em>Once again: include your focus keyword. Try to keep the title to</em></strong></p>
<p><strong><em>50 chars or less.</em></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>Tags (BAC</strong> <strong>only)</strong></p></td>
<td><p><strong><em>These are the tags that appear at the bottom of posts on <a href="http://blogs.atlassian.com" class="external-link">blogs.atlassian.com</a>. e.g. agile, Life at Atlassian, foundation</em></strong></p></td>
</tr>
</tbody>
</table>

------------------------------------------------------------------------

## Constructing an outline for your blog post

### Title:

Your title should give a very clear idea of what the whole piece is about. Try to include your focus keyword in your title and keep it to 50 characters or less.

### Concept:

What is the big picture idea that you are trying to get across to your audience?

### Main ideas:

Your main ideas should convey the type of knowledge or learnings a reader should expect to achieve or receive by reading your blog post. They should stand out and be well developed with strong, supporting content.

### Supporting content:

These are the secondary details that support your main ideas. These details should be relevant and carefully selected. Make sure they are well suited to the purpose  of the piece and the audience you are writing for.

### Audience:

The success of your blog post is partly determined by how well your topic resonates with your target audience. Are you talking to Product Managers? JIRA admins? Engineers? Product Marketing Managers? Clearly define who your target audience is and make sure your content relevant to them.

### Keep this in mind:

-   Relevancy is focused on the objective of your blog post.
-   Relevancy is focused on solutions for problems.
-   Relevancy is focused on your niche.

 

------------------------------------------------------------------------

 

## Example

This is an example blog post outline to help you get started.

**Title:** 5 tips for building the right diagram for your business challenge

**Concept:** A quick look at the Atlassian marketplace reveals that diagramming tools are some of the most popular add­ons available. There's a reason for that: complicated tasks and concepts are often better expressed visually. At Lucidchart, diagramming is our business. Here are some of the best practices we've gathered over the years.

-   **(Main idea) Know your objective**
    -   *Supporting content*
        -   Do you want to express ideas?
        -   Do you want to organize ideas?
        -   Do you want to generate ideas?

<!-- -->

-   **(Main idea) Tailor content to your audience**
    -   *Supporting content*
        -   Target audiences determine the complexity of a diagram
        -   The size of a target audience determines the number of slides with diagrams

<!-- -->

-   **(Main idea) Keep it short and sweet**
    -   *Supporting content*
        -   Diagrams should be visually engaging
        -   Diagrams shouldn't be another way to organize text
-   **(Main idea) Don't be afraid to revise, then revise again**
    -   *Supporting content*
        -   Why you should revise your diagrams

<!-- -->

-   **(Main idea) ­ Have fun**
    -   *Supporting content*
        -   Diagrams should simplify work for you
        -   Diagrams have endless possibilities

**
SEO:**

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>Focus keyword</strong></p></td>
<td><p>Business challenges</p></td>
</tr>
<tr class="even">
<td><p><strong>URL</strong></p></td>
<td><p>/5­tips­diagrams­for­business­challenges</p></td>
</tr>
<tr class="odd">
<td><p><strong>Meta description</strong></p></td>
<td><p>Leads, or potential customers that have expressed interest in your products or services, are what drive your business forward. On Twitter, you can generate leads using images.</p></td>
</tr>
<tr class="even">
<td><p><strong>Title</strong></p></td>
<td><p>5 tips for building the right diagram for your business challenge</p></td>
</tr>
<tr class="odd">
<td><p><strong>Tags</strong></p></td>
<td><p>Diagrams, JIRA, Confluence, Atlassian Marketplace</p></td>
</tr>
</tbody>
</table>

..

  *Please contact <a href="mailto:press@atlassian.com" class="external-link">press@atlassian.com</a> with any more detailed questions. *



