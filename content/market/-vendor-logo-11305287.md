---
title: Vendor Logo 11305287
aliases:
    - /market/-vendor-logo-11305287.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11305287
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11305287
confluence_id: 11305287
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Vendor Logo

72x72px PNG/JPG/GIF



