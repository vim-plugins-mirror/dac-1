---
title: Cloud Security Program 39355789
aliases:
    - /market/cloud-security-program-39355789.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39355789
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39355789
confluence_id: 39355789
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Cloud security program

<img src="/market/images/icons/emoticons/lightbulb_on.png" alt="(lightbulb)" class="emoticon-light-on" />  If you need to report a security incident for your add-on. Please <a href="https://ecosystem.atlassian.net/secure/CreateIssue.jspa?pid=17070&amp;issuetype=11400" class="external-link">file an 'Add-on Security Incident' ticket</a>.

Security is an important concern for our customers. The Cloud Security Program is a collaboration between Atlassian and add-on vendors to increase security awareness and improve security practices. The goal is to increase customer confidence in add-ons and provide them with necessary information to perform security evaluations. The current program involves an annual security self-assessment that Atlassian reviews and approves.  The approved application expires after one year and vendors must re-apply with updated information each year.  During approval, Atlassian works with the vendor to pin-point vulnerabilities and identify improvements. If you have a publicly listed cloud add-on in the Marketplace, you can apply. Your application applies to all public cloud add-ons under your vendor name. Vendors that meet passing criteria will see all their public cloud add-on listings with 'cloud security compliant' in the Security and Privacy area, as follows:

![]

This page explains the following:

-   [Objectives ]
-   [Resources ]
-   [Requirements ]
-   [How to apply ]
-   [How to re-apply]
-   [How to update security changes]
-   [Expired applications]

## Objectives 

The program aims to encourage security mindfulness in three main areas:

 

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Area
</div></th>
<th><div class="tablesorter-header-inner">
Details
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Data security</td>
<td>The cloud vendor has a clear data security policy. Data vulnerabilities are considered and handled.</td>
</tr>
<tr class="even">
<td>Sensitive Data Handling</td>
<td>The cloud vendor is mindful of the different types of data it handles and places extra security on sensitive data.</td>
</tr>
<tr class="odd">
<td>Backups and Disaster Recovery</td>
<td>The cloud vendor backs up its data regularly and has a clear plan for data recovery in case of disaster.   </td>
</tr>
</tbody>
</table>

## Resources 

For more information on how to secure your plugin, please read <a href="https://confluence.atlassian.com/display/CAMP/Securing+your+Add-on" class="external-link">Securing your Plugin</a><a href="https://confluence.atlassian.com/display/CAMP/Securing+your+Plugin" class="external-link">.</a>

For more information on how Atlassian handles Security, please visit <a href="https://www.atlassian.com/security" class="external-link">Security @ Atlassian</a>

## Requirements 

The Security Self-Assessment contains 13 security questions with the following passing criteria. Your Self-Assessment answers are reviewed by Atlassian staff against passing criteria. Your answers are kept private to Atlassian. No additional audit or testing is performed. While passing this criteria gives customers some peace of mind about your security protocols, customers are advised to take additional necessary steps to meet company security requirements. So you may be contacted by customers directly for more information regarding security.  

<table>
<thead>
<tr class="header">
<th> </th>
<th>Question</th>
<th>Passing Criteria</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1a</td>
<td>Do you store customer data from the customer Atlassian instance? If so, please outline any protection mechanisms you will have in place to protect this customer data.</td>
<td>Ideally No. If Yes, provide details of controls in place.</td>
</tr>
<tr class="even">
<td>1b</td>
<td>If you have answered YES to Question Number 1a, what is the jurisdiction(s) of where this data is hosted?</td>
<td>N/A Reference information.</td>
</tr>
<tr class="odd">
<td>2</td>
<td>Is your application designed to store sensitive information? (For example: Credit card data, Personally Identifiable Information, Financial data, Source code, Trading algorithms or proprietary models)</td>
<td>Ideally No. If Yes, provide details of controls in place.</td>
</tr>
<tr class="even">
<td>3</td>
<td>Do you have an Information Security Policy with supporting Standards and Procedures? Please provide details (or provide a copy of the Policy).</td>
<td>Yes, and provides details.</td>
</tr>
<tr class="odd">
<td>4</td>
<td>Do you have formal change control and release management processes to manage code changes? Please provide details (or provide a copy of the documented process).</td>
<td>Ideally Yes and provides process documents. If no, describe the current process.</td>
</tr>
<tr class="even">
<td>5</td>
<td>Do you undertake audits or other reviews to ensure that security controls are being implemented and operating effectively?</td>
<td>Yes, and provides details.</td>
</tr>
<tr class="odd">
<td>6</td>
<td>Are you accredited to any relevant security standards (e.g. SSAE16 SOC1/2/3, ISO27001, PCI DSS)?</td>
<td>N/A No accreditation required to pass, but beneficial.</td>
</tr>
<tr class="even">
<td>7</td>
<td>Do you undertake penetration testing (or similar technical security testing, code review or vulnerability assessment); and are you able to provide copies of results / findings? <a href="https://www.atlassian.com/dms-static/c1cb2848-ffcd-4750-a8b3-c8d685a0c0d4/VulnerabilityDetailsSitesReport.pdf" class="external-link">Example penetration testing report</a><a href="https://www.atlassian.com/security/security-faq/pageSections/0/pageSections/0/contentFullWidth/05/content_files/file/document/AttackVectorDetailReport-March2015.pdf" class="external-link"></a></td>
<td>Ideally Yes and provides results. Or Yes and describe process.</td>
</tr>
<tr class="odd">
<td>8</td>
<td>Do you have mechanisms to notify Atlassian in case of a security breach?<br />
An <a href="https://ecosystem.atlassian.net/secure/CreateIssue.jspa?pid=17070&amp;issuetype=11400" class="external-link">'</a><a href="https://ecosystem.atlassian.net/secure/CreateIssue.jspa?pid=17070&amp;issuetype=11400" class="external-link">Add-on Security Incident' ticket</a> should be filed with us immediately upon your detection of a security incident. You must stay available to communicate with our security team during resolution and inform our team via the ticket when the incident is resolved. While you are responsible for informing your affected customers as necessary, your communication with us helps us direct customers who have reached out to Atlassian for help. It also informs us in case we need to take necessary action to prevent additional breaches.</td>
<td>Yes, and provide details of the documented plan with notification and followup procedure.</td>
</tr>
<tr class="even">
<td>9</td>
<td>Do your employees (eg developers or system administrators) have access to Atlassian customer data? How is this access controlled and monitored?</td>
<td>Ideally No. If Yes, provide details of a tightly controlled system.</td>
</tr>
<tr class="odd">
<td>10</td>
<td>Are all personnel required to sign Non Disclosure Agreement (NDA) or Confidentiality Agreements (CA) as a condition of employment to protect customer information?</td>
<td>Yes if they have access to sensitive information. Otherwise not necessary.</td>
</tr>
<tr class="even">
<td>11</td>
<td>Do you have a publicly documented process for managing security vulnerabilities in your application(s)? <a href="https://www.atlassian.com/security/secpol" class="external-link">Example security vulnerability process</a></td>
<td>Yes, and provides the URL to the documentation. Or No, and describes handling of security vulnerability identified in the code.</td>
</tr>
<tr class="odd">
<td>12</td>
<td>Do you have Business Continuity and/or Disaster Recovery Plans? If Yes, please provide details including backup and redundancy mechanisms.</td>
<td>Yes, with description.</td>
</tr>
<tr class="even">
<td>13</td>
<td>Do you have capability to recover data for a specific customer in the case of a failure or data loss? Please outline your processes and recovery capabilities for data loss including time frames. What is the maximum data loss period a customer can expect?</td>
<td>Yes, with backup every 24hrs. </td>
</tr>
</tbody>
</table>

## How to apply 

First, ensure you meet all requirements. Then:

1.  Fill out an<a href="https://ecosystem.atlassian.net/servicedesk/customer/portal/9/create/90" class="external-link"> application for the cloud security program</a>.
2.  Keep the confirmation email you receive for your form submission.  You may use that link to edit the form later.
3.  Our friendly Marketplace team will respond within 30 business days.  

## How to re-apply

If you did not meet all passing criteria initially, have since made changes to your security protocol and wish to reapply, please do the following:

1.  Locate the confirmation email you received when you submitted the form initially.
2.  Make the necessary edits.
3.  Add a comment to indicate that you wish to re-submit, and summarize the key changes that you have made.  

## How to update security changes

Should your security protocol change, please promptly update your application form as follows:

1.  Locate the confirmation email you received when you submitted the form initially.
2.  Make the necessary edits.
3.  Add a comment to summarize the key changes that you have made.

## Expired applications

Approved applications are active for one year.  You will be notified 30 days before the end of the one year period.  Please apply with a new application each year.  

 

  []: /market/attachments/39355789/41232669.png
  [Objectives ]: #objectives-
  [Resources ]: #resources-
  [Requirements ]: #requirements-
  [How to apply ]: #how-to-apply-
  [How to re-apply]: #how-to-re-apply
  [How to update security changes]: #how-to-update-security-changes
  [Expired applications]: #expired-applications

