---
title: JIRA Service Desk Modules Agent View 
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
    - /jiracloud/jira-service-desk-modules-agent-view-39988009.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988009
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988009
confluence_id: 39988009
date: "2016-07-01"
---
# Agent view module

This pages lists the JIRA Service Desk modules for the agent view. These can be used to inject new groups (tabs) in the JIRA Service Desk agent view.

**On this page:**

-   [Queues area]
    -   [Queue group]
    -   [Queue]
-   [Reports area]
    -   [Report group]
    -   [Report]

# Queues area

## Queue group


 
<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A group of <a href="#JIRAServiceDeskmodules-Agentview-Queue">serviceDeskQueues</a></td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><pre>serviceDeskQueueGroups</pre></td>
        </tr>
        <tr>
            <th>Screenshot</th>
                <td colspan="2">
                            <p>
                                <span class="confluence-embedded-file-wrapper confluence-embedded-manual-size">
                                    
                                    <img src="../images/sd-queue-content.png"/> 
                                </span> 
                            </p>
                </td>
        </tr>
        <tr>
            <th colspan="1">Sample JSON</th>
                <td colspan="2">
                    <div>
                        <div id="expander-1525095017">
                            <div id="expander-control-1525095017">
                            </div>
                            <div id="expander-content-$toggleId">
                                <div class="code panel pdl" style="border-width: 1px;">
                                    <div class="codeContent panelContent pdl">
                                        <pre class="brush: javascript; gutter: false; theme: Confluence" style="font-size:12px;">
&quot;modules&quot;: {
    &quot;serviceDeskQueueGroups&quot;: [ 
        { 
            &quot;key&quot;: &quot;my-custom-queues-section&quot;, 
            &quot;name&quot;: { 
                &quot;value&quot;: &quot;My custom queues section&quot; 
            }
        }
    ]
}
                                        </pre>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <p>
                            <em>
                                <img src="../../../../illustrations/warning.png"/> Note that the group will not be displayed if there are no 
                                    <a href="#JIRAServiceDeskmodules-Agentview-Queue">serviceDeskQueues</a> that reference it in the &quot;group&quot; property or if all the queues that reference it are not displayed because of conditions.
                            </em>
                        </p>
                    </div>
                </td>
        </tr>
        <tr>
            <th colspan="1"><span>Properties</span></th>
                <td colspan="1"><strong><code>key</code></strong></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li>
                        <li><strong>Required</strong>: yes</li>
                        <li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <th colspan="1"> </th>
                <td colspan="1"><span><strong><code>name</code></strong></span></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html" rel="nofollow">i18n Property</a></code></li>
                        <li><strong>Required</strong>: yes</li>
                        <li><strong>Description</strong>: A human readable name.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <th colspan="1"> </th>
                <td colspan="1"><strong><code>weight</code></strong></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>integer</code></li>
                        <li><strong>Default</strong>: 100</li>
                        <li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <th colspan="1"> </th>
                <td colspan="1"><strong><code>conditions</code></strong></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li>
                        <li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li>
                    </ul>
                </td>
        </tr>
    </tbody>
</table>

## Queue


<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A queue in the queues sidebar</td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><pre><span>serviceDeskQueues</span></pre></td>
        </tr>
        <tr>
            <th>Screenshot</th>
                <td colspan="2">
                    <div id="expander-231477888">
                        <div id="expander-control-231477888">
                        </div>
                        <div id="expander-content-$toggleId">
                            <p>
                                <span class="confluence-embedded-file-wrapper confluence-embedded-manual-size">
                                    
                                    <img src="../images/sd-queue-content.png"/> 
                                </span> 
                            </p>
                        </div>
                    </div>
                </td>
        </tr>
        <tr>
            <th colspan="1">Sample JSON</th>
                <td colspan="2">
                    <div>
                        <div id="expander-1996601728">
                            <div id="expander-control-1996601728">
                            </div>
                            <div id="expander-content-$toggleId">
                                <div class="code panel pdl" style="border-width: 1px;">
                                    <div class="codeContent panelContent pdl">
                                        <pre class="brush: javascript; gutter: false; theme: Confluence" style="font-size:12px;">
&quot;modules&quot;: {
    &quot;serviceDeskQueues&quot;: [ 
        { 
            &quot;key&quot;: &quot;my-custom-queue&quot;, 
            &quot;name&quot;: { 
                &quot;value&quot;: &quot;My custom queue&quot; 
            }, 
            &quot;group&quot;: &quot;my-custom-queues-section&quot;, 
            &quot;url&quot;: &quot;/sd-queue-content&quot; 
        }
    ]
}
                                        </pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
        </tr>
        <tr>
            <th rowspan="6">
                <span style="color: rgb(0,0,0);">Properties</span> 
                    <br /><br /><br /><br /><br />
            </th>
                <td colspan="1"><strong><code>key</code></strong></td><td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li><li><strong>Required</strong>: yes</li>
                        <li><strong>Description</strong><span>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</span></li>
                    </ul>
                </td>
        </tr>
        <tr>
            <td colspan="1"><span style="font-family: monospace;"><strong><code>name</code></strong></span></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html" rel="nofollow">i18n Property</a></code></li>
                    <li><strong>Required</strong>: yes</li>
                    <li><strong>Description</strong>: A human readable name.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>weight</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>integer</code></li>
                    <li><strong>Default</strong>: 100</li><li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>group</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>string</code></li>
                    <li><strong>Required</strong>: no</li>
                    <li><strong>Description</strong>: References the key of a <a href="#JIRAServiceDeskmodules-Agentview-Queuegroup">serviceDeskQueueGroup</a>. If this property is not provided, the queue will appear in a generic &quot;Add-ons&quot; group.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>url</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>string, uri-template</code></li>
                    <li><strong>Required</strong>: yes</li>
                    <li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="https://developer.atlassian.com/display/jiracloud/JIRA+Service+Desk+modules+-+Customer+portal" rel="nofollow">additional context</a> from the application by using variable tokens in the URL attribute.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>conditions</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow" > <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li>
                    <li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

# Reports area

## Report group
 
<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A group of <a href="#JIRAServiceDeskmodules-Agentview-Report">serviceDeskReports</a></td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><pre>serviceDeskReportGroups</pre></td>
        </tr>
        <tr>
            <th>Screenshot</th>
                <td colspan="2">
                    <div id="expander-697627965">
                        <div id="expander-control-697627965">
                        </div>
                        <div id="expander-content-$toggleId">
                            <p>
                                <span class="confluence-embedded-file-wrapper confluence-embedded-manual-size">
                                    <img src="../images/sd-report-content.png"/>
                                </span>
                            </p>
                        </div>
                    </div>
                </td>
        </tr>
        <tr>
            <th colspan="1">Sample JSON</th>
                <td colspan="2">
                    <div>
                        <div id="expander-932352499">
                            <div id="expander-control-932352499">
                            </div>
                            <div id="expander-content-$toggleId">
                                <div class="code panel pdl" style="border-width: 1px;">
                                    <div class="codeContent panelContent pdl">
                                        <pre class="brush: javascript; gutter: false; theme: Confluence" style="font-size:12px;">
&quot;modules&quot;: {
    &quot;serviceDeskReportGroups&quot;: [ 
        { 
            &quot;key&quot;: &quot;my-custom-reports-section&quot;, 
            &quot;name&quot;: { 
                &quot;value&quot;: &quot;My custom reports section&quot; 
            }
        }
    ]
}
                                        </pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                        <img src="../../../../illustrations/warning.png"/>
                            <em> Note that the group will not be displayed if there are no <a href="#JIRAServiceDeskmodules-Agentview-Report">serviceDeskReports</a> that reference it in the &quot;group&quot; property or if all the reports that reference it are not displayed because of conditions.</em></p>
                    </div>
                </td>
        </tr>
        <tr>
            <th colspan="1">Properties</th>
                <td colspan="1"><strong><code>key</code></strong></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li>
                        <li><strong>Required</strong>: yes</li>
                        <li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <th colspan="1"> </th>
                <td colspan="1"><strong><code>name</code></strong></td>
                <td colspan="1">
                    <ul>    
                        <li><strong>Type</strong>: <code><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html" rel="nofollow">i18n Property</a></code></li>
                        <li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A human readable name.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <th colspan="1"> </th>
                <td colspan="1"><strong><code>weight</code></strong></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>integer</code></li>
                        <li><strong>Default</strong>: 100</li>
                        <li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <th colspan="1"> </th>
                <td colspan="1"><strong><code>conditions</code></strong></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li>
                        <li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li>
                    </ul>
                </td>
        </tr>
    </tbody>
</table>

## Report


<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A report in the reports sidebar</td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><pre>serviceDeskReports</pre></td>
        </tr>
        <tr>
            <th>Screenshot</th>
                <td colspan="2">
                    <div id="expander-1536826703">
                        <div id="expander-content-$toggleId">
                            <p>
                                <span class="confluence-embedded-file-wrapper confluence-embedded-manual-size">
                                
                                    <img src="../images/sd-report-content.png"/>

                                </span>
                            </p>
                        </div>
                    </div>
                </td>
        </tr>
        <tr>
            <th colspan="1">Sample JSON</th>
                <td colspan="2">
                                <div class="code panel pdl" style="border-width: 1px;">
                                    <pre style="font-size:12px;">
&quot;modules&quot;: {
 &quot;serviceDeskReports&quot;: [ 
     { 
         &quot;key&quot;: &quot;my-custom-report&quot;, 
         &quot;name&quot;: { 
             &quot;value&quot;: &quot;My custom report&quot; 
         }, 
         &quot;group&quot;: &quot;my-custom-reports-section&quot;, 
         &quot;url&quot;: &quot;/sd-report-content&quot; 
        }
    ]
}
                                       </pre>
                                </div>
                </td>
        </tr>
        <tr>
            <th rowspan="6"><span>Properties</span><br /><br /><br /><br /><br /></th>
                <td colspan="1"><strong><code>key</code></strong></td>
                    <td colspan="1">
                        <ul>
                            <li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li>
                            <li><strong>Required</strong>: yes</li>
                            <li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li>
                        </ul>
                    </td>
        </tr>
        <tr>
            <td colspan="1"><span><strong><code>name</code></strong></span></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html" rel="nofollow">i18n Property</a></code></li>
                    <li><strong>Required</strong>: yes</li>
                    <li><strong>Description</strong>: A human readable name.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>weight</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>integer</code></li>
                    <li><strong>Default</strong>: 100</li>
                    <li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>group</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>string</code></li>
                    <li><strong>Required</strong>: no</li>
                    <li><strong>Description</strong>: References the key of a <a href="#JIRAServiceDeskmodules-Agentview-Reportgroup">serviceDeskReportGroup</a>. If this property is not provided, the report will appear in a generic &quot;Add-ons&quot; group.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>url</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>string, uri-template</code></li>
                    <li><strong>Required</strong>: yes</li><li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="https://developer.atlassian.com/display/jiracloud/JIRA+Service+Desk+modules+-+Customer+portal" rel="nofollow">additional context</a> from the application by using variable tokens in the URL attribute.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><strong><code>conditions</code></strong></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow" > <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li>
                    <li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

  [Queues area]: #queues-area
  [Queue group]: #queue-group
  [Queue]: #queue
  [Reports area]: #reports-area
  [Report group]: #report-group
  [Report]: #report
  [(warning)]: /jiracloud/images/icons/emoticons/warning.png
  [ `Single Condition` ]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html
  [ `Composite Condition` ]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html
  [Conditions]: https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html
  [additional context]: https://developer.atlassian.com/display/jiracloud/JIRA+Service+Desk+modules+-+Customer+portal

