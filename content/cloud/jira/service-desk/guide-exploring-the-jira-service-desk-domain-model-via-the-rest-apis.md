---
title: Guide Exploring the JIRA Service Desk Domain Model via the REST APIs
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning 
guides: guides
aliases:
- /jiracloud/guide-exploring-the-jira-service-desk-domain-model-via-the-rest-apis-39990278.html
- /jiracloud/guide-exploring-the-jira-service-desk-domain-model-via-the-rest-apis-39990278.md
confluence_id: 39990278
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990278
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990278
date: "2016-09-20"
---
# Exploring the JIRA Service Desk domain model via the REST APIs

This guide will help you understand the JIRA Service Desk domain model, by using the <a href="https://docs.atlassian.com/jira-servicedesk/REST/latest/" class="external-link">JIRA Service Desk REST API</a> and <a href="https://docs.atlassian.com/jira/REST/latest/" class="external-link">JIRA platform REST API</a> to examine it. JIRA Service Desk is built upon the JIRA platform, so key domain objects of the JIRA platform will also be discussed.

You should start by reading the "Overview" section. Each section after that covers different objects in the JIRA Service Desk domain model. We recommend that you read all of those sections from top to bottom, but you can read them in a different order or skip sections if you want to.

If you want to browse the REST APIs for JIRA Service Desk or JIRA platform instead, you can find them here: [JIRA APIs].

**On this page:**

-   [Before you begin]
-   [Overview]
-   [Service Desks and Projects]
-   [Requests and Issues]
-   [Request Types and Request Type Fields]
-   [Comments]
-   [Request Statuses]
-   [Request Service Level Agreements]

## Before you begin

-   You can use any REST client to work through the examples in this tutorial. The examples were written using the <a href="http://curl.haxx.se/docs/manpage.html" class="external-link">curl tool</a>. We have also piped the results through Python to make the JSON responses easier to read. 
-   The examples in this guide were last tested with **JIRA Service Desk 3.1**.

## Overview

### Design philosophy -- specific versus generic domain objects

The JIRA Service Desk domain objects are built upon peer JIRA platform counterparts. For example, service desks are based on projects. However, the JIRA Service Desk objects have a much more specific purpose, compared to the JIRA platform objects. For example, you might have a Request Type named "Laptop Purchase" in JIRA Service Desk, which has a much more specific use case than the "Task" Issue Type (in JIRA platform) that it is based on. 

The reason for this is that a key design goal for JIRA Service Desk is to have a simple and targeted experience for people asking for help from a service desk team. The JIRA platform has to cater for many different user experiences, and as a result, it has generic domain objects that are more flexible. However, these generic concepts and terms may not make sense for your service desk customers. Therefore, JIRA Service Desk maps the generic objects to specific JIRA Service Desk objects. For example, you could map the "Summary" issue field (JIRA platform) to a request type field, which you might name "What do you need?". The same field has a dual purpose -- it still functions like the "Summary" field but it is presented as a "What do you need?" field.

If you understand this design philosophy that underlies how JIRA platform and JIRA Service Desk relate to each other, the JIRA Service Desk Domain model will make more sense. 

### Summary of the domain objects

![Alt text](../images/jsd-domain-erd.png)

This section outlines the key domain objects in JIRA Service Desk, as well as the relevant objects in the JIRA platform. The table below describes the objects at a high level. We'll explore each object in more depth, using the REST APIs, in the later sections on this page.

**Service Desk** *(JIRA Service Desk)*

-   Service Desks are the top level "container" objects in JSD, and contain the configuration for a particular service desk. An example might be your IT help desk. 
-   The peer object for the Service Desk is the Project. There is a 1:1 mapping between a service desk and a project. 

**Project** *(JIRA platform)*

-   A Project is simply a collection of issues. In JIRA Service Desk, configuration that is specific to JIRA platform (e.g. workflow, permissions, etc) is defined in terms of the project, not the service desk.*
    *
-   The peer object for the Project is the Service Desk. 

**Request** *(JIRA Service Desk)*

-   A Request contains the data from when a customer asks for help from the service desk team. An example might be a customer's request for a new email account. 
-   A Request consists of a number of Request Fields. 
-   The peer object for the Request is the Issue. Requests are implemented in terms of Issues. A Request must be an Issue, although an Issue may not be a Request.

**Issue** *(*JIRA platform*)*

-   An Issue is the smallest container of data in the JIRA platform, and can represent different things (e.g. a bug, a task, a request, etc) depending on how the organisation is using JIRA.
-   An Issue consists of a number of Issue Fields, and has an "issue key" (derived from the Project key) that is unique across the JIRA instance.
-   The peer object for the Issue is the Request.

**Request Field** *(JIRA Service Desk)*

-   Request Fields are the information you want to capture in Requests. Request Fields are implemented in terms of Issue Fields.
-   You can think of Request Fields as a a logical "view" over Issue Fields, which allow the generic JIRA fields to be renamed into the language that your customers will understand. For example, you might have a Request Field named "What do you need?" which is based on the "Summary" Issue Field.
-   The peer object for the Request Field is the Issue Field.

**Issue Field** *(**JIRA platform**)*

-   Issue Fields are the "named" values contained within Issues. For example, the issue's summary, created date, status, etc.
-   The peer object for the Issue Field is the Request Field.

**Comment** *(JIRA platform & JIRA Service Desk)*

-   Comments are attached to Requests / Issues and represent the conversation involved in servicing Requests.

**Request Type** *(*JIRA Service Desk*)*

-   A Request Type defines the information that needs to be captured for particular type of requests. You can think of them as a way of categorising requests in a Service Desk. For example, you might have a "Systems Access" request type or a "Laptop Purchase" request type.
-   The peer object for the Request Type is the Issue Type, however Request Types and Issues Types are not 1:1 peers. Different Request Types can be defined for the same Issue Type. An individual Request / Issue will always have one Request Type.

**Issue Type** *(JIRA platform)*

-   An Issue Type is used in the platform to map JIRA configuration, such as fields and workflows, to Issues.
-   The peer object for the Issue Type is the Request Type. An individual Request / Issue will always have one Issue Type.

**Request Status** *(JIRA Service Desk)*

-   The Request Status represents the current state of a Request. JIRA Service Desk customers can see the state of of a Request from the Request Status.
-   A Request Status is based on an Issue Status. For example, you might have a "Completed" Request Status which is based on the "Resolved" Issue Status.
-   The peer object for the Request Status is the Issue Status.

**Issue Status** *(JIRA platform)*

-   The Issue Status represents the state of an Issue inside its defined JIRA workflow. Not all Issue Statuses are shown to JIRA Service Desk customers -- it's possible to have workflow steps that are visible only to the service desk team.
-   The peer object for the Issue Status is the Request Status.

**Request Service** **Level Agreement*** (JIRA Service Desk)*

-   A Request SLA is used to track metrics on how much time is spent servicing Requests. Timers are set when Requests enter certain conditions, and goals are defined for moving those Requests in and out of the conditions. For example, you may have a "Time to first response" SLA defined so that your team can aim to get back to a customer within 4 hours of a request being raised.
-   There is no peer object for the Request Service Level Agreement

## Service Desks and Projects

The basic container of JIRA Service Desk domain objects is the **Service Desk** and its peer object, the JIRA platform **Project**. There is a one to one relationship between a Service Desk and a Project; and a Service Desk is implemented from a JIRA platform project. The permissions for managing the users that can access a service desk are controlled via the peer project.

A Service Desk is the starting point for all things in JIRA Service Desk. It contains most of the other domain objects, such as Request Types, Request Type Fields, etc. Let's see what a Service Desk looks like, by getting a list of the Service Desks in the system via the REST API:

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/servicedeskapi/servicedesk?start=0&limit=5" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;_links&quot;: {
        &quot;base&quot;: &quot;http://localhost:2990/jira&quot;,
        &quot;context&quot;: &quot;/jira&quot;,
        &quot;next&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk?limit=5&amp;start=5&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk?start=0&amp;limit=5&quot;
    },
    &quot;isLastPage&quot;: false,
    &quot;limit&quot;: 5,
    &quot;size&quot;: 5,
    &quot;start&quot;: 0,
    &quot;values&quot;: [
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15&quot;
            },
            &quot;id&quot;: 15,
            &quot;projectId&quot;: 11041,
            &quot;projectName&quot;: &quot;All Teams Service Desk&quot;
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/19&quot;
            },
            &quot;id&quot;: 19,
            &quot;projectId&quot;: 11241,
            &quot;projectName&quot;: &quot;Foundation Leave&quot;
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/21&quot;
            },
            &quot;id&quot;: 21,
            &quot;projectId&quot;: 11243,
            &quot;projectName&quot;: &quot;Human Resources Service Desk&quot;
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/16&quot;
            },
            &quot;id&quot;: 16,
            &quot;projectId&quot;: 11042,
            &quot;projectName&quot;: &quot;IT Help&quot;
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/23&quot;
            },
            &quot;id&quot;: 23,
            &quot;projectId&quot;: 11245,
            &quot;projectName&quot;: &quot;Lawyer Up - Company Lawyer Requests&quot;
        }
    ]
}</pre>
```

Here's a few things to note about the Service Desk:

-   Each Service Desk has an `Id`, a JIRA `projectId,` and `projectName`. 
-   The `projectId` comes from the peer JIRA platform domain Project object. You can access more information about the Project via the <a href="https://docs.atlassian.com/jira/REST/latest/" class="external-link">JIRA Platform REST API</a>. In many cases, you can use either the JIRA Service Desk REST API or JIRA Platform REST API to find the same information -- when to use which API depends on your use case and what you want to know.
-   The *\_links* section at the top contains useful HTTP URIs that point other places in the system, such as the JIRA platform base URL.
-   The JIRA Service Desk REST API uses paging for lists of resources. In this case, we asked for `5` in a page starting from `0`; that is, the first five Service Desks.  The *next* link would be used to get the next set of items in the list.

You can see from the response that there is a direct relationship between the Service Desk and its peer Project. Let's explore this further by getting the Project information for one of the Service Desks, via the REST API: the "All Teams Service Desk" (`projectId` = 11041)*:*

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/api/2/project/11041" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;assigneeType&quot;: &quot;UNASSIGNED&quot;,
    &quot;avatarUrls&quot;: {
        &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?size=xsmall&amp;avatarId=10873&quot;,
        &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?size=small&amp;avatarId=10873&quot;,
        &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?size=medium&amp;avatarId=10873&quot;,
        &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?avatarId=10873&quot;
    },
    &quot;components&quot;: [
        {
            &quot;description&quot;: &quot;Issues related to the intranet. Created by JIRA Service Desk.&quot;,
            &quot;id&quot;: &quot;10414&quot;,
            &quot;isAssigneeTypeValid&quot;: false,
            &quot;name&quot;: &quot;Intranet&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/component/10414&quot;
        },
        {
            &quot;description&quot;: &quot;Issues related to JIRA. Created by JIRA Service Desk.&quot;,
            &quot;id&quot;: &quot;10413&quot;,
            &quot;isAssigneeTypeValid&quot;: false,
            &quot;name&quot;: &quot;JIRA&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/component/10413&quot;
        },
        {
            &quot;description&quot;: &quot;Issues related to the public website. Created by JIRA Service Desk.&quot;,
            &quot;id&quot;: &quot;10415&quot;,
            &quot;isAssigneeTypeValid&quot;: false,
            &quot;name&quot;: &quot;Public website&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/component/10415&quot;
        }
    ],
    &quot;description&quot;: &quot;&quot;,
    &quot;expand&quot;: &quot;description,lead,url,projectKeys&quot;,
    &quot;id&quot;: &quot;11041&quot;,
    &quot;issueTypes&quot;: [
        {
            &quot;description&quot;: &quot;For general IT problems and questions. Created by JIRA Service Desk.&quot;,
            &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/servicedesk/issue-type-icons?icon=it-help&quot;,
            &quot;id&quot;: &quot;13&quot;,
            &quot;name&quot;: &quot;IT Help&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issuetype/13&quot;,
            &quot;subtask&quot;: false
        },
        {
            &quot;description&quot;: &quot;Track items that need to be bought. Created by JIRA Service Desk.&quot;,
            &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/servicedesk/issue-type-icons?icon=purchase&quot;,
            &quot;id&quot;: &quot;14&quot;,
            &quot;name&quot;: &quot;Purchase&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issuetype/14&quot;,
            &quot;subtask&quot;: false
        },
        {
            &quot;description&quot;: &quot;Track system outages or incidents. Created by JIRA Service Desk.&quot;,
            &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/servicedesk/issue-type-icons?icon=fault&quot;,
            &quot;id&quot;: &quot;16&quot;,
            &quot;name&quot;: &quot;Fault&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issuetype/16&quot;,
            &quot;subtask&quot;: false
        },
        {
            &quot;description&quot;: &quot;For new system accounts or passwords. Created by JIRA Service Desk.&quot;,
            &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/servicedesk/issue-type-icons?icon=access&quot;,
            &quot;id&quot;: &quot;17&quot;,
            &quot;name&quot;: &quot;Access&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issuetype/17&quot;,
            &quot;subtask&quot;: false
        }
    ],
    &quot;key&quot;: &quot;SD&quot;,
    &quot;lead&quot;: {
        &quot;active&quot;: true,
        &quot;avatarUrls&quot;: {
            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
        },
        &quot;displayName&quot;: &quot;Project Admin of some projects&quot;,
        &quot;key&quot;: &quot;patricia&quot;,
        &quot;name&quot;: &quot;projectad&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=projectad&quot;
    },
    &quot;name&quot;: &quot;All Teams Service Desk&quot;,
    &quot;projectTypeKey&quot;: &quot;service_desk&quot;,
    &quot;roles&quot;: {
        &quot;Administrators&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041/role/10002&quot;,
        &quot;Developers&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041/role/10001&quot;,
        &quot;Doco Team&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041/role/10010&quot;,
        &quot;Service Desk Collaborators&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041/role/10210&quot;,
        &quot;Service Desk Customers&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041/role/10310&quot;,
        &quot;Service Desk Team&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041/role/10111&quot;,
        &quot;Users&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041/role/10000&quot;
    },
    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041&quot;,
    &quot;versions&quot;: []
}</pre>
```

As you can see, there is plenty of additional information about the Project, which you can obtain via the JIRA platform REST API. This includes domain objects like Issue Types, Components, etc. We won't discuss this in detail here, but if you want to know more, check out the REST API documentation on the <a href="https://docs.atlassian.com/jira/REST/latest/#api/2/project" class="external-link">Project resource</a>.

## Requests and Issues

When someone asks for help from a service desk, that is called a **Request**. The Request also has a peer object: the **Issue**. Requests are implemented from Issues, much like Service Desks are implemented from Projects. However, not all Issues are Requests. This is because JIRA can be used for other purposes, such as JIRA Software and JIRA Core. The basic rule is that anything created via a Service Desk Portal is considered a Request.  

Requests can be examined in terms of the person who reported them. We're going to have a look at a few examples from the point of view of a customer (named "Ronald the Customer"). We'll see how they would use the JIRA Service Desk REST API to get information about the Requests they have created. 

The first example shows how the customer would retrieve all of the requests that they have created:

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;_links&quot;: {
        &quot;base&quot;: &quot;http://localhost:2990/jira&quot;,
        &quot;context&quot;: &quot;/jira&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request&quot;
    },
    &quot;expand&quot;: &quot;customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk&quot;,
    &quot;isLastPage&quot;: true,
    &quot;limit&quot;: 50,
    &quot;size&quot;: 3,
    &quot;start&quot;: 0,
    &quot;values&quot;: [
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17229&quot;
            },
            &quot;createdDate&quot;: {
                &quot;epochMillis&quot;: 1446776670000,
                &quot;friendly&quot;: &quot;06/Nov/15 1:24 PM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T13:24:30+1100&quot;
            },
            &quot;currentStatus&quot;: {
                &quot;status&quot;: &quot;Resolved&quot;,
                &quot;statusDate&quot;: {
                    &quot;epochMillis&quot;: 1446776741021,
                    &quot;friendly&quot;: &quot;06/Nov/15 1:25 PM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T13:25:41+1100&quot;
                }
            },
            &quot;expand&quot;: &quot;customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk&quot;,
            &quot;issueId&quot;: 17229,
            &quot;issueKey&quot;: &quot;SD-12&quot;,
            &quot;issueTypeId&quot;: &quot;17&quot;,
            &quot;projectId&quot;: 11041,
            &quot;reporter&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                },
                &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                &quot;key&quot;: &quot;ronald&quot;,
                &quot;name&quot;: &quot;customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;requestFieldValues&quot;: [
                {
                    &quot;fieldId&quot;: &quot;components&quot;,
                    &quot;label&quot;: &quot;Which system?&quot;,
                    &quot;value&quot;: [
                        {
                            &quot;description&quot;: &quot;Issues related to JIRA. Created by JIRA Service Desk.&quot;,
                            &quot;id&quot;: &quot;10413&quot;,
                            &quot;name&quot;: &quot;JIRA&quot;,
                            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/component/10413&quot;
                        }
                    ]
                },
                {
                    &quot;fieldId&quot;: &quot;summary&quot;,
                    &quot;label&quot;: &quot;What type of access do you need?&quot;,
                    &quot;value&quot;: &quot;root access&quot;
                },
                {
                    &quot;fieldId&quot;: &quot;description&quot;,
                    &quot;label&quot;: &quot;Why do you need this?&quot;,
                    &quot;value&quot;: &quot;I want to be able to do stuff on JIRA.  Giving me root access seems like a good way to do that.&quot;
                }
            ],
            &quot;requestTypeId&quot;: 63,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17229&quot;,
            &quot;serviceDeskId&quot;: 15
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17228&quot;
            },
            &quot;createdDate&quot;: {
                &quot;epochMillis&quot;: 1446769068000,
                &quot;friendly&quot;: &quot;06/Nov/15 11:17 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:17:48+1100&quot;
            },
            &quot;currentStatus&quot;: {
                &quot;status&quot;: &quot;Resolved&quot;,
                &quot;statusDate&quot;: {
                    &quot;epochMillis&quot;: 1446776578551,
                    &quot;friendly&quot;: &quot;06/Nov/15 1:22 PM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T13:22:58+1100&quot;
                }
            },
            &quot;expand&quot;: &quot;customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk&quot;,
            &quot;issueId&quot;: 17228,
            &quot;issueKey&quot;: &quot;SD-11&quot;,
            &quot;issueTypeId&quot;: &quot;17&quot;,
            &quot;projectId&quot;: 11041,
            &quot;reporter&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                },
                &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                &quot;key&quot;: &quot;ronald&quot;,
                &quot;name&quot;: &quot;customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;requestFieldValues&quot;: [
                {
                    &quot;fieldId&quot;: &quot;components&quot;,
                    &quot;label&quot;: &quot;Which system?&quot;,
                    &quot;value&quot;: [
                        {
                            &quot;description&quot;: &quot;Issues related to the public website. Created by JIRA Service Desk.&quot;,
                            &quot;id&quot;: &quot;10415&quot;,
                            &quot;name&quot;: &quot;Public website&quot;,
                            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/component/10415&quot;
                        }
                    ]
                },
                {
                    &quot;fieldId&quot;: &quot;summary&quot;,
                    &quot;label&quot;: &quot;What type of access do you need?&quot;,
                    &quot;value&quot;: &quot;I need basic login access&quot;
                },
                {
                    &quot;fieldId&quot;: &quot;description&quot;,
                    &quot;label&quot;: &quot;Why do you need this?&quot;,
                    &quot;value&quot;: &quot;I have joined the Marketing Zoom team and I need access to the Content Management system to update our public messaging.&quot;
                }
            ],
            &quot;requestTypeId&quot;: 63,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17228&quot;,
            &quot;serviceDeskId&quot;: 15
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227&quot;
            },
            &quot;createdDate&quot;: {
                &quot;epochMillis&quot;: 1446768040000,
                &quot;friendly&quot;: &quot;06/Nov/15 11:00 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:00:40+1100&quot;
            },
            &quot;currentStatus&quot;: {
                &quot;status&quot;: &quot;Waiting for Support&quot;,
                &quot;statusDate&quot;: {
                    &quot;epochMillis&quot;: 1446768682499,
                    &quot;friendly&quot;: &quot;06/Nov/15 11:11 AM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T11:11:22+1100&quot;
                }
            },
            &quot;expand&quot;: &quot;customerRequest.participant,customerRequest.status,customerRequest.sla,requestType,serviceDesk&quot;,
            &quot;issueId&quot;: 17227,
            &quot;issueKey&quot;: &quot;SD-10&quot;,
            &quot;issueTypeId&quot;: &quot;13&quot;,
            &quot;projectId&quot;: 11041,
            &quot;reporter&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                },
                &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                &quot;key&quot;: &quot;ronald&quot;,
                &quot;name&quot;: &quot;customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;requestFieldValues&quot;: [
                {
                    &quot;fieldId&quot;: &quot;summary&quot;,
                    &quot;label&quot;: &quot;What do you need?&quot;,
                    &quot;value&quot;: &quot;I need help with the L8 printer&quot;
                },
                {
                    &quot;fieldId&quot;: &quot;description&quot;,
                    &quot;label&quot;: &quot;Why do you need this?&quot;,
                    &quot;value&quot;: &quot;Its not printing at all.  I have changed the cartridge and pressed reset but not deal.\n\nI am at a loss as to what to do next&quot;
                },
                {
                    &quot;fieldId&quot;: &quot;customfield_11440&quot;,
                    &quot;label&quot;: &quot;Your Office?&quot;
                }
            ],
            &quot;requestTypeId&quot;: 62,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227&quot;,
            &quot;serviceDeskId&quot;: 15
        }
    ]
}</pre>
```

Let's have a closer look at the following data items:

``` javascript
            "issueId": 17229,
            "issueKey": "SD-12",
```

The `issueId` and `issueKey` entries are the Id and key of the JIRA platform Issue used to represent this Request. You can use the JIRA platform REST API to find out even more about this Issue, as shown below:

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/api/2/issue/17227" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;expand&quot;: &quot;renderedFields,names,schema,transitions,operations,editmeta,changelog,versionedRepresentations&quot;,
    &quot;fields&quot;: {
        &quot;aggregateprogress&quot;: {
            &quot;progress&quot;: 0,
            &quot;total&quot;: 0
        },
        &quot;aggregatetimeestimate&quot;: null,
        &quot;aggregatetimeoriginalestimate&quot;: null,
        &quot;aggregatetimespent&quot;: null,
        &quot;assignee&quot;: null,
        &quot;attachment&quot;: [],
        &quot;comment&quot;: {
            &quot;comments&quot;: [
                {
                    &quot;author&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;body&quot;: &quot;Hi Customer X,\n\nWe have received your request.  We are about to head our to the company all hands and will get back to you as soon as we can\n\n\u2013 \u00aagent\u2122&quot;,
                    &quot;created&quot;: &quot;2015-11-06T11:02:04.601+1100&quot;,
                    &quot;id&quot;: &quot;37860&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227/comment/37860&quot;,
                    &quot;updateAuthor&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;updated&quot;: &quot;2015-11-06T11:02:04.601+1100&quot;
                },
                {
                    &quot;author&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                        },
                        &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                        &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                        &quot;key&quot;: &quot;ronald&quot;,
                        &quot;name&quot;: &quot;customer&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;body&quot;: &quot;Ahh good point.  I too will be there so I will maybe see you there.&quot;,
                    &quot;created&quot;: &quot;2015-11-06T11:02:41.960+1100&quot;,
                    &quot;id&quot;: &quot;37861&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227/comment/37861&quot;,
                    &quot;updateAuthor&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                        },
                        &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                        &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                        &quot;key&quot;: &quot;ronald&quot;,
                        &quot;name&quot;: &quot;customer&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;updated&quot;: &quot;2015-11-06T11:02:41.960+1100&quot;
                },
                {
                    &quot;author&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;body&quot;: &quot;Hi Customer X,\n\nOk I went up to Level 8 to check on the printer but the floor is being renovated as part of the new building works.  I am guessing your are on another level.  Our directory service is down right now so can you please tell me what floor you are and also can you tell me what operating system you are running.\n\n\u2013 \u00aagent\u2122&quot;,
                    &quot;created&quot;: &quot;2015-11-06T11:09:34.272+1100&quot;,
                    &quot;id&quot;: &quot;37862&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227/comment/37862&quot;,
                    &quot;updateAuthor&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;updated&quot;: &quot;2015-11-06T11:09:34.272+1100&quot;
                },
                {
                    &quot;author&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                        },
                        &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                        &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                        &quot;key&quot;: &quot;ronald&quot;,
                        &quot;name&quot;: &quot;customer&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;body&quot;: &quot;I am on level 7 sorry, in the east wing.  How do I find out what operating system I have?  Is that the Internet browser I am using?  Because I think thats just called Safari.&quot;,
                    &quot;created&quot;: &quot;2015-11-06T11:11:22.404+1100&quot;,
                    &quot;id&quot;: &quot;37863&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227/comment/37863&quot;,
                    &quot;updateAuthor&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                        },
                        &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                        &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                        &quot;key&quot;: &quot;ronald&quot;,
                        &quot;name&quot;: &quot;customer&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;updated&quot;: &quot;2015-11-06T11:11:22.404+1100&quot;
                },
                {
                    &quot;author&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;body&quot;: &quot;\nNote to team - we need to get the directory service back up and running.  We cant find out what floor any one is on without.\n&quot;,
                    &quot;created&quot;: &quot;2015-11-06T11:12:27.488+1100&quot;,
                    &quot;id&quot;: &quot;37864&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227/comment/37864&quot;,
                    &quot;updateAuthor&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;updated&quot;: &quot;2015-11-06T11:12:27.488+1100&quot;
                },
                {
                    &quot;author&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;body&quot;: &quot;Also whats up with all the dust coming down the stair well with the new renovations.  If it gets into the server room there will be hell to pay.&quot;,
                    &quot;created&quot;: &quot;2015-11-06T11:13:22.477+1100&quot;,
                    &quot;id&quot;: &quot;37865&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227/comment/37865&quot;,
                    &quot;updateAuthor&quot;: {
                        &quot;active&quot;: true,
                        &quot;avatarUrls&quot;: {
                            &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                            &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                            &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                            &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                        },
                        &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                        &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                        &quot;key&quot;: &quot;waldo&quot;,
                        &quot;name&quot;: &quot;agent&quot;,
                        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                        &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
                    },
                    &quot;updated&quot;: &quot;2015-11-06T11:13:22.477+1100&quot;
                }
            ],
            &quot;maxResults&quot;: 6,
            &quot;startAt&quot;: 0,
            &quot;total&quot;: 6
        },
        &quot;components&quot;: [],
        &quot;created&quot;: &quot;2015-11-06T11:00:40.459+1100&quot;,
        &quot;creator&quot;: {
            &quot;active&quot;: true,
            &quot;avatarUrls&quot;: {
                &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
            },
            &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
            &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
            &quot;key&quot;: &quot;ronald&quot;,
            &quot;name&quot;: &quot;customer&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
            &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
        },
        &quot;customfield_10001&quot;: null,
        &quot;customfield_10010&quot;: null,
        &quot;customfield_10020&quot;: null,
        &quot;customfield_10022&quot;: null,
        &quot;customfield_10040&quot;: null,
        &quot;customfield_10050&quot;: null,
        &quot;customfield_10051&quot;: null,
        &quot;customfield_10071&quot;: null,
        &quot;customfield_10080&quot;: null,
        &quot;customfield_10090&quot;: null,
        &quot;customfield_10100&quot;: null,
        &quot;customfield_10110&quot;: null,
        &quot;customfield_10120&quot;: null,
        &quot;customfield_10144&quot;: null,
        &quot;customfield_10540&quot;: null,
        &quot;customfield_10541&quot;: &quot;She sells sea shells.&quot;,
        &quot;customfield_10641&quot;: null,
        &quot;customfield_10642&quot;: null,
        &quot;customfield_10740&quot;: null,
        &quot;customfield_11040&quot;: {
            &quot;_links&quot;: {
                &quot;jiraRest&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227&quot;
            },
            &quot;currentStatus&quot;: {
                &quot;status&quot;: &quot;Waiting for Support&quot;,
                &quot;statusDate&quot;: {
                    &quot;epochMillis&quot;: 1446768682499,
                    &quot;friendly&quot;: &quot;06/Nov/15 11:11 AM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T11:11:22+1100&quot;
                }
            },
            &quot;requestType&quot;: {
                &quot;_links&quot;: {
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/62&quot;
                },
                &quot;description&quot;: &quot;Get assistance for general IT problems and questions [example]&quot;,
                &quot;id&quot;: 62,
                &quot;name&quot;: &quot;Get IT help&quot;,
                &quot;serviceDeskId&quot;: 15
            }
        },
        &quot;customfield_11043&quot;: {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/30&quot;
            },
            &quot;completedCycles&quot;: [],
            &quot;id&quot;: 30,
            &quot;name&quot;: &quot;Time to resolution&quot;,
            &quot;ongoingCycle&quot;: {
                &quot;breachTime&quot;: {
                    &quot;epochMillis&quot;: 1446789600000,
                    &quot;friendly&quot;: &quot;06/Nov/15 5:00 PM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T17:00:00+1100&quot;
                },
                &quot;breached&quot;: true,
                &quot;elapsedTime&quot;: {
                    &quot;friendly&quot;: &quot;136h 18m&quot;,
                    &quot;millis&quot;: 490681509
                },
                &quot;goalDuration&quot;: {
                    &quot;friendly&quot;: &quot;4h&quot;,
                    &quot;millis&quot;: 14400000
                },
                &quot;paused&quot;: false,
                &quot;remainingTime&quot;: {
                    &quot;friendly&quot;: &quot;-132h 18m&quot;,
                    &quot;millis&quot;: -476281509
                },
                &quot;startTime&quot;: {
                    &quot;epochMillis&quot;: 1446768040477,
                    &quot;friendly&quot;: &quot;06/Nov/15 11:00 AM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T11:00:40+1100&quot;
                },
                &quot;withinCalendarHours&quot;: true
            }
        },
        &quot;customfield_11044&quot;: {
            &quot;errorMessage&quot;: &quot;metric not found&quot;
        },
        &quot;customfield_11045&quot;: {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/43&quot;
            },
            &quot;completedCycles&quot;: [
                {
                    &quot;breached&quot;: false,
                    &quot;elapsedTime&quot;: {
                        &quot;friendly&quot;: &quot;0m&quot;,
                        &quot;millis&quot;: 0
                    },
                    &quot;goalDuration&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;remainingTime&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;startTime&quot;: {
                        &quot;epochMillis&quot;: 1446768040477,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:00 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:00:40+1100&quot;
                    },
                    &quot;stopTime&quot;: {
                        &quot;epochMillis&quot;: 1446768124651,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:02:04+1100&quot;
                    }
                },
                {
                    &quot;breached&quot;: false,
                    &quot;elapsedTime&quot;: {
                        &quot;friendly&quot;: &quot;0m&quot;,
                        &quot;millis&quot;: 0
                    },
                    &quot;goalDuration&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;remainingTime&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;startTime&quot;: {
                        &quot;epochMillis&quot;: 1446768162075,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:02:42+1100&quot;
                    },
                    &quot;stopTime&quot;: {
                        &quot;epochMillis&quot;: 1446768574276,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:09 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:09:34+1100&quot;
                    }
                }
            ],
            &quot;id&quot;: 43,
            &quot;name&quot;: &quot;Time waiting for support&quot;,
            &quot;ongoingCycle&quot;: {
                &quot;breachTime&quot;: {
                    &quot;epochMillis&quot;: 1446776100000,
                    &quot;friendly&quot;: &quot;06/Nov/15 1:15 PM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T13:15:00+1100&quot;
                },
                &quot;breached&quot;: true,
                &quot;elapsedTime&quot;: {
                    &quot;friendly&quot;: &quot;136h 18m&quot;,
                    &quot;millis&quot;: 490681521
                },
                &quot;goalDuration&quot;: {
                    &quot;friendly&quot;: &quot;15m&quot;,
                    &quot;millis&quot;: 900000
                },
                &quot;paused&quot;: false,
                &quot;remainingTime&quot;: {
                    &quot;friendly&quot;: &quot;-136h 3m&quot;,
                    &quot;millis&quot;: -489781521
                },
                &quot;startTime&quot;: {
                    &quot;epochMillis&quot;: 1446768682500,
                    &quot;friendly&quot;: &quot;06/Nov/15 11:11 AM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T11:11:22+1100&quot;
                },
                &quot;withinCalendarHours&quot;: true
            }
        },
        &quot;customfield_11046&quot;: null,
        &quot;customfield_11047&quot;: null,
        &quot;customfield_11140&quot;: [],
        &quot;customfield_11340&quot;: null,
        &quot;customfield_11341&quot;: null,
        &quot;customfield_11440&quot;: null,
        &quot;description&quot;: &quot;Its not printing at all.  I have changed the cartridge and pressed reset but not deal.\n\nI am at a loss as to what to do next&quot;,
        &quot;duedate&quot;: null,
        &quot;environment&quot;: null,
        &quot;fixVersions&quot;: [],
        &quot;issuelinks&quot;: [],
        &quot;issuetype&quot;: {
            &quot;description&quot;: &quot;For general IT problems and questions. Created by JIRA Service Desk.&quot;,
            &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/servicedesk/issue-type-icons?icon=it-help&quot;,
            &quot;id&quot;: &quot;13&quot;,
            &quot;name&quot;: &quot;IT Help&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issuetype/13&quot;,
            &quot;subtask&quot;: false
        },
        &quot;labels&quot;: [],
        &quot;lastViewed&quot;: &quot;2015-11-06T13:24:34.643+1100&quot;,
        &quot;priority&quot;: {
            &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/images/icons/priorities/major.svg&quot;,
            &quot;id&quot;: &quot;3&quot;,
            &quot;name&quot;: &quot;Major&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/priority/3&quot;
        },
        &quot;progress&quot;: {
            &quot;progress&quot;: 0,
            &quot;total&quot;: 0
        },
        &quot;project&quot;: {
            &quot;avatarUrls&quot;: {
                &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?size=xsmall&amp;avatarId=10873&quot;,
                &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?size=small&amp;avatarId=10873&quot;,
                &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?size=medium&amp;avatarId=10873&quot;,
                &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/projectavatar?avatarId=10873&quot;
            },
            &quot;id&quot;: &quot;11041&quot;,
            &quot;key&quot;: &quot;SD&quot;,
            &quot;name&quot;: &quot;All Teams Service Desk&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/project/11041&quot;
        },
        &quot;reporter&quot;: {
            &quot;active&quot;: true,
            &quot;avatarUrls&quot;: {
                &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
            },
            &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
            &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
            &quot;key&quot;: &quot;ronald&quot;,
            &quot;name&quot;: &quot;customer&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
            &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
        },
        &quot;resolution&quot;: null,
        &quot;resolutiondate&quot;: null,
        &quot;status&quot;: {
            &quot;description&quot;: &quot;This was auto-generated by JIRA Service Desk during workflow import&quot;,
            &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/images/icons/statuses/generic.png&quot;,
            &quot;id&quot;: &quot;10021&quot;,
            &quot;name&quot;: &quot;Waiting for Support&quot;,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/status/10021&quot;,
            &quot;statusCategory&quot;: {
                &quot;colorName&quot;: &quot;yellow&quot;,
                &quot;id&quot;: 4,
                &quot;key&quot;: &quot;indeterminate&quot;,
                &quot;name&quot;: &quot;In Progress&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/statuscategory/4&quot;
            }
        },
        &quot;subtasks&quot;: [],
        &quot;summary&quot;: &quot;I need help with the L8 printer&quot;,
        &quot;timeestimate&quot;: null,
        &quot;timeoriginalestimate&quot;: null,
        &quot;timespent&quot;: null,
        &quot;timetracking&quot;: {},
        &quot;updated&quot;: &quot;2015-11-06T11:13:22.477+1100&quot;,
        &quot;versions&quot;: [],
        &quot;votes&quot;: {
            &quot;hasVoted&quot;: false,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/SD-10/votes&quot;,
            &quot;votes&quot;: 0
        },
        &quot;watches&quot;: {
            &quot;isWatching&quot;: true,
            &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/SD-10/watchers&quot;,
            &quot;watchCount&quot;: 1
        },
        &quot;worklog&quot;: {
            &quot;maxResults&quot;: 20,
            &quot;startAt&quot;: 0,
            &quot;total&quot;: 0,
            &quot;worklogs&quot;: []
        },
        &quot;workratio&quot;: -1
    },
    &quot;id&quot;: &quot;17227&quot;,
    &quot;key&quot;: &quot;SD-10&quot;,
    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/issue/17227&quot;
}</pre>
```

Looking at this data, you can see the fields recorded on this issue. Contrast how the JIRA Service Desk field values differ in representation to their JIRA platform peers, as shown below:

JIRA Service Desk:

``` javascript
"requestFieldValues": [
    {
      "fieldId": "summary",
      "label": "What do you need?",
      "value": "I need help with the L8 printer"
    },
    {
      "fieldId": "description",
      "label": "Why do you need this?",
      "value": "Its not printing at all. I have 
                changed the cartridge and pressed 
                reset.\r\n\r\nI am at a loss as 
                to what to do next"
    }
```

JIRA platform:

``` javascript
"description": "Its not printing at all. I 
    have changed the cartridge and pressed 
    reset.\r\n\r\nI am at a loss as to what 
    to do next",
"summary": "I need help with the L8 printer",
```

###### **A note about licensing and the REST APIs**

You will notice that we accessed the request as the user "customer" using the JIRA Service Desk REST API, but we switched to the user "agent" when using the JIRA platform REST API. This is because of the way that the JIRA Service Desk licensing works: Customers are allowed to use the JIRA Service Desk REST API to access their requests for free, just like they are allowed to use the JIRA Service Desk Portal for free. However, in exchange for that zero dollar cost, they are not allowed to use the JIRA REST API (nor the JIRA web interface).

However, licensed JIRA Service Desk agents are allowed to use both REST APIs, which is why we switched to "agent" to get a JIRA platform view the of the Issue.

## Request Types and Request Type Fields

A Request Type defines what information you want to capture from your customers when they ask for help.  The Request Type Fields are the fields you want to capture and are specified in the language that your customers better understand.

If you want to create a Request, then enumerating the Request Types and their Request Type Fields will allow you to do so in a more generic fashion.

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;_links&quot;: {
        &quot;base&quot;: &quot;http://localhost:2990/jira&quot;,
        &quot;context&quot;: &quot;/jira&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype&quot;
    },
    &quot;isLastPage&quot;: true,
    &quot;limit&quot;: 50,
    &quot;size&quot;: 2,
    &quot;start&quot;: 0,
    &quot;values&quot;: [
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/62&quot;
            },
            &quot;description&quot;: &quot;Get assistance for general IT problems and questions [example]&quot;,
            &quot;id&quot;: 62,
            &quot;name&quot;: &quot;Get IT help&quot;,
            &quot;serviceDeskId&quot;: 15
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/63&quot;
            },
            &quot;description&quot;: &quot;Request a new account for an internal system [example]&quot;,
            &quot;id&quot;: 63,
            &quot;name&quot;: &quot;Request a new account&quot;,
            &quot;serviceDeskId&quot;: 15
        }
    ]
}</pre>
```

This shows that there are two Request Types defined for this Service Desk.  Lets examine the required fields for the first one called "Get IT help"

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/servicedesk/15/requesttype/62/field" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;canAddRequestParticipants&quot;: true,
    &quot;canRaiseOnBehalfOf&quot;: false,
    &quot;requestTypeFields&quot;: [
        {
            &quot;description&quot;: &quot;e.g. &apos;new mailing list&apos;&quot;,
            &quot;fieldId&quot;: &quot;summary&quot;,
            &quot;jiraSchema&quot;: {
                &quot;system&quot;: &quot;summary&quot;,
                &quot;type&quot;: &quot;string&quot;
            },
            &quot;name&quot;: &quot;What do you need?&quot;,
            &quot;required&quot;: true,
            &quot;validValues&quot;: []
        },
        {
            &quot;description&quot;: &quot;&quot;,
            &quot;fieldId&quot;: &quot;description&quot;,
            &quot;jiraSchema&quot;: {
                &quot;system&quot;: &quot;description&quot;,
                &quot;type&quot;: &quot;string&quot;
            },
            &quot;name&quot;: &quot;Why do you need this?&quot;,
            &quot;required&quot;: true,
            &quot;validValues&quot;: []
        },
        {
            &quot;description&quot;: &quot;The best place to find you if we need to chat in person&quot;,
            &quot;fieldId&quot;: &quot;customfield_11440&quot;,
            &quot;jiraSchema&quot;: {
                &quot;custom&quot;: &quot;com.atlassian.jira.plugin.system.customfieldtypes:select&quot;,
                &quot;customId&quot;: 11440,
                &quot;type&quot;: &quot;option&quot;
            },
            &quot;name&quot;: &quot;Your Office?&quot;,
            &quot;required&quot;: false,
            &quot;validValues&quot;: [
                {
                    &quot;children&quot;: [],
                    &quot;label&quot;: &quot;Bradfield&quot;,
                    &quot;value&quot;: &quot;10320&quot;
                },
                {
                    &quot;children&quot;: [],
                    &quot;label&quot;: &quot;North Hampton&quot;,
                    &quot;value&quot;: &quot;10321&quot;
                },
                {
                    &quot;children&quot;: [],
                    &quot;label&quot;: &quot;City&quot;,
                    &quot;value&quot;: &quot;10322&quot;
                }
            ]
        }
    ]
}</pre>
```

We have several request type fields here.  One of the more interesting ones here is "customfield\_11440". Here's some things to note regarding that field:

-   The `jiraSchema` entries tell you about the field and how it operates. In this case, we can see that this is a "select" custom field containing a fixed list of office locations.
-   The `validValues` entries tell you what is considered a valid value for this field and the name and descriptions of that field. The field itself is not a required field, which means that if you created a Request using this Request Type then you wouldn't need to provide a value. However, if you did need to, then it would have to be one of the valid values.

## Comments

You can list the comments that have been made on a request.  Comments come in two flavours: public comments and internal comments.  If a customer (free) is using the REST API, then they will only see public comments. However, if a licensed JIRA Service Desk agent is using the REST API, then they will be able to see all comments, both public and internal. Compare the following two REST API calls, which retrieve the comments for a request: 

A customer retrieves all comments for a request:

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;_links&quot;: {
        &quot;base&quot;: &quot;http://localhost:2990/jira&quot;,
        &quot;context&quot;: &quot;/jira&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment&quot;
    },
    &quot;isLastPage&quot;: true,
    &quot;limit&quot;: 50,
    &quot;size&quot;: 4,
    &quot;start&quot;: 0,
    &quot;values&quot;: [
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37860&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                },
                &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                &quot;key&quot;: &quot;waldo&quot;,
                &quot;name&quot;: &quot;agent&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;Hi Customer X,\n\nWe have received your request.  We are about to head our to the company all hands and will get back to you as soon as we can\n\n\u2013 \u00aagent\u2122&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768124601,
                &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:02:04+1100&quot;
            },
            &quot;id&quot;: 37860,
            &quot;public&quot;: true
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37861&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                },
                &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                &quot;key&quot;: &quot;ronald&quot;,
                &quot;name&quot;: &quot;customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;Ahh good point.  I too will be there so I will maybe see you there.&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768161960,
                &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:02:41+1100&quot;
            },
            &quot;id&quot;: 37861,
            &quot;public&quot;: true
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37862&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                },
                &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                &quot;key&quot;: &quot;waldo&quot;,
                &quot;name&quot;: &quot;agent&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;Hi Customer X,\n\nOk I went up to Level 8 to check on the printer but the floor is being renovated as part of the new building works.  I am guessing your are on another level.  Our directory service is down right now so can you please tell me what floor you are and also can you tell me what operating system you are running.\n\n\u2013 \u00aagent\u2122&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768574272,
                &quot;friendly&quot;: &quot;06/Nov/15 11:09 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:09:34+1100&quot;
            },
            &quot;id&quot;: 37862,
            &quot;public&quot;: true
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37863&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                },
                &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                &quot;key&quot;: &quot;ronald&quot;,
                &quot;name&quot;: &quot;customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;I am on level 7 sorry, in the east wing.  How do I find out what operating system I have?  Is that the Internet browser I am using?  Because I think thats just called Safari.&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768682404,
                &quot;friendly&quot;: &quot;06/Nov/15 11:11 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:11:22+1100&quot;
            },
            &quot;id&quot;: 37863,
            &quot;public&quot;: true
        }
    ]
}</pre>
```

An agent retrieves all comments for a request:

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;_links&quot;: {
        &quot;base&quot;: &quot;http://localhost:2990/jira&quot;,
        &quot;context&quot;: &quot;/jira&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment&quot;
    },
    &quot;isLastPage&quot;: true,
    &quot;limit&quot;: 50,
    &quot;size&quot;: 6,
    &quot;start&quot;: 0,
    &quot;values&quot;: [
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37860&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                },
                &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                &quot;key&quot;: &quot;waldo&quot;,
                &quot;name&quot;: &quot;agent&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;Hi Customer X,\n\nWe have received your request.  We are about to head our to the company all hands and will get back to you as soon as we can\n\n\u2013 \u00aagent\u2122&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768124601,
                &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:02:04+1100&quot;
            },
            &quot;id&quot;: 37860,
            &quot;public&quot;: true
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37861&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                },
                &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                &quot;key&quot;: &quot;ronald&quot;,
                &quot;name&quot;: &quot;customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;Ahh good point.  I too will be there so I will maybe see you there.&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768161960,
                &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:02:41+1100&quot;
            },
            &quot;id&quot;: 37861,
            &quot;public&quot;: true
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37862&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                },
                &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                &quot;key&quot;: &quot;waldo&quot;,
                &quot;name&quot;: &quot;agent&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;Hi Customer X,\n\nOk I went up to Level 8 to check on the printer but the floor is being renovated as part of the new building works.  I am guessing your are on another level.  Our directory service is down right now so can you please tell me what floor you are and also can you tell me what operating system you are running.\n\n\u2013 \u00aagent\u2122&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768574272,
                &quot;friendly&quot;: &quot;06/Nov/15 11:09 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:09:34+1100&quot;
            },
            &quot;id&quot;: 37862,
            &quot;public&quot;: true
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37863&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10092&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10092&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10092&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10092&quot;
                },
                &quot;displayName&quot;: &quot;Ronald The Customer&quot;,
                &quot;emailAddress&quot;: &quot;customer0@atlassian.com&quot;,
                &quot;key&quot;: &quot;ronald&quot;,
                &quot;name&quot;: &quot;customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=customer&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;I am on level 7 sorry, in the east wing.  How do I find out what operating system I have?  Is that the Internet browser I am using?  Because I think thats just called Safari.&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768682404,
                &quot;friendly&quot;: &quot;06/Nov/15 11:11 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:11:22+1100&quot;
            },
            &quot;id&quot;: 37863,
            &quot;public&quot;: true
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37864&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                },
                &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                &quot;key&quot;: &quot;waldo&quot;,
                &quot;name&quot;: &quot;agent&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;\nNote to team - we need to get the directory service back up and running.  We cant find out what floor any one is on without.\n&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768747488,
                &quot;friendly&quot;: &quot;06/Nov/15 11:12 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:12:27+1100&quot;
            },
            &quot;id&quot;: 37864,
            &quot;public&quot;: false
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/comment/37865&quot;
            },
            &quot;author&quot;: {
                &quot;active&quot;: true,
                &quot;avatarUrls&quot;: {
                    &quot;16x16&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=xsmall&amp;avatarId=10082&quot;,
                    &quot;24x24&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=small&amp;avatarId=10082&quot;,
                    &quot;32x32&quot;: &quot;http://localhost:2990/jira/secure/useravatar?size=medium&amp;avatarId=10082&quot;,
                    &quot;48x48&quot;: &quot;http://localhost:2990/jira/secure/useravatar?avatarId=10082&quot;
                },
                &quot;displayName&quot;: &quot;\u00aagent\u2122&quot;,
                &quot;emailAddress&quot;: &quot;agent@example.com&quot;,
                &quot;key&quot;: &quot;waldo&quot;,
                &quot;name&quot;: &quot;agent&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/user?username=agent&quot;,
                &quot;timeZone&quot;: &quot;Australia/Sydney&quot;
            },
            &quot;body&quot;: &quot;Also whats up with all the dust coming down the stair well with the new renovations.  If it gets into the server room there will be hell to pay.&quot;,
            &quot;created&quot;: {
                &quot;epochMillis&quot;: 1446768802477,
                &quot;friendly&quot;: &quot;06/Nov/15 11:13 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:13:22+1100&quot;
            },
            &quot;id&quot;: 37865,
            &quot;public&quot;: false
        }
    ]
}</pre>
```

## Request Statuses

A request can change status several times while you are trying to help the customer. Each time it does, the customer will be able to see what status the request is in.  Here's an example of an request that was re-opened after it was thought to have been handled:

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u customer:customer -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/status" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;_links&quot;: {
        &quot;base&quot;: &quot;http://localhost:2990/jira&quot;,
        &quot;context&quot;: &quot;/jira&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/status&quot;
    },
    &quot;isLastPage&quot;: true,
    &quot;limit&quot;: 50,
    &quot;size&quot;: 4,
    &quot;start&quot;: 0,
    &quot;values&quot;: [
        {
            &quot;status&quot;: &quot;Waiting for Support&quot;,
            &quot;statusDate&quot;: {
                &quot;epochMillis&quot;: 1446768682499,
                &quot;friendly&quot;: &quot;06/Nov/15 11:11 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:11:22+1100&quot;
            }
        },
        {
            &quot;status&quot;: &quot;Waiting for Customer&quot;,
            &quot;statusDate&quot;: {
                &quot;epochMillis&quot;: 1446768574275,
                &quot;friendly&quot;: &quot;06/Nov/15 11:09 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:09:34+1100&quot;
            }
        },
        {
            &quot;status&quot;: &quot;Waiting for Support&quot;,
            &quot;statusDate&quot;: {
                &quot;epochMillis&quot;: 1446768162074,
                &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:02:42+1100&quot;
            }
        },
        {
            &quot;status&quot;: &quot;Waiting for Customer&quot;,
            &quot;statusDate&quot;: {
                &quot;epochMillis&quot;: 1446768124625,
                &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                &quot;iso8601&quot;: &quot;2015-11-06T11:02:04+1100&quot;
            }
        }
    ]
}</pre>
```

Note that these are the public statuses that customer can see. It's possible to have internal statuses that are there to help the service team manage their work (for example "Under Review") but don't need to be shared with the customers.

To see the state transitions that are possible for this issue you can use the JIRA platform REST API, as shown below:

###### Request

``` javascript
curl -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/api/2/issue/17227/transitions" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;expand&quot;: &quot;transitions&quot;,
    &quot;transitions&quot;: [
        {
            &quot;id&quot;: &quot;801&quot;,
            &quot;name&quot;: &quot;Resolve this issue&quot;,
            &quot;to&quot;: {
                &quot;description&quot;: &quot;A resolution has been taken, and it is awaiting verification by reporter. From here issues are either reopened, or are closed.&quot;,
                &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/images/icons/statuses/resolved.png&quot;,
                &quot;id&quot;: &quot;5&quot;,
                &quot;name&quot;: &quot;Resolved&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/status/5&quot;,
                &quot;statusCategory&quot;: {
                    &quot;colorName&quot;: &quot;green&quot;,
                    &quot;id&quot;: 3,
                    &quot;key&quot;: &quot;done&quot;,
                    &quot;name&quot;: &quot;Done&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/statuscategory/3&quot;
                }
            }
        },
        {
            &quot;id&quot;: &quot;851&quot;,
            &quot;name&quot;: &quot;Respond to customer&quot;,
            &quot;to&quot;: {
                &quot;description&quot;: &quot;This was auto-generated by JIRA Service Desk during workflow import&quot;,
                &quot;iconUrl&quot;: &quot;http://localhost:2990/jira/images/icons/statuses/generic.png&quot;,
                &quot;id&quot;: &quot;10022&quot;,
                &quot;name&quot;: &quot;Waiting for Customer&quot;,
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/status/10022&quot;,
                &quot;statusCategory&quot;: {
                    &quot;colorName&quot;: &quot;yellow&quot;,
                    &quot;id&quot;: 4,
                    &quot;key&quot;: &quot;indeterminate&quot;,
                    &quot;name&quot;: &quot;In Progress&quot;,
                    &quot;self&quot;: &quot;http://localhost:2990/jira/rest/api/2/statuscategory/4&quot;
                }
            }
        }
    ]
}</pre>
```

 

 

## Request Service Level Agreements

Request Service Level Agreements (SLAs) are used to track how much time is spent servicing customer requests. Timers are set when Requests enter certain conditions. Goal times can be defined as an ideal time to do work before it is considered a breach of service. For example, you may aim to get back to a customer within four hours, and hence may have a "Time to first response" SLA defined.

SLA information is not visible to customers. It is used to help the service team know what is the most important task they should look at next.

SLA metrics can start and stop during the life time of an request.  For example, you may have a "Time waiting for support" SLA defined, where the timers are only running while it is in the service desk teams hands. So, an SLA can have many repeated "cycles" and a possible "ongoing cycle". Each cycle may have been completed within target time or it may have "breached".

Enough theory, let's see what it looks like! The following REST API call returns the SLA information for a particular Request:

###### Request

``` javascript
url -H "X-ExperimentalApi: true" -u agent:agent -X GET  "http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla" | python -mjson.tool
```

###### Response

``` javascript
<pre>{
    &quot;_links&quot;: {
        &quot;base&quot;: &quot;http://localhost:2990/jira&quot;,
        &quot;context&quot;: &quot;/jira&quot;,
        &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla&quot;
    },
    &quot;isLastPage&quot;: true,
    &quot;limit&quot;: 50,
    &quot;size&quot;: 2,
    &quot;start&quot;: 0,
    &quot;values&quot;: [
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/30&quot;
            },
            &quot;completedCycles&quot;: [],
            &quot;id&quot;: 30,
            &quot;name&quot;: &quot;Time to resolution&quot;,
            &quot;ongoingCycle&quot;: {
                &quot;breachTime&quot;: {
                    &quot;epochMillis&quot;: 1446789600000,
                    &quot;friendly&quot;: &quot;06/Nov/15 5:00 PM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T17:00:00+1100&quot;
                },
                &quot;breached&quot;: true,
                &quot;elapsedTime&quot;: {
                    &quot;friendly&quot;: &quot;136h 18m&quot;,
                    &quot;millis&quot;: 490682163
                },
                &quot;goalDuration&quot;: {
                    &quot;friendly&quot;: &quot;4h&quot;,
                    &quot;millis&quot;: 14400000
                },
                &quot;paused&quot;: false,
                &quot;remainingTime&quot;: {
                    &quot;friendly&quot;: &quot;-132h 18m&quot;,
                    &quot;millis&quot;: -476282163
                },
                &quot;startTime&quot;: {
                    &quot;epochMillis&quot;: 1446768040477,
                    &quot;friendly&quot;: &quot;06/Nov/15 11:00 AM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T11:00:40+1100&quot;
                },
                &quot;withinCalendarHours&quot;: true
            }
        },
        {
            &quot;_links&quot;: {
                &quot;self&quot;: &quot;http://localhost:2990/jira/rest/servicedeskapi/request/17227/sla/43&quot;
            },
            &quot;completedCycles&quot;: [
                {
                    &quot;breached&quot;: false,
                    &quot;elapsedTime&quot;: {
                        &quot;friendly&quot;: &quot;0m&quot;,
                        &quot;millis&quot;: 0
                    },
                    &quot;goalDuration&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;remainingTime&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;startTime&quot;: {
                        &quot;epochMillis&quot;: 1446768040477,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:00 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:00:40+1100&quot;
                    },
                    &quot;stopTime&quot;: {
                        &quot;epochMillis&quot;: 1446768124651,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:02:04+1100&quot;
                    }
                },
                {
                    &quot;breached&quot;: false,
                    &quot;elapsedTime&quot;: {
                        &quot;friendly&quot;: &quot;0m&quot;,
                        &quot;millis&quot;: 0
                    },
                    &quot;goalDuration&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;remainingTime&quot;: {
                        &quot;friendly&quot;: &quot;15m&quot;,
                        &quot;millis&quot;: 900000
                    },
                    &quot;startTime&quot;: {
                        &quot;epochMillis&quot;: 1446768162075,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:02 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:02:42+1100&quot;
                    },
                    &quot;stopTime&quot;: {
                        &quot;epochMillis&quot;: 1446768574276,
                        &quot;friendly&quot;: &quot;06/Nov/15 11:09 AM&quot;,
                        &quot;iso8601&quot;: &quot;2015-11-06T11:09:34+1100&quot;
                    }
                }
            ],
            &quot;id&quot;: 43,
            &quot;name&quot;: &quot;Time waiting for support&quot;,
            &quot;ongoingCycle&quot;: {
                &quot;breachTime&quot;: {
                    &quot;epochMillis&quot;: 1446776100000,
                    &quot;friendly&quot;: &quot;06/Nov/15 1:15 PM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T13:15:00+1100&quot;
                },
                &quot;breached&quot;: true,
                &quot;elapsedTime&quot;: {
                    &quot;friendly&quot;: &quot;136h 18m&quot;,
                    &quot;millis&quot;: 490682163
                },
                &quot;goalDuration&quot;: {
                    &quot;friendly&quot;: &quot;15m&quot;,
                    &quot;millis&quot;: 900000
                },
                &quot;paused&quot;: false,
                &quot;remainingTime&quot;: {
                    &quot;friendly&quot;: &quot;-136h 3m&quot;,
                    &quot;millis&quot;: -489782163
                },
                &quot;startTime&quot;: {
                    &quot;epochMillis&quot;: 1446768682500,
                    &quot;friendly&quot;: &quot;06/Nov/15 11:11 AM&quot;,
                    &quot;iso8601&quot;: &quot;2015-11-06T11:11:22+1100&quot;
                },
                &quot;withinCalendarHours&quot;: true
            }
        }
    ]
}</pre>
```

![Alt img](../../../../illustrations/check.png) **Congratulations!** You've completed this overview of the JIRA Service Desk domain model. Have a chocolate! You should now have a good grasp of how the objects work. Next, check out the JIRA Service Desk REST API documentation and try using it yourself.

  [JIRA APIs]: https://developer.atlassian.com/display/JIRADEV/JIRA+APIs
  [Before you begin]: #before-you-begin
  [Overview]: #overview
  [Service Desks and Projects]: #service-desks-and-projects
  [Requests and Issues]: #requests-and-issues
  [Request Types and Request Type Fields]: #request-types-and-request-type-fields
  [Comments]: #comments
  [Request Statuses]: #request-statuses
  [Request Service Level Agreements]: #request-service-level-agreements
