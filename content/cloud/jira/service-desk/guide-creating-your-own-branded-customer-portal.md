---
title: Guide Creating Your Own Branded Customer Portal 
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning
guides: guides
aliases:
- /jiracloud/guide-creating-your-own-branded-customer-portal-41224344.html
- /jiracloud/guide-creating-your-own-branded-customer-portal-41224344.md
confluence_id: 41224344
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41224344
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41224344
date: "2016-09-16"
---
# Creating your own branded customer portal

This guide will show you how to create your own branded customer portal. We'll take you on a guided tour of an example Node.js <a href="https://expressjs.com/" class="external-link">Express</a> application, **jira-servicedesk-branded-customer-portal**, that implements this functionality and explain the key concepts. You won't be building an application yourself in this guide, but you can browse, download, and even run the source code for the example application:

#### Show me the code!

The code for the final output of this tutorial is here: <a href="https://bitbucket.org/atlassianlabs/jira-servicedesk-branded-customer-portal" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/jira-servicedesk-branded-customer-portal</a>. Feel free to check it out first, if you prefer to figure things out from the source. Otherwise, the guide below will explore the key parts of the application and explain what's going on.

**On this page:**

-   [Client-side]
    -   [The contact form]
    -   [Authentication]
    -   [Configuration]
-   [Server-side]
    -   [Serving the contact form]
    -   [Creating a customer account and raising a request on their behalf]
    -   [Next steps]

# Client-side

## The contact form

For our simple contact form, we will be using the default templating engine that ships with Express: <a href="http://jade-lang.com/" class="external-link">Jade</a>. The form will also make use of <a href="http://getbootstrap.com/" class="external-link">Bootstrap</a> and <a href="https://jquery.com/" class="external-link">jQuery</a>. The code for the form is shown below:

``` js
doctype html
html(lang='en')
  head
    meta(charset='UTF-8')
    title Contact Us - MyCompany
    link(rel='stylesheet', href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', media='all')
    script(src='//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js')
    script(src='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')
  body.container
    .jumbotron
      h1 Welcome to MyCompany!
    .content.col-xs-6
      h4
        | Please use the form below to contact us.
      br
      form
        each field in fields
          .form-group
            label(for=field.fieldId)
              strong #{field.name}
            input(id=field.fieldId).form-control(type='text', name=field.fieldId)
        .form-group
          label(for='email')
            strong What is your email address?
          input#email.form-control(type='text', name='email')
        button#contact.btn.btn-primary
          | Send
    script(src='/javascripts/contact.js')
```

A few things to note in the above code:

-   The form is dynamically generated, based on fields that are configured against a certain Request Type in our JIRA Service Desk project. We'll look at this in more detail later.
-   The form includes a page, **contact.js**. This page is responsible for making the call to our server to create a customer request. 

The code for the **contact.js** page is shown below. The script builds a **formFields** object that contains all the form input fields, then makes a POST request to our server.

``` js
$(function () {
    $("#contact").click(function () {
        var formFields = {};
        $("form input").each(function() {
            formFields[this.name] = $(this).val();
        });

        $("form :input").prop("disabled", true);
        $("button").text("Please wait...");

        $.ajax("/contact", {
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(formFields)
        }).done(function (issueKey) {
            $(".content").html("<p>Thank you. We will get back to you ASAP. Your issue key is " + issueKey + "</p>")
        }).fail(function (jqXhr) {
            $(".content").html("Failed creating request: " + jqXhr.responseText + " (" + jqXhr.status + " - " + jqXhr.statusText + ")");
        });

        return false;
    });
});
```

 

## Authentication

Each <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud" class="external-link">REST API</a> call to JIRA Service Desk needs to be authenticated. We'll be using [HTTP Basic Authentication] with the credentials specified in a config file. You probably wouldn't do this in a production instance, but this will make things simpler in this guide.

If you want to learn more about authentication, see: [Authentication and authorization].

## Configuration

The configuration needed for the application to run is stored in a config file: **config.json**. The repository contains a sample config file, **config.json.sample**:

``` js
{
  "instance": {
    "url": "http://jira.example.com",
    "username": "your-username",
    "password": "your-password"
  },
  "serviceDesk": {
    "id": "x",
    "requestTypeId": "y"
  }
}
```

A few things to note about this file:

-   All HTTP requests will be authenticated using the `username` and `password` in this file. The user specified in this file will be used to raise requests on a customer's behalf, so the user will need to be an agent (or project administrator) of the service desk project chosen in the `serviceDesk` section.
-   A service desk ID (`id`) and request type ID (`requestTypeID`) must also be specified to create requests in a JIRA Service Desk project.

To get the service desk ID and request type ID to populate the `id` and `requestTypeID` fields, you can do either of the following:

-   Use the <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/#servicedeskapi/servicedesk-getServiceDesks" class="external-link">Get service desks</a> endpoint to get the id of the desired service desk and the [Get request types] endpoint to get the id of the desired request type.

-   In JIRA Service Desk, do the following:

    -   Navigate to desired customer portal and extract the service desk ID from the URL (e.g. the service desk id is 9 in this URL: `http://your-jira.example.com/servicedesk/customer/portal/9`),

    -   In the service desk, select the desired request type and extract the request type ID from the URL (e.g. the request type ID is 36 in this URL: `http://your-jira.example.com/servicedesk/customer/portal/9/create/36`). 

Note, if you are trying to run the example add-on, you'll need to edit **config.json.sample**, substitute the placeholder values in this file with your own values, then rename the file to **config.json**.

# Server-side

## Serving the contact form

Since we need to know what fields are configured against the request type specified in our config file, we need to make a call to the <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/#servicedeskapi%2Fservicedesk%2F%7BserviceDeskId%7D%2Frequesttype%2F%7BrequestTypeId%7D%2Ffield-getRequestTypeFields" class="external-link">Get request type fields</a> endpoint to retrieve the fields before we serve the contact form. For simplicity, we only support string fields here.

Our example application uses the <a href="https://www.npmjs.com/package/request" class="external-link">request library</a> to call the JIRA Service Desk REST API:

``` js
const express = require("express");
const request = require("request");
const config = require("../config.json");

const router = express.Router();

const jsdRequest = request.defaults({
  baseUrl: config.instance.url + "/rest/servicedeskapi",
  auth: {
    user: config.instance.username,
    pass: config.instance.password
  },
  json: true
});

router.get("/", (req, res) => {
  jsdRequest.get(`/servicedesk/${config.serviceDesk.id}/requesttype/${config.serviceDesk.requestTypeId}/field`,
  (err, httpResponse, body) => {
    if (httpResponse.statusCode === 200) {
      const requestTypeFields = body.requestTypeFields;
      const fields = extractFields(requestTypeFields);
      res.render("contact", {fields: fields});
    } else {
      writeError(res, httpResponse, body);
    }
  });

  function extractFields(requestTypeFields) {
    const fields = [];
    requestTypeFields.forEach(field => {
      if (field.jiraSchema.type === "string") {
        fields.push(field);
      } else {
        // TODO: handle cases where field is not a string
      }
    });
    return fields;
  }
});
```

## Creating a customer account and raising a request on their behalf

Before creating a request on behalf of the customer in our JIRA Service Desk project, we have to create a new customer account. We use the <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/#servicedeskapi/customer-createCustomer" class="external-link">Create customer</a> endpoint to do that. We also handle the case where the username already exists.

``` js
router.post("/contact", (req, res) => {
  const formFields = req.body;

  jsdRequest.post({
    url: "/customer",
    body: {
      email: formFields.email,
      fullName: formFields.email.replace(/@.*/, "")
    },
    headers: {
      "X-ExperimentalApi": true  // At the moment, the Create customer endpoint is experimental
    }
  }, (err, httpResponse, body) => {
    let username;
    if (httpResponse.statusCode === 201) {
      username = body.name;
    } else if (httpResponse.statusCode === 400 && body.errorMessage.indexOf("username already exists") > -1) {
      username = formFields.email;
    } else {
      writeError(res, httpResponse, body);
      return;
    }

    createRequest(res, username, formFields);
  });
  ....
```

Now that the customer account is created, we can raise a request on their behalf. The <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/#servicedeskapi/request-createCustomerRequest" class="external-link">Create customer request</a> endpoint is used to do this, as shown below. By default, JIRA Service Desk will notify the customer that they have raised a request via an email.

``` js
  ....
  function createRequest(res, username, fields) {
    delete fields.email; // email is not a JIRA field, delete it from fields

    jsdRequest.post({
      url: "/request",
      body: {
        serviceDeskId: config.serviceDesk.id,
        requestTypeId: config.serviceDesk.requestTypeId,
        raiseOnBehalfOf: username,
        requestFieldValues: fields
      }
    }, (err, httpResponse, body) => {
      if (httpResponse.statusCode < 200 || httpResponse.statusCode >= 300) {
        writeError(res, httpResponse, body);
        return;
      }
      res.statusCode = 201;
      res.end(body.issueKey);
    });
  }
});
```

 

![Alt img](../../../../illustrations/check.png) Congratulations! You now know how to build your own branded customer portal with JIRA Service Desk. Have a chocolate!

## Next steps

Now that you've finished this tutorial, check out these other resources:

-   [JIRA Service Desk tutorials] -- Try your hand at another tutorial.

 

  [Client-side]: #client-side
  [The contact form]: #the-contact-form
  [Authentication]: #authentication
  [Configuration]: #configuration
  [Server-side]: #server-side
  [Serving the contact form]: #serving-the-contact-form
  [Creating a customer account and raising a request on their behalf]: #creating-a-customer-account-and-raising-a-request-on-their-behalf
  [Next steps]: #next-steps
  [HTTP Basic Authentication]: /cloud/jira/platform/jira-rest-api-basic-authentication
  [Authentication and authorization]: /cloud/jira/platform/authentication-and-authorization
  [Get request types]: 
  [JIRA Service Desk tutorials]: /jiracloud/jira-service-desk-tutorials-39988298.html
