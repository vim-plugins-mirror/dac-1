---
title: Guide Implementing Automation Actions 
platform: cloud
product: jsdcloud
category: reference
subcategory: learning
guides: guides
aliases:
- /jiracloud/guide-implementing-automation-actions-41223090.html
- /jiracloud/guide-implementing-automation-actions-41223090.md
confluence_id: 41223090
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41223090
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41223090
date: "2016-08-12"
---
# Implementing automation actions

This feature is in beta

Automation actions is currently in beta and **disabled by default**. If you want it enabled, add a comment with the URL of your Cloud instance to this ticket: <a href="https://ecosystem.atlassian.net/browse/JSDECO-2" class="external-link">JSDECO-2</a>*.*

We'd also love to hear your feedback on this feature. Share your thoughts with us by adding comments to: <a href="https://ecosystem.atlassian.net/browse/JSDECO-2" class="external-link">JSDECO-2</a>.

This guide will show you how to use automation actions, as part of the JIRA Service Desk automation framework, to allow your add-on to take actions in remote systems. You should also read the reference documentation for automation actions: [JIRA Service Desk Automation Action module].
The JIRA Service Desk automation framework allows admins to craft their own rules to eliminate repetitive tasks. For example,

-   `WHEN an issue is created IF it contains the word "account" THEN assign it to the "Account Help" request type`
-   `WHEN an issue is about to breach a service level agreement IF it is reported by a member of the management group THEN alert the service desk team lead`
-   `WHEN a comment is added IF the issue is closed and the customer made the comment THEN re-open the ticket`

The automation framework is powerful, however the rules are limited to acting on entities in JIRA Service Desk.

**Automation actions** open up the automation framework, by allowing Cloud add-ons to react to changes in JIRA Service Desk tickets by taking actions in remote systems. 

#### Show me the code!

We've created a sample add-on here: <a href="https://bitbucket.org/atlassianlabs/jira-servicedesk-hipchat-action" class="external-link">JSD HipChat Automation Action repository in Bitbucket</a> *(open source)*. This add-on provides an action to send notification message to a HipChat room. Feel free to check it out first, if you prefer to figure things out from the source. Otherwise, the guide below will explain the key concepts involved in building similar add-ons.

 

**On this page:**

-   [What is an automation action?]
-   [Defining the automation action configuration in an add-on]
-   [Validating input and passing the action configuration back to JIRA Service Desk]
-   [Examining the webhook payload]

## What is an automation action?

An automation action allows Connect add-ons to define their own **THEN action** in JIRA Service Desk's automation feature, for example "Send SMS" (see screenshot below). When this action is triggered, a webhook request is fired from JIRA Service Desk to your add-on (see [JIRA Service Desk webhooks]), including the configuration data supplied by the administrators. 

![Alt text](../images/jdev-remote-then-dropdown.png)

Add-ons need to provide a configuration UI that allows JIRA Service Desk administrators to customise an action. The configuration UI is provided by supplying a URL, which will be rendered in an iframe (see screenshot below). When this dialog is saved, the add-on must return a JSON object that is saved with the automation rule. This is described in more detail in the following sections.

![Alt text](../images/jdev-automation-action-config.png)

## Defining the automation action configuration in an add-on

Add-ons define the automation action configuration in the Connect add-on descriptor: **[atlassian-connect.json]**. An example is shown below:

``` js
"modules": {
    ...,
    "automationActions": [
      {
        "key": "send-sms-automation-action",
        "name": { "value": "Send an SMS" },
        "webhook": {
            "url": "/send-sms" /* this is the webhook URL used by JSD automation when SMS rule triggers */
        },
        "configForm": { 
            "url": "/config-sms.html" /* this is the URL to add-on supplied configuration UI -- rendered in an iframe in automation settings */
        }
      }
    ],
    ...
  }
```

 

Context parameters -- When the `configForm.url` is retrieved to draw the configuration UI, context parameters will automatically be passed in. For example, you could request `/config-sms?projectKey=ITOPS&ruleId=abc123 `(note, the `ruleId` parameter would only be provided if a previously saved rule is being edited).

For more information on defining the `automationActions` module, see [JIRA Service Desk Automation Action module].

## Validating input and passing the action configuration back to JIRA Service Desk

When the user saves the **THEN action** configuration form, the add-on needs to validate and return a JSON object representing the configuration data back to JIRA Service Desk. This JSON object can only contain a simple (non-nested) object, i.e. a map of strings. Note, the JS object will be serialized using `JSON.serialize`, so you may want to refer to this method: `JSON.stringify()`.

**Javascript loaded by the configuration form (eg: config-sms.html)**

``` js
$(function () {
    AP.require(["events"], function(events) {
        var automation = new AP.jiraServiceDesk.Automation();

        // Retrieve configuration values previously stored by JSD to render when the form loads
        automation.getConfig(function(config) {
            if (config) {
                $("#number").val(config.number);
                $("#body").val(config.body);
            }
        });
        
        // Invoked by JSD after input is validated with successful outcome
        events.on("jira-servicedesk-automation-action-serialize", function() {
            automation.serialize({
                number: $("#number").val(),
                body: $("#body").val()
            });
        });

        // Invoked by JSD when "Confirm" button is hit
        events.on("jira-servicedesk-automation-action-validate", function() {
            var valid = true;

            if (!$("#number").val()) {
                $("#number").parent().append("<p>number can't be empty</p>");
                valid = false;
            }

            if (!$("#body").val()) {
                $("#body").parent().append("<p>body can't be empty</p>");
                valid = false;
            }

            automation.validate(valid);
        });
    });
});
```

## Examining the webhook payload

When the configuration UI is saved, the JSON object returned by the add-on above will be appended to the "config" field of a standard JIRA Webhook payload shape. See the example below:

``` js
{ 
    "timestamp": 1461049397396,
    "issue": {
        "key" : "FB-123",
        "fields" : {
            "summary" : "Your flight is delayed",
            "description" : "Please contact our customer support",
            ...
        } ,
        ...
        // More details in docs link below
    },
    "action": { 
        "configuration": {
            "number" : "+61412012345",
            "body" : "A SEV1 incident just came in: https://mycompany.atlassian.net/browse/IT-412"
        }
    }
}
```

See [JIRA platform webhook documentation] for more info on the issue JSON shapes.

  [JIRA Service Desk Automation Action module]: /cloud/jira/service-desk/jira-service-desk-automation-action-module
  [What is an automation action?]: #what-is-an-automation-action
  [Defining the automation action configuration in an add-on]: #defining-the-automation-action-configuration-in-an-add-on
  [Validating input and passing the action configuration back to JIRA Service Desk]: #validating-input-and-passing-the-action-configuration-back-to-jira-service-desk
  [Examining the webhook payload]: #examining-the-webhook-payload
  [JIRA Service Desk webhooks]: /cloud/jira/service-desk/jira-service-desk-webhooks
  [atlassian-connect.json]: https://developer.atlassian.com/static/connect/docs/latest/modules/
  [JIRA platform webhook documentation]: /cloud/jira/platform/webhooks
