---
title: JIRA Service Desk Modules Customer Portal 
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
    - /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988271
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988271
confluence_id: 39988271
date: "2016-09-16"
---
# Customer portal modules

This pages lists the JIRA Service Desk modules for the customer portal. These can be used to inject panels and actions to various locations in JIRA Service Desk customer portal.

A panel is simply a section of HTML content on the page. An action is a clickable link or menu entry for user to perform specific operation. The target location of this link or menu entry is defined in the **atlassian-connect.json** descriptor file. The target location can be a URL hosted in the host application (JIRA Service Desk) or your Atlassian Connect add-on.

**On this page:**

-   [Header panel]
-   [Sub header panel]
-   [Footer panel]
-   [Request view panel]
-   [Profile page panel]
-   [Request view action]
-   [Profile page action]
-   [User menu action]

# Panels

## Header panel

<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A panel rendered at the top of customer portal pages.</td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><code>serviceDeskPortalHeader</code></td>
        </tr>
        <tr>
            <th colspan="1">Screenshot</th>
                <td colspan="2">
                    <div id="expander-1577529138">
                        <p>
                            <span><img src="../images/jdev-service-desk-portal-header.png"/> </span>
                        </p>
                    </div>
                </td>
        </tr>
        <tr>
            <th>Sample JSON</th>
                <td colspan="2">
                    <div style="border-width: 1px;">
                        <div class="codeContent panelContent pdl">
                            <pre style="font-size:12px;">
...
 &quot;modules&quot;: {
     &quot;serviceDeskPortalHeaders&quot;: [
         {
             &quot;key&quot;: &quot;sd-portal-header&quot;,
             &quot;url&quot;: &quot;/sd-portal-header&quot;
         }
     ]
 }
...
                                </pre>
                        </div>
                    </div>
                </td>
        </tr>
        <tr>
            <th rowspan="5">Properties</th>
                <td colspan="1">
                    <p><strong><code>key</code></strong></p>
                </td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li>
                        <li><strong style="line-height: 1.42857;">Required</strong>: yes</li>
                        <li><strong style="line-height: 1.42857;">Description</strong> <span style="line-height: 1.42857;">: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</span></li>
                    </ul>
                </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>url</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>string, uri-template</code></li>
                    <li><strong style="line-height: 1.42857;">Required</strong>: yes</li>
                    <li><strong style="line-height: 1.42857;">Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>weight</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>integer</code></li>
                    <li><strong style="line-height: 1.42857;">Default</strong>: 100</li>
                    <li><strong style="line-height: 1.42857;">Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>conditions</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html"  rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li>
                    <li><strong style="line-height: 1.42857;">Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" style="line-height: 1.42857;" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>pages</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>[ string, ... ]</code></li>
                    <li><strong style="line-height: 1.42857;">Allowed values</strong>: <span style="font-family: monospace;line-height: 1.42857;">help_center, </span> <span style="font-family: monospace;line-height: 1.42857;">portal, </span> <span style="font-family: monospace;line-height: 1.42857;">create_request, </span> <span style="font-family: monospace;line-height: 1.42857;">view_request, </span> <span style="font-family: monospace;line-height: 1.42857;">my_requests, </span> <span style="font-family: monospace;line-height: 1.42857;">approvals, </span> <span style="font-family: monospace;line-height: 1.42857;">profile</span></li>
                    <li><strong style="line-height: 1.42857;">Description</strong>: Restrict the module to only be visible in specified customer portal page(s).</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

## Sub header panel

<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A panel rendered underneath the title of customer portal pages.</td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><code><span>serviceDeskPortalSubHeader</span></code></td>
        </tr>
        <tr>
            <th colspan="1">Screenshot</th>
                <td colspan="2">
                    <p><span><img src="../images/jdev-service-desk-portal-sub-header.png"/> </span> 
                    </p>
                </td>
        </tr>
        <tr>
            <th>Sample JSON</th>
                <td colspan="2">
                    <div style="border-width: 1px;">  
                        <pre style="font-size:12px;">
...
&quot;modules&quot;: {
    &quot;serviceDeskPortalSubHeaders&quot;: [
            {
            &quot;key&quot;: &quot;sd-portal-subheader&quot;,
            &quot;url&quot;: &quot;/sd-portal-subheader&quot;
            }
        ]
    }
...
                            </pre>      
                    </div>
                </td>
        </tr>
        <tr>
            <th rowspan="5">Properties</th>
                <td colspan="1"><p><strong> <code>key</code> </strong></p></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li>
                        <li><strong>Required</strong>: yes</li>
                        <li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>url</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>string, uri-template</code></li>
                    <li><strong>Required</strong>: yes</li>
                    <li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>weight</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>integer</code></li>
                    <li><strong>Default</strong>: 100</li>
                    <li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>conditions</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html"  rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li>
                    <li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>pages</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>[ string, ... ]</code></li>
                    <li><strong>Allowed values</strong>: <code>
                    <span>help_center, </span>
                    <span>portal, </span>
                    <span>create_request, </span>
                    <span>view_request, </span>
                    <span>my_requests, </span>
                    <span>approvals, </span>
                  </code> <span> <code>profile</code> </span></li><li><span> </span> <strong>Description</strong>: Restrict the module to only be visible in specified customer portal page(s).</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

## Footer panel

<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A panel rendered at the bottom of customer portal pages.</td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><code><span>serviceDeskPortalFooter</span></code></td>
        </tr>
        <tr>
            <th colspan="1">Screenshot</th>
                <td colspan="2">
                    <p><span><img src="../images/jdev-service-desk-portal-footer.png"/> </span> 
                    </p>
                </td>
        </tr>
        <tr>
            <th>Sample JSON</th>
                <td colspan="2">
                    <div style="border-width: 1px;">
                        <pre style="font-size:12px;">
...
&quot;modules&quot;: {
    &quot;serviceDeskPortalFooters&quot;: [
        {
            &quot;key&quot;: &quot;sd-portal-footer&quot;,
            &quot;url&quot;: &quot;/sd-portal-footer&quot;
        }
    ]
}
...
                        </pre>
                    </div>
                </td>
        </tr>
        <tr>
            <th rowspan="5">Properties</th>
                <td colspan="1"><p><strong> <code>key</code> </strong></p></td>
                <td colspan="1">
                    <ul>
                        <li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li>
                        <li><strong>Required</strong>: yes</li>
                        <li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li>
                    </ul>
                </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>url</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>string, uri-template</code></li>
                    <li><strong>Required</strong>: yes</li>
                    <li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>weight</code> </strong></p><p> </p><p> </p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>integer</code></li>
                    <li><strong>Default</strong>: 100</li>
                    <li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>conditions</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li>
                    <li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="1"><p><strong> <code>pages</code> </strong></p></td>
            <td colspan="1">
                <ul>
                    <li><strong>Type</strong>: <code>[ string, ... ]</code></li>
                    <li><strong>Allowed values</strong>: <code>
                    <span>help_center, </span> <span>portal, </span> <span>create_request, </span> <span>view_request, </span> <span>my_requests, </span> <span>approvals, </span> <span>profile</span>
                  </code></li>
                    <li><span> </span> <strong>Description</strong>: Restrict the module to only be visible in specified customer portal page(s).</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

## Request view panel

<table>
    <tbody>
        <tr>
            <th colspan="1">Description</th>
                <td colspan="2">A panel that shows up in the bottom-right corner of request view page.</td>
        </tr>
        <tr>
            <th>Module type</th>
                <td colspan="2"><pre>serviceDeskPortalRequestViewPanel</pre></td>
        </tr>
        <tr>
            <th colspan="1">Screenshot</th>
                <td colspan="2">
                   <p><span><img src="../images/jdev-service-desk-portal-request-view-panel.png"/> </span> </p>
                </td>
        </tr>
        <tr><th>Sample JSON</th><td colspan="2"><div><div id="expander-476701590"><div id="expander-control-476701590" class="expand-control"></div><div id="expander-content-$toggleId">
    <div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre style="font-size:12px;">...
&quot;modules&quot;: {
    &quot;serviceDeskPortalRequestViewPanels&quot;: [
        {
            &quot;key&quot;: &quot;sd-portal-request-view-content&quot;,
            &quot;url&quot;: &quot;/sd-portal-request-view-content&quot;
        }
    ]
}
...</pre>
</div></div></div></div></div></td></tr><tr><th rowspan="5">Properties</th><td colspan="1"><div><p><strong> <code>key</code> </strong></p></div></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>url</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string, uri-template</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>weight</code> </strong></p><p> </p><p> </p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>integer</code></li><li><strong>Default</strong>: 100</li><li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>conditions</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html"  rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li><li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li></ul></td></tr></tbody></table>

## Profile page panel

<table><tbody><tr><th colspan="1">Description</th><td colspan="2"><p>A panel that shows up in the profile page of the customer portal.</p></td></tr><tr><th>Module type</th><td colspan="2"><pre>serviceDeskPortalProfilePanel</pre></td></tr><tr><th colspan="1">Screenshot</th><td colspan="2"><div id="expander-211099210"><div id="expander-control-211099210" class="expand-control"></div><div id="expander-content-$toggleId">
    <p><span><img src="../images/jdev-service-desk-portal-profile-panel.png"/> </span> </p></div></div></td></tr><tr><th>Sample JSON</th><td colspan="2"><div><div id="expander-1097261388"><div id="expander-control-1097261388" class="expand-control"></div><div id="expander-content-$toggleId">
    <div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre style="font-size:12px;">...
&quot;modules&quot;: {
    &quot;serviceDeskPortalProfilePanels&quot;: [
        {
            &quot;key&quot;: &quot;sd-portal-profile-content&quot;,
            &quot;url&quot;: &quot;/sd-portal-profile-content&quot;
        }
    ]
}
...</pre>
</div></div></div></div></div></td></tr><tr><th rowspan="5">Properties</th><td colspan="1"><div><p><strong> <code>key</code> </strong></p></div></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>url</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string, uri-template</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>weight</code> </strong></p><p> </p><p> </p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>integer</code></li><li><strong>Default</strong>: 100</li><li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>conditions</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li><li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li></ul></td></tr></tbody></table>

# Actions

## Request view action

<table><tbody><tr><th colspan="1">Description</th><td colspan="2"><p>A link that shows up in the request view of the customer portal. </p></td></tr><tr><th>Module type</th><td colspan="2"><pre>serviceDeskPortalRequestViewAction</pre></td></tr><tr><th colspan="1">Screenshot</th><td colspan="2"><div id="expander-985059251"><div id="expander-control-985059251" class="expand-control"></div><div id="expander-content-$toggleId">
    <p><span><img src="../images/jdev-service-desk-portal-request-view-action.png"/></span></p></div></div></td></tr><tr><th>Sample JSON</th><td colspan="2"><div><div id="expander-121358455"><div id="expander-control-121358455" class="expand-control"></div><div id="expander-content-$toggleId">
    <div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre style="font-size:12px;">...
&quot;modules&quot;: {
    &quot;serviceDeskPortalRequestViewActions&quot;: [
        {
            &quot;key&quot;: &quot;sd-portal-request-view-action&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My request view action&quot;
            },
            &quot;url&quot;: &quot;/sd-portal-request-view-content&quot;,
            &quot;icon&quot;: {
              &quot;url&quot;: &quot;/img/testicon.png&quot;
            }
        }
    ]
}
...</pre>
</div></div></div></div></div></td></tr><tr><th rowspan="10">Properties</th><td colspan="1"><div><p><strong> <code>key</code> </strong></p></div></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li></ul></td></tr><tr><td colspan="1"><span style="font-family: monospace;"> <strong><code>name</code></strong> </span></td><td colspan="1"><ul><li><strong>Type</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html" rel="nofollow"> <code>i18n Property</code> </a></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A human readable name.</li></ul></td></tr><tr><td colspan="1"><strong> <code>context</code> </strong></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string</code></li><li><strong>Default</strong>: <code>addon</code></li><li><strong>Allowed values</strong>: <code>
                    <span style="line-height: 1.42857;">page, </span>
                    <span style="line-height: 1.42857;">PAGE, </span>
                    <span style="line-height: 1.42857;">addon, </span>
                    <span style="line-height: 1.42857;">ADDON, </span>
                    <span style="line-height: 1.42857;">product, </span>
                    <span style="line-height: 1.42857;">PRODUCT</span>
                  </code></li><li><span style="line-height: 1.42857;"> <strong>Description</strong>: </span>The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be either <code style="line-height: 1.42857;">addon</code>, which renders the URL relative to the add-on's base URL, <code style="line-height: 1.42857;">page</code> which targets a page module by specifying the page's module key as the url or <code style="line-height: 1.42857;">product</code>, which renders the URL relative to the product's base URL.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>url</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string, uri-template</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li></ul></td></tr><tr><td colspan="1"><strong> <code>target</code> </strong></td><td colspan="1"><ul><li><strong>Type</strong>: <a href="JIRA-Service-Desk-modules---Web-item-target_40004227.html"> <code>Web Item Target</code> </a></li><li><strong>Description:</strong> Defines the way the url is opened in the browser, such as in its own page or a modal dialog. If omitted, the url behaves as a regular hyperlink.</li></ul></td></tr><tr><td colspan="1"><strong> <code>icon</code> </strong></td><td colspan="1"><ul><li><strong>Type: </strong><code><a href="JIRA-Service-Desk-modules---Portal-icon_41229857.html">Portal Icon</a></code></li><li><strong>Description:</strong> An icon that will be rendered next to the action. If the URL is relative it will be resolved against add-on's baseUrl. If none provided a default one will be rendered. Icon should follow this guideline: monochromatic with #333333 colour and transparent background, 16x16 pixel in size.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>weight</code> </strong></p><p> </p><p> </p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>integer</code></li><li><strong>Default</strong>: 100</li><li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>conditions</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li><li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li></ul></td></tr></tbody></table>

## Profile page action

<table><tbody><tr><th colspan="1">Description</th><td colspan="2"><p>A link that shows up in the profile page. </p></td></tr><tr><th>Module type</th><td colspan="2"><pre>serviceDeskPortalProfileAction</pre></td></tr><tr><th colspan="1">Screenshot</th><td colspan="2"><div id="expander-663942515"><div id="expander-control-663942515" class="expand-control"></div><div id="expander-content-$toggleId">
    <p><span><img src="../images/jdev-service-desk-portal-profile-action.png"/></span> </p></div></div></td></tr><tr><th>Sample JSON</th><td colspan="2"><div><div id="expander-83162360"><div id="expander-control-83162360" class="expand-control"></div><div id="expander-content-$toggleId">
    <div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre style="font-size:12px;">...
&quot;modules&quot;: {
    &quot;serviceDeskPortalProfileActions&quot;: [
        {
            &quot;key&quot;: &quot;sd-portal-profile-action&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My profile page action&quot;
            },
            &quot;url&quot;: &quot;/sd-portal-profile-content&quot;
        }
    ]
}
...</pre>
</div></div></div></div></div></td></tr><tr><th rowspan="9">Properties</th><td colspan="1"><div><p><strong> <code>key</code> </strong></p></div></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li></ul></td></tr><tr><td colspan="1"><code>
                <span> <strong>name</strong> </span>
              </code></td><td colspan="1"><ul><li><strong>Type</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html" rel="nofollow"> <code>i18n Property</code> </a></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A human readable name.</li></ul></td></tr><tr><td colspan="1"><strong> <code>context</code> </strong></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string</code></li><li><strong>Default</strong>: <code>addon</code></li><li><strong>Allowed values</strong>: <code>
                    <span>page, </span>
                    <span>PAGE, </span>
                    <span>addon, </span>
                    <span>ADDON, </span>
                    <span>product, </span>
                    <span>PRODUCT</span>
                  </code></li><li><span> <strong>Description</strong>: </span>The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be either <code>addon</code>, which renders the URL relative to the add-on's base URL, <code>page</code> which targets a page module by specifying the page's module key as the url or <code>product</code>, which renders the URL relative to the product's base URL.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>url</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string, uri-template</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li></ul></td></tr><tr><td colspan="1"><strong> <code>target</code> </strong></td><td colspan="1"><ul><li><strong>Type</strong>: <a href="JIRA-Service-Desk-modules---Web-item-target_40004227.html"> <code>Web Item Target</code> </a></li><li><strong>Description:</strong> <span>Defines the way the url is opened in the browser, such as in its own page or a modal dialog. If omitted, the url behaves as a regular hyperlink.</span></li></ul></td></tr><tr><td colspan="1"><p><strong> <code>weight</code> </strong></p><p> </p><p> </p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>integer</code></li><li><strong>Default</strong>: 100</li><li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>conditions</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li><li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li></ul></td></tr></tbody></table>

## User menu action

<table><tbody><tr><th colspan="1">Description</th><td colspan="2"><p>A menu entry that shows up in the user menu.</p></td></tr><tr><th>Module type</th><td colspan="2"><pre>serviceDeskPortalUserMenuAction</pre></td></tr><tr><th colspan="1">Screenshot</th><td colspan="2"><div id="expander-1229206049"><div id="expander-control-1229206049" class="expand-control"></div><div id="expander-content-$toggleId">
    <p><span><img src="../images/jdev-service-desk-portal-user-menu-action.png"/> </span> </p></div></div></td></tr><tr><th>Sample JSON</th><td colspan="2"><div><div id="expander-1275017268"><div id="expander-control-1275017268" class="expand-control"></div><div id="expander-content-$toggleId">
    <div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre style="font-size:12px;">...
&quot;modules&quot;: {
    &quot;serviceDeskPortalUserMenuActions&quot;: [
        {
            &quot;key&quot;: &quot;sd-portal-user-menu-action&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My user menu action&quot;
            },
            &quot;url&quot;: &quot;/sd-portal-user-menu-content&quot;
        }
    ]
}
...</pre>
</div></div></div></div></div></td></tr><tr><th rowspan="9">Properties</th><td colspan="1"><div><p><strong> <code>key</code> </strong></p></div></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string (^[a-zA-Z0-9-]+$)</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.</li></ul></td></tr><tr><td colspan="1"><code>
                <strong>name</strong>
              </code></td><td colspan="1"><ul><li><strong>Type</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html" rel="nofollow"> <code>i18n Property</code> </a></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: A human readable name.</li></ul></td></tr><tr><td colspan="1"><strong> <code>context</code> </strong></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string</code></li><li><strong>Default</strong>: <code>addon</code></li><li><strong>Allowed values</strong>: <code>page, PAGE, addon, ADDON, product, PRODUCT</code></li><li><strong>Description</strong>: The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be either <code>addon</code>, which renders the URL relative to the add-on's base URL, <code>page</code> which targets a page module by specifying the page's module key as the url or <code>product</code>, which renders the URL relative to the product's base URL.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>url</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>string, uri-template</code></li><li><strong>Required</strong>: yes</li><li><strong>Description</strong>: The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive <a href="JIRA-Service-Desk-modules---Customer-portal_39988271.html">additional context</a> from the application by using variable tokens in the URL attribute.</li></ul></td></tr><tr><td colspan="1"><strong> <code>target</code> </strong></td><td colspan="1"><ul><li><strong>Type</strong>: <a href="JIRA-Service-Desk-modules---Web-item-target_40004227.html"> <code>Web Item Target</code> </a></li><li><strong>Description:</strong> <span>Defines the way the url is opened in the browser, such as in its own page or a modal dialog. If omitted, the url behaves as a regular hyperlink.</span></li></ul></td></tr><tr><td colspan="1"><p><strong> <code>weight</code> </strong></p><p> </p><p> </p></td><td colspan="1"><ul><li><strong>Type</strong>: <code>integer</code></li><li><strong>Default</strong>: 100</li><li><strong>Description</strong>: Determines the order in which the web item appears in the menu or list. The &quot;lightest&quot; weight (i.e., lowest number) appears first, rising relative to other items, while the &quot;heaviest&quot; weights sink to the bottom of the menu or list. Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.</li></ul></td></tr><tr><td colspan="1"><p><strong> <code>conditions</code> </strong></p></td><td colspan="1"><ul><li><strong>Type</strong>: [ <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html" rel="nofollow"> <code>Single Condition</code> </a>, <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html" rel="nofollow"> <code>Composite Condition</code> </a>, … ]</li><li><strong>Description</strong>: <a href="https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html" rel="nofollow">Conditions</a> can be added to display only when all the given conditions are true.</li></ul></td></tr></tbody></table>

  [Header panel]: #header-panel
  [Sub header panel]: #sub-header-panel
  [Footer panel]: #footer-panel
  [Request view panel]: #request-view-panel
  [Profile page panel]: #profile-page-panel
  [Request view action]: #request-view-action
  [Profile page action]: #profile-page-action
  [User menu action]: #user-menu-action
  [additional context]: /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
  [`Single Condition`]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/single-condition.html
  [`Composite Condition`]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/composite-condition.html
  [Conditions]: https://developer.atlassian.com/static/connect/docs/latest/concepts/conditions.html
  [`i18n Property`]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/i18n-property.html
  [`Web Item Target` ]: /jiracloud/jira-service-desk-modules-web-item-target-40004227.html

