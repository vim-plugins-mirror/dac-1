---
title: JIRA Service Desk Cloud REST API 
platform: cloud
product: jsdcloud
category: reference
subcategory: api
aliases:
- /jiracloud/jira-service-desk-cloud-rest-api-39988033.html
- /jiracloud/jira-service-desk-cloud-rest-api-39988033.md
confluence_id: 39988033
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988033
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988033
date: "2016-05-23"
---
# JIRA Cloud : JIRA Service Desk Cloud REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. JIRA Service Desk Cloud provides a REST API for application-specific features, like queues and requests. Read the reference documentation below to get started.

#### <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link"><br />
</a>

#### ![Alt text](../../../../illustrations/atlassian-software-51.png)

#### <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="external-link">JIRA Service Desk REST API</a>

If you haven't used the JIRA REST APIs before, make sure you read the [Atlassian REST API policy].

 

## Other JIRA REST APIs

The JIRA Cloud platform provides a REST API for common features, like issues and workflows. 

-   [JIRA Cloud platform REST API]

JIRA Software Cloud also has its own REST API for application-specific features, like sprints and boards.

-   [JIRA Software Cloud REST API]

## Using the REST APIs

The best way to get started with the REST APIs is to jump into the reference documentation above. However, here are a few other resources to help you along the way.

### Authentication guides

The REST APIs support basic authentication, cookie-based (session) authentication, and OAuth. The following pages will get you started on using each of these authentication types with the REST APIs:

-   [Authentication and authorization] overview
-   [JIRA REST API - Basic authentication]
-   [JIRA REST API - Cookie-based Authentication]
-   [JIRA REST API - OAuth authentication]

  [Atlassian REST API policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Cloud platform REST API]: /cloud/jira/platform/jira-cloud-platform-rest-api
  [JIRA Software Cloud REST API]: /jiracloud/jira-software-cloud-rest-api-39988028.html
  [Authentication and authorization]: /cloud/jira/platform/authentication-and-authorization
  [JIRA REST API - Basic authentication]: /cloud/jira/platform/jira-rest-api-basic-authentication
  [JIRA REST API - Cookie-based Authentication]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication
  [JIRA REST API - OAuth authentication]: /cloud/jira/platform/jira-rest-api-oauth-authentication
