---
title: JIRA Service Desk Modules Portal Icon 
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-service-desk-modules-portal-icon-41229857.html
- /jiracloud/jira-service-desk-modules-portal-icon-41229857.md
confluence_id: 41229857
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41229857
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41229857
date: "2016-08-24"
---
# Portal icon module

An icon that will be rendered next to the action. If the URL is relative it will be resolved against add-on's baseUrl. If none provided a default one will be rendered. Icon should follow this guideline: monochromatic with \#333333 colour and transparent background, 16x16 pixel in size

# Example

``` js
{
  ...,
  "icon": {
    "url": "/img/send_mail.png"
  }
}
```

# Properties

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><code>url</code></td>
<td><ul>
<li><strong>Type:</strong> <code>string</code></li>
<li><strong>Required:</strong> <code>yes</code></li>
<li><strong>Description:</strong> Path to the icon image. If path is relative, it will be resolved against add-on's basUrl.</li>
</ul></td>
</tr>
</tbody>
</table>
