---
aliases:
- /jiracloud/authentication.html
- /jiracloud/authentication.md
category: devguide
platform: cloud
product: jiracloud
subcategory: blocks
title: Authentication
---
# Authentication
Defines the authentication type to use when signing requests from the host application to the Connect add-on.

## Example
``` json
{
  "authentication": {
    "type": "jwt"
  }
}
```

## Properties
<div class="ac-properties"><a id="publicKey"></a><h3><code>publicKey</code></h3><div class="aui-group"><div class="aui-item ac-property-key"><h5>Type</h5></div><div class="aui-item"><code>string</code></div></div><div class="aui-group"><div class="aui-item ac-property-key"><h5>Description</h5></div><div class="aui-item"><p>The public key used for asymmetric key encryption. Ignored when using JWT with a shared secret.</p></div></div><a id="type"></a><h3><code>type</code></h3><div class="aui-group"><div class="aui-item ac-property-key"><h5>Type</h5></div><div class="aui-item"><code>string</code></div></div><div class="aui-group"><div class="aui-item ac-property-key"><h5>Defaults to</h5></div><div class="aui-item"><code>jwt</code></div></div><div class="aui-group"><div class="aui-item ac-property-key"><h5>Allowed values</h5></div><div class="aui-item"><ul></ul><li><code>jwt</code></li><li><code>JWT</code></li><li><code>none</code></li><li><code>NONE</code></li></div></div><div class="aui-group"><div class="aui-item ac-property-key"><h5>Description</h5></div><div class="aui-item"><p>The type of authentication to use.</p></div></div></div>