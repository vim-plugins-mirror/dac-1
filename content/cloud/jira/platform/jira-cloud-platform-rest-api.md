---
title: JIRA Cloud Platform REST API
platform: cloud
product: jiracloud
category: reference
subcategory: api 
aliases:
- /jiracloud/jira-cloud-platform-rest-api-39987036.html
- /jiracloud/jira-cloud-platform-rest-api-39987036.md
confluence_id: 39987036
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987036
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987036
date: "2016-09-14"
---
# JIRA Cloud : JIRA Cloud platform REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. The JIRA Cloud platform provides a REST API for common features, like issues and workflows. Read the reference documentation below to get started:
#### [JIRA Cloud platform REST API]

If you haven't used the JIRA REST APIs before, make sure you read the [Atlassian REST API policy].

## REST APIs for the JIRA applications

The JIRA Software and JIRA Service Desk applications also have REST APIs for their application-specific features, like sprints (JIRA Software) or customer requests (JIRA Service Desk).

-   [JIRA Software Cloud REST API]
-   [JIRA Service Desk Cloud REST API]

## Using the REST APIs

The best way to get started with the JIRA Cloud platform REST API is to jump into the reference documentation. However, here are a few other resources to help you along the way.

### Authentication guides

The REST APIs support OAuth (1.0a), basic authentication, cookie-based (session) authentication. The following tutorials will get you started on using each of these authentication types with the REST APIs:

-   [OAuth authentication for the REST APIs]
-   [Basic authentication for the REST APIs]
-   [Cookie-based authentication for the REST APIs]

  [Atlassian REST API policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Cloud platform REST API]: https://docs.atlassian.com/jira/REST/cloud/
  [JIRA Software Cloud REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [JIRA Service Desk Cloud REST API]: https://docs.atlassian.com/jira-servicedesk/REST/cloud/
  [OAuth authentication for the REST APIs]: /cloud/jira/platform/jira-rest-api-oauth-authentication
  [Basic authentication for the REST APIs]: /cloud/jira/platform/jira-rest-api-basic-authentication
  [Cookie-based authentication for the REST APIs]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication
