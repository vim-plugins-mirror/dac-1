---
title: JIRA Platform Modules Administration Console
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-administration-console-39988333.html
- /jiracloud/jira-platform-modules-administration-console-39988333.md
confluence_id: 39988333
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988333
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988333 
date: "2016-09-15"
---
# Extension points for add-on configuration

This pages lists the entry points to place modules on the JIRA administration console. These can be used to inject menu sections and items in the Add-ons menu.

**On this page:**

-   [Add-ons]

## Add-ons

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Adds sections and items to the 'Add-ons' menu of the JIRA administration area.</p></td>
</tr>
<tr class="even">
<td><strong>Module </strong></td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>admin_plugins_menu</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jira-admin-addons.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
<pre class="javascript" style="font-size:12px;"><code>...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example-menu-section&quot;,
            &quot;location&quot;: &quot;admin_plugins_menu&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example-section-link&quot;,
            &quot;location&quot;: &quot;admin_plugins_menu/example-menu-section&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            },
            &quot;url&quot;: &quot;/example-section-link&quot;
        }
    ]
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Add-ons]: #add-ons
