---
title: Webhooks 
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/webhooks-39987038.html
- /jiracloud/webhooks-39987038.md
confluence_id: 39987038
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987038
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987038
date: "2016-09-16"
---
# Webhooks

A webhook is a user-defined callback over HTTP. You can use JIRA webhooks to notify your add-on or web application when certain events occur in JIRA. For example, you might want to alert your remote application when an issue has been updated or when sprint has been started. Using a webhook to do this means that your remote application doesn't have to periodically poll JIRA (via the REST APIs) to determine whether changes have occurred.

**On this page:**

-   [Overview]
-   [Registering a webhook]
-   [Configuring a webhook]
-   [Adding a webhook as a post function to a workflow]
-   [Executing a webhook]
-   [Known issues]

## Overview

A webhook in JIRA is defined by the following information, which you need to provide when registering (i.e. creating) a new webhook:

-   a **name** for the webhook created ("Webhook for my remote app")
-   the **URL** where the callback should be sent
-   the **scope** of the webhook, either "all issues" or a limited set of issues specified by JQL ("project = TEST and fixVersion = future")
-   the **events** to post to the URL, either "all events" or a specific set of events

A simple webhook might look like this:

**Name**: "My simple webhook"<br>
**URL**: http://www.myremoteapplication.com/webhookreceiver<br>
**Scope**: all issues<br>
**Events**: all issue events<br>

A more complex webhook might look like this:

**Name**: "A slightly more advanced webhook"<br>
**URL**: http://www.myremoteapplication.com/webhookreceiver<br>
**Scope**: Project = JRA AND fixVersion IN ("6.4", "7.0")<br>
**Events**: Issue Updated, Issue Created<br>

## Registering a webhook

To register (i.e. create) a webhook, you can use any of the following methods:

-   [Register via the JIRA administration console]
-   [Register via the JIRA REST API] -- The user must have the JIRA Administrators global permission. Note, Connect add-ons cannot register webhooks via the JIRA REST API (use the 'Register via an Atlassian Connect add-on' method below).
-   Register via an Atlassian Connect add-on (which provides a webhook registration in its descriptor) -- this is described in the [Connect documentation]. 

#### Registering a webhook via the JIRA administration console

1.  Navigate to the JIRA administration console &gt; **System** &gt; **Webhooks** (in the Advanced section).
    You can also use the quick search (keyboard shortcut: **.**), then type 'webhooks'. 
2.  Click **Create a webhook**.
3.  Enter the details for your new webhook in the form that is shown. For more information on this, see [Configuring a webhook][1] below.
4.  Click **Create** to register your webhook.

![Alt text](../images/jira-admin-webhooks.png)

#### Registering a webhook via the JIRA REST API

**To register a webhook via REST:**

1.   POST a webhook in JSON format to:
    ` <JIRA_URL>/rest/webhooks/1.0/webhook` `  `

    ``` javascript
     {
      "name": "my first webhook via rest",
      "url": "http://www.example.com/webhooks",
      "events": [
        "jira:issue_created",
        "jira:issue_updated"
      ],
      "jqlFilter": "Project = JRA AND resolution = Fixed",
      "excludeIssueDetails" : false
    }
    ```

2.  The response will return the webhook in JSON with additional information, including the user that created the webhook, the created timestamp, etc.

**To unregister (i.e. delete) a webhook via REST:**

-   Execute a DELETE to the following URL:
    `<JIRA_URL>/rest/webhooks/1.0/webhook/{id of the webhook}` 

     The following would delete the webhook with an ID of 70:

    ``` javascript
    curl --user username:password -X DELETE -H "Content-Type: application/json" <JIRA_URL>/rest/webhooks/1.0/webhook/70
    ```

**To query a webhook via REST:**

-   To get all webhooks for a JIRA instance, perform a GET with the following URL: `<JIRA_URL>/rest/webhooks/1.0/webhook. `

    ``` javascript
    curl --user username:password -X GET -H "Content-Type: application/json" http://jira.example.com/rest/webhooks/1.0/webhook
    ```

-   To get a specific webhook by its ID, perform a GET with the following URL: `<JIRA_URL>/rest/webhooks/1.0/webhook/<webhook ID>`

    The following would get a webhook with an ID of 72:

    ``` javascript
    curl --user username:password -X GET -H "Content-Type: application/json" http://jira.example.com/rest/webhooks/1.0/webhook/72
    ```

## Configuring a webhook

The information in this section will help you configure a webhook, whether you are creating a new one or modifying an existing one.

#### Registering events for a webhook

The following events are available for JIRA webhooks. The string in parentheses is the name of the webhookEvent in the response.

**Issue-related:**

-   created (*jira:issue\_created*)
-   updated (*jira:issue\_updated*)
-   deleted (*jira:issue\_deleted*)
-   worklog changed (*jira:worklog\_updated*)
    *This event fires if a worklog is updated or deleted*

*Note, worklogs and comments will soon be removed from callbacks for issue-related events. The *jira:worklog\_updated event has also been deprecated.* See the [notice].*

-   created (*worklog\_created*)
-   updated (*worklog\_updated*)
-   deleted (*worklog\_deleted*)

*Note, callbacks for 'worklog' events will soon be restricted to only include the 100 most recent items. *See the [notice].**

-   created (*comment\_created*)
-   updated (*comment\_updated*)
-   deleted (*comment\_deleted*)

**Project-related:**

-   created (*project\_created*)
-   updated (*project\_updated*)
-   deleted (*project\_deleted*)

<!-- -->

-   released (*jira:version\_released*)
-   unreleased (*jira:version\_unreleased*)
-   created (*jira:version\_created*)
-   moved (*jira:version\_moved*)
-   updated (*jira:version\_updated*)
-   deleted (*jira:version\_deleted*)
-   merged (*jira:version\_deleted)
    note, this is the same webhookEvent name as the 'deleted' event, but the response will include a mergedTo property*

**User-related:**

-   created (*user\_created*)
-   updated (*user\_updated*)
-   deleted (*user\_deleted*)

**JIRA configuration-related:**

-   voting (*option\_voting\_changed*)
-   watching (*option\_watching\_changed*)
-   unassigned issues (*option\_unassigned\_issues\_changed*)
-   subtasks (*option\_subtasks\_changed*)
-   attachments (*option\_attachments\_changed*)
-   issue links (*option\_issuelinks\_changed*)
-   time tracking (*option\_timetracking\_changed*)

**JIRA Software-related:**

-   created (*sprint\_created*)
-   deleted (*sprint\_deleted*)
-   updated (*sprint\_updated*)
-   started (*sprint\_started*)
-   closed (*sprint\_closed*)

<!-- -->

-   created (*board\_created*)
-   updated (*board\_updated*)
-   deleted (*board\_deleted*)
-   configuration changed (*board\_configuration\_changed*)

#### Choosing events for your webhook

If you are unsure which events to register your webhook for, then the simplest approach is to register your webhook for all events for the relevant entity (e.g. all issue-related events). Then you won't need to update the webhook if you need to handle these events in future; you can just add code in your add-on or web application once you want to react to them.

#### Restricting your events to a subset of issues via JQL

If you only want your webhook events to fire for a specific set of issues, you can specify a JQL query in your webhook to send only events triggered by matching issues. For example, you could register for all Issue Deleted events on issues in the Test ("TEST") Project with the 'Performance' component.

![Alt text](../images/webhook-jql.png)

Note, although the JQL that uses standard JIRA fields is very performant, some custom fields, because of their implementation, can take multiple seconds to query. You should take this into account when using JQL for a webhook, as the query will be run with each event.

#### Variable substitution in the webhook URL

You can append variables to the webhook URL when creating or updating a webhook. The variables are listed below:

-   `${board.id} `
-   `${issue.id} `
-   `${issue.key} `
-   `${mergedVersion.id} `
-   `${modifiedUser.key} `
-   `${modifiedUser.name} `
-   `${project.id} `
-   `${project.key} `
-   `${sprint.id} `
-   `${version.id}`

You can use these variables to dynamically insert the value of the current issue key, sprint ID, project key, etc, into the webhook URL when it is triggered. Consider the following example:

Say you specified the URL for your webhook with the `${issue.key}` variable, like this:

``` javascript
http://jira.example.com/webhook/${issue.key}
```

If an issue with the key **EXA-1** triggers a webhook, then the URL that will be used is:

``` javascript
http://service.example.com/webhook/EXA-1
```

## Adding a webhook as a post function to a workflow

Webhooks can be attached as a <a href="https://confluence.atlassian.com/display/AdminJIRACloud/Advanced+workflow+configuration#Advancedworkflowconfiguration-postfunctions" class="external-link">workflow post function</a>. This makes it easy to trigger a webhook when an issue makes a workflow transition, for instance when it gets rejected from QA or when it gets resolved.

**To add a webhook as a post function to a workflow:**

1.  Configure your workflow and select the desired transition in the workflow designer. For more details, see <a href="https://confluence.atlassian.com/display/AdminJIRACloud/Working+with+workflows" class="external-link">Working with workflows</a>.
2.  Click **Post functions**, then click **Add post function**.
3.  Select **Trigger a Webhook** from the list of post functions and click **Add**.
4.  Select the desired webhook from the list of webhooks and click **Add** to add the webhook as a post function.

Notes:

-   If the webhook you choose is **also** configured to listen to events, then the webhook will be triggered twice: once for the event and once for the workflow transition. For this reason, you should always unselect all events from the webhook admin screen.
-   If a webhook is associated with a post-function, you will not be able to delete the webhook. You must disassociate it from the post-function first.

## Executing a webhook

Webhooks will be run without a specific user context, e.g. all issues will be available to the webhook, rather than having them scoped to a single user. *(Note the URL will also contain user parameters in the form `?user_id=(u'sysadmin',)&user_key=(u'sysadmin',)` appended at the end.)*

By default, a webhook will send a request with a JSON callback when it is triggered. If you don't want the JSON body sent, then you will need to select **Exclude body** when configuring the webhook.

At a high level, every callback contains the webhookEvent ID, the timestamp, and information about the entity associated with the event (e.g. issue, project, board, etc). Callbacks may have additional information, depending on the type of event associated with it. As an example, the structure of a callback for an issue-related event is described below:

#### Example: callback for an issue-related event

A callback for an issue-related event is structured like this:

``` javascript
{ 
    "timestamp"
    "event"
    "user": {
               --> See User shape in table below
    },
    "issue": { 
               --> See Issue shape in table below
    },
    "changelog" : {
               --> See Changelog shape in table below    
    },
    "comment" : {
               --> See Comment shape in table below  
    }
}
```
<br>

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Issue shape</strong></td>
<td><ul>
<li>The same shape returned from the JIRA REST API when an issue is retrieved with NO expand parameters</li>
<li>REST API docs: <a href="https://docs.atlassian.com/jira/REST/latest/#api/2/issue-getIssue" class="uri" class="external-link">https://docs.atlassian.com/jira/REST/latest/#api/2/issue-getIssue</a></li>
<li>Example: <a href="https://jira.atlassian.com/rest/api/2/issue/JRA-2000" class="uri" class="external-link">https://jira.atlassian.com/rest/api/2/issue/JRA-2000</a></li>
<li>The issue is always present in a webhook POST</li>
</ul></td>
</tr>
<tr class="even">
<td><strong>User shape</strong></td>
<td><ul>
<li>The same shape returned from the JIRA REST API when a user is retrieved, but without the <em>active</em>, <em>timezone</em>, or <em>groups</em> elements.<br />

<ul>
<li>The shape of the user returned is a condensed shape from the normal user API in REST, but similar to what is returned when the user is embedded in another response.</li>
<li>For the full user details, use the JIRA REST API and query with the username.</li>
</ul></li>
<li>REST API docs: <a href="https://docs.atlassian.com/jira/REST/latest/#api/2/user-getUser" class="uri" class="external-link">https://docs.atlassian.com/jira/REST/latest/#api/2/user-getUser</a></li>
<li>Example:  <a href="https://jira.atlassian.com/rest/api/2/user?username=brollins" class="uri" class="external-link">https://jira.atlassian.com/rest/api/2/user?username=brollins</a></li>
<li>The user is always present in a webhook POST for issue events</li>
</ul></td>
</tr>
<tr class="odd">
<td><strong>Changelog shape</strong></td>
<td><ul>
<li>An array of change items, with one entry for each field that has been changed</li>
<li>The changelog is only provided for the issue_updated event</li>
<li><p>This is similar in format to the changelog you would retrieve from a JIRA issue, but without the user (since that is already in the JSON) and without the created timestamp (since that is also already in the JSON for a webhook event) </p></li>
</ul>
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
<pre class="javascript" style="font-size:12px;"><code>&quot;changelog&quot;: {
        &quot;items&quot;: [
            {
                &quot;toString&quot;: &quot;A new summary.&quot;,
                &quot;to&quot;: null,
                &quot;fromString&quot;: &quot;What is going on here?????&quot;,
                &quot;from&quot;: null,
                &quot;fieldtype&quot;: &quot;jira&quot;,
                &quot;field&quot;: &quot;summary&quot;
            },
            {
                &quot;toString&quot;: &quot;New Feature&quot;,
                &quot;to&quot;: &quot;2&quot;,
                &quot;fromString&quot;: &quot;Improvement&quot;,
                &quot;from&quot;: &quot;4&quot;,
                &quot;fieldtype&quot;: &quot;jira&quot;,
                &quot;field&quot;: &quot;issuetype&quot;
            },
        ],
        &quot;id&quot;: 10124
}</code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td><strong>Comment shape</strong></td>
<td><ul>
<li>The same shape returned from the JIRA REST API when a comment is retrieved </li>
<li>REST API docs: <a href="https://docs.atlassian.com/jira/REST/latest/#api/2/issue-getComments" class="uri" class="external-link">https://docs.atlassian.com/jira/REST/latest/#api/2/issue-getComments</a></li>
<li>Example: <a href="https://jira.atlassian.com/rest/api/2/issue/JRA-9/comment/252789" class="uri" class="external-link">https://jira.atlassian.com/rest/api/2/issue/JRA-9/comment/252789</a></li>
<li>The comment will be provided if a comment was provided with the change</li>
</ul></td>
</tr>
</tbody>
</table>

 

You can see a full example of this below. This JSON callback shows an update to an issue (some fields changed value) where a comment was also added:

``` javascript
 {
    "id": 2,
    "timestamp": "2009-09-09T00:08:36.796-0500",
    "issue": { 
        "expand":"renderedFields,names,schema,transitions,operations,editmeta,changelog",
        "id":"99291",
        "self":"https://jira.atlassian.com/rest/api/2/issue/99291",
        "key":"JRA-20002",
        "fields":{
            "summary":"I feel the need for speed",
            "created":"2009-12-16T23:46:10.612-0600",
            "description":"Make the issue nav load 10x faster",
            "labels":["UI", "dialogue", "move"],
            "priority": "Minor"
        }
    },
    "user": {
        "self":"https://jira.atlassian.com/rest/api/2/user?username=brollins",
        "name":"brollins",
        "emailAddress":"bryansemail at atlassian dot com",
        "avatarUrls":{
            "16x16":"https://jira.atlassian.com/secure/useravatar?size=small&avatarId=10605",
            "48x48":"https://jira.atlassian.com/secure/useravatar?avatarId=10605"
        },
        "displayName":"Bryan Rollins [Atlassian]",
        "active" : "true"
    },
    "changelog": {
        "items": [
            {
                "toString": "A new summary.",
                "to": null,
                "fromString": "What is going on here?????",
                "from": null,
                "fieldtype": "jira",
                "field": "summary"
            },
            {
                "toString": "New Feature",
                "to": "2",
                "fromString": "Improvement",
                "from": "4",
                "fieldtype": "jira",
                "field": "issuetype"
            }
        ],
        "id": 10124
    },
    "comment" : {
        "self":"https://jira.atlassian.com/rest/api/2/issue/10148/comment/252789",
        "id":"252789",
        "author":{
            "self":"https://jira.atlassian.com/rest/api/2/user?username=brollins",
            "name":"brollins",
            "emailAddress":"bryansemail@atlassian.com",
            "avatarUrls":{
                "16x16":"https://jira.atlassian.com/secure/useravatar?size=small&avatarId=10605",
                "48x48":"https://jira.atlassian.com/secure/useravatar?avatarId=10605"
            },
            "displayName":"Bryan Rollins [Atlassian]",
            "active":true
        },
        "body":"Just in time for AtlasCamp!",
        "updateAuthor":{
            "self":"https://jira.atlassian.com/rest/api/2/user?username=brollins",
            "name":"brollins",
            "emailAddress":"brollins@atlassian.com",
            "avatarUrls":{
                "16x16":"https://jira.atlassian.com/secure/useravatar?size=small&avatarId=10605",
                "48x48":"https://jira.atlassian.com/secure/useravatar?avatarId=10605"
            },
            "displayName":"Bryan Rollins [Atlassian]",
            "active":true
        },
        "created":"2011-06-07T10:31:26.805-0500",
        "updated":"2011-06-07T10:31:26.805-0500"
    },  
    "timestamp": "2011-06-07T10:31:26.805-0500",
    "webhookEvent": "jira:issue_updated"
}
```

#### Response codes

If a webhook is triggered successfully, a response code of 200 is returned. All other response codes should be treated as an unsuccessful attempt to trigger the webhook.

## Known issues

-   What happens if the remote system doesn't "catch" the webhook POST? JIRA will retry sending three times, and then log an error in the log file: atlassian-jira.log. For JIRA Cloud customers, who don't have access to log files, we are investigating a way to provide more visibility into any webhook errors.
-   Post function web hooks will not fire if added to the **Create Issue** workflow transition. We recommend that you configure your web hook to fire from the **issue\_created** event instead.
-   If you are using webhooks in JIRA Cloud, there is a known issue that restricts the port numbers that can be specified in the webhook URL. If you do not follow the guidelines below, then the webhook will not trigger.
    -   If you are using HTTP and specify a port number in the webhook URL, the port number must be 80. Note, if you do not specify a port number, the webhook will still work.
    -   If you are using HTTPS and specify a port number in the webhook URL, the port number must be 443. Note, if you do not specify a port number, the webhook will still work.

  [Overview]: #overview
  [Registering a webhook]: #registering-a-webhook
  [Configuring a webhook]: #configuring-a-webhook
  [Adding a webhook as a post function to a workflow]: #adding-a-webhook-as-a-post-function-to-a-workflow
  [Executing a webhook]: #executing-a-webhook
  [Known issues]: #known-issues
  [Register via the JIRA administration console]: #register-via-the-jira-administration-console
  [Register via the JIRA REST API]: #register-via-the-jira-rest-api
  [Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/webhook.html
  [1]: #1
  [notice]: /cloud/jira/platform/deprecation-notice-worklog-data-in-issue-related-events-for-webhooks
