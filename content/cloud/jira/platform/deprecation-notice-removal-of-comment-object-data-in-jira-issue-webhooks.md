---
title: Deprecation notice - removal of comment object data
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
aliases:
- /jiracloud/41746521.html
- /jiracloud/41746521.md
confluence_id: 41746521
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41746521
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41746521
date: "2016-09-07"
---

# Deprecation notice - removal of comment object data in jira:issue\_\* webhooks

On **1 April, 2017** we will be making changes to how `comment` data is sent in webhooks. 

The following webhooks will no longer contain any `comment` objects in their body:

- `jira:issue_created`
- `jira:issue_deleted`
- `jira:issue_updated`

These features will be removed six months after this notice is published, as described in the [Atlassian REST API Policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

## What will happen if I do nothing?

If your add-on uses the `comment` object data from any registered `jira:issue_*` webhooks, you may encounter complications from the absence of this object after its removal.

## Replacement

These features have been replaced with the `comment_created`, `comment_deleted` and `comment_updated` [webhooks](/cloud/jira/platform/webhooks/). These webhooks are available today.

Report feature requests and bugs for JIRA Cloud and webhooks in the [ACJIRA project](https://ecosystem.atlassian.net/projects/ACJIRA) on ecosystem.atlassian.net.