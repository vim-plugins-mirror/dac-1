---
title: Latest updates
platform: cloud
product: jiracloud
category: devguide
subcategory: index
aliases:
- /jiracloud/latest-updates-39988013.html
- /jiracloud/latest-updates-39988013.md
confluence_id: 39988013
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988013
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988013
date: "2016-10-10"
---

# Latest updates

![JIRA Platform Cloud](images/jira-hero.png)

We deploy updates to JIRA Cloud frequently. As a JIRA developer, it's important that you are aware of the changes. The resources below will help you keep track of what's happening.

## What's new

Check out new features in [JIRA Cloud](https://confluence.atlassian.com/display/Cloud/What%27s+New). This is the changelog for all Cloud applications.

## Announcements

-   [Deprecation notice - toString representation of sprints in Get issue response](/cloud/jira/platform/deprecation-notice-tostring-representation-of-sprints-in-get-issue-response/)
-   [Change notice - changelogs are now limited to the 100 most recent items](/cloud/jira/platform/change-notice-changelogs-are-now-limited-to-the-100-most-recent-items/)
-   [Deprecation notice - removal of comment object data in jira:issue\_\* webhooks](/cloud/jira/platform/deprecation-notice-removal-of-comment-object-data-in-jira-issue-webhooks/)
-   [Deprecation notice - removal of worklog object data in jira:issue\_\* webhooks and jira:worklog\_updated webhook](/cloud/jira/platform/deprecation-notice-worklog-data-in-issue-related-events-for-webhooks/)

## Recently updated docs

- [OAuth 2.0 - JWT Bearer token authorization grant type](cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type/)
- [Getting started](/cloud/jira/platform/getting-started/)
- [Global permission](/cloud/jira/platform/connect/modules/jiraGlobalPermissions/)
- [Add-on descriptor](/cloud/jira/platform/temp/add-on-descriptor/)

## Looking for more info?

Check out the [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/). Major changes that affect JIRA Cloud developers are announced in this blog, such as deprecating APIs. You'll also find handy tips and articles related to JIRA development.