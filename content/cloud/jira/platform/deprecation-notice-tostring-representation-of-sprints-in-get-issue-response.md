---
title: Deprecation notice - toString representation of sprints in Get issue response
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2016-08-26"
---

# Deprecation notice - toString representation of sprints in Get issue response

**From 1 April 2017**, JIRA REST API responses that include the **Sprint** custom field type will no longer return string representations of sprints in the body. This data will still be available in its proper JSON representation wherever a string was previously expected.

### Why?

Our goal is to have JSON as the standard format for JIRA REST API responses.

### Replacement

After this date, you will only be able to get data for the **Sprint** custom field through its JSON representation. This is available via the get issue method with expanded `versionedRepresentations,` in the [JIRA Platform REST API](https://docs.atlassian.com/jira/REST/latest/#api/2/issue-getIssue) and the [JIRA Software REST API](https://docs.atlassian.com/jira-software/REST/cloud/#agile/1.0/issue-getIssue).

#### Feature Parity

Currently, the JSON representation of a sprint does not include all the data provided by the former string representation. We would like to provide feature parity on the data that our vendors need. This work is being tracked in the following ticket: [ACJIRA-1082](https://ecosystem.atlassian.net/browse/ACJIRA-1082).

Please comment on this ticket if the JSON representation does not include the data necessary for your add-on to perform as intended.

### Example

#### Soon to be deprecated:

**Get Issue call:**

``` bash
https://TESTINSTANCE.atlassian.net/rest/api/2/issue/DEMO-1
```

**GET Issue response:**

``` js
"customfield_11458": [
    "com.atlassian.greenhopper.service.sprint.Sprint@1bf75fd[id=1,rapidViewId=1,state=CLOSED,name=Sprint 1
,startDate=2016-06-06T21:30:53.537+10:00,endDate=2016-06-20T21:30:00.000+10:00,
completeDate=2016-06-06T21:30:57.523+10:00,sequence=1]",
    "com.atlassian.greenhopper.service.sprint.Sprint@1689feb[id=2,rapidViewId=1,state=FUTURE,name=Sprint 2
,startDate=<null>,endDate=<null>,completeDate=<null>,sequence=2]"
]
```

#### New:

**Get Issue call:**

``` bash
https://TESTINSTANCE.atlassian.net/rest/api/2/issue/DEMO-1?expand=versionedRepresentations
```

**GET Issue response:**

``` js
"customfield_10021": {
    "1": [
        "com.atlassian.greenhopper.service.sprint.Sprint@1bf75fd[id=1,rapidViewId=1,state=CLOSED,
name=Sprint 1,startDate=2016-06-06T21:30:53.537+10:00,endDate=2016-06-20T21:30:00.000+10:00,
completeDate=2016-06-06T21:30:57.523+10:00,sequence=1]",
        "com.atlassian.greenhopper.service.sprint.Sprint@1689feb[id=2,rapidViewId=1,state=FUTURE,
name=Sprint 2,startDate=<null>,endDate=<null>,completeDate=<null>,sequence=2]"
    ],
    "2": [
        {
            "id": 1,
            "name": "Sprint 1",
            "state": "closed",
            "boardId": 1
        },
        {
            "id": 2,
            "name": "Sprint 2",
            "state": "future",
            "boardId": 1
        }
    ]
}
```

You can learn more about interacting with the **Sprint** custom field via JIRA's REST API in [JIRA Software's REST API Documentation](https://docs.atlassian.com/jira-software/REST/cloud/).

You can report feature requests and bugs for JIRA Cloud and our REST API in the [ACJIRA project on ecosystem.atlassian.net](https://ecosystem.atlassian.net/projects/ACJIRA).