---
title: Deprecation notice removal of worklog object data
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
aliases:
- /jiracloud/41225789.html
- /jiracloud/41225789.md
confluence_id: 41225789
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41225789
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41225789
date: "2016-09-07"
---
# Deprecation notice - removal of worklog object data in jira:issue\_\* webhooks and jira:worklog\_updated webhook

On **1 April, 2017** we will be making changes to how `worklog` data is sent in webhooks. The following webhooks will no longer contain any `worklog` objects in their body:

- `jira:issue_created`
- `jira:issue_deleted`
- `jira:issue_updated`

Also:

-   The `jira:worklog_updated` webhook will no longer be sent.

These features will be removed six months after this notice is published, as described in the [Atlassian REST API Policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

## What will happen if I do nothing?

If your add-on uses the `worklog` object data from any registered `jira:issue_*` webhooks, you may encounter complications from the absence of this object after its removal.

Also, if you have registered to receive events from the `jira:worklog_updated` event after the removal date, your add-on will no longer receive them. 

## Replacement

These features have been replaced with the `worklog_created`, `worklog_deleted` and `worklog_updated` [webhooks](/cloud/jira/platform/webhooks/). These webhooks are available today.

Report feature requests and bugs for JIRA Cloud and webhooks in the [ACJIRA project](https://ecosystem.atlassian.net/projects/ACJIRA) on ecosystem.atlassian.net.