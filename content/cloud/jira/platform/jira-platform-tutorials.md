---
title: JIRA Platform Tutorials
platform: cloud
product: jiracloud
category: devguide
subcategory: learning 
aliases:
- /jiracloud/jira-platform-tutorials-39987044.html
- /jiracloud/jira-platform-tutorials-39987044.md
confluence_id: 39987044
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987044
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987044
date: "2016-09-22"
---
# JIRA Cloud : JIRA platform tutorials DELETE THIS PAGE

The following pages will help you learn how to develop for the JIRA platform. Guides provide examples and best practices on JIRA concepts or processes. Tutorials provide a more hands-on approach to learning, by getting you to build something with step-by-step procedures.

Check them out below:

-   [Guide - Using the Issue Field module] -- This guide will show you how to use the JIRA issue field module <https://developer.atlassian.com/static/connect/docs/latest/modules/jira/issue-field.html>. We'll take you on a guided tour of an example Connect add-on, jira-issue-field-demo, that uses the issue field module and explain the key concepts. 

 

  [Guide - Using the Issue Field module]: https://developer.atlassian.com/display/jiracloud/Guide+-+Using+the+Issue+Field+module
