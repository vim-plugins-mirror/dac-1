---
title: Welcome to JIRA Cloud development
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/jira-cloud-for-developers-39375886.html
- /jiracloud/jira-cloud-for-developers-39375886.md
confluence_id: 39375886
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39375886
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39375886
date: "2016-10-10"
---
# JIRA Cloud : Welcome to JIRA Cloud development

#### Extend, enhance, and automate JIRA Software, JIRA Service Desk, and JIRA Core.

|              |                 |
|--------------|-----------------|
|![Alt text](../../../../illustrations/atlassian-objects-11.png) | **[Build your first JIRA Cloud add-on]** |

## JIRA Cloud platform

|              |                 |
|--------------|-----------------|
| ![Alt text](../../../../illustrations/atlassian-objects-36.png) | Atlassian Connect and the JIRA Cloud platform are the foundation for add-ons for any JIRA product. Learn about [integrating with JIRA Cloud].<br>Other references:<br>[JIRA platform REST API]<br>[JIRA platform webhooks] |


## JIRA Cloud products

|              |                 |
|--------------|-----------------|
| ![Alt text](../../../../illustrations/atlassian-software-13.png) | **JIRA Software**: Build awesome add-ons for software teams with a dedicated API and webhooks for JIRA Software features like boards, backlogs, and estimation. Learn about [integrating with JIRA Software].<br>Other references:<br>[JIRA Software REST API]<br>[JIRA Software webhooks] |
| ![Alt text](../../../../illustrations/atlassian-software-51.png) | **JIRA Service Desk**: Build awesome add-ons for IT teams with a dedicated API and webhooks for features like queues, SLAs, and the customer portal. Learn about [integrating with JIRA Service Desk].<br>Other references:<br>[JIRA Service Desk REST API]<br>[JIRA Service Desk webhooks] |

  [Build your first JIRA Cloud add-on]: /cloud/jira/platform/getting-started
  [integrating with JIRA Cloud]: /cloud/jira/platform/integrating-with-jira-cloud
  [JIRA platform webhooks]: /cloud/jira/platform/webhooks
  [JIRA platform REST API]: https://docs.atlassian.com/jira/REST/cloud/
  [integrating with JIRA Software]: /cloud/jira/software/
  [JIRA Software REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [JIRA Software webhooks]: /cloud/jira/software/jira-software-webhooks
  [integrating with JIRA Service Desk]: /cloud/jira/service-desk
  [JIRA Service Desk REST API]: https://docs.atlassian.com/jira-servicedesk/REST/cloud/
  [JIRA Service Desk webhooks]: /cloud/jira/service-desk/jira-service-desk-webhooks
  [Atlassian Design Guidelines]: https://design.atlassian.com/product/
  [Atlassian User Interface (AUI)]: https://docs.atlassian.com/aui/latest/
  [Atlassian Connect Google group]: https://groups.google.com/forum/#!forum/atlassian-connect-dev
  [Atlassian answers]: https://answers.atlassian.com/questions/topics/754005/atlassian-connect
  [Atlassian Developer Help Centre﻿]: https://ecosystem.atlassian.net/servicedesk/customer/portals