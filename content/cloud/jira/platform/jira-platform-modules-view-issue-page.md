---
title: JIRA Platform Modules View Issue Page
platform: cloud
product: jiracloud
category: reference
subcategory: modules 
aliases:
- /jiracloud/jira-platform-modules-view-issue-page-39988380.html
- /jiracloud/jira-platform-modules-view-issue-page-39988380.md
confluence_id: 39988380
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988380
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988380
date: "2016-09-30"
---
# Extension points for 'View Issue' page

This pages lists the JIRA platform modules for the view issue page.

**On this page:**

-   [Issue actions locations]
-   [Right-side of the 'View Issue' page location]

## Issue actions locations

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>The <code>jira.issue.tools</code> and <code>operations-*</code> locations together define the actions (share, export, more) on the top left of the 'View Issue' page.</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webItem</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><ul>
<li><code>jira.issue.tools</code> &mdash; more (<img src="../images/ellipsis.png"/>) menu on top right of 'View Issue' page.</li>
<li><code>/operations-top-level, /operations-work, /operations-attachments, /operations-voteswatchers, /operations-subtasks, /operations-operations, /operations-delete</code> &mdash; sections in the more (<img src="../images/ellipsis.png"/>) menu (see screenshot below).</li>
</ul></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jdev-jiraissuetoolsops.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
<pre class="javascript" style="font-size:12px;"><code>...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "jira.issue.tools",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "jira.issue.tools/operations-operations",
            "name": {
                "value": "Example add-on link"
            },
            "url": "/example-section-link
        }
    ]
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Right-side of the 'View Issue' page location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Defines web panels for the right hand side of the 'View Issue' page, as shown in the screenshot below.</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td> atl.jira.view.issue.right.context</td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p> <img src="../images/jdev-viewissueright-location.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
<pre class="javascript" style="font-size:12px;"><code>...
&quot;modules&quot;: {
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;example-panel&quot;,
            &quot;url&quot;: &quot;/my-example-panel&quot;,
            &quot;location&quot;: &quot;atl.jira.view.issue.right.context&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example panel&quot;
            }
        }
    ]   
}
...</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Issue actions locations]: #issue-actions-locations
  [Right-side of the 'View Issue' page location]: #right-side-of-the-view-issue-page-location
