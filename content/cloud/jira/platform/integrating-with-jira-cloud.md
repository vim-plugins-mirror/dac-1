---
title: Integrating with JIRA Cloud
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/integrating-with-jira-cloud-43648301.html
- /jiracloud/integrating-with-jira-cloud-43648301.md    
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?pageId=43648301
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?pageId=43648301
confluence_id: 43648301
date: "2016-09-30"
---
# Integrating with JIRA Cloud

If you want to integrate with JIRA Cloud, then **Atlassian Connect** is the solution you are looking for! Atlassian Connect is an extensibility framework that handles discovery, installation, authentication, and seamless integration into the JIRA UI. An Atlassian Connect add-on could be an integration with another existing service, new features for JIRA, or even a new product that runs within JIRA. 

This overview will introduce you to the Atlassian Connect framework. It will also cover the JIRA features and services that you can use when building an add-on (as shown in the diagram below).

![Alt text](../images/jdev-jiraconnectdiagram.jpeg)

We recommend that you read this entire page if you are new to JIRA Cloud development, but if you want to jump to a specific topic, use the links below:

-   [Atlassian Connect](#introduction): *The framework for integrating with Atlassian Cloud applications, including registration, authorization and authentication, and more.*
-   [Modules](#modules): *Integration points for interacting with JIRA, including the JIRA UI*
-   [Entity properties](#entity-properties)\*: *Key-values stores in JIRA*
-   [REST APIs](#rest-apis)\*: *This includes REST APIs for the JIRA platform and the JIRA products.
-   [Webhooks](#webhooks)\*: User-defined callbacks over HTTP
-	[JIRA UI tools](#jira-ui-tools): Tools for working with the JIRA UI, like the Atlassian Connect JS API, conditions, and context parameters.

*\* These features and services can be used independently of Atlassian Connect, if you need to implement an integration without Atlassian Connect. *

## Atlassian Connect

### Introduction

The Atlassian Connect framework is used to build add-ons for Atlassian Cloud. Fundamentally, add-ons can do following:

-   Add content or features, like pages, panels, reports, JQL functions, gadgets, in certain defined places in the Atlassian application's UI via modules.
-   Make secure requests to the Atlassian application's REST API.
-   Listen and respond to webhooks fired by the Atlassian application.

You can write an add-on with any programming language and web framework, and deploy it in almost any way that you can imagine. From massive SaaS services to static apps served directly from a code repository, Atlassian Connect was designed to allow you to connect anything to Atlassian products.

For example, the <a href="https://marketplace.atlassian.com/plugins/whoslooking-connect/cloud/overview" class="external-link">&quot;Who's looking&quot; add-on</a> (*<a href="https://bitbucket.org/atlassian/whoslooking-connect" class="external-link">source</a>*) is a Java application that runs on the Play framework and is hosted on Heroku. It uses the JIRA REST API to retrieve the details of users viewing an issue, persists it locally (checking if users are still watching via an XHR heartbeat), and displays it in a web panel module on the JIRA view issue screen.

### Technical overview

Atlassian Connect add-ons are essentially web applications that operate remotely over HTTP. To an end user, an add-on appears as a fully integrated part of the Atlassian application. Once your add-on is registered with the application, features are delivered from the UI and workflows of the host application. This deep level of integration is part of what makes Atlassian Connect add-ons so powerful.

![Alt text](../images/jdev-connectarchitecture.png)

Most Atlassian Connect add-ons are implemented as multi-tenanted services. This means that a single add-on will support multiple Atlassian Cloud tenants. For more information on how Atlassian Cloud applications work, see [Architecture overview].

#### The add-on descriptor

The basic building block of an Atlassian Connect add-on is the add-on descriptor. The add-on descriptor is a JSON file that is served by the add-on, which does the following:

-   provides basic information about the add-on (name, vendor, where it's hosted, etc),
-   handles the installation lifecycle,
-   handles authorization (scopes) and authentication, and
-   describes the modules that your add-on uses to the application (read more about [modules](#modules) below).

For more information on how the add-on descriptor works, see [Add-on descriptor].

### Developing with Atlassian Connect

You can use Atlassian Connect to build a range of integrations with different Atlassian Cloud applications. However, regardless of what you are building, you'll need to understand the following key development processes:

|              |                 |
|--------------|-----------------|
| **Implementing security for your add-on (authentication and authorization)** | Security is critical in a distributed component model such as Atlassian Connect. Atlassian Connect relies on HTTPS and JWT authentication to secure communication between your add-on, the Atlassian product and the user.<br>Your add-on's actions are constrained by well-defined permissions. Your add-on can only make API requests based on the scopes in its descriptor. These permissions are granted by Atlassian application administrators when they install your add-on. Examples of permissions include reading content, creating pages, creating issues, and more. These permissions help ensure the security and stability of cloud instances.<br>Read our [security overview] for more details. |
| **Registering your add-on with the Atlassian marketplace** | List your add-on on the [Atlassian marketplace] to make it installable. You can list your add-on privately on the site if you don't intend to sell or distribute your code, but all add-ons need to be listed in order to be installable. Private listings are supported with secret tokens that you can generate yourself.<br>Read our [selling on marketplace page] for more details. |
| **Designing your add-on** | Since Atlassian Connect add-ons can insert content directly into the Atlassian host application, it is critical that add-ons are visually compatible with the Atlassian application's design. Our designers and developers have created a number of resources to help you with this:<ul><li><a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> — Our design guidelines define core interactions with the Atlassian applications.</li><li><a href="https://docs.atlassian.com/aui/" class="external-link">Atlassian User Interface (AUI)</a> — AUI is a library of reusable front-end UI components.</li></ul> |
| **Implementing licensing for your add-on** | Every request from the Atlassian application to your add-on contains the add-on license status for that instance. Your add-on can respond appropriately, for example, by alerting the user, locking down functionality, or encouraging an upgrade.<br>Read our [licensing guide] for more details. |

## Modules

Modules are the most important component of your JIRA add-on or integration. Simply put, these are the **integration points** that your add-on uses to provide rich interactions with JIRA. There are two types of modules: basic iframes that allow you to display content in different places in JIRA, and more advanced modules that let you provide advanced JIRA-specific functionality. 

|              |                 |
|--------------|-----------------|
| **Basic modules** | *These modules can be placed in a number of JIRA locations. Learn more about [UI module locations].*<ul><li><strong><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/common/dialog.html">Dialog</a></strong>: show content inside of a modal dialog</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/common/page.html"><strong>Page</strong></a>: display content on a full screen page within JIRA</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-panel.html"><strong>Web panel</strong></a>: display content in a panel (like on the View Issue screen)</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-item.html"><strong>Web item</strong></a>: add a link or button to a defined location in JIRA (usually used on conjunction with dialogs or pages)</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-section.html"><strong>Web section</strong></a>: define a new section to add multiple web items</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/common/webhook.html"><strong>Webhook</strong></a>: see the <a href="#webhooks">section on webhooks</a> below</li></ul> |
| **Advanced modules** | *These advanced modules provide access to pre-defined locations or features in JIRA.*<ul><li><strong><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/dashboard-item.html">Dashboard item</a></strong>: provide a new gadget to display on JIRA dashboards</li><li><a href="/jiracloud/integrating-with-jira-cloud-43648301.html"><strong>Entity property</strong></a>: see the section about <a href="#entity-properties">storing data in JIRA</a> below</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/global-permission.html"><strong>Global permission</strong></a>: define a new global permission in JIRA</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-admin-tab-panel.html"><strong>Project admin tab pane</strong></a>l: add a new page to JIRA project settings</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/project-permission.html"><strong>Project permission</strong></a>: define a new project-level permission in JIRA</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/report.html"><strong>Report</strong></a>: add a new type of report to the JIRA reports page</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/search-request-view.html"><strong>Search request view</strong></a>: render a custom view of a search result that's accessible from the JIRA issue navigator</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/tab-panel.html"><strong>Tab panel</strong></a>: add panels to the JIRA project sidebar, user profile page, or view issue page</li><li><a href="https://developer.atlassian.com/static/connect/docs/latest/modules/jira/workflow-post-function.html"><strong>Workflow post-function</strong></a>: add a new post-function to a JIRA workflow</li></ul> |


## Entity properties

Entity properties are key-value stores attached to JIRA objects, that can be created, updated, and deleted via the JIRA REST APIs. This is a powerful system for storing data on the JIRA host; it easily support imports, exports, and migrations between instances because the data is stored locally with the JIRA Cloud tenant. Here's how entity properties can help you with your JIRA Cloud integration:

-   Simplify your add-on or integration by reducing how much data you store on your own server.
-   Improve performance by evaluating `entity_property_equal_to` conditions in-process.
-   Implement advanced features like [JQL integration using search extractions].

The following entities can have entity properties added to them:

|              |                 |
|--------------|-----------------|
| **JIRA (all products)** | <ul><li>issues</li><li>comments</li><li>projects</li><li>users</li><li>issue types</li><li>dashboard items</li></ul> |
| **JIRA Software** | <ul><li>sprints</li><li>boards</li></ul> |
| **Atlassian Connect add-ons** | Atlassian Connect provides <strong>add-on properties</strong> that are scoped specifically to your add-on and only accessible by you. This is a great place to store tenant-specific configuration data.<br>Read our documentation on [add-on properties] for more details.  |

To learn more about JIRA entity properties, read [entity properties]. 

## REST APIs

### JIRA REST APIs

The JIRA REST APIs are used to interact with JIRA remotely. In fact, for most communication with JIRA Cloud, you will use the JIRA REST APIs and webhooks. 

|                                                                                                                                  |                                                                                                                                                                                            |
|----------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **[JIRA platform Cloud REST API]** | The JIRA platform REST API covers most of the primary operations with basic JIRA objects, like issues, projects, users, dashboard, and more. This REST API is common to all JIRA products. |
| **[JIRA Software Cloud REST API]** | The JIRA Software REST API covers advanced features specific to JIRA Software, like boards, sprints, epics, and more. |
| **[JIRA Service Desk Cloud REST API]** | The JIRA Service Desk REST API covers advanced features specific to JIRA Service Desk, like customer requests, queues, SLAs, and more. |

## Webhooks

Webhooks are outgoing messages from JIRA that allow your add-on or integration to react to events, like someone transitioning an issue or closing a sprint. Like the REST APIs, there is a set of platform-level webhooks and additional advanced webhooks for JIRA Software and JIRA Service Desk.

To learn more about webhooks, read [webhooks]. 

## JIRA UI tools

### Atlassian Connect JavaScript API

The Atlassian Connect framework provides a JavaScript API to allow your add-on iframe to interact with the content around it on the page in JIRA. The Atlassian Connect JavaScript API is provided by JIRA; simply include the `all.js` file in the scripts in each of your pages. The JavaScript API includes methods for:

-   messaging between multiple iframes
-   automatic resizing for your content
-   advanced JIRA UI actions like opening date pickers and the create issue dialog
-   making XHR requests to JIRA REST resources without requiring CORS
-   and much more!

To learn more, read the [Atlassian Connect JavaScript API documentation].

### Conditions

More often than not, you will not want to load your modules on every page, for every user, every time. You can use **conditions** in your add-on descriptor to determine whether JIRA should load your add-on's UI modules. For example, you may only want to load a certain panel if the current user is an administrator, or if a certain property on an issue has been configured. 

Setting up conditions is simple: you define which conditions are applicable when you declare each module in your add-on descriptor, and JIRA will evaluate them in-process when the page is loaded. For example:

``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in"
                    }
                ]
            }
        ]
    }
}
```

To learn more, read [conditions].

### Context parameters

Frequently, the content you show in a panel will vary depending on the context around it (like the current issue, board, user, or project). When JIRA makes a request to your add-on to load an iframe, it can send additional context parameters that can tell your add-on how to respond to the request and which content to load. There are some standard context parameters (like user language and timezone) as well as JIRA specific parameters. 

To learn more, read [context parameters].

## Get started

Time to get hands-on with JIRA Cloud development! Read our [getting started guide] to learn how to set up a development environment and build a JIRA Cloud add-on.

  [Architecture overview]: /cloud/jira/platform/architecture-overview
  [Add-on descriptor]: /cloud/jira/platform/jira-platform-modules
  [security overview]: /cloud/jira/platform/security-overview
  [selling on marketplace page]: https://developer.atlassian.com/static/connect/docs/latest/developing/selling-on-marketplace.html
  [licensing guide]: https://developer.atlassian.com/static/connect/docs/latest/concepts/licensing.html
  [UI module locations]: /cloud/jira/platform/jira-platform-modules 
  [Dialog]: /cloud/jira/platform/jira-platform-modules/dialog
  [**Page**]: /cloud/jira/platform/jira-platform-modules/page
  [**Web panel**]: /cloud/jira/platform/jira-platform-modules/web-panel
  [**Web item**]: /cloud/jira/platform/jira-platform-modules/web-item
  [**Web section**]: /cloud/jira/platform/jira-platform-modules/web-section
  [**Webhook**]: /cloud/jira/platform/jira-platform-modules/webhook
  [Dashboard item]: /cloud/jira/platform/jira-platform-modules/dashboard-item
  [**Global permission**]: /cloud/jira/platform/jira-platform-modules/global-permission
  [**Project admin tab pane**]: /cloud/jira/platform/jira-platform-modules/project-admin-tab-panel
  [**Project permission**]: /cloud/jira/platform/jira-platform-modules/project-permission
  [**Report**]: /cloud/jira/platform/jira-platform-modules/report
  [**Search request view**]: /cloud/jira/platform/jira-platform-modules/search-request-view
  [**Tab panel**]: /cloud/jira/platform/jira-platform-modules/tab-panel
  [**Workflow post-function**]: /cloud/jira/platform/jira-platform-modules/workflow-post-function
  [conditions]: /cloud/jira/platform/conditions
  [context parameters]: /cloud/jira/platform/context-parameters
  [JQL integration using search extractions]: /cloud/jira/platform/jira-entity-properties
  [add-on properties]: https://developer.atlassian.com/static/connect/docs/latest/concepts/hosted-data-storage.html#add-on-properties
  [entity properties]: /cloud/jira/platform/jira-entity-properties
  [JIRA platform Cloud REST API]: https://docs.atlassian.com/jira/REST/cloud/
  [JIRA Software Cloud REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [JIRA Service Desk Cloud REST API]: https://docs.atlassian.com/jira-servicedesk/REST/cloud/
  [Atlassian Connect JavaScript API documentation]: https://developer.atlassian.com/static/connect/docs/latest/concepts/javascript-api.html
  [webhooks]: /cloud/jira/platform/webhooks
  [getting started guide]: /cloud/jira/platform/getting-started

