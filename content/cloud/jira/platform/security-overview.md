---
title: Security overview
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
- /jiracloud/security-overview.html
- /jiracloud/security-overview.md
date: "2016-10-10"
---
# JIRA Cloud : Security overview

Implementing security is an essential part of integrating with JIRA Cloud. It lets Atlassian applications protect customer data from unauthorized access and from malicious or accidental changes. It also allows administrators to install add-ons with confidence, letting users enjoy the benefits of add-ons in a secure manner.

There are two parts to securing your JIRA add-on or integration: authentication and authorization. Authentication tells JIRA Cloud the identity of your add-on, authorization determines what actions it can take within JIRA.

**On this page:**

-   [Authentication](#authentication)
	- [Authentication for add-ons](#authentication-for-add-ons)
	- [Authentication for REST API requests](#authentication-for-rest-api-requests)
-   [Authorization](#authentication)
	- [Authorization for add-ons](#authorization-for-add-ons)
	- [Authorization for REST API requests](#authorization-for-rest-api-requests)

## Authentication

Authentication is the process of identifying your add-on or integration to the Atlassian application and is the basis for all other security. JIRA Cloud offers several authentication patterns, depending on whether you are building an Atlassian Connect add-on or calling the JIRA REST APIs directly.

### Authentication for add-ons

Atlassian Connect uses JWT (JSON Web Tokens) to authenticate add-ons. This is built into the supported Atlassian Connect libraries.

When an add-on is installed, a security context is exchanged with the application. This context is used to create and validate JWT tokens, embedded in API calls. The use of JWT tokens guarantees that:

*   JIRA Cloud can verify it is talking to the add-on, and vice versa (authenticity).
*   None of the query parameters of the HTTP request, nor the path (excluding the context path), nor the HTTP method, were altered in transit (integrity).

To learn more, read our page on [authentication for add-ons].

### Authentication for REST API requests

The JIRA platform, JIRA Software, and JIRA Service Desk REST APIs can use one of the following three methods to authenticate clients directly. 

|  |  |
|------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| OAuth (1.0a) authentication | OAuth is a token-based authentication method, that uses request tokens generated from JIRA Cloud to authenticate the client. We recommend that you use OAuth in most cases. It takes more effort to implement, but it is more flexible and secure compared to the other two authentication methods. <br>Read the [OAuth tutorial]. |
| Basic authentication | Basic authentication uses a pre-defined set of user credentials to authenticate the client. We recommend that you don't use basic authentication, except for tools like personal scripts or bots. It may be easier to implement, but it is much less secure. <br>Read the [Basic authentication tutorial]. |
| Cookie-based authentication | Cookie-based authentication uses basic authentication to obtain a session cookie, then uses the cookie for additional requests. We recommend that you do not use cookie-based authentication in most cases, as OAuth is generally better. <br>Read the [Cookie-based authentication tutorial]. |

## Authorization

Authorization is the process of allowing your add-on or integration to take certain actions in the Atlassian application, after it has been authenticated. JIRA Cloud has different types of authorization, depending on whether you are building an Atlassian Connect add-on or calling the JIRA REST APIs directly.

### Authorization for add-ons

Atlassian Connect provides two types of authorization: 

*   **Authorization via scopes and add-on users** — This is the default authorization method. You should use this for most cases.
*   **Authorization via JWT bearer token authorization grant type for OAuth 2.0** — You should only use this method when your add-on needs to make server-to-server requests on behalf of a user.

#### Authorization for add-ons via scopes and add-on users

This method has two levels of authorization: static authorization via scopes and run-time authorization via add-on users. 

| | |
|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Scopes       | Scopes are defined in the add-on descriptor and statically specify the maximum set of actions that an add-on may perform: read, write, etc. This security level is enforced by Atlassian Connect and cannot be bypassed by add-on implementations. To learn more, read our page on [scopes]. |
| Add-on users | Every add-on is assigned its own user in a Cloud instance. In general, server-to-server requests are made by the add-on user. This allows an administrator to restrict an add-on's access to certain projects and issues, just as they would for any other user. To learn more, read our [architecture overview]. <br> Note, it is possible to make a request on behalf of the current user, rather than the add-on user. To do this, the request needs to be a client-side REST API request (using the [`AP.request()` method]) or the add-on needs to make a server-to-server request using an OAuth 2.0 bearer token (see next section). |

{{% tip title="Combining static and run-time authorization"%}}The set of actions that an add-on is capable of performing is the intersection of the static scopes and the permissions of the user assigned to the request. This means that requests can be rejected because the assigned user lacks the required permissions. Therefore, your add-on should always defensively detect HTTP 403 forbidden responses from the product.{{% /tip %}}

#### Authorization for add-ons via JWT bearer token authorization grant type for OAuth 2.0

At a high level, this method works by the add-on exchanging a JWT for an OAuth 2.0 access token (provided by the application). The access token can be used to make server-to-server calls, on behalf of the user, to the application's API.

To learn more, read our page on [OAuth 2.0 - JWT Bearer token authorization grant type].  

### Authorization for REST API requests

Authorization for direct calls to the JIRA Cloud REST APIs is based on the user used in the authentication process:

|  |  |
|------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| OAuth | The user who authorizes the request token, in the authentication process for the client, is used to make requests to JIRA Cloud. |
| Basic authentication and Cookie-based authentication | The actions your client can perform are based on the permissions of the user credentials you are using. For example, if you are using basic authentication, your user must have Edit Issue permission on an issue in order to make a PUT request to the `/issue` resource. |

[authentication for add-ons]: /cloud/jira/platform/addon-authentication
[OAuth tutorial]: /cloud/jira/platform/jira-rest-api-oauth-authentication
[Basic authentication tutorial]: /cloud/jira/platform/jira-rest-api-basic-authentication
[Cookie-based authentication tutorial]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication
[scopes]: /cloud/jira/platform/jira-rest-api-scopes
[architecture overview]: /cloud/jira/platform/architecture-overview
[`AP.request()` method]: /cloud/jira/platform/request
[OAuth 2.0 - JWT Bearer token authorization grant type]: /cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type