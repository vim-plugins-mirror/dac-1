---
title: JIRA Software Modules 
platform: cloud
product: jswcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-software-modules-39987281.html
- /jiracloud/jira-software-modules-39987281.md
confluence_id: 39987281
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987281
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987281
date: "2016-09-19"
---
# About JIRA Software modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows.

The JIRA platform and JIRA Service Desk also have their own modules. For more information, see the following pages:

-   [JIRA platform modules]
-   [JIRA Service Desk modules]

## Using JIRA Software modules

You can use a JIRA Software module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `webPanel` module at the `jira.agile.board.configuration` location in your add-on, which creates a new board configuration page in JIRA Software.

**atlassian-connect.json**

``` java
...
"modules": {       
    "webPanels": [
        {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "location": "jira.agile.board.configuration",
            "name": {
                "value": "My board configuration page"
            },
            "weight": 1
        }
    ]
}
...
```

## JIRA Software modules

-   [JIRA Software modules - Boards]

 

  [JIRA platform modules]: /cloud/jira/platform/jira-platform-modules
  [JIRA Service Desk modules]: /cloud/jira/service-desk/jira-service-desk-modules
  [JIRA Software modules - Boards]: https://developer.atlassian.com/display/jiracloud/JIRA+Software+modules+-+Boards
