# Deprecation notices

### Installation lifecycle attributes

The [installation lifecycle payload](../modules/lifecycle.html) includes a field for the product public key. This field
is deprecated and should not be used.

<div class="ac-deprecations">
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Deprecated in</h5>
    </div>
    <div class="aui-item">
        <span class="aui-lozenge">1.1.103</span>
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Atlassian Cloud removal</h5>
    </div>
    <div class="aui-item">
        __June, 2017__
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Upgrade guide</h5>
    </div>
    <div class="aui-item">
        Ignore the `publicKey` property from the installation event.
    </div>
</div>
</div>

### Remote conditions

<div class="ac-deprecations">
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Deprecated in</h5>
    </div>
    <div class="aui-item">
        <span class="aui-lozenge">1.1.95</span>
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Atlassian Cloud removal</h5>
    </div>
    <div class="aui-item">
        __December, 2016__
    </div>
</div>
</div>

### `server_upgraded` and `plugins_upgraded` webhooks

<div class="ac-deprecations">
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Deprecated in</h5>
    </div>
    <div class="aui-item">
        <span class="aui-lozenge">1.1.81</span>
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Atlassian Cloud removal</h5>
    </div>
    <div class="aui-item">
        __September, 2016__
    </div>
</div>
</div>


### `/license` REST API resource

<div class="ac-deprecations">
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Deprecated in</h5>
    </div>
    <div class="aui-item">
        <span class="aui-lozenge">1.1.21</span>
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Atlassian Cloud removal</h5>
    </div>
    <div class="aui-item">
        __August, 2015__
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Upgrade guide</h5>
    </div>
    <div class="aui-item">
        Use the newly available REST API resource [`/addons/{addonKey}`](../rest-apis#get-addons-addonkey) instead.
    </div>
</div>
</div>

### JIRA REST API

Both global and project permissions are changing from integer based to a key based representation. The following REST resources are affected: 
<ul>
    <li>
        /rest/api/v2/mypermissions<br/>
        A `type` attribute has been added to each permission, indicating whether it is a `PROJECT` or `GLOBAL` one. <br/>
        The `id` attribute is being deprecated and will be removed in the future.
    </li>
</ul>

<div class="ac-deprecations">
    <div class="aui-group">
        <div class="aui-item ac-property-key">
            <h5>Deprecated in</h5>
        </div>
        <div class="aui-item">
            <span class="aui-lozenge">JIRA 7.0-OD4</span>
        </div>
    </div>
    <div class="aui-group">
        <div class="aui-item ac-property-key">
            <h5>Atlassian Cloud removal</h5>
        </div>
        <div class="aui-item">
            __January, 2015__
        </div>
    </div>
    <div class="aui-group">
        <div class="aui-item ac-property-key">
            <h5>Upgrade guide</h5>
        </div>
        <div class="aui-item">
            We recommend identifying a permission by its `key` instead of its `id`. 
            `GlobalPermissionKey` and `ProjectPermissionKey` are the key types for global and project permissions respectively.
        </div>
    </div>
</div>

### Confluence `page.*` context variables

The `page.id`, `page.version`, `page.type` context variables available in Confluence have been deprecated in favour of
`content.id`, `content.version` and `content.type` variables respectively.

<div class="ac-deprecations">
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Deprecated in</h5>
    </div>
    <div class="aui-item">
        <span class="aui-lozenge">1.1.0-final</span>
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Atlassian Cloud removal</h5>
    </div>
    <div class="aui-item">
        __2015__
    </div>
</div>
<div class="aui-group">
    <div class="aui-item ac-property-key">
        <h5>Upgrade guide</h5>
    </div>
    <div class="aui-item">
        <p>Use the newly available `content.*` variables, documented in <a href="../concepts/context-parameters.html">Context Parameters</a>.</p>
    </div>
</div>
</div>
