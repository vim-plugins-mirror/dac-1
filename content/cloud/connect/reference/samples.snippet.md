# Sample add-ons

The following repositories contain sample Atlassian Connect add-ons. This list is growing all the time, so check back often.

Please be aware that this list only contains Sample add-ons. Go to the page on [Frameworks and Tools](../developing/frameworks-and-tools.html)
if you want to find frameworks, libraries and tools that you can use in your Atlassian Connect development.

<table class='aui aui-table-sortable'>
    <thead>
        <th>Add-on</th>
        <th>Language</th>
        <th>Framework(s)</th>
        <th class='aui-table-column-unsortable'>Description</th>
    </thead>
    <tbody>
        <tr>
            <td>[Sequence Diagramr](https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-sequence-diagramr)</td>
            <td>Javascript</td>
            <td>[Atlassian Connect Express (ACE)][2] - [Node.js][1]</td>
            <td>A Confluence macro for creating UML sequence diagrams on the page.</td>
        </tr>
        <tr>
            <td>[Tim's Word Cloud](https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-word-cloud)</td>
            <td>Javascript</td>
            <td>[Atlassian Connect Express (ACE)][2] - [Node.js][1]</td>
            <td>A macro that takes the contents of a page and constructs an SVG based word cloud.</td>
        </tr>
        <tr>
            <td>[Webhook Inspector](https://bitbucket.org/atlassianlabs/webhook-inspector)</td>
            <td>Javascript</td>
            <td>[Atlassian Connect Express (ACE)][2] - [Node.js][1]</td>
            <td>Inspects the response bodies of the available webhooks in Atlassian Connect.</td>
        </tr>
        <tr>
            <td>[Entity Property Tool](https://bitbucket.org/robertmassaioli/ep-tool)</td>
            <td>Javascript</td>
            <td>[Node.js][1]</td>
            <td>An Atlassian Connect add-on that allows manipulation of entity properties in JIRA.</td>
        </tr>
        <tr>
            <td>[Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview)</td>
            <td>Java</td>
            <td>[Atlassian Connect Play Java][3] - [Play Framework][4]</td>
            <td>Displays the users who are currently looking at a JIRA issue.</td>
        </tr>
        <tr>
            <td>[JIRA Autowatcher](https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin)</td>
            <td>Java</td>
            <td>[Atlassian Connect Play Java][3] - [Play Framework][4]</td>
            <td>Automatically add watchers to newly created JIRAs</td>
        </tr>
        <tr>
            <td>[Hackathon](https://bitbucket.org/robertmassaioli/hackathon)</td>
            <td>[Haskell][5]</td>
            <td>[Snap framework][6]</td>
            <td>An add-on to run any Hackathon via JIRA. A feature rich sample add-on with tight integration with JIRA.
                This is the add-on that Atlassian uses for [ShipIt][7].
            </td>
        </tr>
        <tr>
            <td>[My Reminders](https://bitbucket.org/atlassianlabs/my-reminders/overview)</td>
            <td>[Haskell][5]</td>
            <td>[Snap framework][6]</td>
            <td>Sends you reminders about JIRA issues</td>
        </tr>
        <tr>
            <td>[Google Maps macro](https://bitbucket.org/acgmaps/acgmaps.bitbucket.org)</td>
            <td>HTML / CSS / Javascript (Static add-on)</td>
            <td>No framework<br />(Static add-on)</td>
            <td>Insert Google Maps into your Confluence pages</td>
        </tr>
        <tr>
            <td>[Sketchfab for Confluence](https://bitbucket.org/robertmassaioli/sketchfab-connect)</td>
            <td>HTML / CSS / Javascript (Static add-on)</td>
            <td>No framework<br />(Static add-on)</td>
            <td>A static Atlassian Connect add-on that renders Sketchfab 3D models in Confluence pages</td>
        </tr>
        <tr>
            <td>[Who's Looking Front-End Only Prototype](https://bitbucket.org/atlassianlabs/atlassian-connect-whoslooking-connect-v2)</td>
            <td>HTML / CSS / Javascript (Static add-on)</td>
            <td>No framework<br />(Static add-on)</td>
            <td>Similar functionality to the original [Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview) Play-based
                add-on, but written entirely using static HTML, JS & CSS using [Firebase](https://www.firebase.com/).</td>
        </tr>
        <tr>
            <td>[Who's Looking (Play Scala)](https://bitbucket.org/atlassianlabs/whoslooking-connect-scala)</td>
            <td>[Scala][8]</td>
            <td>[Play Scala framework][4]</td>
            <td>Same functionality to the original [Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview) Play-based
                Java add-on, but written in Scala, built on the Play Scala framework.</td>
        </tr>
        <tr>
            <td>[Atlas Lingo](https://bitbucket.org/atlassianlabs/atlas-lingo)</td>
            <td>[Scala][8]</td>
            <td>[Play Scala framework][4]</td>
            <td>Translate JIRA issues into other languages using Google Translate API.</td>
        </tr>
    </tbody>
</table>

*Note:* A simple Atlassian Connect addon can consist of entirely static content (HTML, CSS and Javascript). 
These add-ons do not have a framework in the traditional sense and have been marked as "Static add-on" in this list.



 [1]: https://nodejs.org
 [2]: https://bitbucket.org/atlassian/atlassian-connect-express
 [3]: https://bitbucket.org/atlassian/atlassian-connect-play-java
 [4]: https://www.playframework.com/
 [5]: https://www.haskell.org/
 [6]: http://snapframework.com/
 [7]: https://www.atlassian.com/company/shipit
 [8]: http://www.scala-lang.org/