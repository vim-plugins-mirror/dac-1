# OAuth 2.0 - JWT Bearer token authorization grant type
Connect supports the [JWT Bearer token](https://tools.ietf.org/html/rfc7523#section-2.1) authorization grant type for [OAuth 2.0](https://tools.ietf.org/html/rfc6749), allowing add-ons with the appropriate scope (`ACT_AS_USER`) to access resources and perform actions in JIRA and Confluence on behalf of users.

## Connect OAuth 2.0 JWT Bearer token authorization grant flow
The flow for accessing a user's resources works as follows: 
<table>
  <tbody>
  <tr>
    <td>
      <ol>
        <li>Install hook fires with the <code>oauthClientId</code> and the shared secret.</li>
        <li>Add-on creates a JWT assertion with the shared secret and the <code>oauthClientId</code>, and then POSTs it to the authorization server.</li>
        <li>Authorization server returns the OAuth 2.0 `access token`.</li>
        <li>Add-on uses the `access token` to perform actions as a user.</li>
        </ol>
   </td>
    <td><img src="/cloud/connect/images/connect-oauth-impersonation-flow.png" width="100%" style="border:1px solid #999;margin-top:10px"></td>
  </tr>
</tbody>
</table>

## Requesting an OAuth 2.0 access token
For an add-on to make requests on a user's behalf, you need an OAuth 2.0 `access token`.

The OAuth 2.0 JWT Bearer token flow involves the following general steps to retrieve a token:

1. Admin install the add-on, which initiates the [installation handshake](authentication.html) with the `oauthClientId` and the shared secret in the request body:
```
  {
      "key": "addon-key",
      "oauthClientId": "your-oauth2-client-id",
  }
```
2. The add-on creates an assertion, a JWT that is HMAC-SHA256 signed with the shared secret the add-on received during the [installation handshake](authentication.html), and adds the following claims in the payload:
<table class='aui'>
    <thead>
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tr>
        <td>`iss`</td>
        <td>String</td>
        <td>the issuer of the claim. For example:
        ```urn:atlassian:connect:clientid:{oauthClientId}```
        </td>
    </tr>
    <tr>
        <td>`sub`</td>
        <td>String</td>
        <td>The subject of the token. For example:
        ```urn:atlassian:connect:userkey:{userkey of the user to run services on behalf of}```
**NOTE:** The `userkey` is different from the `username`.
For example, to get the `userkey` for username alex use the REST endpoint `/rest/api/2/user?username=alex` in JIRA or `/rest/api/user?username=alex` in Confluence.
        </td>
    </tr>
    <tr>
        <td>`tnt`</td>
        <td>String</td>
        <td>The instance the add-on is installed on. For example:
        ```https://{your-instance}.atlassian.net```
        </td>
    </tr>
    <tr>
        <td>`aud`</td>
        <td>String</td>
        <td>The Atlassian authentication server. For example:
        ```https://auth.atlassian.io```
        </td>
    </tr>
    <tr>
        <td>`iat`</td>
        <td>Long</td>
        <td>Issue time in seconds since the epoch UTC.
        </td>
    </tr>
    <tr>
        <td>`exp`</td>
        <td>Long</td>
        <td>Expiry time in seconds since the epoch UTC. Must be no later that 60 seconds in the future.
        </td>
    </tr>
</table>
3. The assertion and the payload are POSTed to the authorization server: `https://auth.atlassian.io/oauth2/token` <br />
Example request:
```
POST /oauth2/token HTTP/1.1
Host: auth.atlassian.io
Accept: application/json
Content-Length: {length of the request body}
Content-Type: application/x-www-form-urlencoded
grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&scope=READ+WRITE&assertion={your-signed-jwt}
```
  * `grant_type` is the literal url-encoded `urn:ietf:params:oauth:grant-type:jwt-bearer`.
  * `assertion` is set to the assertion created in the previous step.
  * `scope` is space-delimited and capitalized. Tokens are only granted for scopes your add-on is authorized for. If you omit the scope, the request is interpreted as a request for an `access token` with all the scopes your add-on has been granted.
4. The token endpoint validates the signatures and issues an `access token`. <br />
Example response:
```
HTTP/1.1 200 OK
Status: 200 OK
Content-Type: application/json; charset=utf-8
...
{
    "access_token": "{your access token}",
    "expires_in": {15 minutes expressed as seconds},
    "token_type": "Bearer"
}
```

## Using an access token in a request
Set `Bearer` + `access token` to the authorization header and make your request:

Example request:
````
GET /rest/api/latest/myself HTTP/1.1
Host: {your-registered-instance}.atlassian.net
Accept: application/json
Authorization: Bearer {your-access-token}
````
Example response:
```
HTTP/1.1 200 OK
Status: 200 OK
Content-Type: application/json; charset=utf-8
...
{
  "key": "{key-of-user-to-run-services-on-behalf-of}",
  "displayName": "{impersonated user's display name}",
  ...
}
```
<div class="aui-message">
<p>
The example is from a JIRA REST resource—not every resource has these properties.
</p>
</div>

## Rate limiting
Add-ons are allowed 500 access token requests every 5 minutes for each host product the add-on is installed on.
If you exceed the limit, the server returns a 409 status response and a JSON formatted error message.

Current usage limits are returned in the following headers for every server response:
* `X-RateLimit-Limit` - The number of requests allowed for 5 minutes (set to 500).
* `X-RateLimit-Remaining` - The number of requests remaining before you hit the limit.
* `X-RateLimit-Reset` - Unix timestamp indicating when the limit will update. When this occurs, the `X-RateLimit-Remaining` count will reset to 500 and the `X-RateLimit-Reset` time will reset to 5 minutes in the future.

Example response:
```
HTTP/1.1 200 OK
X-RateLimit-Limit: 500
X-RateLimit-Remaining: 499
X-RateLimit-Reset: 1366037820
...
```

## Token expiration
The OAuth 2.0 `access token` expires after 15 minutes.
Write your code to anticipate the possibility that a granted token might no longer work.
We suggest tracking expiration time and requesting a new token before it expires, rather than handling a token expiration error. Refresh tokens at the 30-60 second mark.

## Atlassian-Connect-Express OAuth 2.0 support

### Getting an access token
Atlassian-Connect-Express provides functionality for the OAuth 2.0 JWT Bearer `access token` exchange:
```
var oauth2 = require('atlassian-oauth2');

var options = {
    hostBaseUrl: 'https://instance.atlassian.net',
    oauthClientId: '{oauth client id your add-on was given at installation}',
    sharedSecret: '{the shared secret your add-on was given at installation}',
    userKey: '{the userkey of the user to retreive the token for}',
    scopes: ['{an array of scopes requested}']
}

oauth2.getAccessToken(options).then(function (token) {
    console.log(token);
    // token.access_token = '{your access token}'
    // token.expires_in = '{expiry time}'
    // token.token_type = 'Bearer'
});
```

### Making requests for services on behalf of a user
Atlassian-Connect-Express provides the `.asUser` method to make requests on users' behalf.
````
var httpClient = addon.httpClient(req);
httpClient.asUser('userkey-of-user-to-act-as').get('/rest/api/latest/myself', function (err, res, body) {
  ...
})
````
