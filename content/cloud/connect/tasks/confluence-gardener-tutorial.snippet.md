## Tutorial: manage your Confluence instance

In this tutorial, you'll learn about:

* [Configuring your development environment](#environment)
* [Hosting your add-on locally](#hosting-locally)
* [Implement a Confluence REST API client](#rest-api)
* [Display a full-screen dialog](#dialog)

This tutorial shows you how to build a static Connect add-on that displays page hierarchy in a Confluence space. This 
add-on is handy to query, update, and delete Confluence pages. You'll also create a full-screen confirmation dialog
displaying content from your add-on.  

Your add-on will use the <a href="https://docs.atlassian.com/confluence/REST/latest/" target="_blank">
Confluence REST API</a>. At completion, your add-on will look a lot like this: 

<img src="/cloud/connect/images/confluence-gardener-screen.png" width="80%" style="border:1px solid #999;margin-top:10px;" />

## <a name="environment"></a> Configuring your development environment

This step confirms your development environment is configured correctly. You'll need <a href="http://git-scm.com/" 
target="_blank">Git</a>, <a href="http://nodejs.org/" target="_blank">Node.js</a>, and your
[Atlassian add-on development environment configured](#environment).

Once you have all the prerequisites, you'll clone an existing repository to kick things off.  

{{< include path="content/cloud/connect/tasks/install-git.snippet.md" >}}

{{< include path="content/cloud/connect/tasks/install-nodejs.snippet.md" >}}

1. Clone the Confluence Gardener repository. 
    ```shell
    $ git clone https://bitbucket.org/atlassianlabs/confluence-gardener.git
    ```
1. Change into your new `confluence-gardener` directory.  
    ``` shell
    $ cd confluence-gardener
    ```
1. Get a cloud development environment setup by following the [Development setup][1] walk-through.


## <a name="hosting-locally"></a> Host & install your add-on

Confluence Gardener is a static Connect add-on that can be served by a simple web server and cached with a CDN.
We'll use a simple Node.js-powered static web server to host your add-on locally.

After you've spun up your server, you'll install your copy of Gardener to Confluence. You'll use a Bash script 
included in the repo you cloned to install the add-on.   

1. From the `confluence-gardener` directory, start your server on port 8000:
```
$ npm install -g http-server
$ http-server -p 8000
```
The server indicates that it's serving HTTP at the current address and port. You'll see something like:
```
Starting up http-server, serving ./ on: http://0.0.0.0:8000
```
1. In your browser, navigate to your descriptor file at <a href="http://localhost:8000/atlassian-connect.json" 
    target="_blank">http://localhost:8000/atlassian-connect.json</a>  
``` javascript
    {
        "key": "confluence-gardener",
        "name": "Confluence Gardener",
        "description": "Prune back your Confluence page graph.",
        "baseUrl": "https://addon-dev-url.ngrok.io",
        "vendor": {
            "name": "Atlassian Labs",
            "url": "https://www.atlassian.com"
        },
        "authentication": {
            "type": "none"
        },
        "version": "0.1",
        "modules": {
            "generalPages": [
                 {
                     "key": "gardener",
                     "url": "/index.html?spaceKey={space.key}",
                     "location": "system.content.action",
                     "name": {
                         "value": "Confluence Gardener"
                     }
                 }
             ]
        },
        "scopes": [
            "read",
            "write",
            "delete"
        ]
    }
```
1. For the next step you will need to host your add-on so that it is accessible anywhere on the internet. Check out [Developing Locally][2] for a way to do this.
1. Install Gardener using the <a href="https://confluence.atlassian.com/x/8AJTE" target="_blank">Universal Plugin Manger (UPM)</a>: 
   *  Navigate to https://&lt;my-dev-environment&gt;.atlassian.net/wiki/plugins/servlet/UPM
   * Click <b>Upload add-on</b>.
   * Enter a url that points to your descriptor. For example <tt>https://&lt;my-addon-url&gt;.ngrok.io/atlassian-connect.json</tt>
   * Click <b>Upload</b>.
    Gardener doesn't have functionality yet (you'll implement that in future steps), but feel free to try to load it anyway. 
1. Create a space in your development version of Confluence and navigate to the space.
1. Click <b>Tools</b> at the top right, and choose <b>Confluence Gardener</b>.  
    You should see a page like this: 
    <img src="/cloud/connect/images/confluence-gardener-0.png" width="80%" style="border:1px solid #999;margin-top:10px;" />

Now you're ready to start developing functionality for your Gardener add-on. 

## <a name="rest-api"></a> Implement a Confluence REST API client

All the functions that request data from Confluence are in your `js/data.js` file. The functions are incomplete, 
so in this step you'll flesh these out. 

You'll use the [Confluence REST API Docs](https://docs.atlassian.com/atlassian-confluence/REST/latest/) and
the [AP.request documentation](../javascript/module-request.html) to implement functions to get
page and space hierarchy in Confluence, and add Gardener functionality to move and remove pages.

1. Open the `js/data.js` file from your `confluence-gardener` source code.
    You should see the stub code below:  
    ``` javascript
    define(function() {
        return {
            getPageContentHierarchy: function(pageId, callback) {
            },

            getSpaceHierarchy: function(spaceKey, callback) {
            },

            removePage: function(pageId, callback) {
            },

            movePage: function(pageId, newParentId, callback) {
            },

            movePageToTopLevel: function(pageId, spaceKey, callback) {
            }
        }
    });
    ```
1. Implement `getPageContentHierarchy` to get the page hierachy in your Confluence instance:
    ``` javascript
    getPageContentHierarchy: function(pageId, callback) {
        AP.request({
            url: "/rest/prototype/1/content/" + pageId + ".json?expand=children",
            success: callback
        });
    },
    ```
1.  Next, implement `getSpaceHierarchy` to see the space hierarchy: 
    ``` javascript
    getSpaceHierarchy: function(spaceKey, callback) {
        AP.request({
            url: "/rest/prototype/1/space/" + spaceKey + ".json?expand=rootpages",
            success: callback
        });
    },
    ```
1. Implement `removePage` so your add-on can effectively delete Confluence pages: 
    ``` javascript
    removePage: function(pageId, callback) {
        AP.request({
            url: "/rpc/json-rpc/confluenceservice-v2/removePage",
            contentType: "application/json",
            type: "POST",
            data: JSON.stringify([pageId]),
            success: callback
        });
    },
    ```
1. Finally, try to implement `movePage` and `movePageToTopLevel` on your own. 
    If you get stuck, take a look at the example below.  
    ``` javascript
    define(function() {
        return {
            getPageContentHierarchy: function(pageId, callback) {
                AP.request({
                    url: "/rest/prototype/1/content/" + pageId + ".json?expand=children",
                    success: callback
                });
            },

            getSpaceHierarchy: function(spaceKey, callback) {
                AP.request({
                    url: "/rest/prototype/1/space/" + spaceKey + ".json?expand=rootpages",
                    success: callback
                });
            },

            removePage: function(pageId, callback) {
                AP.request({
                    url: "/rpc/json-rpc/confluenceservice-v2/removePage",
                    contentType: "application/json",
                    type: "POST",
                    data: JSON.stringify([pageId]),
                    success: callback
                });
            },

            movePage: function (pageId, newParentId, callback) {
                AP.request({
                    url: "/rpc/json-rpc/confluenceservice-v2/movePage",
                    contentType: "application/json",
                    type: "POST",
                    data: JSON.stringify([pageId, newParentId, "append"]),
                    success: callback
                });
            },

            movePageToTopLevel: function(pageId, spaceKey, callback) {
                AP.request({
                    url: "/rpc/json-rpc/confluenceservice-v2/movePageToTopLevel",
                    contentType: "application/json",
                    type: "POST",
                    data: JSON.stringify([pageId, spaceKey]),
                    success: callback
                });
            }
        }
    });
    ```
1. Now, refresh Gardener in your browser.  
    You should see a page like this:  
    <img src="/cloud/connect/images/confluence-gardener-.5.png" width="80%" style="border:1px solid #999;margin-top:10px;" />  
    Dark blue names indicate the space names, and light blue signifies pages with children.  
1. Click a light blue name, like the __Welcome to the Confluence Demonstration Space__ page name.  
    You should see the menu snap open to display the page children:  
    <img src="/cloud/connect/images/confluence-gardener-1.png" width="80%" style="border:1px solid #999;margin-top:10px;" />  
    Blue pages have children, whereas grey pages have no child pages underneath. You can also use your mouse to zoom in and out - 
    just scroll your trackball up and down.   

As you explore your Gardener add-on, you might notice that you're not able to actually remove pages. Let's fix that in the next 
step. 

## <a name="dialog"></a> Display a full-screen dialog

When you attempt to remove a page, nothing happens. In this step, you'll add a [a full-screen 
dialog](../javascript/module-Dialog.html) to confirm the action,
and make sure it actually works. 

First, you'll declare the dialog in your `atlassian-connect.json` descriptor file. These dialogs are full-fledged AUI dialogs 
that exist in the parent frame (not in the same iframe Gardener uses). We'll provide the HTML source for the dialog, you'll 
register a new [`webItem`](../modules/common/web-item.html) that loads inside the
full-screen dialog.

1. Open `atlassian-connect.json` in your editor.  
1. Add the following snippet after the `generalPages` entry in the `modules` object:
    ``` json
    "webItems": [
        {
            "key": "gardener-remove-dialog",
            "url": "/remove-page-dialog.html",
            "location": "system.top.navigation.bar",
            "weight": 200,
            "context": "addon",
            "target": {
                "type": "dialog",
                "options": {
                    "width": "234px",
                    "height": "324px"
                }
            },
            "name": {
                "value": "dialog"
            }
        }
    ]
    ```
Atlassian-connect.json [+]</a> with the dialog `webItems` entry
    ``` json
    {
        "key": "confluence-gardener",
        "name": "Confluence Gardener",
        "description": "Prune back your Confluence page graph.",
        "baseUrl": "https://add-on-dev-url.ngrok.io",
        "vendor": {
            "name": "Atlassian Labs",
            "url": "https://www.atlassian.com"
        },
        "authentication": {
            "type": "none"
        },
        "version": "0.1",
        "modules": {
        "generalPages": [
             {
                 "key": "gardener",
                 "url": "/index.html?spaceKey={space.key}",
                 "location": "system.content.action",
                 "name": {
                     "value": "Confluence Gardener"
                 }
             }
         ],
         "webItems": [
             {
                 "key": "gardener-remove-dialog",
                 "url": "/remove-page-dialog.html",
                 "location": "system.top.navigation.bar",
                 "weight": 200,
                 "context": "addon",
                 "target": {
                     "type": "dialog",
                     "options": {
                         "width": "234px",
                         "height": "324px"
                     }
                 },
                 "name": {
                     "value": "dialog"
                 }
             }
         ]
        },
        "scopes": [
            "read",
            "write",
            "delete"
        ]
    }
    ```
1. Reinstall your add-on through the UPM **Manage add-ons** page.
    Your add-on won't show any changes - yet. In the next step, you'll implement a function to display the dialog.
1. In your editor, open `removePageDialog.js`. 
    You should see another empty function: 
    ``` javascript
    define(function() {
        return function (deleteCallback) {
        }
    });
    ```
1. Load the [`dialog`](../javascript/module-Dialog.html)
    and [`events`](../javascript/module-Events.html) modules using `AP.require`:
    ``` javascript
    return function(deleteCallback) {
        AP.require(["dialog", "events"], function (dialog, events) {
            dialog.create({
                key: 'gardener-remove-dialog',
                width: "400px",
                height: "80px",
                header: "Remove page?",
                submitText: "Remove",
                cancelText: "cancel",
                chrome: true
            }); 
    ```
1. After creating the dialog, unsubscribe from any previous `confirmPageRemoval` event bindings:
   <pre><code data-lang="javascript">events.offAll("confirmPageRemoval");</code></pre>
1. Now, try subscribing to `confirmPageRemoval` event bindings using `events.on` to register the event and
   passing `deleteCallback`. removePageDialog.js with subscription to `confirmPageRemoval`
``` javascript
    define(function() {
        return function(deleteCallback) {
            AP.require(["dialog", "events"], function (dialog, events) {
                dialog.create({
                    key: 'gardener-remove-dialog',
                    width: "400px",
                    height: "80px",
                    header: "Remove page?",
                    submitText: "Remove",
                    cancelText: "cancel",
                    chrome: true
                });

                events.offAll("confirmPageRemoval");
                events.on("confirmPageRemoval", deleteCallback);
            });
        }
    });
```
1. In Confluence Gardener, click __Remove__ on a page. 
    You should see a dialog appear: 
    <img src="/cloud/connect/images/confluence-gardener-dialog.png" width="80%" style="border:1px solid #999;margin-top:10px;" />  
    If you don't see a dialog, try a hard refresh, or turning off the cache in your browser developer console. 

## What's next? 

You've built an add-on that can help you prune pages from Confluence spaces. Now you're ready to explore our documentation.
How about reading through:

 * The [Javascript API][3].
 * The [Frameworks and Tools][4] available to you.

  [1]: ../guides/development-setup.html
  [2]: ../developing/developing-locally-ngrok.html
  [3]: ../concepts/javascript-api.html 
  [4]: ../developing/frameworks-and-tools.html 
