---
title: Showcase Wittified
category: casestudy
heading1: Taking the leap
byline: "Wittified builds a successful business on the Atlassian Marketplace"
lede: "An Appfire company, Wittified is an Atlassian Marketplace provider of more than 20 add-ons that solve common challenges around JIRA, Confluence and Bitbucket in three main areas: delegated administration, add-on development, and distributed communications."
---

With over a decade of experience developing and administering Atlassian tools, the Wittified founders truly understand the needs of Atlassian administrators.

## Creating a business on the Atlassian platform

"I had written a plugin that integrates Chef (a popular configuration management solution) with Atlassian Bamboo... and realized that I could make a business out of developing add-ons," says Wittified cofounder Daniel Wester. He got the idea while working for an enterprise organization and has since built a thriving software company on the Atlassian Marketplace.

The Wittified team surveys its growing customer base and follows support forums to find pain points within Atlassian products, as well as encourages users to send requests for new add-ons.

Wester also looks inward to his own development experience for inspiration: "I initially built our Web Fragment Finder add-on to solve my own challenges while trying to find extension points within each of Atlassian's products. Once I realized it was a common issue, I released [Web Fragment Finder](https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder/cloud/overview) on the Atlassian Marketplace so other add-on developers could benefit from it like I have."

Wittified maintains an exhaustive release cycle in order to ensure that its add-ons can support multiple versions of Atlassian products. The team leverages Bamboo and Bitbucket Server to simplify and automate integration testing - one of the most time-consuming aspects of the software release cycle.

Wittified has also branched out into Atlassian Cloud products, quickly adding users for its Connect add-ons that make it easy to customize the Cloud product experience in JIRA, Confluence, and Bitbucket. So far, Wittified has experienced fast uptake on the new Cloud offerings and is excited for the future. 

> <img class="quote-profile" src="/img/daniel-wester-quote.png"> "I had written a plugin that integrates Chef (a popular configuration management solution) with Atlassian Bamboo... and realized that I could make a business out of developing add-ons," 

## We've got your back

There's inherent risk and upfront investment required when starting any business, which is why the Atlassian Marketplace team provides expert tooling, support, and enablement for any add-on developer looking to launch a business within the Atlassian ecosystem.

"The jumpstart that Atlassian's Marketplace team provided for us at such an early stage was absolutely invaluable," says Wester, who remains excited about the ongoing growth of the ecosystem.

## A developer's dream come true

In just three short years, Wittified's hard work has more than paid off. The company was recently acquired by Appfire, one of Atlassian's largest commercial add-on providers. The Wittified team remains fully dedicated to its product line as members of the Appfire family, and Wester has stayed on as the product visionary behind the Wittified brand, doing the work he loves.

"Developing Atlassian add-ons is incredibly rewarding. Think about it, we get to use Atlassian's products to develop our products, which in turn extend the use of Atlassian products worldwide. It's great!" says Wester.

In the last year alone, Wittified has launched [more than 10 new add-ons and over 100 product updates](https://marketplace.atlassian.com/vendors/1210682?_ga=1.208794450.1515724451.1453414285) within the Marketplace.

Hats off to you, Wittified. That's a lot of love!

{{% linkblock href="../zephyr/" img="/img/chat-bubble.png" text="Zephyr" section="cloud" align="right" %}}
{{% linkblock href="../zephyr/" img="/img/clouds.png" text="Zephyr" section="cloud" align="left" %}}
