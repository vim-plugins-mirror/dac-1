FROM debian:jessie

RUN apt-get update && \
    apt-get -y install apache2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Setup Apache
COPY micros/apache2 /etc/apache2
RUN a2enconf micros-logging \
             servername \
             robots-control \
             jiracloud-redirects.conf
RUN a2enmod rewrite \
            headers \
            log_forensic
RUN a2dismod -f autoindex

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data

EXPOSE 8080


# Install content
COPY public /var/www/html
RUN echo '{"status":"OK"}' > /var/www/html/healthcheck.json


CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND", "-e", "info"]
