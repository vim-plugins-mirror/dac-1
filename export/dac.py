import sys
import glob
import io
import logging as log
import os
import re
import shutil
import xmlrpc.client
import zipfile
from distutils.dir_util import copy_tree
from distutils.spawn import find_executable

import pypandoc
import requests
from pyquery import PyQuery
from bs4 import BeautifulSoup
from requests.auth import HTTPBasicAuth

"""
Library functions for handling Confluence space export and transform to static markdown tree.
"""

# per-file YAML metadata:
MARKDOWN_HEADER = '''---
title: %s
aliases:
    - %s
dac_edit_link: %s
confluence_id: %s
platform:
product:
category:
subcategory:
---
'''

CONTENT_PROBLEM_REPORT_TEMPLATE = """
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DAC Problem Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
     integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head><body><div class="container"><div class="starter-template">
    <h1>DAC Problem Report <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></h1>
    <p><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        See also
        <a href="https://extranet.atlassian.com/pages/viewpage.action?pageId=2938638805">
        Pre-migration cleanup
        </a>
    </p>
    <div class="table-responsive">
    <table class="table table-striped">
      <thead><tr>
          <th>File</th>
          <th>Problem</th>
      </tr></thead>
      <tbody>
%s
      </tbody></table></div></div></div></body></html>
"""

BUFFER_SIZE = 1024  # in bytes
REPORT_DELTA = 1024 * 1024  # only log every meg downloaded
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

CHAR_NBSP = u"\u00A0"
CHAR_EM_DASH = u"\u2014"
CHAR_EN_DASH = u"\u2013"
CHAR_LEFT_DOUBLE_QUOTE = u"\u201C"
CHAR_RIGHT_DOUBLE_QUOTE = u"\u201D"
CHAR_LEFT_SINGLE_QUOTE = u"\u2018"
CHAR_RIGHT_SINGLE_QUOTE = u"\u2019"


def unzip(dest, r):
    """
    Processor that unzips the response stream to the destination folder.
    :param dest: target dir for unzipping into.
    :param r: "Requests" response object form which the content is ready to be streamed.
    """
    fname = re.findall("filename=(.+)", r.headers['content-disposition']) or "export.zip"
    log.info("Unzipping file %s", fname)
    export_zip = zipfile.ZipFile(io.BytesIO(r.content))
    export_zip.extractall(dest)


def save(dest, r):
    """
    Processor that saves the file stream into its filename into the given filename.
    :param dest: target directory for saving.
    :param r: "Requests" response object from which the content is ready to be streamed.
    """
    written = 0
    filename = re.findall("filename=(.+)", r.headers['content-disposition']) or "export.zip"
    with open(dest + "/" + filename, 'wb') as fd:
        for chunk in r.iter_content(BUFFER_SIZE):
            written += BUFFER_SIZE
            if written % REPORT_DELTA == 0:
                log.debug("%s: downloading %d bytes", filename, written)
            fd.write(chunk)


def get_stream(url, auth, dest, processor=unzip):
    """
    Retreive the given `url` and process it with the given processor.
    :param url: the url to process
    :param auth: any authentication
    :param dest: the destination folder for the content to be stored.
    :param processor: callback function that takes args destination, response.
    """
    # disble gzip Content-Encoding because borked with the iter_content streaming method
    r = requests.get(url, auth=auth, stream=True, headers={'Accept-Encoding': 'identity'})
    if r.status_code == 200:
        processor(dest, r)
    else:
        log.warning("FAILED to download, got error: %s %s", r.status_code, r.content)
        raise Exception("Failed to get url %s response %s", url, r.status_code)


def backup_file(f):
    new_name = f + "~"
    index = 1
    while os.path.isfile(new_name):
        new_name = "%s~%d" % (f, index)
        index += 1
    shutil.copyfile(f, new_name)


class Confluence:
    def __init__(self, base_url, username, password):
        if not base_url.startswith("https://"):
            log.warning("INSECURE BASE URL may not work: %s", base_url)
        self.base_url = base_url
        self.conf = xmlrpc.client.ServerProxy(base_url + "/rpc/xmlrpc")
        self.bauth = HTTPBasicAuth(username, password)
        log.debug("Authenticating")
        self.token = self.conf.confluence1.login(username, password)

    def export_space_html(self, space):
        return self.conf.confluence1.exportSpace(self.token, space, "TYPE_HTML")

    def get_spaces(self):
        return self.conf.confluence1.getSpaces(self.token)

    def fetch_space(self, space, destination, handler):
        download_url = self.export_space_html(space)
        log.info("Downloading export of '%s' space from %s", space, download_url)
        get_stream(download_url, self.bauth, destination, handler)

    def fetch_spaces(self, spaces_dests, handler):
        for space, destination in spaces_dests:
            self.fetch_space(space, destination, handler)


class Transformer:

    MAIN_HEADING = 'h1#title-heading'
    MAIN_CONTENT = 'div#main-content'
    IMAGE_LINK = re.compile(r'(<img[^>]*\ssrc=")([^":>]+)("[^>]*>)', re.I | re.S)
    INNER_HREF = re.compile(r'(<a[^>]+href=")([^">]+\.html)("[^>]*>)', re.I | re.S)
    CODE_BLOCK = re.compile(r'(<pre[^>]*\sclass=")([^">]+)("[^>]*>)', re.I | re.S)

    def __init__(self, root, base_url="https://developer.atlassian.com", base=""):
        """
        Robots in de skiez.
        :param root: root of the local file tree to transform.
        :param base: prefix of the urls to which local links are rewritten to.
        """
        self.root = root
        self.base_url = base_url
        self.base = base

        self.all_links = set()

        # list of file, problem tuples
        self.content_problems = []

        # list of (src,dest) tuples
        self.link_graph = []

        # current src for next found link
        self.current_file = None
        self.backup_html = True

    def transform_all_html(self, dest):
        """
        Do all cleaning and link rewriting for the root to `dest`
        """

        log.debug("copying images and other attachments")
        self.copy_binaries(dest)

        log.info("Transforming all html from %s to %s", self.root, dest)
        self.for_all_html(self.detect_content_problems,
                          self.preprocess_code_blocks,  # must go before any html parser processors
                          self.clean_dom,
                          self.clean_spans,
                          self.rewrite_image_links,
                          self.transform_links,
                          self.remove_unwanted_classes,
                          self.reduce_code_block_to_language,
                          self.remove_expand_control,
                          self.fix_illegal_unicode_chars)

        log.debug("Cleaning filenames")
        self.clean_filenames()

    def copy_binaries(self, dest):
        for d in ["/images", "/attachments", "/styles"]:
            copy_tree(self.root + d, dest + d)

    def convert_html_to_markdown(self, dest):
        """
            Convert all html to correspondingly named markdown in `dest`.
        """
        find_executable("pandoc") or log.error("You need to install pandoc!")
        for fpath in glob.glob('%s/*.html' % self.root):
            # write the markdown header
            fname = os.path.split(fpath)[-1]
            basename = os.path.splitext(fname)[0]
            new_name = '%s/%s' % (dest, basename + '.md')

            title = self.titlefy(basename).strip()
            page_id = re.match(r'.*?(\d+)$', basename)
            if page_id:
                pid = page_id.group(1)
                dac_edit = self.base_url + "/pages/editpage.action?cjm=wozere&pageId=" + pid
                dac_view = self.base_url + "/pages/viewpage.action?cjm=wozere&pageId=" + pid
            else:
                dac_edit = dac_view = pid = ""
            markdown = MARKDOWN_HEADER % (title, self.base + "/" + fname, dac_edit, dac_view, pid)
            log.info("Converting HTML to markdown: %s", new_name)
            with open(fpath) as f:
                html = f.read()
            markdown += self.html_string_to_markdown(html)
            with open(new_name, 'w+') as markdown_file:
                print(markdown, file=markdown_file)

    def clean_filenames(self):
        """
            Rename all the html files in the given directory to the dac url convention,
            for each file rename, lowercase and reduce multi dashes to one.
        """
        for f in glob.glob('%s/*.html' % self.root):
            fname = os.path.split(f)[-1]
            os.rename(self.root + '/' + fname, self.root + '/' + self.confluence_link_nicer(fname))

    def for_all_html(self, *processors):
        """
        For all html files under root, transform their content using each of the
        given whole file content processors.
        :param processors: fn(string=>string) which modifies html.
        """
        for f in glob.glob('%s/*.html' % self.root):
            log.info("Processing file %s", f)
            self.current_file = f
            self.backup_html and backup_file(f)
            with open(f) as html:
                content = html.read()
                in_count = content.count('\n')
                for processor in processors:
                    content = processor(content)
                out_count = content.count('\n')
                log.debug("file reduction: %d -> %d (%d)", in_count, out_count, out_count - in_count)
            with open(f, 'w+') as html_file:
                print(content, file=html_file)

    def detect_content_problems(self, html):
        """
        HTML whole file processor that returns its input unmodified, collects content problems for reporting
        on in self.content_problem_report()
        :param html: the html file to check for problems in
        :return: the html unchanged.
        """
        probs = [
            ('.expand-control', "expand macro", "info"),
            ('.columnMacro', "column macro", "info"),
            ('.toc-macro', "TOC macro", "info"),
            # ('.childpages-macro', "childpages macro", "info"),
            ('.plugin_attachments_macro_render_param', "attachments macro", "info"),
            ('table.confluenceTable', "confluence table", "warning"),
            ('.gliffy-macro-image', "Gliffy diagram", "warning")
            # see https://extranet.atlassian.com/pages/viewpage.action?pageId=2938638805
        ]
        d = PyQuery(html)

        for detector in probs:
            if len(d(detector[0])) > 0:
                self.content_problem("%s found" % detector[1])

        # test content for preconditions assumed for content cleaner to work reliably
        for expected in [Transformer.MAIN_CONTENT, Transformer.MAIN_HEADING]:
            count = len(d(expected))
            if count != 1:
                self.content_problem("expected 1 '%s', found %s" % (expected, count))

        extranet = r'extranet\.atlassian\.com'
        for img in d('img'):
            src = img.get('src')
            if len(re.findall(extranet, src)) > 0:
                self.content_problem("extranet img src: %s " % src)

        return html

    def content_problem(self, desc):
        pid = re.sub(r'.*?(\d+).*', r'\1', self.current_file)

        view_link = self.base_url + '/pages/viewpage.action?pageId=' + pid
        edit_link = self.base_url + '/pages/editpage.action?pageId=' + pid
        html = """
        <a href="%s">%s</a> <a href="%s" class="edit">
        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        </a>
        """ % (view_link, os.path.split(self.current_file)[-1], edit_link)
        self.content_problems.append((html, desc))

    def preprocess_code_blocks(self, html):
        """
        Transforms the html by transforming the weird code block style minilang that is used in Confluence,
        http://alexgorbatchev.com/SyntaxHighlighter/manual/installation.html which contains stuff like this
        in the pre tag: class="brush: js; gutter: false; theme: Confluence" so we smoosh the kv pairs to
        ensure they survive the reordering that arises from the html parser's use of a set to hold attribute
        multivalues.

        :param html: some html
        :return: transformed html
        """
        log.debug("fixing code blocks")
        return re.sub(self.CODE_BLOCK, lambda m: m.group(1) + re.sub(': ', ':', m.group(2)) + m.group(3), html)

    def rewrite_image_links(self, html):
        """
        Rewrites the image onto the configured base fragment at `self.base`

        :param html: to be transformed
        :return: html transformed
        """
        log.debug("rewriting image links")
        re.match(self.IMAGE_LINK, html) and log.debug("MATCHED IMAGE")
        return re.sub(self.IMAGE_LINK, r'\1%s/\2\3' % self.base, html)

    def transform_links(self, html):
        return re.sub(self.INNER_HREF, self.fix_internal_link, html)

    def store_link(self, link):
        self.all_links.add(link)
        if self.current_file:
            self.link_graph.append((self.current_file, link))

    def content_problem_report(self):
        """
        Returns a rendered html report of content problems found during file processing.
        """
        item = "<tr><td>%s</td><td>%s</td></tr>"
        report = CONTENT_PROBLEM_REPORT_TEMPLATE
        return report % "\n".join(map(lambda p: item % (p[0], p[1]), self.content_problems))

    def fix_internal_link(self, match):
        link = match.group(2)
        self.store_link(link)
        if os.path.isfile(os.path.join(self.root, link)):
            log.debug("Rewriting link %s", link)
            nicer = self.confluence_link_nicer(link)
            return match.group(1) + self.base + '/' + nicer + match.group(3)
        else:
            return match.group(0)

    @staticmethod
    def reduce_code_block_to_language(html):
        """
        Take the preprocessed code block kv pairs and throw everything away except the language.
        For example, `class="brush:js; gutter:false; theme:Confluence"` becomes `class="js"`
        :param html: the html to transform
        :return: transformed html
        """
        soup = BeautifulSoup(html, 'html.parser')
        # find the lang and stomp entire class attribute with that, pandoc likes it
        lang = re.compile(r'.*brush:([^;\s]+).*')
        if lang == "jscript":
            lang = "js"
        for pre in soup.select("div.code div.codeContent pre"):
            classes = " ".join(pre['class'])
            pre['class'] = [re.sub(lang, r'\1', classes)]
        return str(soup)

    @staticmethod
    def fix_illegal_unicode_chars(html):
        log.debug("swapping weird unicode chars")
        html.replace(CHAR_EM_DASH, '--')
        html.replace(CHAR_EN_DASH, '-')
        html.replace(CHAR_LEFT_DOUBLE_QUOTE, '"')
        html.replace(CHAR_RIGHT_DOUBLE_QUOTE, '"')
        html.replace(CHAR_LEFT_SINGLE_QUOTE, "'")
        html.replace(CHAR_RIGHT_SINGLE_QUOTE, "'")

    @staticmethod
    def remove_expand_control(html):
        log.debug("removing expand macro controls")
        soup = BeautifulSoup(html, 'html.parser')
        for t in soup.select("div.expand-control"):
            t.decompose()
        for t in soup.select('div.expand-content'):
            t.unwrap()
        for t in soup.select('div.expand-container'):
            t.unwrap()
        return str(soup)

    @staticmethod
    def remove_unwanted_classes(html):
        unwanted_classes = [
            'conf-macro',
            'confluence-embedded-image',
            'output-inline',
            'confluence-content-image-border',
            'emoticon',
            'emoticon-information',
            'emoticon-warning',
        ]
        d = PyQuery(html)("*")
        for c in unwanted_classes:
            d.each(lambda i, e: PyQuery(e).remove_class(c))
        return d.outer_html()

    @staticmethod
    def clean_dom(html):
        d = PyQuery(html)
        # select only the h1 and the div with the main content
        # this removes all the unneeded header and footer gumpf
        h1 = d(Transformer.MAIN_HEADING)
        h1 = h1.html(h1.text()).outer_html() or ""
        main_content = d(Transformer.MAIN_CONTENT).outer_html() or ""
        new_html = h1 + main_content
        return new_html

    @staticmethod
    def clean_spans(html_with_spans):
        soup = BeautifulSoup(html_with_spans, 'html.parser')
        for t in soup.select('span'):
            t.unwrap()
        return str(soup)

    @staticmethod
    def titlefy(basename):
        """
        Make a page title with correct case from the base filename.
        :param basename: the filename without the extension or leading path.
        :return: a nice page title
        """
        t = basename.replace('-', ' ').title()
        # title() doesn't know acronyms or abbreviations
        t = re.sub(r'\bUi(s?)\b', "UI\\1", t)
        t = re.sub(r'\bRest Api(s?)\b', "REST API\\1", t)
        t = re.sub(r'\bApi(s?)\b', "API\\1", t)
        t = re.sub(r'\bJira\b', "JIRA", t)
        t = re.sub(r'\bOauth\b', "OAuth", t)
        # lower case special cases
        t = re.sub(r'(?<!^)\b(The|A|An|And|For|In|Is|Via|With|To)\b', lambda m: m.group(1).lower(), t)
        return t

    @staticmethod
    def html_string_to_markdown(html):
        """
            Converts the given `html` string into github markdown.
        """
        args = ['--atx-headers', '--wrap=none', '--reference-links', '--normalize']
        return pypandoc.convert_text(html, 'markdown_github', format='html', extra_args=args)

    @staticmethod
    def confluence_link_nicer(link):
        """
            Process the given string from Confluence page URLs to the nice url format preferred on dac.
            Sequences of dash (-) are reduced to single dash and all to lower case.
        """
        return re.sub(r'(--+|_)', r'-', link.lower())
