#!/usr/bin/env python

import argparse
import glob
import logging as log
import os.path
import sys
import re

import yaml

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
PLATFORM = 'platform'
PRODUCT = 'product'
CATEGORY = 'category'  # was 'subnav'
SUBCATEGORY = 'subcategory'  # was 'heading'
STRIP_SUFFIX = 'strip_link_suffix'
REQUIRED_KEYS = [PRODUCT, PLATFORM, CATEGORY, SUBCATEGORY]
MOVE = 'move'
FILE = 'file'
PREAMBLE_SEPARATOR = "---"  # AKA a YAML "document" separator


class ProductsAndPlatforms:
    def __init__(self, products, default_product, platforms, default_platform):
        if default_product and default_product not in products:
            raise Exception("expected default product to be in permitted products")
        if default_platform and default_platform not in platforms:
            raise Exception("expected default platform to be in permitted platforms")
        self.products = products
        self.default_product = default_product
        self.platforms = platforms
        self.default_platform = default_platform

    @staticmethod
    def get_defaults(migration, products, platforms):
        default_product = None
        default_platform = None
        if 'defaults' in migration:
            if PRODUCT in migration['defaults']:
                default_product = migration['defaults'][PRODUCT]
                if default_product not in products:
                    log.error("default product %s not in products list: %s", default_product, products)
            if PLATFORM in migration['defaults']:
                default_platform = migration['defaults'][PLATFORM]
                if default_platform not in platforms:
                    log.error("default platform %s not in platforms list: %s", default_product, products)
        return default_product, default_platform


def get_options():
    parser = argparse.ArgumentParser(description='migrate the markdown content into a specified structure')
    parser.add_argument('content_dir', metavar='content_dir', nargs="?", default="content",
                        help='Base directory of content to work with, migrator file referencess are relative to this')
    parser.add_argument('-c --config-file', dest="config_file",
                        metavar='config file', default="migrator.yaml",
                        help='Name of the config file used to specify the target structure')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='verbose: print out debug information')
    options = parser.parse_args()
    log.basicConfig(level=log.DEBUG if options.verbose else log.INFO, format=LOG_FORMAT)
    return options


def member_check(key, m, values):
    if key in m:
        if m[key] not in values:
            log.error("unknown %s: %s try %s", key, m[PRODUCT], values)
        else:
            log.debug("%s: %s", key, m[key])
            return m[key]
    return None


def rewrite_metadata(markdown, metadata, aliases):
    head, body = decapitate(markdown)
    # TODO modify the metadata preamble, returning new one
    preamble = yaml.load(head)
    for a in aliases:
        preamble['aliases'].append(a)
    for k, v in metadata.items():
        preamble[k] = v if v else ""
    for k in REQUIRED_KEYS:
        if k not in preamble.keys() or not preamble[k]:
            preamble[k] = ""
    new_markdown = "---\n" + yaml.dump(preamble, default_flow_style=False) + "---" + body
    return new_markdown, preamble


def decapitate(markdown):
    """
    returns markdown with separated head (yaml metadata) and body (actual markdown).
    :param markdown: markdown file with '---' delineated preamble containing metadata
    :return: tuple of head and body
    """
    parts = markdown.split(PREAMBLE_SEPARATOR, 2)
    # sanity check incoming file format
    if not markdown[:3] == PREAMBLE_SEPARATOR or not len(parts) == 3:
        log.error("markdown preamble expected! all bets are off!")  # maybe die here?
    return parts[1], parts[2]  # parts[0] is empty


def migrate_metadata(m, content_dir, pp):
    """
    Do the file content changes.
    :param m: the migration spec for a single file
    :param content_dir: the base dir for all file paths
    :param pp: a ProductsAndPlatforms
    """
    log.info("migrating %s:" % m[FILE])
    metadata = {}
    if pp.default_product:
        log.debug("default product: %s", pp.default_product)
        metadata[PRODUCT] = pp.default_product
    if pp.default_platform:
        log.debug("default platform: %s", pp.default_platform)
        metadata[PLATFORM] = pp.default_platform

    product = member_check(PRODUCT, m, pp.products)
    if product:
        metadata[PRODUCT] = product

    platform = member_check(PLATFORM, m, pp.platforms)
    if platform:
        metadata[PLATFORM] = platform

    # read all metadata settings except the ones we've done
    for k in filter(lambda x: x not in (PLATFORM, PRODUCT, FILE, MOVE), m.keys()):
        log.debug("setting %s to %s", k, m[k])
        metadata[k] = m[k]
    aliases = []
    if MOVE in m:
        log.debug("will move file to %s", m[MOVE])
        aliases.append("/" + m[FILE])

    # now we have metadata for the file and alias(es) to add
    file = os.path.join(content_dir, m[FILE])
    markdown = ""
    with open(file) as f:
        # edit the metadata
        markdown, new_metadata = rewrite_metadata(f.read(), metadata, aliases)
    # write the file back
    with open(file, 'w') as markdown_file:
        print(markdown, file=markdown_file)

    return new_metadata


def rewrite_links_files(files, old_links, new_link):
    """
    Find all the links in the list of files and if they match any of the old_links, rewrite them to the new_link
    :param files: a list of files to modify
    :param old_links: a list of links to find in the old_files
    :param new_link: a single new replacement link
    """
    # footnote style links are at the bottom of .md files
    footlinks = re.compile(r'(?<=\n)(  \[[^]]*\]: )(\S+)((?=\n))', re.S)
    html_a = re.compile(r'(<a[^>]+href=")([^"]+)("[^>]*>)')
    matchers = [footlinks, html_a]

    def replacer(m):
        if m.group(2) in old_links:
            return m.group(1) + new_link + m.group(3)
        else:
            return m.group(0)

    for file in files:
        log.debug("rewriting links in %s", file)
        content = ""
        with open(file) as f:
            content = f.read()
            for match in matchers:
                content = re.sub(match, replacer, content)
        with open(file, 'w') as f:
            print(content, file=f)


def rewrite_links(glb, old_links, new_link):
    """
    Take a glob and a list of old links to update to the new link.
    :param glb: a file match pattern like *.md or **/*.md
    :param old_links: a list of links to update to the new link
    :param new_link: a new URL to update instances of the old link to
    """
    files = glob.glob(glb)
    rewrite_links_files(files, old_links, new_link)


def trim_file_tail(file):
    trailing = re.compile(r'\s+$', re.S)
    content = ""
    with open(file) as f:
        # strip excess trailing whitespace, end last line and add one blank line
        content = f.read()
        old_content = content
        content = re.sub(trailing, '\n\n', content)
        log.debug("trimming %s from %d lines to %d", file, old_content.count('\n'), content.count('\n'))
    with open(file, 'w') as f:
        print(content, file=f)


def do_migration(config, content_dir):
    products = config['products']
    log.debug("configured products: %s", products)
    platforms = config['platforms']
    log.debug("configured platforms: %s", platforms)
    for migration in config['migrations']:
        for k in ['glob', 'files']:
            if k not in migration:
                log.error("invalid migration spec, missing %s", k)
                break
        default_product, default_platform = ProductsAndPlatforms.get_defaults(migration, products, platforms)
        pnp = ProductsAndPlatforms(products, default_product, platforms, default_platform)

        file_moves = []
        for m in migration['files']:
            if FILE not in m:
                log.error("check yo self. Migration missing 'file'")
            else:
                f = os.path.join(content_dir, m[FILE])
                if not os.path.isfile(f):
                    log.error("Missing file: %s ", f)
                else:
                    new_metadata = migrate_metadata(m, content_dir, pnp)
                    # squirrel away this intended move operation to do after all content migration is complete
                    # if we do it now the glob may not match moved files and they could be missed
                    if MOVE in m:
                        new_file = os.path.join(content_dir, m[MOVE])
                        file_moves.append((f, new_file))
                        new_link = '/' + m[MOVE]
                        if STRIP_SUFFIX in config and config[STRIP_SUFFIX]:
                            new_link = re.sub(r'(.*).md$', r'\1', new_link)
                            log.debug("stripping suffix from %s to %s", m[MOVE], new_link)
                        rewrite_links(content_dir + "/" + migration['glob'], new_metadata['aliases'], new_link)
                    trim_file_tail(f)
        # after all content has been migrated, now actually move all the files
        files_to_move = len(file_moves)
        if files_to_move > 0:
            log.info("Now moving %d files", files_to_move)
        for from_path, to_path in file_moves:
            try:
                if os.path.exists(to_path):
                    log.warning("Target path exists, clobbering! %s", to_path)
                # create any missing dirs
                target_dir = os.path.split(to_path)[0]
                os.makedirs(target_dir, exist_ok=True)
                log.debug("moving %s to %s", from_path, to_path)
                os.rename(from_path, to_path)
            except Exception as e:
                log.error("Problemo moving file %s : %s", from_path, e)


def main():
    opt = get_options()
    stream = open(opt.config_file, "r")
    config = yaml.load(stream)
    if not os.path.isdir(opt.content_dir):
        log.error("content dir %s is MISSING!!", opt.content_dir)
        return 1
    do_migration(config, opt.content_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
