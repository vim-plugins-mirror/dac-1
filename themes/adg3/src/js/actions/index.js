import fetch from 'isomorphic-fetch'

export const REQUEST_AID_PROFILE = 'REQUEST_AID_PROFILE'
function requestAIDProfile() {
  return {
    type: REQUEST_AID_PROFILE
  }
}

export const RECEIVE_AID_PROFILE = 'RECEIVE_AID_PROFILE'
function receiveAIDProfile(json) {
  return {
    type: RECEIVE_AID_PROFILE,
    authenticated: true,
    profile: json,
    receivedAt: Date.now()
  }
}

export function fetchAIDProfile() {

  // Thunk middleware knows how to handle functions.
  // It passes the dispatch method as an argument to the function,
  // thus making it able to dispatch actions itself.
  return function (dispatch) {

    // First dispatch: the app state is updated to inform
    // that the API call is starting.

    dispatch(requestAIDProfile())

    // The function called by the thunk middleware can return a value,
    // that is passed on as the return value of the dispatch method.

    // In this case, we return a promise to wait for.
    // This is not required by thunk middleware, but it is convenient for us.
    return fetch(process.env.AID_SERVER + `/profile/rest/profile/current`, {credentials: 'include', mode: 'cors' })
    .then(function(response) {
      if (response.status >= 400) {
        throw new Error("User is not logged in");
      }
     return response.json()
    })
    .then(function(json) {
      // Here, we update the app state with the results of the API call.
      dispatch(receiveAIDProfile(json))
    })
    .catch(function(err) {
      dispatch({
        type: RECEIVE_AID_PROFILE,
        authenticated: false,
        receivedAt: Date.now()
      })
    })
  }
}

export const REQUEST_LOGIN_AID_USER = 'REQUEST_LOGIN_AID_USER'
function requestLoginAIDUser() {
  return {
    type: REQUEST_LOGIN_AID_USER
  }
}

export const RECEIVE_LOGIN_AID_USER = 'RECEIVE_LOGIN_AID_USER'
function receiveLoginAIDUser(json) {
  return {
    type: RECEIVE_LOGIN_AID_USER,
    user: json,
    receivedAt: Date.now()
  }
}

export function loginAIDUser() {
  return function (dispatch) {
    dispatch(requestLoginAIDUser())
    const host = window.location.protocol+'//'+window.location.hostname+(window.location.port ? ':'+window.location.port: '')
    const authUrl = process.env.AID_SERVER + "/openid/v2/op"
    + "?openid.ax.type.fullname=http://schema.openid.net/contact/fullname"
    + "&openid.ax.required=email,fullname"
    + "&openid_shutdown_ack=2015-04-20"
    + "&openid.ns.atlassian=https://developer.atlassian.com/display/CROWDDEV/CrowdID%2BOpenID%2Bextensions%23CrowdIDOpenIDextensions-login-page-parameters"
    + "&openid.ns.ax=http://openid.net/srv/ax/1.0"
    + "&openid.return_to=" + window.location.href
    + "&openid.ns=http://specs.openid.net/auth/2.0"
    + "&openid.ax.type.email=http://schema.openid.net/contact/email"
    + "&openid.ns.sreg=http://openid.net/extensions/sreg/1.1"
    + "&openid.ax.mode=fetch_request"
    + "&openid.ns.ext2=http://specs.openid.net/extensions/ui/1.0"
    + "&openid.ext2.icon=true"
    + "&openid.identity=http://specs.openid.net/auth/2.0/identifier_select"
    + "&openid.realm=" + host
    + "&openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select"
    + "&openid.sreg.required=email,fullname&openid.mode=checkid_setup"
    window.location.replace(authUrl)
  }
}
